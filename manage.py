#! /usr/bin/env python
import os

from flask_migrate import MigrateCommand
from flask_assets import ManageAssets

from plant import setup_app, initialize_views, initialize_api

app = setup_app('atele', 'DevelopmentConfig')
manager = app.manager

manager.add_command('db', MigrateCommand)
manager.add_command('assets', ManageAssets)

# Allow the manager access config files on the fly
manager.add_option('-c', '--config', dest='config_obj', default='settings.DevelopmentConfig', required=False)


with app.app_context():
    # from application.models import *


    @manager.command
    def install_assets():
        """ Installs all required assets to begin with """
        from startup import start
        start()


    @manager.command
    def setup_app():
        """ Setup application using this helper function """
        install_assets()


    @manager.command
    def odoo_setup():
        from integrations.storage import odoo
        from integrations.utils import Payload

        config = Payload(**app.config['ODOO_CONFIG'])
        # change admin password
        odoo.Connector.create_database(config.password, config.database, admin_password='admin')


    @manager.command
    def runserver():
        from application.views import main

        initialize_views(app, main.landing)

        port = int(os.environ.get('PORT', 5900))
        app.run(host='0.0.0.0', port=port, use_reloader=False)

    @manager.command
    def runmails():
        from application.views import emails

        initialize_views(app, emails.email_bp)

        port = int(os.environ.get('PORT', 5901))
        app.run(host='0.0.0.0', port=port, use_reloader=False)


    @manager.command
    def runclient():
        from application.views import client, apis
        initialize_views(app, client.client_blueprint, apis.api)

        from application.resources import account, authentication, api, equipment, payment, base
        initialize_api(app, app.api)

        port = int(os.environ.get('PORT', 5800))
        app.run(host='0.0.0.0', port=port, use_reloader=True)


    @manager.command
    def run_support():
        from application.views import supports

        initialize_views(app, supports.support)

        port = int(os.environ.get('PORT', 5700))
        app.run(host='0.0.0.0', port=port)


    @manager.command
    def run_documentation():
        from application.views import documentation

        initialize_views(app, documentation.doc)

        port = int(os.environ.get('PORT', 5600))
        app.run(host='0.0.0.0', port=port)


    @manager.command
    def add_pv():
        from startup import create_metrics, add_service, create_products, create_installation_types
        create_metrics()
        add_service()
        create_products()
        create_installation_types()

    if __name__ == "__main__":
        manager.run()
