from plant import setup_app, initialize_views

app = setup_app('atele', 'ProductionConfig')

with app.app_context():
    from application.views import documentation
    initialize_views(app, documentation.doc)


if __name__ == '__main__':
    app.run()
