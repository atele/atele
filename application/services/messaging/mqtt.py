from flask import current_app as app

from integrations.messaging import mqtt


class MqttService(object):
    __ID = app.config['MQTT_CLIENT_ID']
    __USERNAME = app.config['MQTT_USERNAME']
    __PASSWORD = app.config['MQTT_PASSWORD']
    __ADDRESS = app.config['MQTT_BIND_ADDRESS']
    __PORT = app.config['MQTT_PORT']
    __HOST = app.config['MQTT_HOST']
    __KEEP_ALIVE = app.config['MQTT_KEEP_ALIVE']
    __QOS = app.config['MQTT_QOS']

    @classmethod
    def ping_device(cls, device_code):
        try:
            ping = mqtt.MQTTClient(client_id=cls.__ID, host=cls.__HOST, port=cls.__PORT,
                                   keep_alive=cls.__KEEP_ALIVE, qos=0, bind_address=cls.__ADDRESS,
                                   topic='dev/{}/ping'.format(device_code), username=cls.__USERNAME,
                                   password=cls.__PASSWORD)

            ping.client.loop_start()
            ping.publish(topic='dev/{}/ping'.format(device_code), payload='', qos=0, retain=False)
            ping.client.loop_stop()
            return True
        except Exception as e:
            app.log.error('[IDENTIFIER: %s][ACTION: PING][STATUS: FAILED]' % device_code)
            app.log.error(e)
            return False


def publish_message(self):
    pass
