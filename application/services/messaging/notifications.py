import time

from flask import current_app as app
from integrations.storage import mongo
# from solidwebpush import Pusher


def record_notifications(client_id, body):
    """
    record notifications
    :param client_id:
    :param body:
    :return:
    """
    timestamp = time.time()
    try:
        identifier = '{}:{}:{}'.format(client_id, body, timestamp)
        mongo.MongoService.record_data('notifications', identifier , **{
            'timestamp': timestamp,
            'body': body,
            'is_read': False,
            'client_id': client_id
        })
        return trigger_notification.delay(identifier)
    except Exception, e:
        app.log.error('[BODY: %s][TIMESTAMP: %s][STATUS: FAILED]' % (body, timestamp))
        app.log.error(e)
        return False


@app.celery.task
def trigger_notification(notification_id, client_id):
    """
    trigger push notification
    """
    notification = mongo.MongoService.query_collection('notifications', first_only=True, **{"id": notification_id})

    subscriptions = mongo.MongoService.query_collection('subscriptions', **{'client_id': client_id})
    # push = Pusher(app.config['PUSH_PUBLIC_KEY'])
    # push.sendNotificationToAll([c.endpoint for c in subscriptions], notification)

    return True


def save_subscription(user_id, client_id, endpoint):
    """
    save browser subscription endpoint
    """
    timestamp = time.time()
    try:
        return mongo.MongoService.record_data('subscriptions', endpoint, **{
            'timestamp': timestamp,
            'endpoint': endpoint,
            'client_id': client_id,
            'user_id': user_id
        })
    except Exception, e:
        app.log.error('[ENDPOINT: %s][TIMESTAMP: %s][STATUS: FAILED]' % (endpoint, timestamp))
        app.log.error(e)
        return False

