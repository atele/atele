import logging

from flask import current_app as app

from integrations.storage import tsdb

from application.services import ServiceLabs

logger = ServiceLabs.setup_log('mqtt_info', 'logs/info.log')
error_logger = ServiceLabs.setup_log('mqtt_error', 'logs/error.log', logging.ERROR)

with app.app_context():
    from application.services.equipment import DeviceService

    def get_device(topic):
        """
        return device matching the topic
        :return:
        """
        exploded_string = topic.split('/')
        base_string = next(curr for curr in exploded_string)
        if len(exploded_string) > 2 or len(exploded_string) < 2 or base_string != app.config['MQTT_TOPIC_BASE']:
            error_logger.error('[OBJECT: TOPIC][ACTOR: {}][[ACTION: INVALID TOPIC]'.format(topic))
            return None

        device = DeviceService.filter_by(code=exploded_string[1])
        return device


    def store_data(topic, **kwargs):
        """
        store data into a tsdb
        :param topic:
        :param kwargs:
        :return:
        """
        device = get_device(topic)
        if not device:
            error_logger.error(
                '[OBJECT: DEVICE][ACTOR: {}][STATUS: NOT FOUND][ACTION: DATA WRITE]'.format("%s" % topic))
            return False

        timestamp = kwargs.pop('timestamp')

        for key, value in kwargs.iteritems():
            processed_value = float(value) if isinstance(value, int) else value
            recipient = tsdb.DataStorage.store_data(device.code, **{'value': processed_value, 'metric': key,
                                                                    'timestamp': timestamp})
            logger.info('[OBJECT: DATA][ACTOR: {}][STATUS:{}][METRIC: {}]'.format(device.code, recipient, key))

        return True


    def query_via_topic(topic, start=None, end=None, **kwargs):
        """
        store data into a tsdb
        :param topic:
        :param kwargs:
        :return:
        """
        device = get_device(topic)
        if not device:
            error_logger.error(
                '[OBJECT: DEVICE][ACTOR: {}][STATUS: NOT FOUND][ACTION: DATA WRITE]'.format("%s" % topic))
            return []

        if start and end:
            return tsdb.DataStorage.query_range(start, end, device.code)
        return tsdb.DataStorage.query_data(device.code, **kwargs)


    def query_via_code(code, start=None, end=None, **kwargs):
        """
        store data into a tsdb
        :param code:
        :param kwargs:
        :return:
        """
        if start and end:
            return tsdb.DataStorage.query_range(start, end, code)
        return tsdb.DataStorage.query_data(code, **kwargs)