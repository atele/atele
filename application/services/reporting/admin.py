from sqlalchemy import func
from application.services.reporting import *

from model_package import models

from datetime import timedelta

celery = app.celery
db = app.db


@celery.task(queue="reports")
def clients_report(start, data_type, just_data, period=1):
    """ Generates a report listing clients that registered within the specified period """

    handle = "clients"

    end = start + timedelta(days=period)

    query = models.Client.query.filter(models.Client.date_created >= start, models.Client.date_created < end)

    results = query.all()

    headers = ["date | time", "name", "subdomain", "url", "city", "state", "country"]

    # create the workbook
    filename = "%s-%s-to-%s" % ("clients-report", str(start.date()), str(end.date()))

    # Next write the content. Starting from the second row

    data = []
    for idx, obj in enumerate(results):
        item = (
            obj.date_created.strftime("%d-%m-%Y | %H:%M"),
            obj.name,
            obj.subdomain,
            obj.url,
            obj.city,
            obj.state.name,
            obj.country.name
        )

        data.append(item)

    if data_type == "csv":
        generated = ReportGen.download_csv(filename, headers, *data)
    elif data_type == "xlsx":
        generated = ReportGen.download_xlsx(filename, headers, *data)
    elif data_type == "json":
        generated = ReportGen.download_json(filename, headers, *data)
    elif data_type == "xls":
        generated = ReportGen.download_xls(filename, headers, *data)
    elif data_type == "html":
        generated = ReportGen.download_html(filename, headers, *data)
    else:
        return False

    report_description = "Clients Report for the period between %s and %s" % (str(start.date()), str(end.date()))
    if not just_data:
        res = create_report(handle, filename, data_type, report_description, start, end)
    return generated


@celery.task(queue="reports")
def users_report(start, data_type, just_data, period=1):
    """ Generates a list of users that registered within the specified period """

    handle = "users"

    end = start + timedelta(days=period)

    query = models.User.query.filter(models.User.date_created >= start, models.User.date_created < end)

    results = query.all()

    headers = ["date | time", "name", "email", "is_active", "is_verified", "client"]

    # create the workbook
    filename = "%s-%s-to-%s" % ("users-report", str(start.date()), str(end.date()))

    # Next write the content. Starting from the second row

    data = []
    for idx, obj in enumerate(results):
        item = (
            obj.date_created.strftime("%d-%m-%Y | %H:%M"),
            obj.name,
            obj.email,
            obj.is_active,
            obj.is_verified,
            obj.client.name if obj.client else ""
        )

        data.append(item)

    if data_type == "csv":
        generated = ReportGen.download_csv(filename, headers, *data)
    elif data_type == "xlsx":
        generated = ReportGen.download_xlsx(filename, headers, *data)
    elif data_type == "json":
        generated = ReportGen.download_json(filename, headers, *data)
    elif data_type == "xls":
        generated = ReportGen.download_xls(filename, headers, *data)
    elif data_type == "html":
        generated = ReportGen.download_html(filename, headers, *data)
    else:
        return False

    report_description = "Users Report for the period between %s and %s" % (str(start.date()), str(end.date()))
    if not just_data:
        res = create_report(handle, filename, data_type, report_description, start, end)
    return generated


@celery.task(queue="reports")
def customers_report(start, data_type, just_data, period=1):
    """ Generates a report listing client's customers within the specified period """

    handle = "customers"

    end = start + timedelta(days=period)
    book_data = ()
    query = db.session.query(models.Customer.client_id, func.count(models.Customer.id)).filter(
        models.Customer.date_created >= start, models.Customer.date_created < end).group_by(
        models.Customer.client_id).all()
    customers_count = []
    for client_id, count in query:
        customers_count.append(count)

    headers = ["date | time", "name", "email", "phone", "city", "state", "country"]

    # create the workbook
    filename = "%s-%s-to-%s" % ("customers-report", str(start.date()), str(end.date()))

    # Next write the content. Starting from the second row
    for client_id, count in query:
        client = models.Client.query.get(client_id)
        data = []
        for obj in client.customers:
            item = (
                obj.date_created.strftime("%d-%m-%Y | %H:%M"),
                obj.address.full_name,
                obj.address.email,
                obj.address.phone,
                obj.address.city.name,
                obj.address.state.name,
                obj.address.country.name
            )
        data.append(item)

        obj_data = ReportGen.prepare_data(headers, *data)
        book_data = book_data + obj_data

    generated = ReportGen.download_workbook(filename, book_data)

    report_description = "Customers Report for the period between %s and %s" % (
        str(start.date()), str(end.date()))
    res = create_report(handle, filename, data_type, report_description, start, end)

    return generated
