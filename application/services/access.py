from flask import current_app as app
from application.services import ServiceLabs, ObjectNotFoundException
# from application import models
from model_package import models

db = app.db

RoleService = ServiceLabs.create_instance(models.Role, db)
RestrictedDomainService = ServiceLabs.create_instance(models.RestrictedDomain, db)
BaseAccessGroupService = ServiceLabs.create_instance(models.AccessGroup, db)


class AccessGroupService(BaseAccessGroupService):

    @classmethod
    def add_user(cls, obj_id, group, **kwargs):
        """
        update user account
        :param obj_id:
        :param ignored:
        :param kwargs:
        :return:
        """
        _obj = models.User.query.get(obj_id)

        if not _obj:
            raise ObjectNotFoundException(models.User, obj_id)

        return _obj.add_access_group(group)

    @classmethod
    def remove_user(cls, obj_id, group, **kwargs):
        """
        update user account
        :param obj_id:
        :param ignored:
        :param kwargs:
        :return:
        """
        _obj = models.User.query.get(obj_id)

        if not _obj:
            raise ObjectNotFoundException(models.User, obj_id)
        return _obj.remove_access_group(group)

    @classmethod
    def add_user_access_groups(cls, obj_id, access_group_ids):
        """
        Grant access group rights to user object
        :param obj_id:
        :param access_group_ids:
        :return:
        """
        obj = models.User.query.get(obj_id)

        if not obj:
            raise ObjectNotFoundException(models.User, obj_id)

        merged_obj = db.session.merge(obj)
        try:
            groups = cls.query.filter(cls.model_class.id.in_(access_group_ids)).all()
            for grp in groups:
                merged_grp = db.session.merge(grp)
                merged_obj.access_groups.append(merged_grp)

            db.session.add(merged_obj)
            db.session.commit()
        except Exception, e:
            db.session.rollback()
            raise e

    @classmethod
    def update_user_access_groups(cls, obj_id, access_group_ids):
        """
        Update user access groups
        """
        obj = models.User.query.get(obj_id)

        if not obj:
            raise ObjectNotFoundException(models.User, obj_id)

        merged_obj = db.session.merge(obj)
        try:
            merged_obj.access_groups = list()
            db.session.commit()

            groups = cls.query.filter(cls.model_class.id.in_(access_group_ids)).all()
            for grp in groups:
                merged_grp = db.session.merge(grp)
                merged_obj.access_groups.append(merged_grp)

            db.session.add(merged_obj)
            db.session.commit()

            return merged_obj
        except Exception, e:
            raise e