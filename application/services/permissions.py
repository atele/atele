"""
permissions.py

@Date: 15, July 2017

This module contains all permissions used throughout the application

"""

from flask.ext.principal import Permission, RoleNeed


# Permissions made Available on the Overall Platform Administration Level (Atele)
super_admin_permission = Permission(RoleNeed("super_admin"))
admin_reports_permission = Permission(RoleNeed("super_admin"), RoleNeed("admin_reports_permission"))
admin_clients_permission = Permission(RoleNeed("super_admin"), RoleNeed("admin_clients_permission"))
admin_client_activation_permission = Permission(RoleNeed("super_admin"), RoleNeed("admin_client_activation_permission"))
admin_messages_permission = Permission(RoleNeed("super_admin"), RoleNeed("admin_messages_permission"))
admin_complaints_permission = Permission(RoleNeed("super_admin"), RoleNeed("admin_complaints_permission"))
admin_payments_permission = Permission(RoleNeed("super_admin"), RoleNeed("admin_payments_permission"))
admin_staff_permission = Permission(RoleNeed("super_admin"), RoleNeed('admin_staff_permission'))
admin_assets_permission = Permission(RoleNeed("super_admin"), RoleNeed('admin_assets_permission'))
admin_transactions_permission = Permission(RoleNeed("super_admin"), RoleNeed('admin_transactions_permission'))


# Permissions made Available on the Client Panel
client_admin_permission = Permission(RoleNeed("client_admin"))
client_reports_permission = Permission(RoleNeed("client_admin"), RoleNeed("client_reports_permission"))
client_customers_permission = Permission(RoleNeed("client_admin"), RoleNeed("client_customers_permission"))
client_messages_permission = Permission(RoleNeed("client_admin"), RoleNeed("client_messages_permission"))
client_complaints_permission = Permission(RoleNeed("client_admin"), RoleNeed("client_complaints_permission"))
client_payments_permission = Permission(RoleNeed("client_admin"), RoleNeed("client_payments_permission"))
client_staff_permission = Permission(RoleNeed("client_admin"), RoleNeed('client_staff_permission'))
client_assets_permission = Permission(RoleNeed("client_admin"), RoleNeed('client_assets_permission'))
client_transactions_permission = Permission(RoleNeed("client_admin"), RoleNeed('client_transactions_permission'))
