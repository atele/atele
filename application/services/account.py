import time
from flask import current_app as app, render_template

from utilities import utils
from integrations.storage import mongo

from shapely import geometry, wkb

from application.services import ServiceLabs, clean_kwargs, ObjectNotFoundException, populate_obj, basic, payment, \
    access
from application.services.messaging import email
from model_package import models
from pprint import pprint
# from application import models

# from solidwebpush import Pusher

db = app.db
bcrypt = app.bcrypt
logger = app.logger

BaseCustomerService = ServiceLabs.create_instance(models.Customer, db)
BaseClientService = ServiceLabs.create_instance(models.Client, db)
BaseUserService = ServiceLabs.create_instance(models.User, db)
AdminService = ServiceLabs.create_instance(models.Admin, db)


# ClientPushService = ServiceLabs.create_instance(models.ClientPush, db)


# @celery.task
# def load_client_customers(client_id, customer_id):
#     """load categories to products outside of the service factory"""
#     client = models.Client.query.get(client_id)
#     cust = models.Customer.query.get(customer_id)
#     client.customers.append(cust)
#     db.session.add(client)
#     db.session.commit()
#
#
# # @celery.task
# def load_station_customers(station_id, customer_id):
#     """load categories to products outside of the service factory"""
#     station = models.Station.query.get(station_id)
#     cust = models.Customer.query.get(customer_id)
#     station.customers.append(cust)
#     db.session.add(station)
#     db.session.commit()


class UserService(BaseUserService):
    """
    Custom modification to base user service
    """

    @classmethod
    def create(cls, ignored=None, **kwargs):
        """
        custom create method
        :param ignored:
        :param kwargs:
        :return: user model object
        """
        try:
            if not ignored:
                ignored = ["id", "date_created", "last_updated"]

            data = clean_kwargs(ignored, kwargs)

            email_address = data.get('email')
            user_address = basic.AddressService.query.filter(basic.AddressService.model_class.email == email_address).first()

            if not user_address:
                user_address = basic.AddressService.create(ignored, **data)

            data.update({'address_id': user_address.id})

            data['password'] = bcrypt.generate_password_hash(data['password'])

            obj = BaseUserService.create(ignored, **data)

            access_group_ids = data.get("access_group_ids", [])
            if access_group_ids:
                access.AccessGroupService.add_user_access_groups(obj.id, access_group_ids)

            return obj
        except Exception, e:
            raise e

    @classmethod
    def update(cls, obj_id, ignored=None, **kwargs):
        """
        update user account
        :param obj_id:
        :param ignored:
        :param kwargs:
        :return:
        """
        _obj = cls.query.get(obj_id)

        if not _obj:
            raise ObjectNotFoundException(cls.model_class, obj_id)

        if not ignored:
            ignored = ["id", "date_created", "last_updated"]

        data = clean_kwargs(ignored, kwargs)

        # update address
        user_address = _obj.address
        user_address = populate_obj(user_address, data)
        current_db_session = cls.conn.object_session(user_address)
        current_db_session.add(user_address)
        current_db_session.commit()

        # update user
        obj = populate_obj(_obj, data)
        current_db_session = cls.conn.object_session(obj)
        current_db_session.add(obj)
        current_db_session.commit()

        access_group_ids = data.get("access_group_ids", [])
        if access_group_ids:
            # access.AccessGroupService.add_user_access_groups(obj.id, access_group_ids)
            access.AccessGroupService.update_user_access_groups(obj.id, access_group_ids)
        return obj


class ClientService(BaseClientService):
    """
    Custom modification to base client service
    """

    @classmethod
    def create(cls, ignored=None, **kwargs):
        """
        custom create method
        :param ignored:
        :param kwargs:
        :return: client model object
        """
        if not ignored:
            ignored = ["id", "date_created", "last_updated"]

        data = clean_kwargs(ignored, kwargs)

        client_obj = BaseClientService.create(ignored, **data)

        client_setting = basic.SettingsService.query.filter_by(client_id=client_obj.id).first()

        if not client_setting:
            basic.SettingsService.create(ignored, client_id=client_obj.id, **data)

        # setup wallet
        payment.WalletService.create(client_id=client_obj.id)

        # setup push public key
        # pusher = Pusher()
        # ClientPushService.create(ignored, **{'client_id': client.id, 'push_sub_id': pusher.getB64PublicKey()})

        return client_obj


@app.celery.task
def send_request(timestamp, identifier, message, **kwargs):
    resp = mongo.MongoService.record_data("customer_requests", identifier, timestamp, **kwargs)

    # send email to admin
    message = message
    config = app.config
    email.send_email(config['DEVICE_REQUESTS']['SENDER'], [config['DEVICE_REQUESTS']['RECIPIENT']],
                     subject='Device request', html=render_template("email/device_request.html", **locals()))

    return True


class CustomerDeviceService(object):
    @staticmethod
    def request_device(client_id, customer_id, product_id, network_id, station_id):
        timestamp = time.time()

        customer_obj = models.Customer.query.get(customer_id)

        if not customer_obj:
            raise ObjectNotFoundException(models.Customer, customer_id)

        product = models.Product.query.get(product_id)

        if not product:
            raise ObjectNotFoundException(models.Product, product_id)

        client_obj = ClientService.get(client_id)

        station = models.Station.query.get(station_id)

        if not station:
            raise ObjectNotFoundException(models.Station, station_id)

        network = models.Network.query.get(network_id)

        if not network:
            raise ObjectNotFoundException(models.Network, network)

        data = {
            'client_id': client_obj.id,
            'customer_id': customer_obj.id,
            'product_id': product.id,
            'network_id': network.id,
            'station_id': station.id,
            'is_network': False,
            'is_alert': False,
            'is_delivered': False
        }

        identifier = '{}:{}:{}:customer-request'.format(client_obj.id, customer_id, timestamp)

        message = '{} just requested for a device - {}. IDENTIFIER - {}'.format(
            customer_obj.address.full_name, product.name, identifier)

        return send_request.delay(timestamp, identifier, message, **data)

    @staticmethod
    def deliver_device(identifier, code, **coordinates):
        """
        deliver device to client
        """
        req = mongo.MongoService.query_collection("device_requests", first_only=True, **{"id": identifier})
        device_req = utils.Payload(**req)
        product = basic.ProductService.get(getattr(device_req, 'product_id'))
        client_obj = ClientService.get(getattr(device_req, 'client_id'))
        station = models.Station.query.get(getattr(device_req, 'station_id'))
        customer_obj = CustomerService.get(getattr(device_req, 'customer_id'))
        network = models.Network.query.get(getattr(device_req, 'network_id'))

        longitude = coordinates.get('longitude')
        latitude = coordinates.get('latitude')

        point = basic.GeojsonPointService.create(longitude=longitude, latitude=latitude)

        data = {
            'code': code,
            'station_id': station.id,
            'client_id': client_obj.id,
            'product_id': product.id,
            'customer_id': customer_obj.id,
            'network_id': network.id,
            'point_id': point.id
        }

        device = models.Device.create(**data)
        db.session.add(device)
        db.session.commit()

        if device:
            mongo.MongoService.update_record("customer_requests", identifier, **{'is_delivered': True})

        network = models.Network.query.get(device.network_id)
        devices = list()
        for dev in network.devices:
            point = wkb.loads(bytes(dev.point.point.data))
            devices.append(point)

        if len(devices) >= 3:
            polygon = geometry.Polygon([[p.x, p.y] for p in devices])
            network.polygon = polygon
            models.db.session.add(network)
            models.db.session.commit()

        return device


class CustomerService(BaseCustomerService):
    """
    Custom modification to base client service
    """

    @staticmethod
    def add_client(customer_id, client_id):
        customer = CustomerService.get(customer_id)
        client = ClientService.get(client_id)

        merged_customer = db.session.merge(customer)
        merged_client = db.session.merge(client)
        merged_client.customers.append(merged_customer)

        db.session.add(merged_client)
        db.session.commit()

        return merged_customer

    @staticmethod
    def add_station(customer_id, station_id):
        customer = CustomerService.get(customer_id)
        station = models.Station.query.get(station_id)

        merged_customer = db.session.merge(customer)
        merged_station = db.session.merge(station)
        merged_station.customers.append(merged_customer)

        db.session.add(merged_station)
        db.session.commit()

        return merged_customer

    @classmethod
    def create(cls, ignored=None, **kwargs):
        """
        custom create method
        :param ignored:
        :param kwargs:
        :return: client model object
        """

        if not ignored:
            ignored = ["id", "date_created", "last_updated"]

        data = clean_kwargs(ignored, kwargs)

        station = models.Station.query.get(data.pop('station_id'))

        if not station:
            raise ObjectNotFoundException(models.Station,
                                          '(station_id - {})'.format(data['station_id']))

        product = basic.ProductService.get(data.pop('product_id'))

        network = models.Network.query.get(data.pop('network_id'))

        if not network:
            raise ObjectNotFoundException(models.Network,
                                          '(network_id - {})'.format(data['network_id']))

        client_id = data.pop('client_id')

        client = ClientService.get(client_id)

        email_add = data.get('email')

        customer = cls.filter_by(email=email_add)

        if not customer:
            address = basic.AddressService.filter_by(email=email_add)

            if not address:
                address = basic.AddressService.create(ignored, **data)

            password = bcrypt.generate_password_hash(utils.generate_code(prefix='', length=10))
            data.update({'address_id': address.id, 'password': bcrypt.generate_password_hash('utada')})
            customer = BaseCustomerService.create(ignored, **data)

#         load_client_customers(client_id, customer.id)
#         load_station_customers(station.id, customer.id)
# >>>>>>> 3b34d447996fb0126d4e2e7fec5d1acff635c6c3

        customer_setting = basic.CustomerSettingsService.filter_by(customer_id=customer.id)

        if not customer_setting:
            basic.CustomerSettingsService.create(ignored, customer_id=customer.id, **data)

        if customer not in client.customers:
            cls.add_client(customer.id, client.id)

        if customer not in station.customers:
            cls.add_station(customer.id, station.id)
            # station_customer = db.session.merge(customer)
            # customer_station = db.session.merge(station)
            # customer_station.customers.append(station_customer)
            # db.session.add(customer_station)
            # db.session.commit()

        # request device
        CustomerDeviceService.request_device(client_id=client.id, customer_id=customer.id, product_id=product.id,
                                             network_id=network.id, station_id=station.id)

        merged_customer = db.session.merge(customer)

        return merged_customer
