import urllib
import json

import sqlalchemy
from flask import request, g, make_response, current_app
from flask_restful import Resource, fields, marshal, abort, reqparse

from werkzeug.exceptions import HTTPException
from werkzeug.utils import escape
from werkzeug.wrappers import Response
from werkzeug._compat import text_type

from geoalchemy2 import shape, elements
from shapely import geometry, wkb

from application.services import ObjectNotFoundException, DateJSONEncoder


@current_app.api.representation('application/json')
def output_json(data, code, headers=None):
    resp = make_response(json.dumps(data, cls=DateJSONEncoder), code)
    resp.headers.extend(headers or {})
    return resp


class CustomException(HTTPException):
    def __init__(self, code, data=None, description=None, name=None):
        """
        custom model for handling HTTPExceptions
        :param code: Response Code
        :param description: Description of error
        """
        super(CustomException, self).__init__(code)
        self.code = code
        self.response_name = name
        self.description = description

        if data:
            self.data = data

    def get_description(self, environ=None):
        """Get the description."""
        return u'<p>Description: %s</p>' % escape(self.description)

    def get_body(self, environ=None):
        """Get the HTML body."""
        return text_type((
            u'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">\n'
            u'<title>%(code)s %(name)s</title>\n'
            u'<h1>%(name)s</h1>\n'
            u'%(description)s\n'
        ) % {
            'code':         self.code,
            'name':         escape(self.response_name),
            'description':  self.get_description()
        })

    def get_response(self, environ=None):
        """
        Update HTTPException response processing
        :param environment:
        :return:
        """
        if self.response is not None:
            return self.response
        headers = self.get_headers()
        return Response(self.get_body(environ), self.code, headers)


class ModelField(fields.Raw):
    """
    Extension of flask_restful Raw Field to accommodate sqlalchemy model objects
    """

    def __init__(self, _fields=None, exclude=list()):
        """
        Initializing ModelField
        :param fields:
        """
        super(ModelField, self).__init__()
        self.fields = _fields
        self.exclude = exclude
        # self.fields = filter(lambda x: x not in exclude, _fields)

    @staticmethod
    def process(value, exclude):
        """
        custom field for processing model objects
        :param value:
        :return:
        """
        if isinstance(value, dict) or isinstance(value, unicode):
            data = value
        else:
            data = value.as_dict

        for item in exclude:
            if item in data:
                data.pop(item)

        # for key, value in data.iteritems():
        #     print(key)
        #     if isinstance(value, elements.WKBElement):
        #         data[key] = shape.to_shape(value).to_wkt()

        return data

    def format(self, value):
        """
        custom field for formatting  sqlalchemy model objects
        :param value: model field
        :return:
        """
        return self.process(value, self.exclude)


class ModelListField(ModelField):
    """
    Extension of Model Class to support iterable sqlalchemy model object list
    """

    def __init__(self, _fields=None, exclude=list()):
        """
        Field class to support sqlalchemy model list values in json serialization
        :param _fields: Fields to serialize
        :return:
        """
        super(ModelListField, self).__init__(_fields, exclude=exclude)

    def format(self, values):
        """ Values should be an iteratable property """
        result = [self.process(value, self.exclude) for value in values]
        data = list()
        for res in result:
            new_res = dict()
            for key, val in res.iteritems():
                if isinstance(val, dict):
                    for k, v in val.iteritems():
                        if isinstance(v, elements.WKBElement):
                            # print(v.point)
                            # point = wkb.loads(bytes(v.point.point.data))
                            # val[k] = '{}, {}'.format(point.x, point.y)
                            val[k] = shape.to_shape(v).to_wkt()
                new_res.update({key: val})
            data.append(new_res)
        return data


class BaseResource(Resource):
    """
    Skeletal base resource
    """
    resource_fields = {}
    resource_name = None
    service_class = None
    default_page_size = 20
    default_order_param = 'date_created'
    default_order_direction = 'desc'
    validation_form = None

    @staticmethod
    def current_user():
        """ Retrieves the current user of the api """
        return getattr(g, "user")

    @staticmethod
    def get_user_id():
        _user = getattr(g, "user", None)

        if not _user:
            raise CustomException(401, name='Invalid Credentials', description="You don't have the necessary "
                                                                               "credentials to access this endpoint")

        return _user.id

    @staticmethod
    def get_client_id():
        client = getattr(g, "client", None)

        if not client:
            raise CustomException(401, name='Invalid Credentials', description="You don't have the necessary "
                                                                               "credentials to access this endpoint")

        return client.id

    @staticmethod
    def process_errors(errors):

        _errors = {}
        for k, v in errors.items():
            _res = [str(z) for z in v]
            _errors[str(k)] = _res

        return _errors

    @property
    def output_fields(self):
        """ Property function to always generate a clean base value for output fields """
        return {
            'id': fields.Integer,
            'date_created': fields.DateTime(dt_format='rfc822'),
            'last_updated': fields.DateTime(dt_format='rfc822')
        }

    @staticmethod
    def filter_parser():
        """
        parser arguments to filter query
        :return:
        """
        return {}

    @staticmethod
    def group_parser():

        parser = reqparse.RequestParser()
        parser.add_argument("group_ids", type=int, location='args', action='append')

        return parser.parse_args()

    @staticmethod
    def group_action_parser():

        parser = reqparse.RequestParser()
        parser.add_argument("action_name", type=str, location='args', default=None)

        return parser.parse_args()

    def page_sort(self):
        """
        sort response by pages
        :return:
        """
        parser = reqparse.RequestParser()
        parser.add_argument('page', type=int, default=1, location='args')
        parser.add_argument('per_page', type=int, default=self.default_page_size, location='args')
        parser.add_argument('pages', type=int, default=1, location='args')

        return parser.parse_args()

    def order_sort(self):
        """
        Builds the request parser for extracting sorting parameters from the request.

        :returns: a dict containing extracted values
        :rtype: dict

        """
        parser = reqparse.RequestParser()
        parser.add_argument("order_by", type=str, default=self.default_order_param, location='args')
        parser.add_argument("asc_desc", type=str, default=self.default_order_direction, location='args')

        return parser.parse_args()

    @staticmethod
    def filter_sort():
        """
        Determine the query operator tactic to use. The default value will be '==' or 'eq'. Possible values include:
        'eq', 'neq', 'gte', 'gt', 'lte', 'lt', 'btw'
        """

        parser = reqparse.RequestParser()
        parser.add_argument("operator", type=str, location='args', default='eq')

        return parser.parse_args()

    @staticmethod
    def filtering(query, service_class, operator, name, value):
        """
        :param query:
        :param service_class:
        :param operator:
        :param name:
        :param value:
        :return: Returns the query filtered according to the operator and property used.
            Values in use include:
                eq:  ==
                neq: !=
                gt:  >
                gte: >=
                lt: <
                lte: <=
                in: in_
                btw: between
        """
        if not isinstance(value, (list, tuple)):
            value = [value]

        if not hasattr(service_class.model_class, name):
            return query

        if operator == 'eq':
            return query.filter(getattr(service_class.model_class, name) == value[0])
        if operator == 'neq':
            return query.filter(getattr(service_class.model_class, name) != value[0])
        if operator == 'gt':
            return query.filter(getattr(service_class.model_class, name) > value[0])
        if operator == 'gte':
            return query.filter(getattr(service_class.model_class, name) >= value[0])
        if operator == 'lt':
            return query.filter(getattr(service_class.model_class, name) < value[0])
        if operator == 'lte':
            return query.filter(getattr(service_class.model_class, name) <= value[0])
        if operator == 'in':
            return query.filter(getattr(service_class.model_class, name).in_(value[0]))
        if operator == 'btw':
            start, finish = None, None
            if value and len(value) > 1:
                start = value[0]
                finish = value[1]
            elif value:
                start = value[0]

            return query.filter(getattr(service_class.model_class, name).between(start, finish))

    def query(self):
        """
        query model object using service class
        :return: query object
        """
        return self.service_class.query

    def execute_group_action(self, obj_ids, attrs, files=None):
        """
        Executes group actions by obj_ids
        :param obj_ids:
        :param attrs:
        :param files:
        :return:
        """
        action_name = attrs.get("action_name", None)

        resp = None
        status = 201

        if action_name:
            action_func = getattr(self, "%s_group_action" % action_name, None)

            # if action_func exists, then pass the attrs
            if action_func:

                # inject validation here
                validation_form = getattr(self, "%s_validation_form" % action_name, None)
                adjust_func = getattr(self, "%s_adjust_form_fields" % action_name, None)

                if not validation_form:
                    abort(401, message="No validation form needed to access this resource")

                data, files = self.validate(validation_form, adjust_func=adjust_func)
                data = self.adjust_form_data(data)

                resp = action_func(obj_ids, data, files)

        return resp, status

    def execute_query(self, query, resource_fields, filters, page_sorts, order_sorts, filter_sorts):

        page, per_page = page_sorts.get("page"), page_sorts.get("per_page")
        order_by, asc_desc = order_sorts.get('order_by'), order_sorts.get('asc_desc')
        filter_operator = filter_sorts.get("operator")

        for key, value in filters.items():
            if value:
                query = self.filtering(query, self.service_class, filter_operator, key, value)

        sort_func = getattr(sqlalchemy, asc_desc)
        query = query.order_by(sort_func(order_by))

        # execute the query and include paging
        paging = query.paginate(page, per_page, error_out=False)

        resp = dict(order_by=order_by, asc_desc=asc_desc, page=paging.page, total=paging.total, pages=paging.pages,
                    per_page=per_page)

        request_args = {}
        request_args.update(request.args)

        if paging.has_next:
            # build next page query parameters
            request_args["page"] = paging.next_num
            resp["next"] = paging.next_num
            resp["next_page"] = "%s%s" % ("?", urllib.urlencode(request_args))

        if paging.has_prev:
            # build previous page query parameters
            request_args["page"] = paging.prev_num
            resp["prev"] = paging.prev_num
            resp["prev_page"] = "%s%s" % ("?", urllib.urlencode(request_args))

        output_fields = self.output_fields

        _resource_fields = resource_fields or self.resource_fields

        output_fields.update(_resource_fields)

        resp["results"] = marshal(paging.items, output_fields)

        return resp, 200

    def adjust_form_fields(self, form):
        """
        update form fields based on need
        :param form:
        :return:
        """
        return form

    def adjust_form_data(self, data):
        """
        update form data before validation
        :param data:
        :return:
        """
        return data

    def update_form_output(self, data):
        """
        update form data after validation to insert fields not necessary for validating the form
        :param data:
        :return:
        """
        return data

    def validate(self, klass, adjust_func, obj=None):
        """
        validate form data before creating new model object
        :param klass: form class
        :param adjust_func:
        :param obj:
        :return:
        """
        form = klass(obj=obj)
        form = adjust_func(form)

        if form.validate():
            data = self.update_form_output(form.data)
            return data, request.files
        else:
            error_data = self.process_errors(form.errors)
            # abort(422, message=error_data)
            raise CustomException(code=422, data=error_data, name='Validation Failed')

    def get(self, obj_id=None):
        """
        retrieve object matching resource_name and obj_id
        :param resource_name:
        :param obj_id:
        :return:
        """

        filter_args = self.filter_parser()
        page_args = self.page_sort()
        order_sorts = self.order_sort()
        filter_sorts = self.filter_sort()

        if obj_id is None:
            query = self.query()
            return self.execute_query(query, self.resource_fields, filter_args, page_args, order_sorts, filter_sorts)

        else:
            try:
                obj = self.service_class.get(obj_id)
                output = self.output_fields
                output.update(self.resource_fields)

                return marshal(obj, output), 200
            except ObjectNotFoundException:
                abort(404, message="{} with id ({}) not found".format(self.resource_name, obj_id))

    def bulk_get(self, obj_ids):
        """
        return self.service_class.model_class objects matching the request obj_ids
        :param obj_ids:
        :return:
        """
        return self.service_class.update_by_ids(obj_ids)

    def bulk_update(self, obj_ids, attrs, files=None):
        """
        bulk update object ids
        :param obj_ids:
        :param attrs:
        :param files:
        :return:
        """
        ignored_args = ["id", "date_created", "last_updated"]
        return self.service_class.update_by_ids(obj_ids, ignored_args=ignored_args, **attrs)

    def save(self, attrs, files=None):
        """
        create new model object
        :param attrs:
        :param files:
        :return:
        """

        obj = self.service_class.create(**attrs)

        return obj

    def update(self, obj_id, attrs, files=None):
        """
        update model object matching obj_id
        :param attrs:
        :param files:
        :return:
        """

        obj = self.service_class.update(obj_id, **attrs)

        return obj

    def post(self, obj_id=None, resource_name=None, obj_ids=None, **kwargs):
        """
        handle post request
        :param resource name:
        :param obj_id:
        :param kwargs:
        :return:
        """

        if not self.validation_form:
            raise CustomException(400, description="Validation Form Missing for {}".format(self.resource_name))

        try:
            if obj_id is None and obj_ids is None:
                attrs, files = self.validate(self.validation_form, adjust_func=self.adjust_form_fields)
                attrs = self.adjust_form_data(attrs)
                res = self.save(attrs, files)
                output = self.output_fields
                output.update(self.resource_fields)
                return marshal(res, output), 201

            elif obj_id is None and obj_ids is not None:
                attrs = self.group_action_parser()
                files = None
                resp, status = self.execute_group_action(obj_ids, attrs, files)

                return resp, status

            elif obj_id is not None and resource_name is not None:
                # when obj_id is passed along with a resource_name. this implies a do_method call.
                obj = self.service_class.get(obj_id)  # check if you're permitted first

                sub_resource_fields = getattr(self, "%s_resource_fields" % resource_name.lower(),
                                              self.resource_fields)  # find the resource fields
                adjust_func = getattr(self, "%s_adjust_form_fields" % resource_name, None)
                do_method = getattr(self, "do_%s" % resource_name, None)
                validation_form = getattr(self, "%s_validation_form" % resource_name, None)

                if do_method is None:
                    raise CustomException(405, description="Method not Allowed")

                attrs = request.data or {}
                files = None

                # if there is a validation form, use it
                if validation_form:
                    attrs, files = self.validate(validation_form, adjust_func=adjust_func, obj=obj)
                    attrs = self.adjust_form_data(attrs)

                res = do_method(obj_id, attrs, files)  # Functionality for saving data implemented here

                output_fields = self.output_fields
                output_fields.update(sub_resource_fields)

                return marshal(res, output_fields), 201

            obj = self.service_class.get(obj_id)

            attrs, files = self.validate(self.validation_form, adjust_func=self.adjust_form_fields, obj=obj)

            res = self.update(obj_id, attrs, files)
            output = self.output_fields
            output.update(self.resource_fields)
            return marshal(res, output), 201

        except Exception, e:
            status_code = 500 if not hasattr(e, 'status') else getattr(e, 'status')
            data = {'message': e.message} if not hasattr(e, 'data') else getattr(e, 'data')
            print(e)
            raise CustomException(code=status_code, data=data, description=e.message, name="Integrity Exception")

    def put(self, obj_id=None, resource_name=None, obj_ids=None, **kwargs):
        """
        put resource request
        :param obj_id:
        :param kwargs:
        :return:
        """
        return self.post(obj_id=obj_id, resource_name=resource_name, obj_ids=obj_ids)

    def delete(self, obj_id):
        """
        Deletes and object sent in by DELETE request.
        The functionality is usually implemented in one of the service functions

        :param obj_id: the id of the object to be deleted
        """

        try:
            resp = self.service_class.delete(obj_id)
            return resp, 204
        except Exception, e:
            raise CustomException(code=500, data=e, name="Integrity Exception")


class ApiResource(Resource):
    """
    Base API resource for data communication
    """

    @staticmethod
    def current_user():
        """ Retrieves the current user of the api """
        return getattr(g, "user")

    @staticmethod
    def get_user_id():
        _user = getattr(g, "user", None)

        if not _user:
            raise CustomException(401, name='Invalid Credentials', description="You don't have the necessary "
                                                                               "credentials to access this endpoint")

        return _user.id

    @staticmethod
    def get_client_id():
        client = getattr(g, "client", None)

        if not client:
            raise CustomException(401, name='Invalid Credentials', description="You don't have the necessary "
                                                                               "credentials to access this endpoint")

        return client.id

    @staticmethod
    def prepare_errors(errors):
        """
        Helper class to prepare errors for response
        """
        _errors = {}
        for k, v in errors.items():
            _res = [str(z) for z in v]
            _errors[str(k)] = _res

        return _errors

    @staticmethod
    def filter_parser():
        """
        parser arguments to filter query
        :return:
        """
        return {}

    @staticmethod
    def prepare_response(code, data):
        """
        prepare json response
        :param message:
        :return:
        """
        _data_ = json.dumps(data)
        resp = make_response(_data_, code)
        resp.headers['Content-Type'] = "application/json"
        return resp

    def get(self, *args, **kwargs):
        abort(405)

    def post(self, *args, **kwargs):
        abort(405)

    def delete(self, obj_id):
        abort(405)


