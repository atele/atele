from flask import current_app as app
from flask_restful import fields, abort, reqparse
from flask_restplus import inputs

from application.services import basic, equipment
from application.resources import ModelField, ModelListField
from application.resources.authentication import ClientBaseResource, PublicBaseResource, AdminBaseResource
from model_package import forms


class AddressResource(PublicBaseResource):
    """
    Address resource
    """
    service_class = basic.AddressService
    resource_name = 'address'
    resource_fields = {
        'first_name': fields.String,
        'last_name': fields.String,
        'line1': fields.String,
        'line2': fields.String,
        'phone': fields.String,
        'email': fields.String,
        'city_id': fields.Integer,
        'city': ModelField,
        'state_id': fields.Integer,
        'state': ModelField,
        'country_id': fields.Integer,
        'country': ModelField
    }


class GeoJsonAddressResource(PublicBaseResource):
    """
    GeojsonAddress resource
    """
    service_class = basic.GeojsonAddressService
    resource_name = 'geojson_address'
    resource_fields = {
        'longitude': fields.Float,
        'latitude': fields.Float
    }


class GeoJsonPointResource(PublicBaseResource):
    """
    GeojsonPoint resource
    """
    service_class = basic.GeojsonPointService
    validation_form = forms.ProductForm
    resource_name = 'geojson_point'
    resource_fields = {
        'point': fields.String
    }



class CityResource(PublicBaseResource):
    """
    City resource
    """
    service_class = basic.CityService
    resource_name = 'city'
    resource_fields = {
        'name': fields.String,
        'code': fields.String,
        'state_id': fields.Integer,
        'state': ModelField(exclude=['country', 'cities']),
        'country_id': fields.Integer,
        'country': ModelField(exclude=['states', 'banks', 'cities'])
    }

    @staticmethod
    def filter_parser():
        """
        updated filter parser argument
        :return:
        """
        parser = reqparse.RequestParser()
        parser.add_argument('slug', type=str, location='args')
        parser.add_argument('state_id', type=int, location='args')
        return parser.parse_args()


class StateResource(PublicBaseResource):
    """
    State resource
    """
    service_class = basic.StateService
    resource_name = 'state'
    resource_fields = {
        'name': fields.String,
        'code': fields.String,
        'slug': fields.String,
        'country_id': fields.Integer,
        'country': ModelField(exclude=['states', 'banks', 'cities']),
        'cities': ModelListField(exclude=['country', 'state'])
    }

    @staticmethod
    def filter_parser():
        """
        updated filter parser argument
        :return:
        """
        parser = reqparse.RequestParser()
        parser.add_argument('slug', type=str, location='args')
        parser.add_argument('country_id', type=int, location='args')
        return parser.parse_args()


class CountryResource(PublicBaseResource):
    """
    Country resource
    """
    service_class = basic.CountryService
    resource_name = 'country'
    resource_fields = {
        'name': fields.String,
        'code': fields.String,
        'slug': fields.String,
        'enabled': fields.Boolean,
        'states': ModelListField,
        'cities': ModelListField
    }

    @staticmethod
    def filter_parser():
        """
        updated filter parser argument
        :return:
        """
        parser = reqparse.RequestParser()
        parser.add_argument('slug', type=str, location='args')
        return parser.parse_args()


class CurrencyResource(PublicBaseResource):
    """
    Currency resource
    """
    service_class = basic.CurrencyService
    resource_name = 'currency'
    resource_fields = {
        'name': fields.String,
        'code': fields.String,
        'symbol': fields.String,
        'enabled': fields.Boolean,
        'payment_code': fields.String
    }


class TimezoneResource(PublicBaseResource):
    """
    Timezone resource
    """
    service_class = basic.TimezoneService
    resource_name = 'timezone'
    resource_fields = {
        'name': fields.String,
        'code': fields.String,
        'offset': fields.String
    }


class ServiceResource(PublicBaseResource):
    """
    Services resource
    """
    service_class = basic.DefaultService
    resource_name = 'service'
    validation_form = forms.ServiceForm
    resource_fields = {
        'name': fields.String,
        'code': fields.String,
        'icon_class': fields.String,
        'description': fields.String
    }

    @staticmethod
    def filter_parser():
        """
        updated filter parser argument
        :return:
        """
        parser = reqparse.RequestParser()
        parser.add_argument('code', type=str, location='args')
        return parser.parse_args()


class SettingsResource(ClientBaseResource):
    """
    Settings resource
    """
    service_class = basic.SettingsService
    resource_name = 'settings'
    resource_fields = {
        'client_id': fields.Integer,
        'client': ModelField,
        'debt_threshold_warning': fields.Float,
        'billing_period': fields.String,
        'is_postpaid': fields.Boolean
    }

    def delete(self, obj_id=None, **kwargs):
        abort(405)


class AlertTypeResource(PublicBaseResource):
    """
    Alert Type resource
    """
    service_class = basic.AlertTypeService
    resource_name = 'alert_types'
    validation_form = forms.AlertTypeForm
    resource_fields = {
        'parameter': fields.String,
        'metric': fields.String,
        'symbol': fields.String,
        'description': fields.String,
    }


class CustomerSettingsResource(ClientBaseResource):
    """
    CustomerSettings resource
    """
    service_class = basic.CustomerSettingsService
    resource_name = 'customer_settings'
    resource_fields = {
        'debt_threshold_warning': fields.Float,
        'sms_notification': fields.Boolean,
        'email_notification': fields.Boolean
    }

    def delete(self, obj_id=None, **kwargs):
        abort(405)


class InstallationTypeResource(PublicBaseResource):
    """
    Installation type resource
    """
    service_class = basic.InstallationTypeService
    resource_name = 'installation_types'
    validation_form = forms.InstallationTypeForm
    resource_fields = {
        'title': fields.String,
        'code': fields.String,
    }

    @staticmethod
    def filter_parser():
        """
        updated filter parser argument
        :return:
        """
        parser = reqparse.RequestParser()
        parser.add_argument('code', type=str, location='args')
        parser.add_argument('service_id', type=int)
        return parser.parse_args()

    def query(self):
        service_id = self.filter_parser().pop('service_id', None)
        return self.service_class.query.join(self.service_class.model_class.services).filter_by(
            id=service_id)


class ProductResource(PublicBaseResource):

    service_class = basic.ProductService
    resource_name = 'products'
    validation_form = forms.ProductForm
    resource_fields = {
        'is_network': fields.Boolean,
        'is_alert': fields.Boolean,
        'code': fields.String,
        'name': fields.String,
        'description': fields.String,
        'metrics': ModelListField(exclude='products')
    }

    @staticmethod
    def filter_parser():
        """
        updated filter parser argument
        :return:
        """
        parser = reqparse.RequestParser()
        parser.add_argument('is_alert', type=inputs.boolean, default=False, location='args')
        parser.add_argument('is_network', type=inputs.boolean, default=False, location='args')
        return parser.parse_args()


app.api.add_resource(AddressResource, '/addresses', '/addresses/<int:obj_id>')
app.api.add_resource(GeoJsonAddressResource, '/geojson_addresses', '/geojson_addresses/<int:obj_id>')
app.api.add_resource(GeoJsonPointResource, '/geojson_points', '/geojson_points/<int:obj_id>')
app.api.add_resource(CityResource, '/cities', '/cities/<int:obj_id>')
app.api.add_resource(StateResource, '/states', '/states/<int:obj_id>')
app.api.add_resource(CountryResource, '/countries', '/countries/<int:obj_id>')
app.api.add_resource(CurrencyResource, '/currencies', '/currencies/<int:obj_id>')
app.api.add_resource(TimezoneResource, '/timezones', '/timezones/<int:obj_id>')
app.api.add_resource(ServiceResource, '/services', '/services/<int:obj_id>')
app.api.add_resource(SettingsResource, '/settings', '/settings/<int:obj_id>')
app.api.add_resource(CustomerSettingsResource, '/customer_settings', '/customer_settings/<int:obj_id>')
app.api.add_resource(AlertTypeResource, '/alert_types', '/alert_types/<int:obj_id>')
app.api.add_resource(InstallationTypeResource, '/installation_types', '/installation_types/<int:obj_id>')
app.api.add_resource(ProductResource, '/products', '/products/<int:obj_id>')