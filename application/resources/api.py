import json

from flask import current_app, request
from flask_restful import reqparse, abort

from bson.json_util import dumps

from integrations.storage import mongo, tsdb
from model_package import forms
from application.resources import ApiResource, authentication, CustomException
from application.services.messaging import notifications, alerts, mqtt
from application.services import equipment, account


# from application.services.storage.odoo import conn


class MongoAPiResource(ApiResource):
    """
    Base API resource for data communication from a mongo database
    """
    database_name = current_app.config['MONGODB_DATABASE']

    @staticmethod
    def filter_parser():
        """
        updated filter parser argument
        :return:
        """
        parser = reqparse.RequestParser()
        parser.add_argument('installation_id', type=int, location='args')
        parser.add_argument('station_id', type=int, location='args')
        parser.add_argument('network_id', type=int, location='args')
        parser.add_argument('device_id', type=int, location='args')
        parser.add_argument('is_read', type=bool, location='args')

        return parser.parse_args()

    @authentication.client_auth.login_required
    def get(self, resource_name, obj_id=None):
        filter_params = {'client_id': self.get_client_id()}
        if obj_id:
            filter_params.update({'identifier': obj_id})

        filter_args = self.filter_parser()
        filter_params.update(filter_args)

        args = {field: value for field, value in filter_params.items() if value is not None}

        data = mongo.MongoService.query_collection(resource_name, **args)

        json_data = [json.loads(dumps(i)) for i in data]

        return self.prepare_response(200, {"data": json_data})


class PushApi(ApiResource):
    @authentication.client_auth.login_required
    def post(self, *args, **kwargs):
        """
        save push subscription endpoint
        """
        resp = notifications.save_subscription(user_id=self.get_user_id(), client_id=self.get_client_id(),
                                               endpoint=request.form.get('endpoint'))
        return self.prepare_response(200, {"resp": resp})


class FaultsApi(ApiResource):
    @authentication.client_auth.login_required
    def post(self, identifier, **kwargs):
        """
        resolve fault endpoint
        """
        data = json.loads(request.data)
        form = forms.FaultForm(obj=data)
        if form.validate():
            resp = alerts.FaultService.resolve_fault(identifier, **form.data)
            return self.prepare_response(201, {"response": resp})

        error_data = self.prepare_errors(form.errors)
        raise CustomException(code=422, data=error_data, name='Validation Failed')


class DeviceApi(ApiResource):
    __APPROVED_ENDPOINTS = ('ping', 'publish')

    @authentication.client_auth.login_required
    def post(self, resource_name, code):
        if resource_name not in DeviceApi.__APPROVED_ENDPOINTS:
            raise CustomException(405, description='Method not Allowed')

        resp = mqtt.MqttService.ping_device(code)
        return self.prepare_response(200, {'response': resp})


class TsdbApi(ApiResource):
    @staticmethod
    def filter_parser():
        """
        updated filter parser argument
        :return:
        """
        parser = reqparse.RequestParser()
        parser.add_argument('start_date', type=float, location='args')
        parser.add_argument('end_date', type=float, location='args')

        return parser.parse_args()

    @authentication.client_auth.login_required
    def get(self, code):
        """
        """
        filter_args = self.filter_parser()
        form = forms.QueryForm(start_date=filter_args.start_date, end_date=filter_args.end_date)
        if form.validate():
            data = tsdb.DataStorage.query_range(form.start_date.data, form.end_date.data, code)
            return self.prepare_response(200, {'data': data})

        error_data = self.prepare_errors(form.errors)
        raise CustomException(code=422, data=error_data, name='Validation Failed')


# class OdooResource(ApiResource):
#
#     def get(self, model_name, obj_id=None):
#         """
#         """
#
#         try:
#             if obj_id:
#                 obj = conn.get_record(model_name, obj_id)
#                 return self.prepare_response(200, obj)
#             data = list(conn.query_records(model_name))
#             return self.prepare_response(200, {'data': data})
#         except Exception, e:
#             abort(404)


class DeviceRequest(ApiResource):
    @staticmethod
    def filter_parser():
        """
        updated filter parser argument
        :return:
        """
        parser = reqparse.RequestParser()
        parser.add_argument('station_id', type=int, location='args')
        parser.add_argument('installation_id', type=int, location='args')
        parser.add_argument('network_id', type=int, location='args')
        return parser.parse_args()

    @authentication.client_auth.login_required
    def get(self, obj_id=None):
        """
        """
        filter_args = self.filter_parser()
        filter_args.update({'client_id': self.get_client_id(), 'is_network': False, 'is_alert': False,
                            'is_delivered': False})

        if obj_id:
            filter_args.update({'identifier': obj_id})

        args = {field: value for field, value in filter_args.items() if value is not None}

        data = mongo.MongoService.query_collection("device_requests", **args)
        json_data = [json.loads(dumps(i)) for i in data]
        return self.prepare_response(200, {"data": json_data})

    def post(self, *args, **kwargs):
        data = json.loads(request.data)
        form = forms.DeviceRequestForm(obj=data)
        if form.validate():
            resp = equipment.DeviceService.request_device(**form.data)
            return self.prepare_response(201, {"response": resp})

        error_data = self.prepare_errors(form.errors)
        raise CustomException(code=422, data=error_data, name='Validation Failed')


class NetworkRequest(ApiResource):
    @staticmethod
    def filter_parser():
        """
        updated filter parser argument
        :return:
        """
        parser = reqparse.RequestParser()
        parser.add_argument('station_id', type=int, location='args')
        parser.add_argument('network_id', type=int, location='args')
        return parser.parse_args()

    @authentication.client_auth.login_required
    def get(self, obj_id=None):
        """
        """
        filter_args = self.filter_parser()
        filter_args.update({'client_id': self.get_client_id(), 'is_network': True, 'is_alert': False,
                            'is_delivered': False})

        if obj_id is not None:
            filter_args.update({'identifier': obj_id})

        args = {field: value for field, value in filter_args.items() if value is not None}

        data = mongo.MongoService.query_collection("device_requests", **args)
        json_data = [json.loads(dumps(i)) for i in data]

        return self.prepare_response(200, {"data": json_data})

    def post(self, *args, **kwargs):
        data = json.loads(request.data)
        form = forms.NetworkRequestForm(obj=data)
        if form.validate():
            resp = equipment.NetworkService.request_device(**form.data)
            return self.prepare_response(201, {"response": resp})

        error_data = self.prepare_errors(form.errors)
        raise CustomException(code=422, data=error_data, name='Validation Failed')


class CustomerRequest(ApiResource):
    @staticmethod
    def filter_parser():
        """
        updated filter parser argument
        :return:
        """
        parser = reqparse.RequestParser()
        parser.add_argument('station_id', type=int, location='args')
        parser.add_argument('customer_id', type=int, location='args')
        parser.add_argument('network_id', type=int, location='args')
        return parser.parse_args()

    @authentication.client_auth.login_required
    def get(self, obj_id=None):
        """
        """
        filter_args = self.filter_parser()
        filter_args.update({'client_id': self.get_client_id(), 'is_network': False, 'is_alert': False,
                            'is_delivered': False})

        if obj_id:
            filter_args.update({'identifier': obj_id})

        args = {field: value for field, value in filter_args.items() if value is not None}

        data = mongo.MongoService.query_collection("customer_requests", **args)
        json_data = [json.loads(dumps(i)) for i in data]
        return self.prepare_response(200, {"data": json_data})

    def post(self, *args, **kwargs):
        data = json.loads(request.data)
        form = forms.CustomerRequestForm(obj=data)
        if form.validate():
            resp = account.CustomerDeviceService.request_device(**form.data)
            return self.prepare_response(201, {"response": resp})

        error_data = self.prepare_errors(form.errors)
        raise CustomException(code=422, data=error_data, name='Validation Failed')


class AlertRequest(ApiResource):
    @staticmethod
    def filter_parser():
        """
        updated filter parser argument
        :return:
        """
        parser = reqparse.RequestParser()
        parser.add_argument('station_id', type=int, location='args')
        parser.add_argument('installation_id', type=int, location='args')
        return parser.parse_args()

    @authentication.client_auth.login_required
    def get(self, obj_id=None):
        """
        """
        filter_args = self.filter_parser()
        filter_args.update({'client_id': self.get_client_id(), 'is_alert': True, 'is_delivered': False})

        if obj_id:
            filter_args.update({'identifier': obj_id})

        data = mongo.MongoService.query_collection("device_requests", **filter_args)
        json_data = [json.loads(dumps(i)) for i in data]
        return self.prepare_response(200, {"data": json_data})

    def post(self, *args, **kwargs):
        print(request.form)
        print(request.data)
        exit()


current_app.api.add_resource(MongoAPiResource, '/mongo/<string:resource_name>',
                             '/mongo/<string:resource_name>/<string:obj_id>')
current_app.api.add_resource(PushApi, '/push')
current_app.api.add_resource(FaultsApi, '/faults/<string:identifier>')
current_app.api.add_resource(DeviceApi, '/mqtt/<string:resource_name>/<string:code>')
current_app.api.add_resource(TsdbApi, '/tsdb/<string:code>')
current_app.api.add_resource(DeviceRequest, '/device-requests', '/device-requests/<int:obj_id>')
current_app.api.add_resource(NetworkRequest, '/network-requests', '/network-requests/<int:obj_id>')
current_app.api.add_resource(AlertRequest, '/alert-requests', '/alert-requests/<int:obj_id>')
current_app.api.add_resource(CustomerRequest, '/customer-requests', '/customer-requests/<int:obj_id>')
# current_app.api.add_resource(OdooResource, '/odoo/<string:model_name>', '/odoo/<string:model_name>/<int:obj_id>')
