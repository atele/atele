from flask import current_app as app, request
from flask_restful import fields, reqparse
from flask_restplus import inputs

from application.services import equipment, account, basic
from application.resources import ModelField, ModelListField, BaseResource
from application.resources.authentication import ClientBaseResource
from model_package import forms


class DeviceTypeResource(ClientBaseResource):
    """
    DeviceType resource
    """
    service_class = equipment.DeviceTypeService
    resource_name = 'device_type'
    validation_form = forms.DeviceTypeForm
    resource_fields = {
        'name': fields.String,
        'slug': fields.String,
        'devices': ModelListField
    }


class DeviceResource(ClientBaseResource):
    """
    Device resource
    """
    service_class = equipment.DeviceService
    resource_name = 'device'
    validation_form = forms.DeviceForm
    resource_fields = {
        'name': fields.String,
        'code': fields.String,
        'reference_code': fields.String,
        'customer_id': fields.Integer,
        'customer': ModelField,
        'is_coordinator': fields.Boolean,
        'is_node': fields.Boolean,
        'is_active': fields.Boolean,
        'point_id': fields.Integer,
        'point': ModelField,
        'network_id': fields.Integer,
        'network': ModelField(exclude=['devices', 'client', 'station']),
        'client_id': fields.Integer,
        'client': ModelField(exclude=['city', 'state', 'installations', 'subscriptions', 'settings', 'devices',
                                      'country', 'wallet', 'bank_accounts', 'customers', 'cards', 'invoices',
                                      'reports']),
        'installation_id': fields.Integer,
        'service_id': fields.Integer,
        'product_id': fields.Integer,
        'product': ModelField(exclude=['client', 'station', 'services'])
    }

    @staticmethod
    def filter_parser():
        """
        updated filter parser argument
        :return:
        """
        parser = reqparse.RequestParser()
        parser.add_argument('installation_id', type=int, location='args')
        parser.add_argument('client_id', type=int, location='args')
        parser.add_argument('product_id', type=int, location='args')
        parser.add_argument('service_id', type=int, location='args')
        parser.add_argument('network_id', type=int, location='args')
        parser.add_argument('customer_id', type=int, location='args')
        return parser.parse_args()

    def adjust_form_fields(self, form):
        form.location_id.choices = [c.id for c in basic.LocationService.all()]
        form.client_id.choices = [(c.id, c.name) for c in account.ClientService.all()]
        form.service_id.choices = [(c.id, c.name) for c in basic.DefaultService.all()]
        form.station_id.choices = [(c.id, c.name) for c in equipment.StationService.all()]

        return form


class NetworkResource(ClientBaseResource):
    """
    Network resource
    """
    service_class = equipment.NetworkService
    resource_name = 'network'
    validation_form = forms.NetworkForm
    resource_fields = {
        'client_id': fields.Integer,
        'client': ModelField(exclude=['city', 'state', 'installations', 'subscriptions', 'settings', 'devices',
                                      'country', 'wallet', 'bank_accounts', 'customers', 'cards', 'invoices', 'reports']),
        'code': fields.String,
        'devices': ModelListField(exclude=['station', 'client', 'product', 'network', 'installation']),
        'station_id': fields.Integer,
        'station': ModelField(exclude=['devices', 'installations', 'station_services', 'networks', 'client_id',
                                       'assignees', 'client']),
    }

    def adjust_form_fields(self, form):
        form.product_id.choices = [c.id for c in basic.ProductService.all()]
        form.client_id.choices = [(c.id, c.name) for c in account.ClientService.all()]
        form.station_id.choices = [(c.id, c.name) for c in equipment.StationService.all()]

        return form

    def adjust_form_data(self, data):
        if 'location_id' not in data:
            station = equipment.StationService.get(data['station_id'])
            data['location_id'] =  station.location_id

        return data


    @staticmethod
    def filter_parser():
        """
        updated filter parser argument
        :return:
        """
        parser = reqparse.RequestParser()
        parser.add_argument('client_id', type=int, location='args')
        parser.add_argument('service_id', type=int, location='args')
        parser.add_argument('station_id', type=int, location='args')
        return parser.parse_args()


class InstallationResource(ClientBaseResource):
    """
    Installation resource
    """
    service_class = equipment.InstallationService
    resource_name = 'installations'
    validation_form = forms.InstallationForm
    resource_fields = {
        'client_id': fields.Integer,
        'client': ModelField(exclude=['bank_accounts', 'reports', 'customers', 'installations', 'subscriptions',
                                      'invoices', 'devices', 'cards']),
        'model_number': fields.String,
        'capacity': fields.Float,
        'measurement_index': fields.String,
        'devices': ModelListField,
        'location_id': fields.Integer,
        'location': ModelField,
        'code': fields.String,
        'title': fields.String,
        'station_id': fields.Integer,
        'service_id': fields.Integer,
        'installation_type_id': fields.Integer,
        'installation_type': ModelField(exclude=['services'])
    }

    def adjust_form_fields(self, form):
        form.location_id.choices = [c.id for c in basic.LocationService.all()]
        form.client_id.choices = [(c.id, c.name) for c in account.ClientService.all()]
        form.service_id.choices = [(c.id, c.name) for c in basic.DefaultService.all()]
        form.station_id.choices = [(c.id, c.name) for c in equipment.StationService.all()]
        form.product_id.choices = [(c.id, c.name) for c in basic.ProductService.all()]

        return form

    @staticmethod
    def filter_parser():
        """
        updated filter parser argument
        :return:
        """
        parser = reqparse.RequestParser()
        parser.add_argument('client_id', type=int, location='args')
        parser.add_argument('service_id', type=int, location='args')
        parser.add_argument('installation_type_id', type=int, location='args')
        return parser.parse_args()


class SubStationResource(ClientBaseResource):
    """
    Installation resource
    """
    service_class = equipment.StationService
    resource_name = 'sub_stations'
    validation_form = forms.SubStationForm
    resource_fields = {
        'client_id': fields.Integer,
        'client': ModelField,
        'name': fields.String,
        # 'installations': ModelListField,
        'location_id': fields.Integer,
        # 'location': ModelField,
        'manager_id': fields.Integer,
        'manager': ModelField(exclude=['password']),
        'city': ModelField,
        'state': ModelField,
        'country': ModelField
    }

    def adjust_form_fields(self, form):
        form.city_id.choices = [(c.id, c.name) for c in basic.CityService.all()]
        form.state_id.choices = [(c.id, c.name) for c in basic.StateService.all()]
        form.country_id.choices = [(c.id, c.name) for c in basic.CountryService.all()]
        form.client_id.choices = [(c.id, c.name) for c in account.ClientService.all()]

        return form

    @staticmethod
    def filter_parser():
        """
        updated filter parser argument
        :return:
        """
        parser = reqparse.RequestParser()
        parser.add_argument('client_id', type=int, location='args')
        return parser.parse_args()


class AlertResource(ClientBaseResource):
    """
    Installation resource
    """
    service_class = equipment.AlertService
    resource_name = 'alerts'
    validation_form = forms.AlertForm
    resource_fields = {
        'client_id': fields.Integer,
        'threshold': fields.Float,
        'code': fields.String,
        'alert_type_id': fields.Integer,
        'alert_type': ModelField,
        'device_id': fields.Integer,
        'device': ModelField(exclude=['point', 'installation', 'station', 'product', 'client', 'customer']),
        'installation_id': fields.Integer,
        'installation': ModelField(exclude=['installation_type', 'devices', 'client', 'location', 'installation']),
        'service_id': fields.Integer,
        'service': ModelField(exclude=['installation_types', 'station_services', 'networks', 'products', 'subscriptions']),
        'station_id': fields.Integer,
        'is_warning': fields.Boolean,
        'is_critical': fields.Boolean
    }

    @staticmethod
    def filter_parser():
        """
        updated filter parser argument
        :return:
        """
        parser = reqparse.RequestParser()
        parser.add_argument('client_id', type=int, location='args')
        parser.add_argument('station_id', type=int, location='args')
        parser.add_argument('service_id', type=int, location='args')
        parser.add_argument('installation_id', type=int, location='args')
        parser.add_argument('is_warning', type=inputs.boolean, default=True)
        parser.add_argument('is_critical', type=inputs.boolean, default=False)
        parser.add_argument('alert_type_id', type=int, location='args')
        return parser.parse_args()

    def adjust_form_fields(self, form):
        form.alert_type_id.choices = [(c.id,) for c in basic.AlertTypeService.all()]
        form.device_id.choices = [(c.id,) for c in equipment.DeviceService.all()]
        form.installation_id.choices = [(c.id,) for c in equipment.InstallationService.all()]
        form.client_id.choices = [(c.id,) for c in account.ClientService.all()]
        form.service_id.choices = [(c.id,) for c in basic.DefaultService.all()]
        form.station_id.choices = [(c.id,) for c in equipment.StationService.all()]

        return form

app.api.add_resource(DeviceTypeResource, '/device_types', '/device_types/<int:obj_id>')
app.api.add_resource(DeviceResource, '/devices', '/devices/<int:obj_id>')
app.api.add_resource(NetworkResource, '/networks', '/networks/<int:obj_id>')
app.api.add_resource(InstallationResource, '/installations', '/installations/<int:obj_id>')
app.api.add_resource(SubStationResource, '/sub_stations', '/sub_stations/<int:obj_id>')
app.api.add_resource(AlertResource, '/alerts', '/alerts/<int:obj_id>')

