# from flask import current_app as app
#
# # Database
# db = app.db
#
#
# @app.teardown_request
# def shutdown_session(exception=None):
#     print "------------- tearing down context now ---------"
#     if exception:
#         db.session.rollback()
#         db.session.close()
#         db.session.remove()
#     db.session.close()
#     db.session.remove()
#
#
# @app.teardown_appcontext
# def teardown_db(exception=None):
#     print "------------- tearing down app context now ---------"
#     db.session.commit()
#     db.session.close()
#     db.session.remove()
