from datetime import datetime

from flask import current_app as app

from model_package import models
from model_package.signals import *

from application.services import payment

celery = app.celery
logger = app.logger

today = datetime.today()
month = today.strftime("%B")
year = "%s" % today.year


@celery.task(queue="tasks")
def daily_billings():
    logger.info("--------------- in daily billings")
    clients = models.Client.query.all()
    for client in clients:
        data = {"client_id": client.id}
        payment.BillingService.create(**data)


@celery.task(queue="tasks")
def monthly_invoices():
    logger.info("--------------- in monthly invoice")
    clients = models.Client.query.all()
    for client in clients:
        for i in models.Invoice.query.filter(models.Invoice.client_id==client.id,models.Invoice.is_paid==False).all():
            invoice_payment_notification.send(i.id)
        data = {"month": month, "year": year, "client_id": client.id, "service_charge": client.subscription_fee}
        payment.InvoiceService.create(**data)
