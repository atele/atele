from datetime import datetime

from model_package import models
from flask import Blueprint, current_app as app, session, g
# from application.models import User
from services import render_domain_template

doc = Blueprint('doc', __name__)
logger = app.logger


@doc.context_processor
def main_context():
    """ Include some basic assets in the startup page """
    today = datetime.today()
    length = len
    minimum = min

    return locals()


@app.before_request
def load_user():
    if session.get("user_id"):
        user = models.User.query.get(int(session["user_id"]))
        g.user = user


@app.errorhandler(404)
def page_not_found(e):
    page_title = "404- Page Not Found"
    error_code = "404"
    error_title = "Page not found"
    error_info = "For Some Reason The Page You Requested Could Not Be Found On Our Server."

    return render_domain_template('support/404.html', **locals()), 404


@app.errorhandler(500)
@app.errorhandler(502)
def page_not_found(e):
    page_title = "502- Bad Gateway"
    error_code = "502"
    error_title = "Bad Gateway"
    error_info = "Our Servers are experiencing an overload in connections. Please contact the Administrator."

    return render_domain_template('support/502.html', **locals()), 502


@doc.route('/', methods=["GET"])
def index():
    page_title = "Documentation Manual"
    page_caption = "Platform Usage Manual Documentation"
    return render_domain_template('documentation/index.html', **locals())
