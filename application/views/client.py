from datetime import datetime
import urllib
import json
from pprint import pprint
from itertools import chain

from integrations_package.storage import mongo
from shapely import geometry, wkb

from flask import Blueprint, current_app as app, render_template, redirect, url_for, request, make_response, session, \
    g, abort, flash, send_from_directory, jsonify
from flask_login import login_required, login_user, logout_user, current_user
from sqlalchemy import desc, or_, and_
from flask_principal import Identity, AnonymousIdentity, identity_changed, PermissionDenied, identity_loaded, UserNeed
from echarts import Echart, Legend, Bar, Axis
from werkzeug.datastructures import FileStorage
from model_package import models, forms
from services_package import basic, account, authentication, payment, equipment, access
from utilities import ObjectNotFoundException
from application.core.utils import build_view_filter_url
from utilities.utils import encrypt_dict_to_string, decrypt_string_to_dict, copy_dict, render_domain_template
# from application import models
from services_package.printing import return_pdf
from services_package.permissions import *
from services_package.reporting.core import client_custom_report
from model_package.signals import *

client_blueprint = Blueprint('client_blueprint', __name__)
logger = app.logger
login_manager = app.login_manager

# setup principal
principal = app.principal

report_handles = ["devices", "stations", "installations", "billings"]


@identity_loaded.connect_via(app)
def on_identity_loaded(sender, identity):
    # Set the identity user object
    identity.user = current_user

    # Add the UserNeed to the identity
    if hasattr(current_user, 'id'):
        identity.provides.add(UserNeed(current_user.id))

    # Assuming the User model has a list of roles, update the
    # identity with the roles that the user provides
    if hasattr(current_user, 'roles'):
        for role in current_user.roles:
            identity.provides.add(RoleNeed(role.name))


def _fetch_station_alerts(obj_id):
    station = models.Station.query.get(obj_id)

    if station:
        alerts = equipment.AlertService.filter_by(first_only=False, station_id=obj_id)
        return alerts
    else:
        return []


def _fetch_station_faults(obj_id):
    station = models.Station.query.get(obj_id)

    if station:
        faults = mongo.MongoService.query_collection(collection_name='faults', **{'client_id': station.client_id,
                                                                                  'is_resolved': False,
                                                                                  'station_id': obj_id})
        return faults
    else:
        return []


def _timestamp_to_date(stamp):
    try:
        return datetime.fromtimestamp(stamp)
    except:
        return None


@client_blueprint.context_processor
def main_context():
    """ Global context processor available on all routes """
    today = datetime.today()
    services_url = app.config['SERVICES_URL']
    length = len
    minimum = min
    user = current_user
    page_view_filter_url_build = build_view_filter_url
    use_default_nav = True
    fetch_station_alerts = _fetch_station_alerts
    fetch_station_faults = _fetch_station_faults
    timestamp_to_date = _timestamp_to_date
    if user.is_authenticated:
        current_client = fetch_client()
        services = list()
        filtered_services = list()
        current_service = None
        if current_client:
            subscriptions = payment.SubscriptionService.filter_by(first_only=False, client_id=current_client.id)
            services = [c.service for c in subscriptions]
            setting = current_client.settings
            default_resolved = True if setting.stations_dep_resolved and setting.accounts_dep_resolved and \
                                       setting.billing_dep_resolved else False
            if len(services) > 0:
                current_service = next(current for current in services)
                filtered_services = filter(lambda x: x.id != current_service.id, services)

    return locals()


# @app.before_request
# def apply_caching():
#     if current_user.is_authenticated:
#         request.headers["Authorization"] = "Token %s" % current_user.get_authorization_token()
#         return request

@login_manager.user_loader
def load_user(user_id):
    return models.User.query.get(int(user_id))


@app.before_request
def pre_load_user():
    user_id = session.get("user_id",None)
    if user_id:
        user = load_user(user_id)
        g.user = user


@app.errorhandler(PermissionDenied)
def page_access_denied(e):
    return render_domain_template('client/access_denied.html'), 403


@app.errorhandler(404)
def page_not_found(e):
    page_title = "404- Page Not Found"
    error_code = "404"
    error_title = "Page not found"
    error_info = "For Some Reason The Page You Requested Could Not Be Found On Our Server."

    return render_domain_template('client/404.html', **locals()), 404


@app.errorhandler(500)
@app.errorhandler(502)
def page_error(e):
    page_title = "502- Bad Gateway"
    error_code = "502"
    error_title = "Bad Gateway"
    error_info = "Our Servers are experiencing an overload in connections. Please contact the Administrator."

    return render_domain_template('client/502.html', **locals()), 502


def fetch_client():
    if session.get("client_id"):
        try:
            return account.ClientService.get(int(session.get("client_id")))
        except ObjectNotFoundException, e:
            return redirect('logout')
    else:
        return None


@app.route("/images/<path:path>")
def images(path):
    return send_from_directory('static/images', path)


@app.route("/sw.js")
def service_worker():
    """Exception for the Service Worker"""
    return send_from_directory('static', 'sw.js')


@app.route("/cache-polyfill.js")
def cache_polyfill():
    """Exception for the Service Worker"""
    return send_from_directory('static', 'cache-polyfill.js')


@client_blueprint.route('/fetch/customer_record/', methods=["GET", "POST"])
def fetch_customer_record():
    if request.method == "POST":
        _data = request.data
        if not _data:
            response = make_response("Request Method not Allowed")
            return response
        try:
            email = _data.strip(" ")
            customer = models.Customer.query.filter(models.Customer.email == email).first()
            if customer:
                data = dict(first_name=customer.first_name, last_name=customer.last_name, phone=customer.address.phone,
                            line1=customer.address.line1, line2=customer.address.line2,
                            country_id=customer.address.country_id,
                            state_id=customer.address.state_id, city_id=customer.address.city_id)
                return jsonify({"status": "success", "payload": data})
            return jsonify({"status": "failure", "message": "Customer Does not exist"})

        except Exception as e:
            logger.info("---------Error: %s-----------" % str(e))
            msg = "Failed with Error " + str(e)
            return jsonify({"status": "failure", "message": msg})
    else:
        return jsonify({"status": "failure", "message": "Request Method not Allowed"})


@client_blueprint.route('/fetch/state_cities/', methods=["GET", "POST"])
def fetch_state_cities():
    if request.method == "POST":
        _data = request.data
        if not _data:
            response = make_response("Request Method not Allowed")
            return response

        try:
            state = basic.StateService.get(int(_data))
            data = [{"id": i.id, "name": i.name} for i in state.cities.order_by(models.City.name)]
            refine = json.dumps(data)
            response = make_response(refine)
            return response

        except Exception as e:
            logger.info("---------Error: %s-----------" % str(e))
            msg = "Failed with Error " + str(e)
            response = make_response(msg)
            return response
    else:
        response = make_response("Request Method not Allowed")
        return response


@client_blueprint.route('/fetch/bank_account_info/', methods=["GET", "POST"])
def fetch_bank_account_info():
    if request.method == "POST":
        _data = request.data

        if not _data:
            response = make_response("Request Method not Allowed")
            return response
        _data = json.loads(_data)
        try:
            res = payment.FlutterBankAccountService.verify_account(**_data)
            return res

        except Exception as e:
            logger.info("---------Error: %s-----------" % str(e))
            msg = "Failed with Error " + str(e)
            response = make_response(msg)
            return response
    else:
        response = make_response("Request Method not Allowed")
        return response


@client_blueprint.route('/login/', methods=["GET", "POST"])
@authentication.anonymous_user
def login():
    """
    Login user to application
    :return:
    """
    page_title = "Login"

    # include the username and api_token in the session
    next_url = request.args.get("next") or url_for('.index')

    form = forms.LoginForm()
    if request.method == 'POST' and form.validate_on_submit():
        username = form.data["username"]
        password = form.data["password"]

        user = authentication.authenticate(username, password)

        if user:
            if user.is_blocked:
                login_error = "User has been deactivated. Please contact support team."
                return render_domain_template("client/auth/login.html", **locals())

            login_user(user, remember=True, force=True)  # This is necessary to remember the user

            # clear flash messages
            session.pop('_flashes', None)

            identity_changed.send(app._get_current_object(), identity=Identity(user.id))

            resp = redirect(next_url)

            is_admin = False

            # check if user is admin
            if user.client_id:
                is_admin = user.client.email == user.email

            # load affiliated station
            if not is_admin:
                affiliated_stations = equipment.StationService.filter_by(manager_id=user.id)
                if len(affiliated_stations) == 1:
                    aff_station = next(affiliated_stations)
                    resp = redirect(url_for('.station_details', obj_id=aff_station.id))

            # Transfer auth token to the frontend for use with api requests
            resp.set_cookie("__cred__", authentication.encode_auth_token(user.id))

            resp.set_cookie('__publicKey__', app.config['PUSH_PUBLIC_KEY'])

            g.user = user
            g.client = user.client
            session["user_id"] = user.id

            return resp
        else:
            login_error = "The username or password is invalid"

    return render_domain_template("client/auth/login.html", **locals())


@client_blueprint.route('/logout/', methods=['GET'])
@login_required
def logout():
    """
    logout user from application
    :return:
    """
    logout_user()

    # Remove session keys set by Flask-Principal
    for key in ('identity.name', 'identity.auth_type', 'encryted_card_data', '_flashes', 'user_id'):
        session.pop(key, None)

    # Tell Flask-Principal the user is anonymous
    identity_changed.send(app, identity=AnonymousIdentity())

    resp = redirect(url_for('.index'))
    resp.set_cookie('__cred__', '', expires=0)
    return resp


@client_blueprint.route('/forgot_password/', methods=["GET", "POST"])
def forgot_password():
    next_url_ = request.args.get("next_url") or url_for(".index")
    form = forms.ForgotPasswordForm()
    if form.validate_on_submit():
        data = form.data
        username = data["username"]
        user = authentication.authenticate_forgot_password(username)

        if user is not None:
            token = authentication.request_user_password(user.id)
            return redirect(url_for(".reset_request_successful"))
        else:
            login_error = "This email address or username does not exist"

    return render_domain_template("client/auth/forgot_password.html", **locals())


@client_blueprint.route('/reset_request_successful/', methods=["GET", "POST"])
def reset_request_successful():
    return render_domain_template("client/auth/reset_request_successful.html", **locals())


@client_blueprint.route('/register/', methods=['GET', 'POST'])
@authentication.anonymous_user
def register():
    """
    register user account
    :return:
    """
    page_title = "Register"

    cities = basic.CityService.all()
    countries = [basic.CountryService.filter_by(slug='nigeria')]
    states = basic.StateService.all()
    services = basic.DefaultService.all()

    form = forms.OnboardingForm()
    form.client_city_id.choices = [(c.id, c.name) for c in cities]
    form.city_id.choices = [(c.id, c.name) for c in cities]

    form.client_state_id.choices = [(c.id, c.name) for c in states]
    form.state_id.choices = [(c.id, c.name) for c in states]

    form.client_country_id.choices = [(c.id, c.name) for c in countries]
    form.country_id.choices = [(c.id, c.name) for c in countries]
    form.subscriptions.choices = [(c.id, c.name) for c in services]

    if request.method == 'POST' and form.validate_on_submit():

        client_data = dict(
            name=form.client_name.data,
            email=form.email.data,
            address=form.client_address.data,
            city_id=form.client_city_id.data,
            state_id=form.client_state_id.data,
            country_id=form.client_country_id.data,
            url=form.url.data,
            sub_domain=form.sub_domain.data
        )
        client = account.ClientService.create(**client_data)

        user_data = dict(
            first_name=form.first_name.data,
            last_name=form.last_name.data,
            line1=form.address.data,
            email=form.email.data,
            password=form.password.data,
            city_id=form.city_id.data,
            state_id=form.state_id.data,
            country_id=form.country_id.data,
            client_id=client.id
        )

        user = account.UserService.create(**user_data)

        service_ids = form.subscriptions.data

        for _id in service_ids:
            payment.SubscriptionService.create(**{'client_id': client.id, 'service_id': _id})

        return redirect('login')

    return render_domain_template("client/auth/register.html", **locals())


@client_blueprint.route('/setup/reminder/', methods=['GET', 'POST'])
def setup_reminder():
    page_title = "Setup Reminder"

    return render_domain_template("client/info/setup_reminder.html", **locals())


@client_blueprint.route('/client/', methods=["GET"])
@client_blueprint.route('/', methods=["GET"])
@login_required
def index():
    """
    Index page
    :return:
    """
    page_title = "Home"
    page_caption = "Client Home"

    user = current_user
    client = user.client

    setting = client.settings

    if not setting.stations_dep_resolved or not setting.accounts_dep_resolved or not setting.billing_dep_resolved:
        return redirect(url_for('.onboarding'))

    faults = mongo.MongoService.query_collection(collection_name='faults', **{'client_id': client.id,
                                                                              'is_resolved': False})

    return render_domain_template('client/info/home.html', **locals())


@client_blueprint.route('/stations/', methods=["GET", "POST"])
@login_required
def stations():
    """
    stations list view
    :return:
    """
    try:
        page = int(request.args.get("page", 1))
        size = request.args.get("size", 20)
        search_q = request.args.get("q", None)
        view_q = request.args.get("view", "all")
    except:
        abort(404)

    page_title = "Stations"
    no_angular = True
    has_sub_lnk = True

    search_place_holder = "Search by (Name, Manager)"

    view_filters = [{"name": "all", "value": "all"}, {"name": "active", "value": "active"},
                    {"name": "in active", "value": "in_active"}]

    client = fetch_client()
    query = equipment.StationService.query.join(models.User).filter(models.Station.client_id == client.id).order_by(
        models.Station.id)

    print "------------------ view_q", view_q
    if view_q:
        query = equipment.StationService.view_filter(query, view_name=view_q)

    request_args = copy_dict(request.args, {})

    if search_q:
        search_place_holder = "Search for %s" % search_q
        queries = [models.Station.name.ilike("%%%s%%" % search_q), models.User.name_.ilike("%%%s%%" % search_q)]
        query = query.filter(or_(*queries)).filter(and_(models.Station.manager_id == models.User.id))

    results = query.paginate(page, size, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

    return render_domain_template('client/general/list/stations.html', **locals())


@client_blueprint.route('/stations/create/', methods=["GET", "POST"])
@login_required
def add_station():
    """
    create new station
    """
    page_title = "Create Station"
    no_angular = True
    has_sub_lnk = True

    cities = basic.CityService.query.order_by(models.City.name).all()
    # cities = basic.CityService.all()
    countries = basic.CountryService.filter_by(slug='nigeria', first_only=False)
    states = basic.StateService.query.order_by(models.State.name).all()

    client = fetch_client()

    users = account.UserService.filter_by(first_only=False, client_id=client.id)

    form = forms.SubStationForm(client_id=client.id)

    form.city_id.choices = [(c.id, c.name) for c in cities]
    form.state_id.choices = [(c.id, c.name) for c in states]
    form.country_id.choices = [(c.id, c.name) for c in countries]
    form.manager_id.choices = [(c.id, c.name) for c in users]
    form.client_id.choices = [(c.id, c.name) for c in account.ClientService.all()]

    if request.method == 'POST' and form.validate_on_submit():
        equipment.StationService.create(**form.data)

        setting = client.settings
        default_resolved = True if setting.stations_dep_resolved and setting.accounts_dep_resolved \
                                   and setting.billing_dep_resolved else False

        if not default_resolved:
            return redirect(url_for('.onboarding'))

        return redirect(url_for('.stations'))

    return render_domain_template('client/general/forms/station.html', **locals())


@client_blueprint.route('/stations/<int:obj_id>/update/', methods=["GET", "POST"])
@login_required
def update_station(obj_id):
    """
    create new station
    """
    page_title = "Update Station"
    no_angular = True
    has_sub_lnk = True

    cities = basic.CityService.all()
    # cities = basic.CityService.all()
    countries = basic.CountryService.filter_by(slug='nigeria', first_only=False)
    states = basic.StateService.all()

    client = fetch_client()

    users = account.UserService.query.filter(models.User.client_id == client.id).all()

    form = forms.SubStationForm(client_id=client.id)

    if obj_id:
        obj = equipment.StationService.filter_by(id=obj_id)

        if not obj:
            abort(404)

        form = forms.SubStationForm(obj=obj.location)
        form.name.data = obj.name
        form.client_id.data = obj.client_id
        form.manager_id.data = obj.manager.id

    form.city_id.choices = [(c.id, c.name) for c in cities]
    form.state_id.choices = [(c.id, c.name) for c in states]
    form.country_id.choices = [(c.id, c.name) for c in countries]
    form.manager_id.choices = [(c.id, c.name) for c in users]
    form.client_id.choices = [(c.id, c.name) for c in account.ClientService.all()]

    if request.method == 'POST' and form.validate_on_submit():
        if obj_id:
            obj = equipment.StationService.filter_by(id=obj_id)
            equipment.StationService.update(obj.id, **form.data)
            return redirect(request.path)
        equipment.StationService.create(**form.data)

        setting = client.settings
        default_resolved = True if setting.stations_dep_resolved and setting.accounts_dep_resolved \
                                   and setting.billing_dep_resolved else False

        if not default_resolved:
            return redirect(url_for('.onboarding'))

        return redirect(url_for('.stations'))

    return render_domain_template('client/general/forms/station.html', **locals())


@client_blueprint.route('/stations/<int:obj_id>/', methods=["GET"])
@client_blueprint.route('/stations/<int:obj_id>/details/', methods=["GET"])
@login_required
def station_details(obj_id):
    """
    details of a station
    :param obj_id:
    :return:
    """
    from integrations.utils import Payload
    # try:
    obj = equipment.StationService.get(obj_id)

    obj = models.db.session.merge(obj)

    client = fetch_client()
    installations = equipment.InstallationService.filter_by(first_only=False, client_id=client.id)
    faults = mongo.MongoService.query_collection(collection_name='faults', **{'client_id': client.id,
                                                                              'is_resolved': False,
                                                                              'station_id': obj.id})
    alerts = equipment.AlertService.filter_by(first_only=False, station_id=obj.id)

    page_title = "%s Station" % obj.name
    no_angular = True
    has_sub_lnk = True

    devices = list(chain.from_iterable([c.devices for c in installations]))
    json_devices = [c.as_dict for c in devices]

    metrics = list(set(chain.from_iterable([m.product.metrics for m in devices])))
    metrics_map = list()

    for metric in metrics:
        met = dict(name=metric.name, slug=metric.slug, dimension=metric.dimension)
        met['devices'] = [c.as_dict for c in filter(lambda x: metric in x.product.metrics, devices)]
        metrics_map.append(met)

    return render_domain_template('client/general/details/station.html', **locals())

    # except ObjectNotFoundException, e:
    #     abort(404)


@client_blueprint.route('/stations/<int:obj_id>/faults/', methods=["GET", "POST"])
@login_required
def station_faults(obj_id):
    """
    details of a station
    :param obj_id:
    :return:
    """
    try:
        obj = equipment.StationService.get(obj_id)

        client = fetch_client()
        faults = mongo.MongoService.query_collection(collection_name='faults', **{'client_id': client.id,
                                                                                  'is_resolved': False,
                                                                                  'station_id': obj.id})

        page_title = "%s Station | Faults" % obj.name
        no_angular = True
        has_sub_lnk = True

        return render_domain_template('client/general/list/faults.html', **locals())

    except ObjectNotFoundException, e:
        abort(404)


@client_blueprint.route('/stations/<int:obj_id>/workers/', methods=["GET", "POST"])
@login_required
def station_workers(obj_id):
    """
    details of a station
    :param obj_id:
    :return:
    """
    try:
        obj = equipment.StationService.get(obj_id)

        client = fetch_client()
        workers = [obj.manager]

        workers.extend(obj.assignees)

        page_title = "%s Station" % obj.name
        no_angular = True
        has_sub_lnk = True

        return render_domain_template('client/general/list/workers.html', **locals())

    except ObjectNotFoundException, e:
        abort(404)


@client_blueprint.route('/stations/<int:obj_id>/alerts/', methods=["GET", "POST"])
@login_required
def station_alerts(obj_id):
    """
    details of a station
    :param obj_id:
    :return:
    """
    try:
        search_q = None
        size = 20
        page = 1

        try:
            page = int(request.args.get("page", 1))
            size = request.args.get("size", 20)
            search_q = request.args.get("q", None)
        except Exception, e:
            print('=========error======')
            print(e)
            print('=========error======')
            abort(404)

        # request_args = copy_dict(request.args, {})
        request_args = request.args.copy()

        obj = equipment.StationService.get(obj_id)
        query = models.Alert.query.filter_by(station_id=obj.id)

        query = query.order_by(desc(models.Alert.date_created))

        results = query.paginate(page, size, False)

        if results.has_next:
            # build next page query parameters
            request_args["page"] = results.next_num
            results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

        if results.has_prev:
            # build previous page query parameters
            request_args["page"] = results.prev_num
            results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

        page_title = "%s Station" % obj.name
        no_angular = True
        has_sub_lnk = False

        return render_domain_template('client/general/list/alerts.html', **locals())

    except ObjectNotFoundException, e:
        abort(404)


@client_blueprint.route('/stations/<int:obj_id>/installations/', methods=["GET"])
@login_required
def station_installations(obj_id):
    """
    details of a station's installations
    :param obj_id:
    :return:
    """
    search_q = None
    size = 20
    page = 1
    no_angular = True
    has_sub_lnk = True

    try:
        page = int(request.args.get("page", 1))
        size = request.args.get("size", 20)
        search_q = request.args.get("q", None)
    except Exception, e:
        print('=========error======')
        print(e)
        print('=========error======')
        abort(404)

    # request_args = copy_dict(request.args, {})
    request_args = request.args.copy()

    obj = equipment.StationService.get(obj_id)
    query = models.Installation.query.filter_by(station_id=obj.id)

    query = query.order_by(desc(models.Installation.date_created))

    results = query.paginate(page, size, False)

    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

    page_title = "%s Station | Installations" % obj.name
    no_angular = True
    has_sub_lnk = False

    return render_domain_template('client/general/list/installations.html', **locals())


@client_blueprint.route('/stations/<int:obj_id>/services/', methods=["GET"])
@login_required
def station_services(obj_id):
    """
    details of a station
    :param obj_id:
    :return:
    """
    try:
        obj = equipment.StationService.get(obj_id)

        client = fetch_client()
        services = obj.station_services

        page_title = "%s Station" % obj.name
        no_angular = True
        has_sub_lnk = True

        return render_domain_template('client/general/list/services.html', **locals())

    except ObjectNotFoundException, e:
        abort(404)


@client_blueprint.route('/stations/<int:obj_id>/networks/', methods=["GET"])
@login_required
def station_networks(obj_id):
    """
    details of a station
    :param obj_id:
    :return:
    """
    try:
        page = int(request.args.get("page", 1))
        size = request.args.get("size", 20)
        search_q = request.args.get("q", None)

        obj = equipment.StationService.get(obj_id)

        client = fetch_client()
        results = obj.networks

        page_title = "%s Station" % obj.name
        no_angular = True
        has_sub_lnk = True

        query = equipment.NetworkService.query.filter(models.Network.client_id == client.id,
                                                      models.Network.station_id == obj_id).order_by(
            models.Network.id)

        request_args = copy_dict(request.args, {})

        results = query.paginate(page, size, False)
        if results.has_next:
            # build next page query parameters
            request_args["page"] = results.next_num
            results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

        if results.has_prev:
            # build previous page query parameters
            request_args["page"] = results.prev_num
            results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

        return render_domain_template('client/general/list/networks.html', **locals())

    except ObjectNotFoundException, e:
        abort(404)


@client_blueprint.route('/stations/<int:obj_id>/devices/', methods=["GET"])
@login_required
def station_devices(obj_id):
    """
    details of a station
    :param obj_id:
    :return:
    """
    try:
        page = int(request.args.get("page", 1))
        size = request.args.get("size", 20)
        search_q = request.args.get("q", None)

        obj = equipment.StationService.get(obj_id)

        client = fetch_client()
        results = obj.networks

        page_title = "%s Station" % obj.name
        no_angular = True
        has_sub_lnk = True

        query = models.Device.query.filter(models.Device.station_id == obj_id,
                                           models.Device.client_id == client.id).order_by(desc(models.Device.id))

        request_args = copy_dict(request.args, {})

        results = query.paginate(page, size, False)
        if results.has_next:
            # build next page query parameters
            request_args["page"] = results.next_num
            results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

        if results.has_prev:
            # build previous page query parameters
            request_args["page"] = results.prev_num
            results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

        return render_domain_template('client/general/list/devices.html', **locals())

    except ObjectNotFoundException, e:
        abort(404)


@client_blueprint.route('/networks/<int:obj_id>/', methods=['GET'])
@client_blueprint.route('/networks/<int:obj_id>/details/', methods=["GET"])
@login_required
def network_details(obj_id):
    """
    details of a station
    :param obj_id:
    :return:
    """
    try:
        obj = equipment.NetworkService.get(obj_id)
        client = fetch_client()
        location = obj.station.location
        devices = [c.as_dict for c in obj.devices if c.is_active]
        faults = mongo.MongoService.query_collection(collection_name='faults', **{'client_id': client.id,
                                                                                  'is_resolved': False,
                                                                                  'network_id': obj.id})

        page_title = "%s Network" % obj.code
        no_angular = True
        has_sub_lnk = True

        return render_domain_template('client/general/details/network.html', **locals())

    except ObjectNotFoundException, e:
        abort(404)


@client_blueprint.route('/networks/<int:obj_id>/requests/', methods=["GET"])
@login_required
def network_req(obj_id):
    """
    details of a station
    :param obj_id:
    :return:
    """
    try:
        obj = equipment.NetworkService.get(obj_id)
        client = fetch_client()
        requests = mongo.MongoService.query_collection(collection_name='customer-requests', **{'client_id': client.id,
                                                                                               'is_delivered': False,
                                                                                               'network_id': obj.id})

        page_title = "%s Network" % obj.code
        no_angular = True
        has_sub_lnk = True

        return render_domain_template('client/general/list/network_requests.html', **locals())

    except ObjectNotFoundException, e:
        abort(404)


@client_blueprint.route('/networks/<int:obj_id>/devices/', methods=["GET"])
@login_required
def network_devices(obj_id):
    """
    details of a station
    :param obj_id:
    :return:
    """
    try:
        search_q = None
        size = 20
        page = 1

        try:
            page = int(request.args.get("page", 1))
            size = request.args.get("size", 20)
            search_q = request.args.get("q", None)
        except Exception, e:
            print('=========error======')
            print(e)
            print('=========error======')
            abort(404)

        # request_args = copy_dict(request.args, {})
        request_args = request.args.copy()

        obj = equipment.NetworkService.get(obj_id)
        query = models.Device.query.filter_by(network_id=obj.id)

        query = query.order_by(desc(models.Device.date_created))

        results = query.paginate(page, size, False)

        if results.has_next:
            # build next page query parameters
            request_args["page"] = results.next_num
            results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

        if results.has_prev:
            # build previous page query parameters
            request_args["page"] = results.prev_num
            results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

        page_title = "Network: %s" % obj.code
        no_angular = True
        has_sub_lnk = False

        return render_domain_template('client/general/list/network_devices.html', **locals())

    except ObjectNotFoundException, e:
        abort(404)


@client_blueprint.route('/networks/<int:obj_id>/devices/new', methods=["GET", "POST"])
@login_required
def network_dev_new(obj_id):
    """
    details of a station
    :param obj_id:
    :return:
    """
    try:
        obj = equipment.NetworkService.get(obj_id)
        client = fetch_client()
        active_devices = equipment.DeviceService.filter_by(first_only=False, client_id=client.id, is_active=True)
        devices = filter(lambda x: x is None or x.network_id != obj.id, active_devices)

        page_title = "%s Network" % obj.code
        no_angular = True
        has_sub_lnk = True

        form = forms.AddDeviceForm()

        if form.validate_on_submit() and request.method == 'POST':

            device = equipment.DeviceService.get(form.device_id.data)
            merged_device = models.db.session.merge(device)
            merged_device.network_id = obj.id
            models.db.session.add(merged_device)
            models.db.session.commit()

            merged_net = models.db.session.merge(obj)
            net_devices = list()

            for dev in merged_net.devices:
                point = wkb.loads(bytes(dev.point.point.data))
                net_devices.append(point)

            if len(net_devices) >= 3:
                polygon = geometry.Polygon([[p.x, p.y] for p in net_devices])
                merged_net.polygon = polygon
                models.db.session.add(merged_net)
                models.db.session.commit()

            return redirect(url_for('.network_devices', obj_id=obj.id))

        return render_domain_template('client/general/forms/network_dev.html', **locals())

    except ObjectNotFoundException, e:
        abort(404)


@client_blueprint.route('/networks/<int:obj_id>/faults/', methods=["GET", "POST"])
@login_required
def network_faults(obj_id):
    """
    details of a station
    :param obj_id:
    :return:
    """
    try:
        obj = equipment.NetworkService.get(obj_id)

        client = fetch_client()
        faults = mongo.MongoService.query_collection(collection_name='faults', **{'client_id': client.id,
                                                                                  'is_resolved': False,
                                                                                  'network_id': obj.id})

        page_title = "%s Network" % obj.code
        no_angular = True
        has_sub_lnk = True

        return render_domain_template('client/general/list/network_faults.html', **locals())

    except ObjectNotFoundException, e:
        abort(404)


@client_blueprint.route('/devices/<int:obj_id>/', methods=['GET'])
@client_blueprint.route('/devices/<int:obj_id>/details/', methods=["GET"])
@login_required
def device_details(obj_id):
    """
    details of a station
    :param obj_id:
    :return:
    """
    try:
        obj = equipment.DeviceService.get(obj_id)

        client = fetch_client()
        faults = mongo.MongoService.query_collection(collection_name='faults', **{'client_id': client.id,
                                                                                  'is_resolved': False,
                                                                                  'device_id': obj.id})

        alerts = equipment.AlertService.filter_by(first_only=False, device_id=obj.id)
        metrics = [x.as_dict for x in obj.product.metrics]

        page_title = "Device: %s" % obj.code
        no_angular = True
        has_sub_lnk = True

        return render_domain_template('client/general/details/device.html', **locals())

    except ObjectNotFoundException, e:
        abort(404)


@client_blueprint.route('/settings/', methods=["GET", "POST"])
@client_blueprint.route('/settings/basic_info/', methods=["GET", "POST"])
@login_required
def settings():
    page_title = "Basic Information"
    no_angular = True
    has_sub_lnk = True

    cities = basic.CityService.query.order_by(models.City.name).all()
    countries = [basic.CountryService.filter_by(slug='nigeria')]
    states = basic.StateService.query.order_by(models.State.name).all()

    form = forms.ClientForm()
    form.city_id.choices = [(c.id, c.name) for c in cities]
    form.state_id.choices = [(c.id, c.name) for c in states]
    form.country_id.choices = [(c.id, c.name) for c in countries]

    if request.method == 'POST' and form.validate_on_submit():
        client = fetch_client()
        client = account.ClientService.update(obj_id=client.id, **form.data)
        return redirect(request.path)

    return render_domain_template('client/general/basic_info.html', **locals())


@client_blueprint.route('/accounts/users/', methods=["GET"])
@login_required
# @client_admin_permission.require()
def accounts():
    page_title = "Setup Accounts"

    try:
        page = int(request.args.get("page", 1))
        size = request.args.get("size", 20)
        search_q = request.args.get("q", None)
    except:
        abort(404)

    no_angular = True
    has_sub_lnk = True

    search_place_holder = "Search by Name"

    client = fetch_client()
    setting = client.settings

    query = account.UserService.query.filter(models.User.client_id == client.id).order_by(models.User.id)

    request_args = copy_dict(request.args, {})

    if search_q:
        search_place_holder = "Search for %s" % search_q
        queries = [models.User.email.ilike("%%%s%%" % search_q), models.User.name_.ilike("%%%s%%" % search_q)]
        query = query.filter(or_(*queries))

    results = query.paginate(page, size, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

    return render_domain_template('client/general/list/accounts.html', **locals())


@client_blueprint.route('/accounts/users/new/', methods=["GET", "POST"])
@login_required
def new_account():
    page_title = "Setup User"
    no_angular = True
    has_sub_lnk = True

    client = fetch_client()

    cities = basic.CityService.all()
    countries = [basic.CountryService.filter_by(slug='nigeria')]
    states = basic.StateService.all()

    form = forms.ClientStaffForm()
    form.city_id.choices = [(c.id, c.name) for c in cities]
    form.state_id.choices = [(c.id, c.name) for c in states]
    form.country_id.choices = [(c.id, c.name) for c in countries]
    form.access_group_ids.choices = [(l.id, l.name) for l in
                                     models.AccessGroup.query.filter(or_(models.AccessGroup.client_id == client.id,
                                                                         models.AccessGroup.is_client == True)).all()]

    if request.method == 'POST' and form.validate_on_submit():
        data = form.data.copy()
        user = account.UserService.create(ignored=None, **data)
        client = fetch_client()
        setting = client.settings

        if not setting.accounts_dep_resolved:
            setting.accounts_dep_resolved = True
            current_db_session = account.UserService.conn.object_session(setting)
            current_db_session.add(setting)
            current_db_session.commit()

        if not setting.stations_dep_resolved or not setting.billing_dep_resolved:
            return redirect(url_for('.onboarding'))

        return redirect(url_for('.accounts'))

    return render_domain_template('client/general/forms/account.html', **locals())


@client_blueprint.route('/accounts/access_groups/', methods=["GET"])
@login_required
def access_groups():
    page_title = "Setup Access Groups"
    no_angular = True
    has_sub_lnk = True

    try:
        page = int(request.args.get("page", 1))
        size = request.args.get("size", 20)
        search_q = request.args.get("q", None)
    except:
        abort(404)

    client = fetch_client()
    request_args = copy_dict(request.args, {})

    search_place_holder = "Search by (Name, Manager)"

    query = models.AccessGroup.query.filter(
        or_(models.AccessGroup.client_id == client.id, models.AccessGroup.is_client == True))

    if search_q:
        search_place_holder = "Search for %s" % search_q
        queries = [models.AccessGroup.name.ilike("%%%s%%" % search_q)]
        query = query.filter(or_(*queries))

    results = query.paginate(page, size, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

    return render_domain_template('client/general/list/access_groups.html', **locals())


@client_blueprint.route('/accounts/access_groups/new/', methods=["GET", "POST"])
@login_required
def new_access_group():
    """
    access group form
    :return:
    """
    page_title = "Setup Access Group"
    no_angular = True
    has_sub_lnk = True
    client = fetch_client()
    form = forms.AccessGroupForm(client_id=client.id)
    form.role_ids.choices = [(l.id, l.description) for l in
                             models.Role.query.filter(models.Role.is_admin == False).all()]

    if request.method == 'POST' and form.validate_on_submit():
        data = form.data.copy()
        data["is_client"] = True
        obj = access.AccessGroupService.create(**data)
        if obj:
            flash("Access Group Created Successfully")
            return redirect(url_for('.access_groups'))

    return render_domain_template('client/general/forms/access_group.html', **locals())


@client_blueprint.route('/accounts/access_groups/<int:id>/update/', methods=["GET", "POST"])
@login_required
def update_access_group(id):
    """
    updates access group object
    :param id:
    :return:
    """
    page_title = "Update Access Group"
    no_angular = True
    has_sub_lnk = True

    client = fetch_client()
    try:
        obj = access.AccessGroupService.get(id)

        form = forms.AccessGroupForm(obj=obj)
        form.is_update = True
        form.role_ids.choices = ((l.id, l.description) for l in
                                 models.Role.query.filter(models.Role.is_admin == False).all())

        if request.method == 'POST' and form.validate_on_submit():
            data = form.data.copy()
            obj = access.AccessGroupService.update(obj.id, **data)
            if obj:
                flash("Access Group Updated Successfully")
                return redirect(url_for('.access_groups'))
    except ObjectNotFoundException, e:
        abort(404)

    return render_domain_template('client/general/forms/access_group.html', **locals())


@client_blueprint.route('/accounts/users/<int:obj_id>/', methods=["GET", "POST"])
@login_required
def account_details(obj_id):
    """
    Account details
    :param obj_id:
    :return:
    """
    page_title = "Update User"
    no_angular = True
    has_sub_lnk = True

    client = fetch_client()

    # try:
    user_account = account.UserService.get(obj_id)

    countries = basic.CountryService.filter_by(slug='nigeria', first_only=False)
    states = basic.StateService.filter_by(country_id=user_account.address.country_id, first_only=False)
    cities = basic.CityService.filter_by(first_only=False, state_id=user_account.address.state_id)

    form = forms.ManageClientStaffForm(access_group_ids=[l.id for l in user_account.access_groups])
    form.city_id.choices = [(c.id, c.name) for c in cities]
    form.state_id.choices = [(c.id, c.name) for c in states]
    form.country_id.choices = [(c.id, c.name) for c in countries]
    user_access_groups = models.AccessGroup.query.filter(or_(models.AccessGroup.client_id == client.id,
                                                             models.AccessGroup.is_client == True)).all() \
        if models.AccessGroup.query.filter_by(slug='client-admin').first().id not in (x.id for x in
                                                                                      user_account.access_groups) else []
    form.access_group_ids.choices = [(l.id, l.name) for l in user_access_groups]

    if request.method == 'POST' and form.validate_on_submit():
        account.UserService.update(user_account.id, **form.data)
        return redirect(request.path)
    # except ObjectNotFoundException, e:
    #     abort(404)

    return render_domain_template('client/general/details/account.html', **locals())


@client_blueprint.route('/reports/', methods=["GET"])
@login_required
def reports():
    """
    Billing Information
    :return:
    """
    page_title = "Reports Overview"
    no_angular = True
    has_sub_lnk = True

    return render_domain_template('client/report/overview.html', **locals())


@client_blueprint.route('/reports/download_report/<int:id>/')
@login_required
def download_report(id):
    page_title = "Download Report"
    obj = models.Report.query.get(int(id))

    if not obj:
        abort(404)

    # url = get_temp_cloudfiles_url(obj.handle, obj.name)
    url = obj.file_path
    return redirect(url)


@client_blueprint.route('/reports/<string:handle>/report/')
@login_required
def report(handle):
    page_title = pageTitle = "%s Reports" % handle.title().replace("_", " ")
    no_angular = True
    has_sub_lnk = True
    if handle not in report_handles:
        abort(404)
    # Protect agains malicous paging parameters
    try:
        page = int(request.args.get("page", 1))
        size = request.args.get("size", 20)
    except:
        abort(404)

    request_args = copy_dict(request.args, {})
    c_handle = "client_%s" % handle

    client = fetch_client()

    query = models.Report.query.filter(models.Report.handle == c_handle.lower(),models.Report.client_id==client.id).order_by(
        desc(models.Report.date_created))

    is_report_list = True

    results = query.paginate(page, size, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

    return render_domain_template("client/report/lists.html", **locals())


@client_blueprint.route('/reports/generate_report/<string:handle>/', methods=["GET", "POST"])
@login_required
def generate_report(handle):
    """
    Generate report matching handle and file type
    """

    page_title = pageTitle = "Generate %s Report" % handle.title()
    if handle not in report_handles:
        abort(404)
    no_angular = True
    has_sub_lnk = True
    is_report_gen = True
    user = current_user
    client = user.client
    next_url = url_for(".report", handle=handle)

    type_choices = [("csv", "CSV"), ("xlsx", "XLSX")]
    form = forms.ReportForm()
    form.data_types.choices = type_choices
    form.data_type.choices = [(0, "---Select One---")] + type_choices

    if form.validate_on_submit():
        data = form.data
        data["handle"] = handle
        data["client_id"] = client.id
        data.pop("data_types", "")
        # custom_report.delay(**data)
        client_custom_report(**data)

        flash("The requested report is now being generated. An email will be sent to the admin shortly")

        return redirect(next_url)

    return render_domain_template("client/report/generate_report.html", **locals())


@client_blueprint.route('/reports/individual_report/<string:handle>/', methods=["GET", "POST"])
@login_required
def individual_report(handle):
    page_title = pageTitle = "%s Report" % handle[:-1].capitalize()
    if handle not in report_handles:
        abort(404)
    search_q = request.args.get("q", None)
    no_angular = True
    has_sub_lnk = True
    is_individual_report = True
    return render_domain_template("client/report/individual_report.html", **locals())


@client_blueprint.route('/reports/individual/<string:handle>/report/', methods=["GET", "POST"])
@login_required
def individual_report_chart(handle):
    if request.method == "POST":
        _data = {}
        if request.data:
            _data = request.data
            _data = json.loads(_data)
        elif request.form:
            _data = json.dumps(request.form)
            _data = json.loads(_data)

        q = _data.get("q")

        client = fetch_client()
        client_id = client.id

        if handle == "devices":
            device = models.Device.query.filter(models.Device.client_id == client_id, models.Device.code == q).first()
            if not device:
                return jsonify({"status": "failure", "payload": _data})
            _data["obj_id"] = device.id
            raw_metrics = device.product.metrics
            metrics = [i.as_dict for i in raw_metrics]
            _data["metrics"] = metrics
            _data["devices_json"] = [device.as_dict]
            _data["longitude"] = device.point.longitude if device.point else "0"
            _data["latitude"] = device.point.latitude if device.point else "0"
            _data["name"] = device.code
            return jsonify({"status": "success", "payload": _data})
        elif handle == "installations":
            installation = models.Installation.query.filter(models.Installation.client_id == client_id,
                                                            models.Installation.code == q).first()
            if not installation:
                return jsonify({"status": "failure", "payload": _data})
            _data["obj_id"] = installation.id
            return jsonify({"status": "success", "payload": _data})
        elif handle == "stations":
            station = models.Station.query.filter(models.Station.client_id == client_id,
                                                  models.Station.code == q).first()
            if not station:
                return jsonify({"status": "failure", "payload": _data})
            _data["obj_id"] = station.id
            _data["longitude"] = station.location.longitude if station.location else "0"
            _data["latitude"] = station.location.latitude if station.location else "0"
            _data["devices_json"] = [device.as_dict for device in station.devices]
            _data["name"] = station.name
            return jsonify({"status": "success", "payload": _data})
        else:
            return jsonify({"status": "failure", "payload": _data})

    return jsonify({"status": "failure", "payload": {"msg": "Request Method Not Allowed"}})


@client_blueprint.route('/reports/individual/<string:handle>/<int:id>/report/', methods=["GET", "POST"])
@login_required
def generate_individual_report_chart(handle, id):
    no_report = render_domain_template("client/report/individuals/no-report.html", **locals())

    if handle == "devices":
        device = models.Device.query.filter(models.Device.id == id).first()
        return render_domain_template("client/report/individuals/device.html", **locals())
    elif handle == "installations":
        installation = models.Installation.query.filter(models.Installation.id == id).first()
        return render_domain_template("client/report/individuals/installation.html", **locals())
    elif handle == "stations":
        station = models.Station.query.filter(models.Station.id == id).first()
        return render_domain_template("client/report/individuals/station.html", **locals())
    else:
        return no_report


@client_blueprint.route('/reports/no/report/', methods=["GET", "POST"])
@login_required
def no_report():
    return render_domain_template("client/report/individuals/no-report.html", **locals())


@client_blueprint.route('/billings/info/', methods=["GET"])
@login_required
def billing_info():
    """
    Billing Information
    :return:
    """
    page_title = "Billing Information"
    no_angular = True
    has_sub_lnk = True

    card_added = request.args.get("card", False)
    # session.pop("encryted_card_data")
    encryted_card_data = session.get("encryted_card_data", None)

    client = fetch_client()
    wallet = payment.WalletService.filter_by(client_id=client.id)
    country = models.Country.query.filter(models.Country.code == "NG").first()

    delete_account_form = forms.DeleteObjectForm()
    delete_card_form = forms.DeleteObjectForm()

    setting = client.settings
    default_resolved = True if setting.stations_dep_resolved and setting.accounts_dep_resolved and \
                               setting.billing_dep_resolved else False

    debt_threshold_form = forms.BillingThresholdForm(debt_threshold_warning=int(setting.debt_threshold_warning),
                                                     allow_debt_warning=setting.allow_debt_warning)

    bank_account_form = forms.BankAccountForm(client_id=client.id)
    bank_account_form.bank_id.choices = [(0, "--- Select Bank ---")] + [(b.id, b.name) for b in
                                                                        models.Bank.query.order_by(
                                                                            models.Bank.name).all()]

    card_verf_form = forms.AddCardVerfForm()

    form = forms.CardForm(country_id=country.id, client_id=client.id)
    form.city_id.choices = [(0, "--- Select City ---")] + [(c.id, c.name) for c in
                                                           models.City.query.order_by(models.City.name).all()]
    form.state_id.choices = [(0, "--- Select State ---")] + [(s.id, s.name) for s in
                                                             models.State.query.order_by(models.State.name).all()]
    form.country_id.choices = [(0, "--- Select Country ---")] + [(c.id, c.name) for c in models.Country.query.order_by(
        models.Country.name).all()]

    return render_domain_template('client/general/billing_info.html', **locals())


@client_blueprint.route('/billings/update_debt_threshold/', methods=["POST"])
@login_required
def update_debt_threshold():
    """
    Billing Information
    :return:
    """
    if request.method != "POST":
        flash("Request Method Not Allowed")
        return redirect(url_for('.billing_info'))

    client = fetch_client()
    setting = client.settings
    debt_threshold_form = forms.BillingThresholdForm()

    if debt_threshold_form.validate_on_submit():
        data = debt_threshold_form.data
        basic.SettingsService.update(setting.id, **data)
        flash("Debt Threshold Notification updated successfully")

    return redirect(url_for('.billing_info'))


@client_blueprint.route('/billings/add_card/', methods=["POST"])
@login_required
def add_card():
    """
    Billing Information
    :return:
    """
    page_title = "Add Card"

    if request.method != "POST":
        flash("Request Method Not Allowed")
        return redirect(url_for('.billing_info'))

    client = fetch_client()
    country = basic.CountryService.filter_by(code='NG')

    card_verf_form = forms.AddCardVerfForm()
    form = forms.CardForm(country_id=country.id, client_id=client.id)
    form.city_id.choices = [(0, "--- Select City ---")] + [(c.id, c.name) for c in basic.CityService.all()]
    form.state_id.choices = [(0, "--- Select State ---")] + [(s.id, s.name) for s in basic.StateService.all()]
    form.country_id.choices = [(0, "--- Select Country ---")] + [(c.id, c.name) for c in models.Country.query.order_by(
        models.Country.name).all()]

    if form.validate_on_submit():
        data = form.data
        card_number = "XXXX-XXXX-XXXX-%s" % form.card_number.data[-4:]
        card_expiry = data.get("card_expiry", "").replace(" ", "").split("/")
        data["expiry_month"] = card_expiry[0] or ""
        data["expiry_year"] = card_expiry[1] or ""
        card = payment.CardService.filter_by(mask=card_number, brand=data.get("brand", ""),
                                             exp_month=data.get("expiry_month", ""),
                                             exp_year=data.get("expiry_year", ""))
        if card:
            flash("Card Record Already exist")
            return redirect(url_for('.billing_info'))

        # try:
        encryted_card_data = encrypt_dict_to_string(**data)
        session["encryted_card_data"] = str(encryted_card_data)
        status, resp = payment.FlutterCardService.register(**data)
        if resp and status == "success" and resp.get("responsemessage", "") == "BVN must be supplied for this option":
            setting = basic.SettingsService.filter_by(client_id=client.id)
            _d = {"billing_dep_resolved": True}
            basic.SettingsService.update(setting.id, **_d)
            flash("Card Record Added successfully")

            if not setting.stations_dep_resolved or not setting.accounts_dep_resolved or not setting.billing_dep_resolved:
                return redirect(url_for('.onboarding'))

            return redirect(url_for('.billing_info', card=True))

            # except Exception, e:
            #     print(e)
            #     flash('Please check your internet connection, then try again!')

    return redirect(url_for('.billing_info'))


@client_blueprint.route('/billings/add_card/verification/', methods=["POST"])
@login_required
def add_card_verf():
    """
    Billing Information
    :return:
    """
    page_title = "Add Card"

    if request.method != "POST":
        flash("Request Method Not Allowed")
        return redirect(url_for('.billing_info'))

    client = fetch_client()
    # encryted_card_data = session.get("encryted_card_data", None)
    card_verf_form = forms.AddCardVerfForm()

    if card_verf_form.validate_on_submit():
        data = card_verf_form.data
        encryted_card_data = data.get("card_data", "")
        try:
            decrypted_card_data = decrypt_string_to_dict(encryted_card_data)
            decrypted_card_data["bvn"] = data.get("bvn", "")
            status, resp = payment.FlutterCardService.register(**decrypted_card_data)
            card_id = resp.get("card_id", None)
            if status == "success" and card_id and resp.get("responsecode", None) in ["0", "00", "200"]:
                setting = client.settings
                _d = {"billing_dep_resolved": True}
                basic.SettingsService.update(setting.id, **_d)
                flash("Card Record Added and Verified successfully")
                card_added.send(card_id)
                session.pop("encryted_card_data")
                if not setting.stations_dep_resolved or not setting.accounts_dep_resolved or not setting.billing_dep_resolved:
                    return redirect(url_for('.onboarding'))

                return redirect(url_for('.billing_info'))
            elif status == "success" and resp.get("responsecode", None) in ["RR"]:
                flash(resp.get("responsemessage", "Failure Adding Card...Please Try again later or contact support."))
                return redirect(url_for('.billing_info'))
        except Exception, e:
            print(e)
            flash('Please check your internet connection, then try again!')

        session.pop("encryted_card_data")
        return redirect(url_for('.billing_info'))


@client_blueprint.route('/billings/cards/<int:id>/test_charge/', methods=["GET"])
@login_required
def test_charge(id):
    """
    Billing Information
    :return:
    """
    page_title = "Add Card"

    client = fetch_client()
    card = payment.CardService.get(id)
    data = {"narration": "Testing visa debit", "amount": 10.0}
    resp = payment.CardService.charge(card.id, **data)
    pprint("Resp after charge ======= %s" % resp)
    flash("Card Charged")
    return redirect(url_for('.billing_info'))


@client_blueprint.route('/billings/remove_card/', methods=["POST"])
@login_required
def remove_card():
    """
    Billing Information
    :return:
    """
    page_title = "Remove Card"

    if request.method != "POST":
        flash("Request Method Not Allowed")
        return redirect(url_for('.billing_info'))

    client = fetch_client()

    delete_card_form = forms.DeleteObjectForm()
    if delete_card_form.validate_on_submit():
        data = delete_card_form.data
        try:
            payment.CardService.delete(data.get("id", 0))
            client = fetch_client()
            if client.cards.count() < 1:
                setting = client.settings
                _d = {"billing_dep_resolved": False}
                basic.SettingsService.update(setting.id, **_d)
            flash("Card Deleted Successfully")
        except:
            flash("Card Deletion Failed")

    return redirect(url_for('.billing_info'))


@client_blueprint.route('/billings/add_bank_account/', methods=["POST"])
@login_required
def add_bank_account():
    """
    Billing Information
    :return:
    """
    page_title = "Add Bank Account"

    if request.method != "POST":
        flash("Request Method Not Allowed")
        return redirect(url_for('.billing_info'))

    client = fetch_client()

    bank_account_form = forms.BankAccountForm(client_id=client.id)
    bank_account_form.bank_id.choices = [(0, "--- Select Bank ---")] + [(b.id, b.name) for b in models.Bank.query.all()]
    if bank_account_form.validate_on_submit():
        data = bank_account_form.data
        _acc = models.BankAccount.query.filter(models.BankAccount.bank_id == data.get("bank_id", 0),
                                               models.BankAccount.account_number == data.get("account_number",
                                                                                             "")).first()
        if _acc:
            flash("Bank Account already exist. Please try again with another account.")
            return redirect(url_for('.billing_info'))
        obj = payment.FlutterBankAccountService.register(**data)
        if obj:
            setting = client.settings
            _d = {"bank_account_dep_resolved": True}
            print "setting %s =================" % setting.id
            basic.SettingsService.update(setting.id, **_d)
            flash("Bank Account Record Created Successfully")
        else:
            flash("Bank Account Record Creation Failed")

    return redirect(url_for('.billing_info'))


@client_blueprint.route('/billings/remove_bank_account/', methods=["POST"])
@login_required
def remove_bank_account():
    """
    Billing Information
    :return:
    """
    page_title = "Remove Bank Account"

    if request.method != "POST":
        flash("Request Method Not Allowed")
        return redirect(url_for('.billing_info'))

    client = fetch_client()

    delete_account_form = forms.DeleteObjectForm()
    if delete_account_form.validate_on_submit():
        data = delete_account_form.data
        try:
            payment.BankAccountService.delete(data.get("id", 0))
            client = fetch_client()
            if client.bank_accounts.count() < 1:
                setting = client.settings
                _d = {"bank_account_dep_resolved": False}
                basic.SettingsService.update(setting.id, **_d)
            flash("Bank Account Record Deleted Successfully")
        except:
            flash("Bank Account Record Deletion Failed")

    return redirect(url_for('.billing_info'))


@client_blueprint.route('/billings/invoices/', methods=["GET"])
@login_required
def invoices():
    """
    Billing Invoices
    :return:
    """
    page_title = "Invoices"
    no_angular = True
    has_sub_lnk = True
    search_q = None
    size = 20
    page = 1

    try:
        page = int(request.args.get("page", 1))
        size = request.args.get("size", 20)
        search_q = request.args.get("q", None)
    except Exception, e:
        print('=========error======')
        print(e)
        print('=========error======')
        abort(404)

    # request_args = copy_dict(request.args, {})
    request_args = request.args.copy()

    query = models.Invoice.query

    # if search_q:
    #     queries = [BillingRecord.customer_id.ilike("%%%s%%" % search_q),
    #                BillingRecord.billing_id.ilike("%%%s%%" % search_q)]
    #     query = query.filter(or_(*queries))

    query = query.order_by(desc(models.Invoice.date_created))

    results = query.paginate(page, size, False)

    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

    return render_domain_template('client/general/list/invoices.html', **locals())


@client_blueprint.route('/billings/records/', methods=["GET"])
@login_required
def invoice_details(id):
    invoice = models.Invoice.query.get(id)
    if not invoice:
        abort(404)
    return render_domain_template('client/general/details/invoice.html', **locals())


@client_blueprint.route('/billings/invoices/<int:id>/print/invoice<string:code>.pdf', methods=["GET"])
@login_required
def print_invoice(id, code):
    invoice = models.Invoice.query.get(id)
    if not invoice:
        abort(404)
    page_title = "Invoice%s" % invoice.code
    return return_pdf('client/prints/invoice.html', **locals())


@client_blueprint.route('/billings/receipts/', methods=["GET"])
@login_required
def receipts():
    """
    Invoice receipts
    :return:
    """
    page_title = "Receipts"
    no_angular = True
    has_sub_lnk = True
    search_q = None
    size = 20
    page = 1

    # Protect agains malicous paging parameters
    try:
        page = int(request.args.get("page", 1))
        size = request.args.get("size", 20)
        search_q = request.args.get("q", None)
    except Exception, e:
        print('=========error======')
        print(e)
        print('=========error======')
        abort(404)

    # request_args = copy_dict(request.args, {})
    request_args = request.args.copy()

    query = models.Receipt.query

    query = query.order_by(desc(models.Receipt.date_created))

    results = query.paginate(page, size, False)

    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

    return render_domain_template('client/general/list/receipts.html', **locals())


@client_blueprint.route('/billings/history/', methods=["GET"])
@login_required
def billing_records():
    """
    Billing Records
    :return:
    """
    page_title = "Billing Records"
    no_angular = True
    has_sub_lnk = True
    search_q = None
    size = 20
    page = 1

    # Protect agains malicous paging parameters
    try:
        page = int(request.args.get("page", 1))
        size = request.args.get("size", 20)
        search_q = request.args.get("q", None)
    except Exception, e:
        print('=========error======')
        print(e)
        print('=========error======')
        abort(404)

    # request_args = copy_dict(request.args, {})
    request_args = request.args.copy()

    query = models.Billing.query

    # if search_q:
    #     queries = [BillingRecord.customer_id.ilike("%%%s%%" % search_q),
    #                BillingRecord.billing_id.ilike("%%%s%%" % search_q)]
    #     query = query.filter(or_(*queries))

    query = query.order_by(desc(models.Billing.date_created))

    results = query.paginate(page, size, False)

    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

    return render_domain_template('client/general/list/billing_records.html', **locals())


@client_blueprint.route('/billings/customers/', methods=["GET"])
@login_required
def customer_records():
    """
    Customer Records
    :return:
    """
    page_title = "Customer Billing Records"
    no_angular = True
    has_sub_lnk = True
    search_q = None
    size = 20
    page = 1

    try:
        page = int(request.args.get("page", 1))
        size = request.args.get("size", 20)
        search_q = request.args.get("q", None)
    except Exception, e:
        print('=========error======')
        print(e)
        print('=========error======')
        abort(404)

    # request_args = copy_dict(request.args, {})
    request_args = request.args.copy()

    query = models.CustomerBilling.query

    # if search_q:
    #     queries = [BillingRecord.customer_id.ilike("%%%s%%" % search_q),
    #                BillingRecord.billing_id.ilike("%%%s%%" % search_q)]
    #     query = query.filter(or_(*queries))

    query = query.order_by(desc(models.CustomerBilling.date_created))

    results = query.paginate(page, size, False)

    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

    return render_domain_template('client/general/list/customer_billings.html', **locals())


@client_blueprint.route('/<slug>/', methods=["GET"])
@login_required
def dashboard(slug):
    """
    Services dashboard
    :param slug:
    :return:
    """
    g.user = current_user

    _client = fetch_client()

    if not _client:
        return redirect(url_for('.logout'))

    client = _client.as_json

    _service = basic.DefaultService.filter_by(code=str(slug))

    if not _service:
        return redirect('page_not_found')

    # service = build_navigation(_service.id)
    service = _service.as_json

    page_title = '%s Home' % _service.name
    page_caption = "%s Dashboard" % _service.name

    setting = _client.settings
    default_resolved = True if setting.stations_dep_resolved and setting.accounts_dep_resolved and \
                               setting.billing_dep_resolved else False
    if not default_resolved:
        return redirect(url_for('.onboarding'))

    navigations = [c.as_json for c in models.Navigation.query.all()]

    return render_domain_template('client/pages/index.html', **locals())


@client_blueprint.route('/test/', methods=['GET'])
def test_route():
    return render_domain_template('test.html')


@client_blueprint.route('/<slug>/partials/<path:path>/<string:template>', methods=['GET'])
def partials(slug, path, template):
    """
    process partial templates
    :param path:
    :return:
    """
    return render_domain_template('partials/{}/{}.html'.format(path, template), **locals())


@client_blueprint.route('/setup-services/')
@login_required
def onboarding():
    """
    onboarding view that forces client to register devices, networks and stations
    :return:
    """
    page_title = 'Configure Services'
    client = fetch_client()
    setting = client.settings
    default_resolved = True if setting.stations_dep_resolved and setting.accounts_dep_resolved and \
                               setting.billing_dep_resolved else False

    subscriptions = payment.SubscriptionService.filter_by(first_only=False, **{'client_id': client.id})

    return render_domain_template('client/info/onboard.html', **locals())


@client_blueprint.route('/check/company/')
def check_records():
    """
    returns True if client or company account matching request already exists
    :return:
    """
    data = request.args.copy()

    client = account.ClientService.filter_by(sub_domain=data['sub_domain'])
    if client:
        return json.dumps({'response': True, 'message': 'Client with this sub domain already exists'})

    user = account.UserService.filter_by(email=data['email'])
    if user:
        return json.dumps({'response': True, 'message': 'User with this email already exists'})

    return json.dumps({'response': False})


@client_blueprint.route('/invoice/', methods=["GET"])
def invoice():
    page_title = "Invoice"
    page_caption = "Client Invoice"

    return render_domain_template('client/info/invoice.html', **locals())


@client_blueprint.route('/details/', methods=["GET"])
def details():
    page_title = "Details Page"
    no_angular = True
    has_sub_lnk = True
    return render_domain_template('client/general/details.html', **locals())


@client_blueprint.route('/components/overall/', methods=["GET"])
def components():
    page_title = "Components"
    no_angular = True
    show_filter = show_form = show_list = show_billingbox = True
    return render_domain_template('client/general/components/overall.html', **locals())

@client_blueprint.route('/components/dashboard/', methods=["GET"])
def components_dash():
    page_title = "Components"
    no_angular = True
    show_filter = show_form = show_list=show_billingbox = False
    return render_domain_template('client/general/components/overall.html', **locals())


@client_blueprint.route('/messages/', methods=["GET"])
@client_blueprint.route('/messages/inbox/', methods=["GET"])
def messages():
    page_title = "Messages - Inbox"
    no_angular = True
    has_sub_lnk = True
    is_inbox = True

    try:
        page = int(request.args.get("page", 1))
        size = request.args.get("size", 20)
        search_q = request.args.get("q", None)
        view_q = request.args.get("view", "all")
    except Exception, e:
        abort(404)

    client = fetch_client()
    search_place_holder = "Search"

    form = forms.ComposeMessageForm()
    form.recipient_type.choices = [(0, "--- Choose a Recipient ---")] + [("admin", "Administrator"),
                                                                         ("customer", "Customer")]
    form.customer_id.choices = [(0, "--- Choose a Customer ---")] + [(i.id, i.name) for i in client.customers]

    request_args = copy_dict(request.args, {})

    view_filters = [{"name": "all", "value": "all"}, {"name": "admin", "value": "admin"},
                    {"name": "customer", "value": "customer"}]

    results = mongo.MongoService.query_collection(collection_name='message', page=page, size=size,
                                                  **{'client_id': client.id,
                                                     'client_inbox': True})

    if view_q and view_q != "all":
        if view_q == "admin":
            is_admin_msg = True
        else:
            is_admin_msg = False
        results = mongo.MongoService.query_collection(collection_name='message', page=page, size=size,
                                                      **{'client_id': client.id,
                                                         'client_inbox': True, "is_admin_message": is_admin_msg})

    if results.get("next", None):
        # build next page query parameters
        request_args["page"] = results.get("next")
        results["next_page"] = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.get("prev", None):
        # build previous page query parameters
        request_args["page"] = results.get("prev")
        results["previous_page"] = "%s%s" % ("?", urllib.urlencode(request.args))

    return render_domain_template('client/messages/inbox.html', **locals())


@client_blueprint.route('/messages/outbox/', methods=["GET"])
def outbox_messages():
    page_title = "Messages - Outbox"
    no_angular = True
    has_sub_lnk = True
    is_outbox = True

    try:
        page = int(request.args.get("page", 1))
        size = request.args.get("size", 20)
        search_q = request.args.get("q", None)
        view_q = request.args.get("view", "all")
    except Exception, e:
        abort(404)

    client = fetch_client()
    search_place_holder = "Search"

    form = forms.ComposeMessageForm()
    form.recipient_type.choices = [(0, "--- Choose a Recipient ---")] + [("admin", "Administrator"),
                                                                         ("customer", "Customer")]
    form.customer_id.choices = [(0, "--- Choose a Customer ---")] + [(i.id, i.name) for i in client.customers]

    request_args = copy_dict(request.args, {})

    view_filters = [{"name": "all", "value": "all"}, {"name": "admin", "value": "admin"},
                    {"name": "customer", "value": "customer"}]

    results = mongo.MongoService.query_collection(collection_name='message', page=page, size=size,
                                                  **{'client_id': client.id,
                                                     'client_outbox': True})

    if view_q and view_q != "all":
        if view_q == "admin":
            is_admin_msg = True
        else:
            is_admin_msg = False
        results = mongo.MongoService.query_collection(collection_name='message', page=page, size=size,
                                                      **{'client_id': client.id,
                                                         'client_outbox': True, "is_admin_message": is_admin_msg})

    if results.get("next", None):
        # build next page query parameters
        request_args["page"] = results.get("next")
        results["next_page"] = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.get("prev", None):
        # build previous page query parameters
        request_args["page"] = results.get("prev")
        results["previous_page"] = "%s%s" % ("?", urllib.urlencode(request.args))

    return render_domain_template('client/messages/inbox.html', **locals())


@client_blueprint.route('/messages/create/', methods=["GET", "POST"])
def create_message():
    if request.method == "POST":
        next_url = request.args.get("next") or url_for(".messages")
        client = fetch_client()

        form = forms.ComposeMessageForm()
        form.recipient_type.choices = [(0, "--- Choose a Recipient ---")] + [("admin", "Administrator"),
                                                                             ("customer", "Customer")]
        form.customer_id.choices = [(0, "--- Choose a Customer ---")] + [(i.id, i.name) for i in client.customers]

        if form.validate_on_submit():
            data = form.data
            recipient_type = data.get("recipient_type", "admin")
            message_type = data.get("message_type", "message")
            if recipient_type == "customer":
                data["is_admin_message"] = False
                data["admin_read"] = False
                data["customer_read"] = False
            else:
                data["is_admin_message"] = True
                data["admin_read"] = False
                data["customer_read"] = False
            data["client_id"] = client.id
            data["name"] = client.name
            data["email"] = client.email
            data["phone"] = client.phone
            data["client_read"] = True
            data["customer_replied"] = False
            data["admin_replied"] = False
            data["client_replied"] = False
            data["client_inbox"] = False
            data["client_outbox"] = True
            process_form = forms.MessageProcessingForm(**data)
            process_form.validate()
            p_data = process_form.data
            if recipient_type == "admin":
                p_data["is_admin_message"] = True
            p_data["client_read"] = True
            p_data["client_outbox"] = True
            timestamp = (datetime.today() - datetime(1970, 1, 1)).total_seconds()
            identifier = mongo.MongoService.get_next_sequence(message_type)
            resp = mongo.MongoService.record_data(message_type, identifier, timestamp, **p_data)
            return redirect(next_url)
        else:
            for key in form.errors:
                try:
                    flash("%s - %s" % (key, form.errors[key][0]))
                except:
                    flash("Error in information provided")
                return redirect(next_url)
            return redirect(next_url)
    else:
        flash("Method Not Allowed")
        return redirect(url_for('.messages'))


@client_blueprint.route('/messages/complaints/', methods=["GET"])
def complaints():
    page_title = "Complaints"
    no_angular = True
    has_sub_lnk = True
    is_complaint = True

    try:
        page = int(request.args.get("page", 1))
        size = request.args.get("size", 20)
        search_q = request.args.get("q", None)
        view_q = request.args.get("view", None)
    except Exception, e:
        abort(404)

    client = fetch_client()
    search_place_holder = "Search"

    form = forms.ComposeMessageForm()
    form.recipient_type.choices = [(0, "--- Choose a Recipient ---")] + [("admin", "Administrator"),
                                                                         ("customer", "Customer")]
    form.customer_id.choices = [(0, "--- Choose a Customer ---")] + [(i.id, i.name) for i in client.customers]

    request_args = copy_dict(request.args, {})

    results = mongo.MongoService.query_collection(collection_name='complaint', page=page, size=size,
                                                  **{'client_id': client.id})

    if results.get("next", None):
        # build next page query parameters
        request_args["page"] = results.get("next")
        results["next_page"] = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.get("prev", None):
        # build previous page query parameters
        request_args["page"] = results.get("prev")
        results["previous_page"] = "%s%s" % ("?", urllib.urlencode(request.args))

    return render_domain_template('client/messages/complaints.html', **locals())
