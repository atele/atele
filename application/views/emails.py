"""
public.py

@Author: kunsam002, stikks
@Date: 2017

The api views
"""

from datetime import datetime
import hashlib

from flask import Blueprint, current_app as app
from services import render_domain_template


logger = app.logger

# setup principal
principal = app.principal

email_bp = Blueprint('email_bp', __name__)


def object_length(data):
    return len(data)


def _hashuser_email(email):
    return hashlib.md5(email).hexdigest()


@email_bp.context_processor
def main_context():
    """ Include some basic assets in the startup page """
    today = datetime.today()
    minimum = min
    hashuser_email = _hashuser_email
    length = object_length

    return locals()


@email_bp.route('/')
def index():
    logger.info("right gher")
    print "right gher"
    page_title = "Email Home"

    return render_domain_template("email/layout.html", **locals())


@email_bp.route('/<string:template_name>/')
def email_template(template_name):
    _t = "email/%s.html" % template_name
    logger.info(_t)
    print _t
    return render_domain_template(_t, **locals())

