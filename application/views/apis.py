"""
public.py

@Author: kunsam002, stikks
@Date: 2017

The api views
"""

from datetime import datetime, timedelta
import json
import hashlib

from flask import Blueprint, current_app as app, request, make_response, jsonify
from model_package import models

from application.services import payment
from utilities.utils import render_domain_template


logger = app.logger

# setup principal
principal = app.principal

api = Blueprint('api', __name__, url_prefix="/api")


def object_length(data):
    return len(data)


def _hashuser_email(email):
    return hashlib.md5(email).hexdigest()


@api.context_processor
def main_context():
    """ Include some basic assets in the startup page """
    today = datetime.today()
    minimum = min
    hashuser_email = _hashuser_email
    length = object_length

    return locals()


@api.route('/')
def index():
    page_title = "API Home"

    return render_domain_template("apis/index.html", **locals())


@api.route('/update_billing', methods=["GET", "POST"])
def update_billing():
    if request.method == "POST":
        _data = {}
        if request.data:
            _data = request.data
            _data = json.loads(_data)
        elif request.form:
            _data = json.dumps(request.form)
            _data = json.loads(_data)

        if not _data.get("size", None):
            data = {"Status": "failure", "message": "Please provide Data Size"}
            data = jsonify(data)
            response = make_response(data)
            response.headers['Content-Type'] = "application/json"
            return response

        device_id = _data.get("device_id", None)
        if not device_id:
            data = {"Status": "failure", "message": "Please provide Device ID"}
            data = jsonify(data)
            response = make_response(data)
            response.headers['Content-Type'] = "application/json"
            return response

        subscription_id = _data.get("subscription_id", None)
        if not subscription_id:
            data = {"Status": "failure", "message": "Please provide Subscription ID"}
            data = jsonify(data)
            response = make_response(data)
            response.headers['Content-Type'] = "application/json"
            return response

        if not models.Device.query.get(device_id):
            data = {"Status": "failure", "message": "Device does not exist on record"}
            data = jsonify(data)
            response = make_response(data)
            response.headers['Content-Type'] = "application/json"
            return response

        client_id = _data.get("client_id", None)
        if not client_id:
            data = {"Status": "failure", "message": "Please specify Client"}
            data = jsonify(data)
            response = make_response(data)
            response.headers['Content-Type'] = "application/json"
            return response

        if not models.Client.query.get(client_id):
            data = {"Status": "failure", "message": "Client does not exist on record"}
            data = jsonify(data)
            response = make_response(data)
            response.headers['Content-Type'] = "application/json"
            return response

        # calculating cost of data transfer
        size = _data.get("size", 0)
        size_value = size * app.config.get("DATA_TRANSFER_FEE_PER_KB", 1)

        # Checking for existence of daily billing object
        today = datetime.today()
        today_start = datetime(today.year, today.month, today.day)
        today_end = today_start + timedelta(days=1)

        billing = models.Billing.query.filter(models.Billing.client_id == client_id,
                                              models.Billing.date_created.between(today_start, today_end)).first()

        if not billing:
            data = {"client_id": client_id}
            billing = payment.BillingService.create(**data)

        data = {"billing_id": billing.id, "device_id": device_id, "client_id": client_id, "data_size": size,
                "amount": size_value, "subscription_id":subscription_id}
        billing_record = payment.BillingRecordService.create(**data)

        data = {"amount": billing.amount + size_value}
        payment.BillingService.update(billing.id, **data)

        data = {"Status": "success", "message": "Billing Record Created successfully"}
        data = jsonify(data)
        response = make_response(data)
        response.headers['Content-Type'] = "application/json"
        return response

    else:
        data = {"Status": "failure", "message": "Request Method not Allowed"}
        data = jsonify(data)
        response = make_response(data)
        response.headers['Content-Type'] = "application/json"
        return response
