from datetime import datetime
from services import render_domain_template

from flask import Blueprint, current_app as app
from application.services.basic import DefaultService

landing = Blueprint('landing', __name__)
logger = app.logger


@landing.context_processor
def main_context():
    """ Include some basic assets in the startup page """
    today = datetime.today()
    services = DefaultService.all()
    return locals()


@landing.route('/', methods=["GET"])
@landing.route('/<path:url>/')
@app.ext.register_generator
def index(url=None):
    page_title = "Home"
    return render_domain_template('main/index.html', **locals())


# def index():
#     # Not needed if you set SITEMAP_INCLUDE_RULES_WITHOUT_PARAMS=True
#     yield 'index', {}


@landing.route('/services/', methods=["GET"])
def services():
    page_title = "Services"
    page_caption = "Directory"

    return render_domain_template('main/services.html', **locals())


@landing.route('/devices/', methods=["GET"])
def devices():
    page_title = "Hardware"
    page_caption = "Devices"

    return render_domain_template('main/devices.html', **locals())


@landing.route('/pricing/', methods=["GET"])
def pricing():
    page_title = "Pricing"
    page_caption = "Plan"

    return render_domain_template('main/pricing.html', **locals())


@landing.route('/solutions/smart-metering/', methods=["GET"])
def smart_metering():
    """
    view for smart grid services
    :return:
    """
    page_title = "Smart Grid Service"

    return render_domain_template('main/smart_grid.html', **locals())


@landing.route('/solutions/mobile-asset/', methods=["GET"])
def mobile_asset():
    """
    view for mobile assets
    :return:
    """
    page_title = "Mobile Asset Tracking Service"

    return render_domain_template('main/mobile_asset.html', **locals())


@landing.route('/solutions/remote-monitoring/', methods=["GET"])
def remote_monitoring():
    """
    view for remote monitoring services
    :return:
    """
    page_title = "Remote Monitoring Service"

    return render_domain_template('main/remote_monitoring.html', **locals())


# @landing.route('/sitemap.xml')
# def sitemap():
#     """
#     Generate sitemaps
#     :return:
#     """
#     return render_domain_template('main/sitemap.xml')