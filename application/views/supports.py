from datetime import datetime
import base64, json

from flask import Blueprint, current_app as app, render_template, session, g
from flask_login import current_user

from model_package import models
from application.services.basic import SettingsService
from application.services.account import ClientService
# from application.models import User


support = Blueprint('support', __name__)
logger = app.logger


@support.context_processor
def main_context():
    """ Include some basic assets in the startup page """
    today = datetime.today()
    services_url = app.config['SERVICES_URL']
    length = len
    minimum = min
    user = current_user
    current_client = fetch_client()
    # navigation = _client_navigation()
    setting = SettingsService.query.filter()

    return locals()


@app.before_request
def load_user():
    if session.get("user_id"):
        user = models.User.query.get(int(session["user_id"]))
        g.user = user


@app.errorhandler(404)
def page_not_found(e):
    page_title = "404- Page Not Found"
    error_code = "404"
    error_title = "Page not found"
    error_info = "For Some Reason The Page You Requested Could Not Be Found On Our Server."

    return render_template('support/404.html', **locals()), 404


@app.errorhandler(500)
@app.errorhandler(502)
def page_not_found(e):
    page_title = "502- Bad Gateway"
    error_code = "502"
    error_title = "Bad Gateway"
    error_info = "Our Servers are experiencing an overload in connections. Please contact the Administrator."

    return render_template('support/502.html', **locals()), 502


def build_dict_get_by_code(seq, key="code"):
    return dict((d[key], dict(d, index=index)) for (index, d) in enumerate(seq))


def fetch_client():
    if session.get("client_id"):
        return ClientService.query.get(int(session.get("client_id")))
    else:
        return None


@support.route('/', methods=["GET"])
def index():
    page_title = "FAQ"
    page_caption = "Support Home"
    return render_template('support/index.html', **locals())


@support.route('/tickets/', methods=["GET"])
def tickets():
    page_title = "Support Ticket"
    page_caption = "Support Home"
    return render_template('support/tickets.html', **locals())
