from flask import current_app
# from application import models as app_models
from model_package import models as app_models

marshmallow = current_app.marshmallow


class SchemaFactory(object):
    @classmethod
    def create_schema(cls, module, model_class):

        class BaseSchema(marshmallow.ModelSchema):
            """
            create a marshmallow schema from a model class
            :param model_class:
            :return:
            """
            class Meta:
                model = getattr(module, model_class)

        return BaseSchema


CitySchema = SchemaFactory.create_schema(app_models, 'City')
StateSchema = SchemaFactory.create_schema(app_models, 'State')
CountrySchema = SchemaFactory.create_schema(app_models, 'Country')
AddressSchema = SchemaFactory.create_schema(app_models, 'Address')


