FROM python:2.7.13
RUN apt-get update \
  && apt-get install -y postgresql postgresql-contrib python-pip \
  && pip install --no-cache-dir -r requirements.txt
ADD . /opt/atele
WORKDIR  /opt/atele
RUN pip install virtualenv && virtualenv venv
CMD ['source venv/bin/activate']
CMD ['python manage.py db init']
CMD ['python manage.py db migrate']
CMD ['python manage.py db update']
CMD ['python manage.py setup_app']


# This Dockerfile doesn't need to have an entrypoint and a command
# as Bitbucket Pipelines will overwrite it with a bash script.