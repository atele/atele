"""
card.py

@Author: Olukunle Ogunmokun
@Date: June 24, 2017

card implementation of flutterwave api. Validation classes will be handled via marshmallow objects
"""
import pprint

from marshmallow import Schema, fields, pprint
from marshmallow.validate import Regexp, OneOf

from .base import FlutterwaveMixin, NumericStringField, AUTHMODEL, CURRENCYMODEL, COUNTRYMODEL, FLUTTERWAVE_KEY, \
    FLUTTERWAVE_ENDPOINT, FLUTTERWAVE_MERCHANT_ID, VALIDATEOPTION
from integrations import pycard


SAMPLE_CHARGE_DATA = dict(cardno="3566002020360505", authmodel=AUTHMODEL.RANDOM_DEBIT, currency=CURRENCYMODEL.NGN,
                          validateoption=VALIDATEOPTION.SMS,
                          amount=100, custid='ademola', cvv="812", expiryyear="18", expirymonth="08",
                          narration="Sample Narration", country=COUNTRYMODEL.NG,
                          responseurl="https://atele.org/auth/vbv/", pin="12345")

SAMPLE_VALIDATE_DATA = dict(cardno="3566002020360505", authmodel=AUTHMODEL.PIN, currency=CURRENCYMODEL.NGN,
                            validateoption=VALIDATEOPTION.SMS,
                            amount=100, custid='ademola', cvv="655", expiryyear="18", expirymonth="08",
                            narration="Sample Narration", country=COUNTRYMODEL.NG,
                            responseurl="https://atele.org/auth/vbv/", pin="12345")


class CardEnquirySchema(Schema):
    """ card validation schema for handling validation workflow """

    cardno = NumericStringField(required=True)
    expirymonth = NumericStringField(required=True)
    expiryyear = NumericStringField(required=True)
    cvv = fields.String(required=True)
    trxreference = fields.String(required=True)
    pin = fields.String()


class CardEnquiryValidateSchema(Schema):
    """ card validation schema for handling validation workflow """

    otp = fields.String(required=True)
    otptransactionidentifier = fields.String(required=True)
    trxreference = fields.String(required=True)


class CardSchema(Schema):
    """ Schema class for handling tokenizing/charging a card """
    # amount = fields.String(validate=Regexp(regex="\d+(\.\d+)?"))
    amount = NumericStringField(required=True)
    authmodel = fields.String(required=True, validate=OneOf(choices=AUTHMODEL.keys()))
    cardno = NumericStringField(required=True)
    currency = fields.String(required=True, validate=OneOf(choices=CURRENCYMODEL.keys()))
    custid = fields.String(required=True)
    customerId = fields.String(required=True)
    country = fields.String(required=False, validate=OneOf(choices=COUNTRYMODEL.keys()))
    cvv = fields.String(required=True)
    pin = fields.String(allow_none=True)
    bvn = fields.String()
    validateoption = fields.String(required=True, validate=OneOf(choices=VALIDATEOPTION.keys()))
    expirymonth = NumericStringField(required=True)
    expiryyear = NumericStringField(required=True)
    narration = fields.String(required=True)
    responseurl = fields.String()  # useful for only VBVSECURECODE payments


class TokenizeSchema(Schema):
    """ Schema class for handling tokenizing/charging a card """
    # amount = fields.String(validate=Regexp(regex="\d+(\.\d+)?"))
    validateoption = fields.String(required=True, validate=OneOf(choices=VALIDATEOPTION.keys()))
    authmodel = fields.String(required=True, validate=OneOf(choices=AUTHMODEL.keys()))
    cardno = NumericStringField(required=True)
    currency = fields.String(required=True, validate=OneOf(choices=CURRENCYMODEL.keys()))
    country = fields.String(required=False, validate=OneOf(choices=COUNTRYMODEL.keys()))
    cvv = fields.String(required=True)
    pin = fields.String(allow_none=True)
    bvn = fields.String(allow_none=True)
    expirymonth = NumericStringField(required=True)
    expiryyear = NumericStringField(required=True)
    narration = fields.String(required=False)
    responseurl = fields.String()  # useful for only VBVSECURECODE payments


class ValidateSchema(Schema):
    """ card validation schema for handling validation workflow """

    otp = fields.String(required=True)
    otptransactionidentifier = fields.String(required=True)
    # trxreference = fields.String(required=True)


class ChargeSchema(Schema):
    """ Schema for handling recurrent charge using accounttoken """

    amount = NumericStringField(required=True)
    currency = fields.String(required=True, validate=OneOf(choices=CURRENCYMODEL.keys()))
    custid = fields.String(required=True)
    country = fields.String(required=False, validate=OneOf(choices=COUNTRYMODEL.keys()))
    narration = fields.String(required=True)
    chargetoken = fields.String(required=True)
    cardtype = fields.String(default="Diamond")


class AuthorizeSchema(Schema):
    """ Schema for handling recurrent charge using accounttoken """

    amount = NumericStringField(required=True)
    currency = fields.String(required=True, validate=OneOf(choices=CURRENCYMODEL.keys()))
    country = fields.String(required=False, validate=OneOf(choices=COUNTRYMODEL.keys()))
    chargetoken = fields.String(required=True)


class CaptureSchema(Schema):
    """ card schema for validating capture/refund on card endpoints """

    amount = NumericStringField(required=True)
    currency = fields.String(required=True, validate=OneOf(choices=CURRENCYMODEL.keys()))
    country = fields.String(required=False, validate=OneOf(choices=COUNTRYMODEL.keys()))
    trxreference = fields.String(required=True)
    trxauthorizeid = fields.String(required=True)


class RefundSchema(Schema):
    """ card schema for validating refund on card endpoints """

    refundamount = NumericStringField(required=True)
    paymentreference = fields.String(required=True)


class Card(FlutterwaveMixin):
    """
    This should optionally be injected into the FlutterwaveMixin object.
    Card payments will include the following features:

    - Tokenize & Charge
    - Validate
    - Recurrent Payments

    Each method will accept values in the format that is expected and utilize the encryption and
    decryption mechanism from the parent to process and dispatch data.
    """

    pay_url = "card/mvva/pay"
    charge_url = "card/mvva/pay"
    validate_url = "card/mvva/pay/validate"

    tokenize_url = "card/mvva/tokenize"
    preauthorize_url = "card/mvva/preauthorize"
    capture_url = "card/mvva/capture"
    refund_url = "card/mvva/refund"
    void_url = "card/mvva/capture"

    enquiry_url = "card/mvva/cardenquiry"
    enquiry_validate_url = "card/mvva/cardenquiry/validate"

    fl_refund_url = "fw/refund"

    def pay(self, **kwargs):
        """ Charge payments will utilize a validation object. The pattern either returns an error or a response """

        try:
            # validate the data
            body = CardSchema(strict=True).load(kwargs)

            # extract the information
            return self.send(self.pay_url, body.data)

        except Exception, e:

            return self.error_response(e)

    def tokenize(self, **kwargs):
        """ Tokenize a card for recurrent transactions """

        try:
            # validate the data
            body = TokenizeSchema(strict=True).load(kwargs)

            # extract the information
            return self.send(self.tokenize_url, body.data)

        except Exception, e:

            return self.error_response(e)

    def validate(self, **kwargs):
        """ Validate a card """

        try:
            # validate the data
            body = ValidateSchema(strict=True).load(kwargs)

            # extract the information
            return self.send(self.validate_url, body.data)

        except Exception, e:

            return self.error_response(e)

    def charge(self, **kwargs):
        """
        Charge a card using the recurrent billing mechanism. This will be used for card payments with a charge token
        """

        try:
            # validate the data
            body = ChargeSchema(strict=True).load(kwargs)

            # extract the information
            return self.send(self.charge_url, body.data)

        except Exception, e:

            return self.error_response(e)

    def preauthorize(self, **kwargs):
        """
        Preauthorize a card using the recurrent billing mechanism. This will be used for card payments with a chargetoken """

        try:
            # validate the data
            body = AuthorizeSchema(strict=True).load(kwargs)

            # extract the information
            return self.send(self.preauthorize_url, body.data)

        except Exception, e:

            return self.error_response(e)

    def capture(self, **kwargs):
        """ Capture funds from a preauthorized card """

        try:
            # validate the data
            body = CaptureSchema(strict=True).load(kwargs)

            # extract the information
            return self.send(self.capture_url, body.data)

        except Exception, e:

            return self.error_response(e)

    def refund(self, **kwargs):
        """ Refund funds from a preauthorized card """

        try:
            # validate the data
            body = CaptureSchema(strict=True).load(kwargs)

            # extract the information
            return self.send(self.refund_url, body.data)

        except Exception, e:

            return self.error_response(e)

    def fl_refund(self, **kwargs):
        """ Refund funds from a preauthorized card """

        try:
            # validate the data
            body = RefundSchema(strict=True).load(kwargs)

            # extract the information
            return self.send(self.fl_refund_url, body.data)

        except Exception, e:

            return self.error_response(e)

    def void(self, **kwargs):
        """ Void captured funds from a preauthorized card """

        try:
            # validate the data
            body = CaptureSchema(strict=True).load(kwargs)

            # extract the information
            return self.send(self.void_url, body.data)

        except Exception, e:

            return self.error_response(e)

    def enquiry(self, **kwargs):
        """ Validate a card """

        try:
            # validate the data
            body = CardEnquirySchema(strict=True).load(kwargs)

            # extract the information
            return self.send(self.enquiry_url, body.data)

        except Exception, e:

            return self.error_response(e)

    def enquiry_validate(self, **kwargs):
        """ Validate a card """

        try:
            # validate the data
            body = CardEnquiryValidateSchema(strict=True).load(kwargs)

            # extract the information
            return self.send(self.enquiry_validate_url, body.data)

        except Exception, e:

            return self.error_response(e)