from plant import setup_app

app = setup_app('atele', 'DevelopmentConfig')

with app.app_context():
    from flask import current_app

    from integrations.messaging.mqtt import MQTTClient
    from integrations.utils import Payload

    config = Payload(**current_app.config)

    def on_message(pub_client, userdata, message):
        data = message.payload.decode()
        print('ping response received: ', data, pub_client._client_id)
        data = message.payload.decode()
        current_app.logger.info('[OBJECT: DATA][ACTOR: {}][STATUS: RECEIVED][ACTION: '
                                '{}]'.format(pub_client._client_id, message.topic, data))

        # todo send notification alert to browser confirming ping

    sub_client = MQTTClient(host=config.MQTT_HOST, port=config.MQTT_PORT, keep_alive=config.MQTT_KEEP_ALIVE,
                        qos=config.MQTT_QOS, bind_address=config.MQTT_BIND_ADDRESS, topic=config.MQTT_PING,
                        username=config.MQTT_USERNAME, password=config.MQTT_PASSWORD)
    sub_client.client.on_message = on_message
    sub_client.client.loop_forever()
