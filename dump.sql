--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.5
-- Dumped by pg_dump version 10.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: tiger; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA tiger;


ALTER SCHEMA tiger OWNER TO postgres;

--
-- Name: tiger_data; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA tiger_data;


ALTER SCHEMA tiger_data OWNER TO postgres;

--
-- Name: topology; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA topology;


ALTER SCHEMA topology OWNER TO postgres;

--
-- Name: SCHEMA topology; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA topology IS 'PostGIS Topology schema';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: address_standardizer; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS address_standardizer WITH SCHEMA public;


--
-- Name: EXTENSION address_standardizer; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION address_standardizer IS 'Used to parse an address into constituent elements. Generally used to support geocoding address normalization step.';


--
-- Name: address_standardizer_data_us; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS address_standardizer_data_us WITH SCHEMA public;


--
-- Name: EXTENSION address_standardizer_data_us; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION address_standardizer_data_us IS 'Address Standardizer US dataset example';


--
-- Name: fuzzystrmatch; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS fuzzystrmatch WITH SCHEMA public;


--
-- Name: EXTENSION fuzzystrmatch; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION fuzzystrmatch IS 'determine similarities and distance between strings';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


--
-- Name: postgis_tiger_geocoder; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis_tiger_geocoder WITH SCHEMA tiger;


--
-- Name: EXTENSION postgis_tiger_geocoder; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis_tiger_geocoder IS 'PostGIS tiger geocoder and reverse geocoder';


--
-- Name: postgis_topology; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis_topology WITH SCHEMA topology;


--
-- Name: EXTENSION postgis_topology; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis_topology IS 'PostGIS topology spatial types and functions';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: access_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE access_group (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    slug character varying(200),
    description character varying(200),
    permanent boolean,
    is_client boolean,
    client_id integer,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE access_group OWNER TO postgres;

--
-- Name: access_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE access_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE access_group_id_seq OWNER TO postgres;

--
-- Name: access_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE access_group_id_seq OWNED BY access_group.id;


--
-- Name: address; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE address (
    id integer NOT NULL,
    first_name character varying,
    last_name character varying NOT NULL,
    line1 character varying NOT NULL,
    line2 character varying,
    phone character varying(50),
    email character varying(255),
    city_id integer,
    state_id integer NOT NULL,
    country_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE address OWNER TO postgres;

--
-- Name: address_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE address_id_seq OWNER TO postgres;

--
-- Name: address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE address_id_seq OWNED BY address.id;


--
-- Name: admin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE admin (
    id integer NOT NULL,
    username character varying(255),
    password text,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE admin OWNER TO postgres;

--
-- Name: admin_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE admin_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin_id_seq OWNER TO postgres;

--
-- Name: admin_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE admin_id_seq OWNED BY admin.id;


--
-- Name: alembic_version; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE alembic_version (
    version_num character varying(32) NOT NULL
);


ALTER TABLE alembic_version OWNER TO postgres;

--
-- Name: alert; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE alert (
    id integer NOT NULL,
    code text NOT NULL,
    alert_type_id integer,
    device_id integer,
    installation_id integer,
    service_id integer NOT NULL,
    station_id integer NOT NULL,
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone,
    is_warning boolean,
    threshold double precision NOT NULL,
    is_critical boolean
);


ALTER TABLE alert OWNER TO postgres;

--
-- Name: alert_channel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE alert_channel (
    id integer NOT NULL,
    name character varying NOT NULL,
    slug character varying NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE alert_channel OWNER TO postgres;

--
-- Name: alert_channel_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE alert_channel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE alert_channel_id_seq OWNER TO postgres;

--
-- Name: alert_channel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE alert_channel_id_seq OWNED BY alert_channel.id;


--
-- Name: alert_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE alert_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE alert_id_seq OWNER TO postgres;

--
-- Name: alert_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE alert_id_seq OWNED BY alert.id;


--
-- Name: alert_recipient; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE alert_recipient (
    id integer NOT NULL,
    alert_id integer NOT NULL,
    recipient text NOT NULL,
    is_device boolean,
    is_email boolean,
    is_push boolean,
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE alert_recipient OWNER TO postgres;

--
-- Name: alert_recipient_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE alert_recipient_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE alert_recipient_id_seq OWNER TO postgres;

--
-- Name: alert_recipient_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE alert_recipient_id_seq OWNED BY alert_recipient.id;


--
-- Name: alert_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE alert_type (
    id integer NOT NULL,
    parameter character varying NOT NULL,
    metric character varying NOT NULL,
    symbol character varying NOT NULL,
    description text,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE alert_type OWNER TO postgres;

--
-- Name: alert_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE alert_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE alert_type_id_seq OWNER TO postgres;

--
-- Name: alert_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE alert_type_id_seq OWNED BY alert_type.id;


--
-- Name: associated_channels; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE associated_channels (
    alert_channel_id integer,
    alert_id integer
);


ALTER TABLE associated_channels OWNER TO postgres;

--
-- Name: associated_services; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE associated_services (
    service_id integer,
    installation_type_id integer
);


ALTER TABLE associated_services OWNER TO postgres;

--
-- Name: bank; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE bank (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    code character varying(200) NOT NULL,
    country_code character varying(200) NOT NULL,
    country_id integer NOT NULL,
    branch character varying(200) NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE bank OWNER TO postgres;

--
-- Name: bank_account; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE bank_account (
    id integer NOT NULL,
    first_name character varying(200) NOT NULL,
    last_name character varying(200) NOT NULL,
    account_number character varying(200) NOT NULL,
    bank_id integer NOT NULL,
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE bank_account OWNER TO postgres;

--
-- Name: bank_account_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE bank_account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bank_account_id_seq OWNER TO postgres;

--
-- Name: bank_account_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE bank_account_id_seq OWNED BY bank_account.id;


--
-- Name: bank_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE bank_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bank_id_seq OWNER TO postgres;

--
-- Name: bank_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE bank_id_seq OWNED BY bank.id;


--
-- Name: billing; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE billing (
    id integer NOT NULL,
    amount double precision,
    invoice_id integer NOT NULL,
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE billing OWNER TO postgres;

--
-- Name: billing_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE billing_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE billing_id_seq OWNER TO postgres;

--
-- Name: billing_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE billing_id_seq OWNED BY billing.id;


--
-- Name: billing_record; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE billing_record (
    id integer NOT NULL,
    billing_id integer NOT NULL,
    subscription_id integer NOT NULL,
    installation_id integer,
    device_id integer NOT NULL,
    network_id integer,
    data_size double precision,
    amount double precision,
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE billing_record OWNER TO postgres;

--
-- Name: billing_record_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE billing_record_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE billing_record_id_seq OWNER TO postgres;

--
-- Name: billing_record_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE billing_record_id_seq OWNED BY billing_record.id;


--
-- Name: card; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE card (
    id integer NOT NULL,
    customer_id integer,
    wallet_id integer,
    address_id integer NOT NULL,
    card_number text,
    token character varying(200) NOT NULL,
    mask character varying(300),
    brand character varying(300),
    exp_month integer,
    exp_year integer,
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE card OWNER TO postgres;

--
-- Name: card_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE card_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE card_id_seq OWNER TO postgres;

--
-- Name: card_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE card_id_seq OWNED BY card.id;


--
-- Name: city; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE city (
    id integer NOT NULL,
    name character varying(200),
    slug character varying(200),
    code character varying(200),
    state_id integer NOT NULL,
    country_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE city OWNER TO postgres;

--
-- Name: city_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE city_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE city_id_seq OWNER TO postgres;

--
-- Name: city_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE city_id_seq OWNED BY city.id;


--
-- Name: client; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE client (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    address text NOT NULL,
    city_id integer,
    state_id integer NOT NULL,
    country_id integer NOT NULL,
    email character varying(255),
    url character varying,
    sub_domain character varying(255) NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE client OWNER TO postgres;

--
-- Name: client_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE client_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE client_id_seq OWNER TO postgres;

--
-- Name: client_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE client_id_seq OWNED BY client.id;


--
-- Name: country; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE country (
    id integer NOT NULL,
    name character varying(200),
    slug character varying(200),
    code character varying(200),
    enabled boolean,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE country OWNER TO postgres;

--
-- Name: country_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE country_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE country_id_seq OWNER TO postgres;

--
-- Name: country_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE country_id_seq OWNED BY country.id;


--
-- Name: currency; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE currency (
    id integer NOT NULL,
    name character varying(200),
    code character varying(200),
    enabled boolean,
    symbol character varying(200),
    payment_code character varying(200),
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE currency OWNER TO postgres;

--
-- Name: currency_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE currency_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE currency_id_seq OWNER TO postgres;

--
-- Name: currency_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE currency_id_seq OWNED BY currency.id;


--
-- Name: customer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE customer (
    id integer NOT NULL,
    address_id integer NOT NULL,
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone,
    station_id integer NOT NULL
);


ALTER TABLE customer OWNER TO postgres;

--
-- Name: customer_billing; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE customer_billing (
    id integer NOT NULL,
    code text NOT NULL,
    start_date timestamp without time zone NOT NULL,
    end_date timestamp without time zone NOT NULL,
    amount double precision NOT NULL,
    customer_id integer NOT NULL,
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE customer_billing OWNER TO postgres;

--
-- Name: customer_billing_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE customer_billing_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_billing_id_seq OWNER TO postgres;

--
-- Name: customer_billing_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE customer_billing_id_seq OWNED BY customer_billing.id;


--
-- Name: customer_billing_record; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE customer_billing_record (
    id integer NOT NULL,
    code text NOT NULL,
    customer_billing_id integer NOT NULL,
    customer_id integer NOT NULL,
    device_id integer NOT NULL,
    network_id integer,
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE customer_billing_record OWNER TO postgres;

--
-- Name: customer_billing_record_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE customer_billing_record_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_billing_record_id_seq OWNER TO postgres;

--
-- Name: customer_billing_record_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE customer_billing_record_id_seq OWNED BY customer_billing_record.id;


--
-- Name: customer_deduction; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE customer_deduction (
    id integer NOT NULL,
    customer_billing_id integer NOT NULL,
    customer_billing_record_id integer NOT NULL,
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE customer_deduction OWNER TO postgres;

--
-- Name: customer_deduction_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE customer_deduction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_deduction_id_seq OWNER TO postgres;

--
-- Name: customer_deduction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE customer_deduction_id_seq OWNED BY customer_deduction.id;


--
-- Name: customer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE customer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_id_seq OWNER TO postgres;

--
-- Name: customer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE customer_id_seq OWNED BY customer.id;


--
-- Name: customer_invoice; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE customer_invoice (
    id integer NOT NULL,
    code text NOT NULL,
    customer_id integer NOT NULL,
    customer_billing_id integer NOT NULL,
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE customer_invoice OWNER TO postgres;

--
-- Name: customer_invoice_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE customer_invoice_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_invoice_id_seq OWNER TO postgres;

--
-- Name: customer_invoice_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE customer_invoice_id_seq OWNED BY customer_invoice.id;


--
-- Name: customer_receipt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE customer_receipt (
    id integer NOT NULL,
    code text NOT NULL,
    customer_invoice_id integer NOT NULL,
    customer_id integer NOT NULL,
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE customer_receipt OWNER TO postgres;

--
-- Name: customer_receipt_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE customer_receipt_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_receipt_id_seq OWNER TO postgres;

--
-- Name: customer_receipt_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE customer_receipt_id_seq OWNED BY customer_receipt.id;


--
-- Name: customer_settings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE customer_settings (
    id integer NOT NULL,
    debt_threshold_warning double precision,
    sms_notification boolean,
    email_notification boolean,
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone,
    customer_id integer NOT NULL
);


ALTER TABLE customer_settings OWNER TO postgres;

--
-- Name: customer_settings_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE customer_settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_settings_id_seq OWNER TO postgres;

--
-- Name: customer_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE customer_settings_id_seq OWNED BY customer_settings.id;


--
-- Name: deduction; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE deduction (
    id integer NOT NULL,
    billing_id integer NOT NULL,
    billing_record_id integer NOT NULL,
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE deduction OWNER TO postgres;

--
-- Name: deduction_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE deduction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE deduction_id_seq OWNER TO postgres;

--
-- Name: deduction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE deduction_id_seq OWNED BY deduction.id;


--
-- Name: device; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE device (
    id integer NOT NULL,
    code character varying NOT NULL,
    reference_code character varying,
    customer_id integer,
    point_id integer NOT NULL,
    is_coordinator boolean,
    is_node boolean,
    is_active boolean,
    product_id integer NOT NULL,
    network_id integer,
    station_id integer,
    installation_id integer,
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE device OWNER TO postgres;

--
-- Name: device_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE device_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE device_id_seq OWNER TO postgres;

--
-- Name: device_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE device_id_seq OWNED BY device.id;


--
-- Name: device_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE device_type (
    id integer NOT NULL,
    name character varying NOT NULL,
    slug character varying(200),
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE device_type OWNER TO postgres;

--
-- Name: device_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE device_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE device_type_id_seq OWNER TO postgres;

--
-- Name: device_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE device_type_id_seq OWNED BY device_type.id;


--
-- Name: geojson_address; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE geojson_address (
    id integer NOT NULL,
    polygon geometry(Polygon),
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE geojson_address OWNER TO postgres;

--
-- Name: geojson_address_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE geojson_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE geojson_address_id_seq OWNER TO postgres;

--
-- Name: geojson_address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE geojson_address_id_seq OWNED BY geojson_address.id;


--
-- Name: geojson_point; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE geojson_point (
    id integer NOT NULL,
    point geography(Point,4326),
    longitude character varying NOT NULL,
    latitude character varying NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE geojson_point OWNER TO postgres;

--
-- Name: geojson_point_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE geojson_point_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE geojson_point_id_seq OWNER TO postgres;

--
-- Name: geojson_point_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE geojson_point_id_seq OWNED BY geojson_point.id;


--
-- Name: installation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE installation (
    id integer NOT NULL,
    title character varying NOT NULL,
    code character varying NOT NULL,
    model_number character varying NOT NULL,
    capacity double precision,
    measurement_index character varying,
    location_id integer NOT NULL,
    station_id integer NOT NULL,
    service_id integer NOT NULL,
    installation_type_id integer NOT NULL,
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE installation OWNER TO postgres;

--
-- Name: installation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE installation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE installation_id_seq OWNER TO postgres;

--
-- Name: installation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE installation_id_seq OWNED BY installation.id;


--
-- Name: installation_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE installation_type (
    id integer NOT NULL,
    title character varying NOT NULL,
    code character varying NOT NULL,
    html text NOT NULL,
    script text NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE installation_type OWNER TO postgres;

--
-- Name: installation_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE installation_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE installation_type_id_seq OWNER TO postgres;

--
-- Name: installation_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE installation_type_id_seq OWNED BY installation_type.id;


--
-- Name: invoice; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE invoice (
    id integer NOT NULL,
    code text NOT NULL,
    month text NOT NULL,
    year text NOT NULL,
    is_paid boolean,
    service_charge double precision,
    billing_amount double precision,
    total_charge double precision,
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE invoice OWNER TO postgres;

--
-- Name: invoice_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE invoice_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE invoice_id_seq OWNER TO postgres;

--
-- Name: invoice_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE invoice_id_seq OWNED BY invoice.id;


--
-- Name: location; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE location (
    id integer NOT NULL,
    line1 character varying NOT NULL,
    line2 character varying,
    city_id integer,
    state_id integer NOT NULL,
    country_id integer NOT NULL,
    longitude character varying,
    latitude character varying,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE location OWNER TO postgres;

--
-- Name: location_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE location_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE location_id_seq OWNER TO postgres;

--
-- Name: location_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE location_id_seq OWNED BY location.id;


--
-- Name: metric; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE metric (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    slug character varying(200),
    dimension character varying(20) NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE metric OWNER TO postgres;

--
-- Name: metric_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE metric_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE metric_id_seq OWNER TO postgres;

--
-- Name: metric_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE metric_id_seq OWNED BY metric.id;


--
-- Name: navigation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE navigation (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    code character varying(50) NOT NULL,
    url text NOT NULL,
    icon text NOT NULL,
    is_img boolean,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE navigation OWNER TO postgres;

--
-- Name: navigation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE navigation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE navigation_id_seq OWNER TO postgres;

--
-- Name: navigation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE navigation_id_seq OWNED BY navigation.id;


--
-- Name: network; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE network (
    id integer NOT NULL,
    code character varying NOT NULL,
    geojson_address_id integer,
    station_id integer,
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE network OWNER TO postgres;

--
-- Name: network_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE network_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE network_id_seq OWNER TO postgres;

--
-- Name: network_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE network_id_seq OWNED BY network.id;


--
-- Name: network_services; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE network_services (
    network_id integer,
    service_id integer
);


ALTER TABLE network_services OWNER TO postgres;

--
-- Name: product; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    description text NOT NULL,
    code text NOT NULL,
    on_default boolean,
    is_network boolean,
    is_alert boolean,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE product OWNER TO postgres;

--
-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_id_seq OWNER TO postgres;

--
-- Name: product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE product_id_seq OWNED BY product.id;


--
-- Name: product_image; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product_image (
    id integer NOT NULL,
    product_id integer NOT NULL,
    image_url text NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE product_image OWNER TO postgres;

--
-- Name: product_image_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE product_image_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_image_id_seq OWNER TO postgres;

--
-- Name: product_image_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE product_image_id_seq OWNED BY product_image.id;


--
-- Name: product_metrics; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product_metrics (
    metric_id integer,
    product_id integer
);


ALTER TABLE product_metrics OWNER TO postgres;

--
-- Name: product_services; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product_services (
    product_id integer,
    service_id integer
);


ALTER TABLE product_services OWNER TO postgres;

--
-- Name: receipt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE receipt (
    id integer NOT NULL,
    code text NOT NULL,
    invoice_id integer NOT NULL,
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE receipt OWNER TO postgres;

--
-- Name: receipt_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE receipt_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE receipt_id_seq OWNER TO postgres;

--
-- Name: receipt_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE receipt_id_seq OWNED BY receipt.id;


--
-- Name: recurrent_card; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE recurrent_card (
    id integer NOT NULL,
    token character varying(200) NOT NULL,
    mask character varying(300),
    brand character varying(300),
    exp_month integer,
    exp_year integer,
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE recurrent_card OWNER TO postgres;

--
-- Name: recurrent_card_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE recurrent_card_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recurrent_card_id_seq OWNER TO postgres;

--
-- Name: recurrent_card_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE recurrent_card_id_seq OWNED BY recurrent_card.id;


--
-- Name: report; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE report (
    id integer NOT NULL,
    handle character varying(200) NOT NULL,
    name character varying(200) NOT NULL,
    description text,
    start timestamp without time zone,
    "end" timestamp without time zone,
    client_id integer,
    file_path text,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE report OWNER TO postgres;

--
-- Name: report_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE report_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE report_id_seq OWNER TO postgres;

--
-- Name: report_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE report_id_seq OWNED BY report.id;


--
-- Name: restricted_domain; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE restricted_domain (
    id integer NOT NULL,
    domain character varying(200) NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE restricted_domain OWNER TO postgres;

--
-- Name: restricted_domain_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE restricted_domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE restricted_domain_id_seq OWNER TO postgres;

--
-- Name: restricted_domain_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE restricted_domain_id_seq OWNED BY restricted_domain.id;


--
-- Name: role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE role (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    slug character varying(200),
    description character varying(200),
    client_id integer,
    is_admin boolean,
    assignable boolean,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE role OWNER TO postgres;

--
-- Name: role_access_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE role_access_groups (
    role_id integer,
    access_group_id integer
);


ALTER TABLE role_access_groups OWNER TO postgres;

--
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE role_id_seq OWNER TO postgres;

--
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE role_id_seq OWNED BY role.id;


--
-- Name: service; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE service (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    code character varying(255) NOT NULL,
    icon_class character varying,
    is_active boolean,
    parent_id integer,
    description text,
    service_charge double precision,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE service OWNER TO postgres;

--
-- Name: service_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE service_id_seq OWNER TO postgres;

--
-- Name: service_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE service_id_seq OWNED BY service.id;


--
-- Name: settings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE settings (
    id integer NOT NULL,
    allow_debt_warning boolean,
    debt_threshold_warning integer,
    billing_period integer,
    is_postpaid boolean,
    logo text,
    alerts_dep_resolved boolean,
    stations_dep_resolved boolean,
    accounts_dep_resolved boolean,
    bank_account_dep_resolved boolean,
    billing_dep_resolved boolean,
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE settings OWNER TO postgres;

--
-- Name: settings_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE settings_id_seq OWNER TO postgres;

--
-- Name: settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE settings_id_seq OWNED BY settings.id;


--
-- Name: state; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE state (
    id integer NOT NULL,
    name character varying(200),
    slug character varying(200),
    code character varying(200),
    country_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE state OWNER TO postgres;

--
-- Name: state_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE state_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE state_id_seq OWNER TO postgres;

--
-- Name: state_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE state_id_seq OWNED BY state.id;


--
-- Name: station; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE station (
    id integer NOT NULL,
    name character varying NOT NULL,
    is_active boolean,
    location_id integer NOT NULL,
    manager_id integer NOT NULL,
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE station OWNER TO postgres;

--
-- Name: station_assignees; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE station_assignees (
    user_id integer,
    station_id integer
);


ALTER TABLE station_assignees OWNER TO postgres;

--
-- Name: station_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE station_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE station_id_seq OWNER TO postgres;

--
-- Name: station_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE station_id_seq OWNED BY station.id;


--
-- Name: station_services; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE station_services (
    service_id integer,
    station_id integer
);


ALTER TABLE station_services OWNER TO postgres;

--
-- Name: sub_navigation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE sub_navigation (
    id integer NOT NULL,
    navigation_id integer NOT NULL,
    name character varying(255) NOT NULL,
    code character varying(50) NOT NULL,
    url text NOT NULL,
    is_img boolean,
    icon text NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE sub_navigation OWNER TO postgres;

--
-- Name: sub_navigation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sub_navigation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sub_navigation_id_seq OWNER TO postgres;

--
-- Name: sub_navigation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE sub_navigation_id_seq OWNED BY sub_navigation.id;


--
-- Name: subscription; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE subscription (
    id integer NOT NULL,
    service_id integer NOT NULL,
    subscription_fee double precision,
    is_active boolean,
    devices_dep_resolved boolean,
    alerts_dep_resolved boolean,
    inst_dep_resolved boolean,
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE subscription OWNER TO postgres;

--
-- Name: subscription_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE subscription_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE subscription_id_seq OWNER TO postgres;

--
-- Name: subscription_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE subscription_id_seq OWNED BY subscription.id;


--
-- Name: table_desc; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE table_desc (
    id integer NOT NULL,
    cls character varying NOT NULL,
    description text NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE table_desc OWNER TO postgres;

--
-- Name: table_desc_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE table_desc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE table_desc_id_seq OWNER TO postgres;

--
-- Name: table_desc_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE table_desc_id_seq OWNED BY table_desc.id;


--
-- Name: timezone; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE timezone (
    id integer NOT NULL,
    name character varying(200),
    code character varying(200),
    "offset" character varying(200),
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE timezone OWNER TO postgres;

--
-- Name: timezone_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE timezone_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE timezone_id_seq OWNER TO postgres;

--
-- Name: timezone_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE timezone_id_seq OWNED BY timezone.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "user" (
    id integer NOT NULL,
    address_id integer NOT NULL,
    email character varying(255),
    password text,
    is_active boolean,
    is_verified boolean,
    is_demo_account boolean,
    is_blocked boolean,
    client_id integer,
    login_count integer,
    last_login_at timestamp without time zone,
    current_login_at timestamp without time zone,
    last_login_ip character varying(200),
    current_login_ip character varying(200),
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE "user" OWNER TO postgres;

--
-- Name: user_access_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE user_access_groups (
    user_id integer,
    access_group_id integer
);


ALTER TABLE user_access_groups OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_id_seq OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_id_seq OWNED BY "user".id;


--
-- Name: user_roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE user_roles (
    user_id integer,
    role_id integer
);


ALTER TABLE user_roles OWNER TO postgres;

--
-- Name: wallet; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE wallet (
    id integer NOT NULL,
    debt_balance double precision,
    credit_balance double precision,
    client_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE wallet OWNER TO postgres;

--
-- Name: wallet_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE wallet_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE wallet_id_seq OWNER TO postgres;

--
-- Name: wallet_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE wallet_id_seq OWNED BY wallet.id;


--
-- Name: access_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY access_group ALTER COLUMN id SET DEFAULT nextval('access_group_id_seq'::regclass);


--
-- Name: address id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY address ALTER COLUMN id SET DEFAULT nextval('address_id_seq'::regclass);


--
-- Name: admin id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin ALTER COLUMN id SET DEFAULT nextval('admin_id_seq'::regclass);


--
-- Name: alert id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alert ALTER COLUMN id SET DEFAULT nextval('alert_id_seq'::regclass);


--
-- Name: alert_channel id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alert_channel ALTER COLUMN id SET DEFAULT nextval('alert_channel_id_seq'::regclass);


--
-- Name: alert_recipient id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alert_recipient ALTER COLUMN id SET DEFAULT nextval('alert_recipient_id_seq'::regclass);


--
-- Name: alert_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alert_type ALTER COLUMN id SET DEFAULT nextval('alert_type_id_seq'::regclass);


--
-- Name: bank id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bank ALTER COLUMN id SET DEFAULT nextval('bank_id_seq'::regclass);


--
-- Name: bank_account id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bank_account ALTER COLUMN id SET DEFAULT nextval('bank_account_id_seq'::regclass);


--
-- Name: billing id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY billing ALTER COLUMN id SET DEFAULT nextval('billing_id_seq'::regclass);


--
-- Name: billing_record id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY billing_record ALTER COLUMN id SET DEFAULT nextval('billing_record_id_seq'::regclass);


--
-- Name: card id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY card ALTER COLUMN id SET DEFAULT nextval('card_id_seq'::regclass);


--
-- Name: city id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY city ALTER COLUMN id SET DEFAULT nextval('city_id_seq'::regclass);


--
-- Name: client id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY client ALTER COLUMN id SET DEFAULT nextval('client_id_seq'::regclass);


--
-- Name: country id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY country ALTER COLUMN id SET DEFAULT nextval('country_id_seq'::regclass);


--
-- Name: currency id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY currency ALTER COLUMN id SET DEFAULT nextval('currency_id_seq'::regclass);


--
-- Name: customer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer ALTER COLUMN id SET DEFAULT nextval('customer_id_seq'::regclass);


--
-- Name: customer_billing id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_billing ALTER COLUMN id SET DEFAULT nextval('customer_billing_id_seq'::regclass);


--
-- Name: customer_billing_record id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_billing_record ALTER COLUMN id SET DEFAULT nextval('customer_billing_record_id_seq'::regclass);


--
-- Name: customer_deduction id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_deduction ALTER COLUMN id SET DEFAULT nextval('customer_deduction_id_seq'::regclass);


--
-- Name: customer_invoice id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_invoice ALTER COLUMN id SET DEFAULT nextval('customer_invoice_id_seq'::regclass);


--
-- Name: customer_receipt id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_receipt ALTER COLUMN id SET DEFAULT nextval('customer_receipt_id_seq'::regclass);


--
-- Name: customer_settings id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_settings ALTER COLUMN id SET DEFAULT nextval('customer_settings_id_seq'::regclass);


--
-- Name: deduction id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY deduction ALTER COLUMN id SET DEFAULT nextval('deduction_id_seq'::regclass);


--
-- Name: device id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY device ALTER COLUMN id SET DEFAULT nextval('device_id_seq'::regclass);


--
-- Name: device_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY device_type ALTER COLUMN id SET DEFAULT nextval('device_type_id_seq'::regclass);


--
-- Name: geojson_address id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY geojson_address ALTER COLUMN id SET DEFAULT nextval('geojson_address_id_seq'::regclass);


--
-- Name: geojson_point id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY geojson_point ALTER COLUMN id SET DEFAULT nextval('geojson_point_id_seq'::regclass);


--
-- Name: installation id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY installation ALTER COLUMN id SET DEFAULT nextval('installation_id_seq'::regclass);


--
-- Name: installation_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY installation_type ALTER COLUMN id SET DEFAULT nextval('installation_type_id_seq'::regclass);


--
-- Name: invoice id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY invoice ALTER COLUMN id SET DEFAULT nextval('invoice_id_seq'::regclass);


--
-- Name: location id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY location ALTER COLUMN id SET DEFAULT nextval('location_id_seq'::regclass);


--
-- Name: metric id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY metric ALTER COLUMN id SET DEFAULT nextval('metric_id_seq'::regclass);


--
-- Name: navigation id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY navigation ALTER COLUMN id SET DEFAULT nextval('navigation_id_seq'::regclass);


--
-- Name: network id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY network ALTER COLUMN id SET DEFAULT nextval('network_id_seq'::regclass);


--
-- Name: product id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product ALTER COLUMN id SET DEFAULT nextval('product_id_seq'::regclass);


--
-- Name: product_image id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_image ALTER COLUMN id SET DEFAULT nextval('product_image_id_seq'::regclass);


--
-- Name: receipt id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY receipt ALTER COLUMN id SET DEFAULT nextval('receipt_id_seq'::regclass);


--
-- Name: recurrent_card id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY recurrent_card ALTER COLUMN id SET DEFAULT nextval('recurrent_card_id_seq'::regclass);


--
-- Name: report id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY report ALTER COLUMN id SET DEFAULT nextval('report_id_seq'::regclass);


--
-- Name: restricted_domain id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY restricted_domain ALTER COLUMN id SET DEFAULT nextval('restricted_domain_id_seq'::regclass);


--
-- Name: role id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY role ALTER COLUMN id SET DEFAULT nextval('role_id_seq'::regclass);


--
-- Name: service id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY service ALTER COLUMN id SET DEFAULT nextval('service_id_seq'::regclass);


--
-- Name: settings id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY settings ALTER COLUMN id SET DEFAULT nextval('settings_id_seq'::regclass);


--
-- Name: state id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY state ALTER COLUMN id SET DEFAULT nextval('state_id_seq'::regclass);


--
-- Name: station id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY station ALTER COLUMN id SET DEFAULT nextval('station_id_seq'::regclass);


--
-- Name: sub_navigation id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sub_navigation ALTER COLUMN id SET DEFAULT nextval('sub_navigation_id_seq'::regclass);


--
-- Name: subscription id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY subscription ALTER COLUMN id SET DEFAULT nextval('subscription_id_seq'::regclass);


--
-- Name: table_desc id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY table_desc ALTER COLUMN id SET DEFAULT nextval('table_desc_id_seq'::regclass);


--
-- Name: timezone id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY timezone ALTER COLUMN id SET DEFAULT nextval('timezone_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "user" ALTER COLUMN id SET DEFAULT nextval('user_id_seq'::regclass);


--
-- Name: wallet id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY wallet ALTER COLUMN id SET DEFAULT nextval('wallet_id_seq'::regclass);


--
-- Data for Name: access_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY access_group (id, name, slug, description, permanent, is_client, client_id, last_updated, date_created) FROM stdin;
1	Super Admin	super-admin	\N	t	f	\N	2017-11-12 18:50:00.29134	2017-11-12 18:50:00.291356
2	Demo	demo	\N	f	f	\N	2017-11-12 18:50:00.296136	2017-11-12 18:50:00.296163
3	Staff	staff	\N	f	f	\N	2017-11-12 18:50:00.297655	2017-11-12 18:50:00.297672
4	Atele Support	atele-support	\N	f	f	\N	2017-11-12 18:50:00.300796	2017-11-12 18:50:00.300809
5	Client Admin	client-admin	\N	f	f	\N	2017-11-12 18:50:00.302683	2017-11-12 18:50:00.302701
6	Facility Manager	facility-manager	\N	f	t	\N	2017-11-12 18:50:00.304612	2017-11-12 18:50:00.304621
7	Finance Manager	finance-manager	\N	f	t	\N	2017-11-12 18:50:00.30582	2017-11-12 18:50:00.305829
8	Operations Manager	operations-manager	\N	f	t	\N	2017-11-12 18:50:00.306952	2017-11-12 18:50:00.306958
\.


--
-- Data for Name: address; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY address (id, first_name, last_name, line1, line2, phone, email, city_id, state_id, country_id, last_updated, date_created) FROM stdin;
1	Nikola	Tesla	Lagos	\N	\N	demo@atele.org	\N	33	149	2017-11-12 18:50:11.106432	2017-11-12 18:50:11.10645
2	Fred	Amata	14, Ebinpejo Lane	National Theatre	+2345898980099	fred@amata.com	647	33	149	2017-11-21 01:29:49.298187	2017-11-21 01:29:49.298207
\.


--
-- Data for Name: admin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY admin (id, username, password, last_updated, date_created) FROM stdin;
1	admin	$2b$12$wB/.xvTtXCUhvOKKf/5SZ.igUgYGCt5L9hd2yEGTBrO9F.PsR8jwW	2017-11-12 18:50:11.968846	2017-11-12 18:50:11.968857
\.


--
-- Data for Name: alembic_version; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY alembic_version (version_num) FROM stdin;
805e4323e6b1
\.


--
-- Data for Name: alert; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY alert (id, code, alert_type_id, device_id, installation_id, service_id, station_id, client_id, last_updated, date_created, is_warning, threshold, is_critical) FROM stdin;
5	966fac3d7c11406787cef4745278d27c	2	2	1	2	1	1	2017-11-20 17:51:25.932749	2017-11-20 17:51:25.932771	t	200	f
\.


--
-- Data for Name: alert_channel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY alert_channel (id, name, slug, last_updated, date_created) FROM stdin;
\.


--
-- Data for Name: alert_recipient; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY alert_recipient (id, alert_id, recipient, is_device, is_email, is_push, client_id, last_updated, date_created) FROM stdin;
5	5	demo@atele.org	f	t	t	1	2017-11-20 17:51:25.979033	2017-11-20 17:51:25.979054
\.


--
-- Data for Name: alert_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY alert_type (id, parameter, metric, symbol, description, last_updated, date_created) FROM stdin;
1	over-voltage	voltage	volts	Reading is over threshold voltage	2017-11-12 18:50:12.023277	2017-11-12 18:50:12.023303
2	under-voltage	voltage	volts	Reading is under threshold voltage	2017-11-12 18:50:12.026651	2017-11-12 18:50:12.026667
\.


--
-- Data for Name: associated_channels; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY associated_channels (alert_channel_id, alert_id) FROM stdin;
\.


--
-- Data for Name: associated_services; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY associated_services (service_id, installation_type_id) FROM stdin;
6	1
4	2
4	3
4	4
2	5
7	5
2	6
7	6
\.


--
-- Data for Name: bank; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY bank (id, name, code, country_code, country_id, branch, last_updated, date_created) FROM stdin;
1	FIRST BANK PLC	011	NG	149	011151003	2017-11-12 18:50:10.859318	2017-11-12 18:50:10.859343
2	MAINSTREET BANK	014	NG	149	014150331	2017-11-12 18:50:10.869053	2017-11-12 18:50:10.869068
3	NIGERIA INTERNATIONAL BANK LIMITED	023	NG	149	023150005	2017-11-12 18:50:10.874714	2017-11-12 18:50:10.874732
4	UNITED BANK FOR AFRICA PLC	033	NG	149	033153513	2017-11-12 18:50:10.884573	2017-11-12 18:50:10.884648
5	WEMA BANK PLC	035	NG	149	035150103	2017-11-12 18:50:10.892662	2017-11-12 18:50:10.892674
6	ACCESS BANK PLC	044	NG	149	044150149	2017-11-12 18:50:10.897278	2017-11-12 18:50:10.897289
7	ECOBANK NIGERIA PLC	050	NG	149	050150010	2017-11-12 18:50:10.903344	2017-11-12 18:50:10.903358
8	OCEANIC BANK INTERNATIONAL PLC	056	NG	149	056080016	2017-11-12 18:50:10.909511	2017-11-12 18:50:10.909525
9	ZENITH BANK PLC	057	NG	149	057150013	2017-11-12 18:50:10.915165	2017-11-12 18:50:10.915183
10	GTBANK PLC	058	NG	149	058152036	2017-11-12 18:50:10.923213	2017-11-12 18:50:10.923228
11	DIAMOND BANK PLC	063	NG	149	063150162	2017-11-12 18:50:10.928543	2017-11-12 18:50:10.928555
12	STANDARD CHARTERED BANK NIGERIA LIMITED	068	NG	149	068150015	2017-11-12 18:50:10.934567	2017-11-12 18:50:10.934584
13	FIDELITY BANK PLC	070	NG	149	070150003	2017-11-12 18:50:10.940992	2017-11-12 18:50:10.941008
14	SKYE BANK PLC	076	NG	149	076151006	2017-11-12 18:50:10.946525	2017-11-12 18:50:10.946539
15	KEYSTONE BANK	082	NG	149	082150017	2017-11-12 18:50:10.953167	2017-11-12 18:50:10.953185
16	ENTERPRISE BANK	084	NG	149	084150015	2017-11-12 18:50:10.959353	2017-11-12 18:50:10.959367
17	FIRST CITY MONUMENT BANK PLC	214	NG	149	214150018	2017-11-12 18:50:10.964474	2017-11-12 18:50:10.964488
18	UNITY BANK PLC	215	NG	149	215154097	2017-11-12 18:50:10.970201	2017-11-12 18:50:10.970215
19	STANBIC IBTC BANK PLC	221	NG	149	221159522	2017-11-12 18:50:10.977955	2017-11-12 18:50:10.977982
20	STERLING BANK PLC	232	NG	149	232150016	2017-11-12 18:50:10.987005	2017-11-12 18:50:10.98702
\.


--
-- Data for Name: bank_account; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY bank_account (id, first_name, last_name, account_number, bank_id, client_id, last_updated, date_created) FROM stdin;
\.


--
-- Data for Name: billing; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY billing (id, amount, invoice_id, client_id, last_updated, date_created) FROM stdin;
\.


--
-- Data for Name: billing_record; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY billing_record (id, billing_id, subscription_id, installation_id, device_id, network_id, data_size, amount, client_id, last_updated, date_created) FROM stdin;
\.


--
-- Data for Name: card; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY card (id, customer_id, wallet_id, address_id, card_number, token, mask, brand, exp_month, exp_year, client_id, last_updated, date_created) FROM stdin;
\.


--
-- Data for Name: city; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY city (id, name, slug, code, state_id, country_id, last_updated, date_created) FROM stdin;
1	Alkaleri	alkaleri	\N	1	149	2017-11-12 18:50:01.152741	2017-11-12 18:50:01.152759
2	Bauchi	bauchi	\N	1	149	2017-11-12 18:50:01.170043	2017-11-12 18:50:01.170063
3	Bogoro	bogoro	\N	1	149	2017-11-12 18:50:01.18489	2017-11-12 18:50:01.184909
4	Damban	damban	\N	1	149	2017-11-12 18:50:01.19895	2017-11-12 18:50:01.198967
5	Darazo	darazo	\N	1	149	2017-11-12 18:50:01.213325	2017-11-12 18:50:01.213342
6	Dass	dass	\N	1	149	2017-11-12 18:50:01.229962	2017-11-12 18:50:01.229979
7	Gamawa	gamawa	\N	1	149	2017-11-12 18:50:01.243419	2017-11-12 18:50:01.243454
8	Ganjuwa	ganjuwa	\N	1	149	2017-11-12 18:50:01.257377	2017-11-12 18:50:01.257397
9	Giade	giade	\N	1	149	2017-11-12 18:50:01.2707	2017-11-12 18:50:01.270715
10	Itas/Gadau	itas-gadau	\N	1	149	2017-11-12 18:50:01.285854	2017-11-12 18:50:01.285877
11	Jama''are	jama-are	\N	1	149	2017-11-12 18:50:01.298147	2017-11-12 18:50:01.298159
12	Katagum	katagum	\N	1	149	2017-11-12 18:50:01.305375	2017-11-12 18:50:01.305383
13	Kirfi	kirfi	\N	1	149	2017-11-12 18:50:01.312568	2017-11-12 18:50:01.312575
14	Misau	misau	\N	1	149	2017-11-12 18:50:01.324777	2017-11-12 18:50:01.324804
15	Ningi	ningi	\N	1	149	2017-11-12 18:50:01.340136	2017-11-12 18:50:01.340154
16	Shira	shira	\N	1	149	2017-11-12 18:50:01.354133	2017-11-12 18:50:01.354153
17	Tafawa Balewa	tafawa-balewa	\N	1	149	2017-11-12 18:50:01.366237	2017-11-12 18:50:01.366251
18	Toro	toro	\N	1	149	2017-11-12 18:50:01.381207	2017-11-12 18:50:01.381229
19	Warji	warji	\N	1	149	2017-11-12 18:50:01.393067	2017-11-12 18:50:01.393079
20	Zaki	zaki	\N	1	149	2017-11-12 18:50:01.404392	2017-11-12 18:50:01.404407
21	Ado Ekiti	ado-ekiti	\N	2	149	2017-11-12 18:50:01.417623	2017-11-12 18:50:01.417644
22	Efon	efon	\N	2	149	2017-11-12 18:50:01.431335	2017-11-12 18:50:01.431359
23	Ekiti East	ekiti-east	\N	2	149	2017-11-12 18:50:01.446741	2017-11-12 18:50:01.446764
24	Ekiti South-West	ekiti-south-west	\N	2	149	2017-11-12 18:50:01.461915	2017-11-12 18:50:01.46193
25	Ekiti West	ekiti-west	\N	2	149	2017-11-12 18:50:01.479486	2017-11-12 18:50:01.479511
26	Emure	emure	\N	2	149	2017-11-12 18:50:01.487297	2017-11-12 18:50:01.487305
27	Gbonyin	gbonyin	\N	2	149	2017-11-12 18:50:01.495113	2017-11-12 18:50:01.495121
28	Ido Osi	ido-osi	\N	2	149	2017-11-12 18:50:01.505266	2017-11-12 18:50:01.505284
29	Ijero	ijero	\N	2	149	2017-11-12 18:50:01.514844	2017-11-12 18:50:01.514858
30	Ikere	ikere	\N	2	149	2017-11-12 18:50:01.526722	2017-11-12 18:50:01.526749
31	Ikole	ikole	\N	2	149	2017-11-12 18:50:01.538479	2017-11-12 18:50:01.53849
32	Ilejemeje	ilejemeje	\N	2	149	2017-11-12 18:50:01.54934	2017-11-12 18:50:01.549353
33	Irepodun/Ifelodun	irepodun-ifelodun	\N	2	149	2017-11-12 18:50:01.560857	2017-11-12 18:50:01.560886
34	Ise/Orun	ise-orun	\N	2	149	2017-11-12 18:50:01.574033	2017-11-12 18:50:01.574048
35	Moba	moba	\N	2	149	2017-11-12 18:50:01.585219	2017-11-12 18:50:01.58523
36	Oye	oye	\N	2	149	2017-11-12 18:50:01.598189	2017-11-12 18:50:01.598204
37	Anka	anka	\N	3	149	2017-11-12 18:50:01.60798	2017-11-12 18:50:01.608003
38	Bakura	bakura	\N	3	149	2017-11-12 18:50:01.619699	2017-11-12 18:50:01.619715
39	Birnin Magaji/Kiyaw	birnin-magaji-kiyaw	\N	3	149	2017-11-12 18:50:01.632349	2017-11-12 18:50:01.632369
40	Bukkuyum	bukkuyum	\N	3	149	2017-11-12 18:50:01.644499	2017-11-12 18:50:01.644508
41	Bungudu	bungudu	\N	3	149	2017-11-12 18:50:01.653969	2017-11-12 18:50:01.653985
42	Gummi	gummi	\N	3	149	2017-11-12 18:50:01.672302	2017-11-12 18:50:01.672312
43	Gusau	gusau	\N	3	149	2017-11-12 18:50:01.682791	2017-11-12 18:50:01.682815
44	Kaura Namoda	kaura-namoda	\N	3	149	2017-11-12 18:50:01.69329	2017-11-12 18:50:01.693304
45	Maradun	maradun	\N	3	149	2017-11-12 18:50:01.705467	2017-11-12 18:50:01.705481
46	Maru	maru	\N	3	149	2017-11-12 18:50:01.715295	2017-11-12 18:50:01.715305
47	Shinkafi	shinkafi	\N	3	149	2017-11-12 18:50:01.727049	2017-11-12 18:50:01.727068
48	Talata Mafara	talata-mafara	\N	3	149	2017-11-12 18:50:01.739199	2017-11-12 18:50:01.739212
49	Chafe	chafe	\N	3	149	2017-11-12 18:50:01.749774	2017-11-12 18:50:01.749785
50	Zurmi	zurmi	\N	3	149	2017-11-12 18:50:01.76145	2017-11-12 18:50:01.761471
51	Abadam	abadam	\N	4	149	2017-11-12 18:50:01.773402	2017-11-12 18:50:01.773436
52	Askira/Uba	askira-uba	\N	4	149	2017-11-12 18:50:01.784968	2017-11-12 18:50:01.784982
53	Bama	bama	\N	4	149	2017-11-12 18:50:01.794906	2017-11-12 18:50:01.794914
54	Bayo	bayo	\N	4	149	2017-11-12 18:50:01.801715	2017-11-12 18:50:01.801723
55	Biu	biu	\N	4	149	2017-11-12 18:50:01.808038	2017-11-12 18:50:01.80805
56	Chibok	chibok	\N	4	149	2017-11-12 18:50:01.817133	2017-11-12 18:50:01.817144
57	Damboa	damboa	\N	4	149	2017-11-12 18:50:01.827729	2017-11-12 18:50:01.827737
58	Dikwa	dikwa	\N	4	149	2017-11-12 18:50:01.836599	2017-11-12 18:50:01.836608
59	Gubio	gubio	\N	4	149	2017-11-12 18:50:01.847272	2017-11-12 18:50:01.84729
60	Guzamala	guzamala	\N	4	149	2017-11-12 18:50:01.859636	2017-11-12 18:50:01.859651
61	Gwoza	gwoza	\N	4	149	2017-11-12 18:50:01.870587	2017-11-12 18:50:01.870601
62	Hawul	hawul	\N	4	149	2017-11-12 18:50:01.884108	2017-11-12 18:50:01.88413
63	Jere	jere	\N	4	149	2017-11-12 18:50:01.897067	2017-11-12 18:50:01.897082
64	Kaga	kaga	\N	4	149	2017-11-12 18:50:01.909586	2017-11-12 18:50:01.909602
65	Kala/Balge	kala-balge	\N	4	149	2017-11-12 18:50:01.920317	2017-11-12 18:50:01.920337
66	Konduga	konduga	\N	4	149	2017-11-12 18:50:01.931212	2017-11-12 18:50:01.931224
67	Kukawa	kukawa	\N	4	149	2017-11-12 18:50:01.939572	2017-11-12 18:50:01.939587
68	Kwaya Kusar	kwaya-kusar	\N	4	149	2017-11-12 18:50:01.951176	2017-11-12 18:50:01.951198
69	Mafa	mafa	\N	4	149	2017-11-12 18:50:01.963331	2017-11-12 18:50:01.963342
70	Magumeri	magumeri	\N	4	149	2017-11-12 18:50:01.970633	2017-11-12 18:50:01.970644
71	Maiduguri	maiduguri	\N	4	149	2017-11-12 18:50:01.981968	2017-11-12 18:50:01.982011
72	Marte	marte	\N	4	149	2017-11-12 18:50:01.994486	2017-11-12 18:50:01.994507
73	Mobbar	mobbar	\N	4	149	2017-11-12 18:50:02.004645	2017-11-12 18:50:02.004662
74	Monguno	monguno	\N	4	149	2017-11-12 18:50:02.014117	2017-11-12 18:50:02.01413
75	Ngala	ngala	\N	4	149	2017-11-12 18:50:02.023747	2017-11-12 18:50:02.023767
76	Nganzai	nganzai	\N	4	149	2017-11-12 18:50:02.032687	2017-11-12 18:50:02.032699
77	Shani	shani	\N	4	149	2017-11-12 18:50:02.042719	2017-11-12 18:50:02.042733
78	Brass	brass	\N	5	149	2017-11-12 18:50:02.055116	2017-11-12 18:50:02.055137
79	Ekeremor	ekeremor	\N	5	149	2017-11-12 18:50:02.064877	2017-11-12 18:50:02.064899
80	Kolokuma/Opokuma	kolokuma-opokuma	\N	5	149	2017-11-12 18:50:02.073837	2017-11-12 18:50:02.07386
81	Nembe	nembe	\N	5	149	2017-11-12 18:50:02.081605	2017-11-12 18:50:02.081613
82	Ogbia	ogbia	\N	5	149	2017-11-12 18:50:02.088028	2017-11-12 18:50:02.088048
83	Sagbama	sagbama	\N	5	149	2017-11-12 18:50:02.097316	2017-11-12 18:50:02.097341
84	Southern Ijaw	southern-ijaw	\N	5	149	2017-11-12 18:50:02.105956	2017-11-12 18:50:02.105974
85	Yenagoa	yenagoa	\N	5	149	2017-11-12 18:50:02.11546	2017-11-12 18:50:02.115471
86	Afijio	afijio	\N	6	149	2017-11-12 18:50:02.124916	2017-11-12 18:50:02.124941
87	Akinyele	akinyele	\N	6	149	2017-11-12 18:50:02.134466	2017-11-12 18:50:02.134478
88	Atiba	atiba	\N	6	149	2017-11-12 18:50:02.144974	2017-11-12 18:50:02.144989
89	Atisbo	atisbo	\N	6	149	2017-11-12 18:50:02.154514	2017-11-12 18:50:02.154528
90	Egbeda	egbeda	\N	6	149	2017-11-12 18:50:02.165539	2017-11-12 18:50:02.16555
91	Ibadan North	ibadan-north	\N	6	149	2017-11-12 18:50:02.171228	2017-11-12 18:50:02.171235
92	Ibadan North-East	ibadan-north-east	\N	6	149	2017-11-12 18:50:02.18204	2017-11-12 18:50:02.182059
93	Ibadan North-West	ibadan-north-west	\N	6	149	2017-11-12 18:50:02.19209	2017-11-12 18:50:02.192107
94	Ibadan South-East	ibadan-south-east	\N	6	149	2017-11-12 18:50:02.201951	2017-11-12 18:50:02.201966
95	Ibadan South-West	ibadan-south-west	\N	6	149	2017-11-12 18:50:02.212138	2017-11-12 18:50:02.212151
96	Ibarapa Central	ibarapa-central	\N	6	149	2017-11-12 18:50:02.220766	2017-11-12 18:50:02.220777
97	Ibarapa East	ibarapa-east	\N	6	149	2017-11-12 18:50:02.231172	2017-11-12 18:50:02.231188
98	Ibarapa North	ibarapa-north	\N	6	149	2017-11-12 18:50:02.243322	2017-11-12 18:50:02.243344
99	Ido	ido	\N	6	149	2017-11-12 18:50:02.251382	2017-11-12 18:50:02.25139
100	Irepo	irepo	\N	6	149	2017-11-12 18:50:02.257874	2017-11-12 18:50:02.25789
101	Iseyin	iseyin	\N	6	149	2017-11-12 18:50:02.264152	2017-11-12 18:50:02.26416
102	Itesiwaju	itesiwaju	\N	6	149	2017-11-12 18:50:02.269017	2017-11-12 18:50:02.269024
103	Iwajowa	iwajowa	\N	6	149	2017-11-12 18:50:02.27826	2017-11-12 18:50:02.27827
104	Kajola	kajola	\N	6	149	2017-11-12 18:50:02.286445	2017-11-12 18:50:02.286466
105	Lagelu	lagelu	\N	6	149	2017-11-12 18:50:02.297444	2017-11-12 18:50:02.297466
106	Ogbomosho North	ogbomosho-north	\N	6	149	2017-11-12 18:50:02.307768	2017-11-12 18:50:02.307789
107	Ogbomosho South	ogbomosho-south	\N	6	149	2017-11-12 18:50:02.316317	2017-11-12 18:50:02.316329
108	Ogo Oluwa	ogo-oluwa	\N	6	149	2017-11-12 18:50:02.324624	2017-11-12 18:50:02.324647
109	Olorunsogo	olorunsogo	\N	6	149	2017-11-12 18:50:02.330445	2017-11-12 18:50:02.330452
110	Oluyole	oluyole	\N	6	149	2017-11-12 18:50:02.335533	2017-11-12 18:50:02.33554
111	Ona Ara	ona-ara	\N	6	149	2017-11-12 18:50:02.343485	2017-11-12 18:50:02.343498
112	Orelope	orelope	\N	6	149	2017-11-12 18:50:02.353404	2017-11-12 18:50:02.35344
113	Ori Ire	ori-ire	\N	6	149	2017-11-12 18:50:02.362269	2017-11-12 18:50:02.362279
114	Oyo	oyo	\N	6	149	2017-11-12 18:50:02.367713	2017-11-12 18:50:02.36772
115	Oyo East	oyo-east	\N	6	149	2017-11-12 18:50:02.37742	2017-11-12 18:50:02.377442
116	Saki East	saki-east	\N	6	149	2017-11-12 18:50:02.391075	2017-11-12 18:50:02.391104
117	Saki West	saki-west	\N	6	149	2017-11-12 18:50:02.403365	2017-11-12 18:50:02.403377
118	Surulere, Oyo State	surulere-oyo-state	\N	6	149	2017-11-12 18:50:02.414685	2017-11-12 18:50:02.414704
119	Akwanga	akwanga	\N	7	149	2017-11-12 18:50:02.425251	2017-11-12 18:50:02.425259
120	Awe	awe	\N	7	149	2017-11-12 18:50:02.435103	2017-11-12 18:50:02.435115
121	Doma	doma	\N	7	149	2017-11-12 18:50:02.445425	2017-11-12 18:50:02.445437
122	Karu	karu	\N	7	149	2017-11-12 18:50:02.452612	2017-11-12 18:50:02.45262
123	Keana	keana	\N	7	149	2017-11-12 18:50:02.464717	2017-11-12 18:50:02.464737
124	Keffi	keffi	\N	7	149	2017-11-12 18:50:02.479626	2017-11-12 18:50:02.479639
125	Kokona	kokona	\N	7	149	2017-11-12 18:50:02.488618	2017-11-12 18:50:02.488628
126	Lafia	lafia	\N	7	149	2017-11-12 18:50:02.499444	2017-11-12 18:50:02.499453
127	Nasarawa	nasarawa	\N	7	149	2017-11-12 18:50:02.510302	2017-11-12 18:50:02.510318
128	Nasarawa Egon	nasarawa-egon	\N	7	149	2017-11-12 18:50:02.519479	2017-11-12 18:50:02.519487
129	Obi, Nasarawa State	obi-nasarawa-state	\N	7	149	2017-11-12 18:50:02.529382	2017-11-12 18:50:02.529397
130	Toto	toto	\N	7	149	2017-11-12 18:50:02.538283	2017-11-12 18:50:02.538295
131	Wamba	wamba	\N	7	149	2017-11-12 18:50:02.547731	2017-11-12 18:50:02.547738
132	Abak	abak	\N	8	149	2017-11-12 18:50:02.561427	2017-11-12 18:50:02.561445
133	Eastern Obolo	eastern-obolo	\N	8	149	2017-11-12 18:50:02.574469	2017-11-12 18:50:02.574489
134	Eket	eket	\N	8	149	2017-11-12 18:50:02.58834	2017-11-12 18:50:02.588355
135	Esit Eket	esit-eket	\N	8	149	2017-11-12 18:50:02.602861	2017-11-12 18:50:02.602876
136	Essien Udim	essien-udim	\N	8	149	2017-11-12 18:50:02.618664	2017-11-12 18:50:02.618683
137	Etim Ekpo	etim-ekpo	\N	8	149	2017-11-12 18:50:02.63427	2017-11-12 18:50:02.634287
138	Etinan	etinan	\N	8	149	2017-11-12 18:50:02.647896	2017-11-12 18:50:02.647913
139	Ibeno	ibeno	\N	8	149	2017-11-12 18:50:02.665301	2017-11-12 18:50:02.665314
140	Ibesikpo Asutan	ibesikpo-asutan	\N	8	149	2017-11-12 18:50:02.680746	2017-11-12 18:50:02.68077
141	Ibiono-Ibom	ibiono-ibom	\N	8	149	2017-11-12 18:50:02.697745	2017-11-12 18:50:02.697766
142	Ika	ika	\N	8	149	2017-11-12 18:50:02.711427	2017-11-12 18:50:02.71145
143	Ikono	ikono	\N	8	149	2017-11-12 18:50:02.726639	2017-11-12 18:50:02.726655
144	Ikot Abasi	ikot-abasi	\N	8	149	2017-11-12 18:50:02.740011	2017-11-12 18:50:02.740025
145	Ikot Ekpene	ikot-ekpene	\N	8	149	2017-11-12 18:50:02.753987	2017-11-12 18:50:02.753999
146	Ini	ini	\N	8	149	2017-11-12 18:50:02.770118	2017-11-12 18:50:02.770139
147	Itu	itu	\N	8	149	2017-11-12 18:50:02.784747	2017-11-12 18:50:02.784763
148	Mbo	mbo	\N	8	149	2017-11-12 18:50:02.799386	2017-11-12 18:50:02.799408
149	Mkpat-Enin	mkpat-enin	\N	8	149	2017-11-12 18:50:02.813043	2017-11-12 18:50:02.813057
150	Nsit-Atai	nsit-atai	\N	8	149	2017-11-12 18:50:02.826205	2017-11-12 18:50:02.826216
151	Nsit-Ibom	nsit-ibom	\N	8	149	2017-11-12 18:50:02.838931	2017-11-12 18:50:02.838953
152	Nsit-Ubium	nsit-ubium	\N	8	149	2017-11-12 18:50:02.851571	2017-11-12 18:50:02.851585
153	Obot Akara	obot-akara	\N	8	149	2017-11-12 18:50:02.864132	2017-11-12 18:50:02.864147
154	Okobo	okobo	\N	8	149	2017-11-12 18:50:02.87758	2017-11-12 18:50:02.87761
155	Onna	onna	\N	8	149	2017-11-12 18:50:02.891296	2017-11-12 18:50:02.891319
156	Oron	oron	\N	8	149	2017-11-12 18:50:02.90585	2017-11-12 18:50:02.905876
157	Oruk Anam	oruk-anam	\N	8	149	2017-11-12 18:50:02.919305	2017-11-12 18:50:02.919319
158	Udung-Uko	udung-uko	\N	8	149	2017-11-12 18:50:02.931548	2017-11-12 18:50:02.931571
159	Ukanafun	ukanafun	\N	8	149	2017-11-12 18:50:02.945275	2017-11-12 18:50:02.945293
160	Uruan	uruan	\N	8	149	2017-11-12 18:50:02.955241	2017-11-12 18:50:02.955256
161	Urue-Offong/Oruko	urue-offong-oruko	\N	8	149	2017-11-12 18:50:02.968181	2017-11-12 18:50:02.968189
162	Uyo	uyo	\N	8	149	2017-11-12 18:50:02.978034	2017-11-12 18:50:02.978051
163	Aniocha North	aniocha-north	\N	9	149	2017-11-12 18:50:02.985194	2017-11-12 18:50:02.985202
164	Aniocha South	aniocha-south	\N	9	149	2017-11-12 18:50:02.996515	2017-11-12 18:50:02.996537
165	Bomadi	bomadi	\N	9	149	2017-11-12 18:50:03.005729	2017-11-12 18:50:03.005739
166	Burutu	burutu	\N	9	149	2017-11-12 18:50:03.018203	2017-11-12 18:50:03.01822
167	Ethiope East	ethiope-east	\N	9	149	2017-11-12 18:50:03.03113	2017-11-12 18:50:03.031145
168	Ethiope West	ethiope-west	\N	9	149	2017-11-12 18:50:03.043803	2017-11-12 18:50:03.043813
169	Ika North East	ika-north-east	\N	9	149	2017-11-12 18:50:03.057729	2017-11-12 18:50:03.057758
170	Ika South	ika-south	\N	9	149	2017-11-12 18:50:03.070301	2017-11-12 18:50:03.070317
171	Isoko North	isoko-north	\N	9	149	2017-11-12 18:50:03.08289	2017-11-12 18:50:03.082903
172	Isoko South	isoko-south	\N	9	149	2017-11-12 18:50:03.094909	2017-11-12 18:50:03.094925
173	Ndokwa East	ndokwa-east	\N	9	149	2017-11-12 18:50:03.106271	2017-11-12 18:50:03.106287
174	Ndokwa West	ndokwa-west	\N	9	149	2017-11-12 18:50:03.118091	2017-11-12 18:50:03.118106
175	Okpe	okpe	\N	9	149	2017-11-12 18:50:03.1312	2017-11-12 18:50:03.131221
176	Oshimili North	oshimili-north	\N	9	149	2017-11-12 18:50:03.144797	2017-11-12 18:50:03.14481
177	Oshimili South	oshimili-south	\N	9	149	2017-11-12 18:50:03.160641	2017-11-12 18:50:03.160656
178	Patani	patani	\N	9	149	2017-11-12 18:50:03.170379	2017-11-12 18:50:03.170389
179	Sapele, Delta	sapele-delta	\N	9	149	2017-11-12 18:50:03.183554	2017-11-12 18:50:03.183573
180	Udu	udu	\N	9	149	2017-11-12 18:50:03.197618	2017-11-12 18:50:03.19764
181	Ughelli North	ughelli-north	\N	9	149	2017-11-12 18:50:03.211364	2017-11-12 18:50:03.211377
182	Ughelli South	ughelli-south	\N	9	149	2017-11-12 18:50:03.221165	2017-11-12 18:50:03.221182
183	Ukwuani	ukwuani	\N	9	149	2017-11-12 18:50:03.232833	2017-11-12 18:50:03.232848
184	Uvwie	uvwie	\N	9	149	2017-11-12 18:50:03.244036	2017-11-12 18:50:03.244062
185	Warri North	warri-north	\N	9	149	2017-11-12 18:50:03.253506	2017-11-12 18:50:03.253519
186	Warri South	warri-south	\N	9	149	2017-11-12 18:50:03.265637	2017-11-12 18:50:03.265653
187	Warri South West	warri-south-west	\N	9	149	2017-11-12 18:50:03.276303	2017-11-12 18:50:03.276318
188	Abi	abi	\N	10	149	2017-11-12 18:50:03.285438	2017-11-12 18:50:03.28545
189	Akamkpa	akamkpa	\N	10	149	2017-11-12 18:50:03.294373	2017-11-12 18:50:03.294385
190	Akpabuyo	akpabuyo	\N	10	149	2017-11-12 18:50:03.305397	2017-11-12 18:50:03.305434
191	Bakassi	bakassi	\N	10	149	2017-11-12 18:50:03.318735	2017-11-12 18:50:03.318756
192	Bekwarra	bekwarra	\N	10	149	2017-11-12 18:50:03.329098	2017-11-12 18:50:03.329121
193	Biase	biase	\N	10	149	2017-11-12 18:50:03.340637	2017-11-12 18:50:03.340653
194	Boki	boki	\N	10	149	2017-11-12 18:50:03.348663	2017-11-12 18:50:03.348673
195	Calabar Municipal	calabar-municipal	\N	10	149	2017-11-12 18:50:03.355714	2017-11-12 18:50:03.355732
196	Calabar South	calabar-south	\N	10	149	2017-11-12 18:50:03.3639	2017-11-12 18:50:03.363925
197	Etung	etung	\N	10	149	2017-11-12 18:50:03.369046	2017-11-12 18:50:03.369053
198	Ikom	ikom	\N	10	149	2017-11-12 18:50:03.376843	2017-11-12 18:50:03.376855
199	Obanliku	obanliku	\N	10	149	2017-11-12 18:50:03.386068	2017-11-12 18:50:03.386087
200	Obubra	obubra	\N	10	149	2017-11-12 18:50:03.397905	2017-11-12 18:50:03.397921
201	Obudu	obudu	\N	10	149	2017-11-12 18:50:03.407733	2017-11-12 18:50:03.407758
202	Odukpani	odukpani	\N	10	149	2017-11-12 18:50:03.418044	2017-11-12 18:50:03.418065
203	Ogoja	ogoja	\N	10	149	2017-11-12 18:50:03.430376	2017-11-12 18:50:03.430394
204	Yakuur	yakuur	\N	10	149	2017-11-12 18:50:03.44116	2017-11-12 18:50:03.441178
205	Yala	yala	\N	10	149	2017-11-12 18:50:03.5493	2017-11-12 18:50:03.549319
206	Bade	bade	\N	11	149	2017-11-12 18:50:03.557997	2017-11-12 18:50:03.558013
207	Bursari	bursari	\N	11	149	2017-11-12 18:50:03.568469	2017-11-12 18:50:03.568482
208	Damaturu	damaturu	\N	11	149	2017-11-12 18:50:03.578454	2017-11-12 18:50:03.578471
209	Fika	fika	\N	11	149	2017-11-12 18:50:03.586914	2017-11-12 18:50:03.586923
210	Fune	fune	\N	11	149	2017-11-12 18:50:03.597199	2017-11-12 18:50:03.597216
211	Geidam	geidam	\N	11	149	2017-11-12 18:50:03.605735	2017-11-12 18:50:03.605747
212	Gujba	gujba	\N	11	149	2017-11-12 18:50:03.613855	2017-11-12 18:50:03.613871
213	Gulani	gulani	\N	11	149	2017-11-12 18:50:03.620665	2017-11-12 18:50:03.620676
214	Jakusko	jakusko	\N	11	149	2017-11-12 18:50:03.630838	2017-11-12 18:50:03.630855
215	Karasuwa	karasuwa	\N	11	149	2017-11-12 18:50:03.640847	2017-11-12 18:50:03.640857
216	Machina	machina	\N	11	149	2017-11-12 18:50:03.650557	2017-11-12 18:50:03.650572
217	Nangere	nangere	\N	11	149	2017-11-12 18:50:03.65997	2017-11-12 18:50:03.659987
218	Nguru	nguru	\N	11	149	2017-11-12 18:50:03.672898	2017-11-12 18:50:03.672919
219	Potiskum	potiskum	\N	11	149	2017-11-12 18:50:03.685457	2017-11-12 18:50:03.685478
220	Tarmuwa	tarmuwa	\N	11	149	2017-11-12 18:50:03.697037	2017-11-12 18:50:03.697054
221	Yunusari	yunusari	\N	11	149	2017-11-12 18:50:03.707249	2017-11-12 18:50:03.707268
222	Yusufari	yusufari	\N	11	149	2017-11-12 18:50:03.717796	2017-11-12 18:50:03.717817
223	Asa	asa	\N	12	149	2017-11-12 18:50:03.728098	2017-11-12 18:50:03.728121
224	Baruten	baruten	\N	12	149	2017-11-12 18:50:03.738275	2017-11-12 18:50:03.738295
225	Edu	edu	\N	12	149	2017-11-12 18:50:03.7489	2017-11-12 18:50:03.748918
226	Ekiti, Kwara State	ekiti-kwara-state	\N	12	149	2017-11-12 18:50:03.761089	2017-11-12 18:50:03.761101
227	Ifelodun, Kwara State	ifelodun-kwara-state	\N	12	149	2017-11-12 18:50:03.771512	2017-11-12 18:50:03.771522
228	Ilorin East	ilorin-east	\N	12	149	2017-11-12 18:50:03.785597	2017-11-12 18:50:03.78561
229	Ilorin South	ilorin-south	\N	12	149	2017-11-12 18:50:03.79695	2017-11-12 18:50:03.796972
230	Ilorin West	ilorin-west	\N	12	149	2017-11-12 18:50:03.807303	2017-11-12 18:50:03.807334
231	Irepodun, Kwara State	irepodun-kwara-state	\N	12	149	2017-11-12 18:50:03.819415	2017-11-12 18:50:03.819429
232	Isin	isin	\N	12	149	2017-11-12 18:50:03.83158	2017-11-12 18:50:03.831602
233	Kaiama	kaiama	\N	12	149	2017-11-12 18:50:03.84192	2017-11-12 18:50:03.841932
234	Moro	moro	\N	12	149	2017-11-12 18:50:03.85619	2017-11-12 18:50:03.85621
235	Offa	offa	\N	12	149	2017-11-12 18:50:03.871131	2017-11-12 18:50:03.871153
236	Oke Ero	oke-ero	\N	12	149	2017-11-12 18:50:03.884975	2017-11-12 18:50:03.884989
237	Oyun	oyun	\N	12	149	2017-11-12 18:50:03.897091	2017-11-12 18:50:03.897112
238	Pategi	pategi	\N	12	149	2017-11-12 18:50:03.908996	2017-11-12 18:50:03.909021
239	Ardo Kola	ardo-kola	\N	13	149	2017-11-12 18:50:03.920831	2017-11-12 18:50:03.920846
240	Bali	bali	\N	13	149	2017-11-12 18:50:03.932899	2017-11-12 18:50:03.932917
241	Donga	donga	\N	13	149	2017-11-12 18:50:03.944398	2017-11-12 18:50:03.944417
242	Gashaka	gashaka	\N	13	149	2017-11-12 18:50:03.955553	2017-11-12 18:50:03.955567
243	Gassol	gassol	\N	13	149	2017-11-12 18:50:03.967195	2017-11-12 18:50:03.967211
244	Ibi	ibi	\N	13	149	2017-11-12 18:50:03.979673	2017-11-12 18:50:03.979689
245	Jalingo	jalingo	\N	13	149	2017-11-12 18:50:03.994325	2017-11-12 18:50:03.994339
246	Karim Lamido	karim-lamido	\N	13	149	2017-11-12 18:50:04.011478	2017-11-12 18:50:04.011503
247	Kumi	kumi	\N	13	149	2017-11-12 18:50:04.02503	2017-11-12 18:50:04.025053
248	Lau	lau	\N	13	149	2017-11-12 18:50:04.038083	2017-11-12 18:50:04.038096
249	Sardauna	sardauna	\N	13	149	2017-11-12 18:50:04.050731	2017-11-12 18:50:04.050752
250	Takum	takum	\N	13	149	2017-11-12 18:50:04.064378	2017-11-12 18:50:04.064408
251	Ussa	ussa	\N	13	149	2017-11-12 18:50:04.0782	2017-11-12 18:50:04.078215
252	Wukari	wukari	\N	13	149	2017-11-12 18:50:04.092613	2017-11-12 18:50:04.092632
253	Yorro	yorro	\N	13	149	2017-11-12 18:50:04.105778	2017-11-12 18:50:04.105793
254	Zing	zing	\N	13	149	2017-11-12 18:50:04.119053	2017-11-12 18:50:04.119081
255	Binji	binji	\N	14	149	2017-11-12 18:50:04.133357	2017-11-12 18:50:04.133371
256	Bodinga	bodinga	\N	14	149	2017-11-12 18:50:04.145112	2017-11-12 18:50:04.145131
257	Dange Shuni	dange-shuni	\N	14	149	2017-11-12 18:50:04.158147	2017-11-12 18:50:04.158171
258	Gada	gada	\N	14	149	2017-11-12 18:50:04.171559	2017-11-12 18:50:04.171583
259	Goronyo	goronyo	\N	14	149	2017-11-12 18:50:04.186408	2017-11-12 18:50:04.186424
260	Gudu	gudu	\N	14	149	2017-11-12 18:50:04.200864	2017-11-12 18:50:04.20088
261	Gwadabawa	gwadabawa	\N	14	149	2017-11-12 18:50:04.218012	2017-11-12 18:50:04.21804
262	Illela	illela	\N	14	149	2017-11-12 18:50:04.231397	2017-11-12 18:50:04.231412
263	Isa	isa	\N	14	149	2017-11-12 18:50:04.245201	2017-11-12 18:50:04.245223
264	Kebbe	kebbe	\N	14	149	2017-11-12 18:50:04.259329	2017-11-12 18:50:04.259343
265	Kware	kware	\N	14	149	2017-11-12 18:50:04.271498	2017-11-12 18:50:04.271518
266	Rabah	rabah	\N	14	149	2017-11-12 18:50:04.285519	2017-11-12 18:50:04.285541
267	Sabon Birni	sabon-birni	\N	14	149	2017-11-12 18:50:04.299022	2017-11-12 18:50:04.299039
268	Shagari	shagari	\N	14	149	2017-11-12 18:50:04.31182	2017-11-12 18:50:04.311901
269	Silame	silame	\N	14	149	2017-11-12 18:50:04.325138	2017-11-12 18:50:04.325175
270	Sokoto North	sokoto-north	\N	14	149	2017-11-12 18:50:04.338926	2017-11-12 18:50:04.338949
271	Sokoto South	sokoto-south	\N	14	149	2017-11-12 18:50:04.352483	2017-11-12 18:50:04.352504
272	Tambuwal	tambuwal	\N	14	149	2017-11-12 18:50:04.366957	2017-11-12 18:50:04.366977
273	Tangaza	tangaza	\N	14	149	2017-11-12 18:50:04.380262	2017-11-12 18:50:04.380297
274	Tureta	tureta	\N	14	149	2017-11-12 18:50:04.391671	2017-11-12 18:50:04.391679
275	Wamako	wamako	\N	14	149	2017-11-12 18:50:04.399352	2017-11-12 18:50:04.39936
276	Wurno	wurno	\N	14	149	2017-11-12 18:50:04.412831	2017-11-12 18:50:04.412864
277	Yabo	yabo	\N	14	149	2017-11-12 18:50:04.424084	2017-11-12 18:50:04.4241
278	Atakunmosa East	atakunmosa-east	\N	15	149	2017-11-12 18:50:04.434694	2017-11-12 18:50:04.434702
279	Atakunmosa West	atakunmosa-west	\N	15	149	2017-11-12 18:50:04.446939	2017-11-12 18:50:04.446953
280	Aiyedaade	aiyedaade	\N	15	149	2017-11-12 18:50:04.458326	2017-11-12 18:50:04.45834
281	Aiyedire	aiyedire	\N	15	149	2017-11-12 18:50:04.47225	2017-11-12 18:50:04.472265
282	Boluwaduro	boluwaduro	\N	15	149	2017-11-12 18:50:04.485574	2017-11-12 18:50:04.485587
283	Boripe	boripe	\N	15	149	2017-11-12 18:50:04.496525	2017-11-12 18:50:04.496537
284	Ede North	ede-north	\N	15	149	2017-11-12 18:50:04.50711	2017-11-12 18:50:04.507119
285	Ede South	ede-south	\N	15	149	2017-11-12 18:50:04.514541	2017-11-12 18:50:04.51455
286	Ife Central	ife-central	\N	15	149	2017-11-12 18:50:04.523091	2017-11-12 18:50:04.523112
287	Ife East	ife-east	\N	15	149	2017-11-12 18:50:04.530395	2017-11-12 18:50:04.530402
288	Ife North	ife-north	\N	15	149	2017-11-12 18:50:04.536116	2017-11-12 18:50:04.536125
289	Ife South	ife-south	\N	15	149	2017-11-12 18:50:04.544723	2017-11-12 18:50:04.544731
290	Egbedore	egbedore	\N	15	149	2017-11-12 18:50:04.550856	2017-11-12 18:50:04.550864
291	Ejigbo	ejigbo	\N	15	149	2017-11-12 18:50:04.560356	2017-11-12 18:50:04.560365
292	Ifedayo	ifedayo	\N	15	149	2017-11-12 18:50:04.566664	2017-11-12 18:50:04.566673
293	Ifelodun	ifelodun	\N	15	149	2017-11-12 18:50:04.57647	2017-11-12 18:50:04.576493
294	Ila	ila	\N	15	149	2017-11-12 18:50:04.586417	2017-11-12 18:50:04.586439
295	Ilesa East	ilesa-east	\N	15	149	2017-11-12 18:50:04.597696	2017-11-12 18:50:04.597709
296	Ilesa West	ilesa-west	\N	15	149	2017-11-12 18:50:04.608623	2017-11-12 18:50:04.60864
297	Irepodun	irepodun	\N	15	149	2017-11-12 18:50:04.619633	2017-11-12 18:50:04.61965
298	Irewole	irewole	\N	15	149	2017-11-12 18:50:04.630991	2017-11-12 18:50:04.631004
299	Isokan	isokan	\N	15	149	2017-11-12 18:50:04.643511	2017-11-12 18:50:04.643531
300	Iwo	iwo	\N	15	149	2017-11-12 18:50:04.654939	2017-11-12 18:50:04.65496
301	Obokun	obokun	\N	15	149	2017-11-12 18:50:04.66758	2017-11-12 18:50:04.6676
302	Odo Otin	odo-otin	\N	15	149	2017-11-12 18:50:04.6801	2017-11-12 18:50:04.680123
303	Ola Oluwa	ola-oluwa	\N	15	149	2017-11-12 18:50:04.690428	2017-11-12 18:50:04.690443
304	Olorunda	olorunda	\N	15	149	2017-11-12 18:50:04.703086	2017-11-12 18:50:04.703117
305	Oriade	oriade	\N	15	149	2017-11-12 18:50:04.716055	2017-11-12 18:50:04.716076
306	Orolu	orolu	\N	15	149	2017-11-12 18:50:04.7277	2017-11-12 18:50:04.727714
307	Osogbo	osogbo	\N	15	149	2017-11-12 18:50:04.738696	2017-11-12 18:50:04.738721
308	Birnin Gwari	birnin-gwari	\N	16	149	2017-11-12 18:50:04.749447	2017-11-12 18:50:04.749461
309	Chikun	chikun	\N	16	149	2017-11-12 18:50:04.761821	2017-11-12 18:50:04.761841
310	Giwa	giwa	\N	16	149	2017-11-12 18:50:04.773004	2017-11-12 18:50:04.773021
311	Igabi	igabi	\N	16	149	2017-11-12 18:50:04.782661	2017-11-12 18:50:04.782679
312	Ikara	ikara	\N	16	149	2017-11-12 18:50:04.792961	2017-11-12 18:50:04.792973
313	Jaba	jaba	\N	16	149	2017-11-12 18:50:04.802761	2017-11-12 18:50:04.80278
314	Jema''a	jema-a	\N	16	149	2017-11-12 18:50:04.815063	2017-11-12 18:50:04.815076
315	Kachia	kachia	\N	16	149	2017-11-12 18:50:04.826174	2017-11-12 18:50:04.826193
316	Kaduna North	kaduna-north	\N	16	149	2017-11-12 18:50:04.834442	2017-11-12 18:50:04.834458
317	Kaduna South	kaduna-south	\N	16	149	2017-11-12 18:50:04.844033	2017-11-12 18:50:04.844053
318	Kagarko	kagarko	\N	16	149	2017-11-12 18:50:04.854219	2017-11-12 18:50:04.85424
319	Kajuru	kajuru	\N	16	149	2017-11-12 18:50:04.865586	2017-11-12 18:50:04.865608
320	Kaura	kaura	\N	16	149	2017-11-12 18:50:04.876016	2017-11-12 18:50:04.876029
321	Kauru	kauru	\N	16	149	2017-11-12 18:50:04.886325	2017-11-12 18:50:04.886338
322	Kubau	kubau	\N	16	149	2017-11-12 18:50:04.895298	2017-11-12 18:50:04.895309
323	Kudan	kudan	\N	16	149	2017-11-12 18:50:04.903615	2017-11-12 18:50:04.903631
324	Lere	lere	\N	16	149	2017-11-12 18:50:04.915939	2017-11-12 18:50:04.915961
325	Makarfi	makarfi	\N	16	149	2017-11-12 18:50:04.927108	2017-11-12 18:50:04.927132
326	Sabon Gari	sabon-gari	\N	16	149	2017-11-12 18:50:04.939568	2017-11-12 18:50:04.939602
327	Sanga	sanga	\N	16	149	2017-11-12 18:50:04.949503	2017-11-12 18:50:04.949516
328	Soba	soba	\N	16	149	2017-11-12 18:50:04.961282	2017-11-12 18:50:04.961304
329	Zangon Kataf	zangon-kataf	\N	16	149	2017-11-12 18:50:04.974359	2017-11-12 18:50:04.974382
330	Zaria	zaria	\N	16	149	2017-11-12 18:50:04.985949	2017-11-12 18:50:04.985964
331	Akoko North-East	akoko-north-east	\N	17	149	2017-11-12 18:50:04.996869	2017-11-12 18:50:04.996882
332	Akoko North-West	akoko-north-west	\N	17	149	2017-11-12 18:50:05.007811	2017-11-12 18:50:05.007827
333	Akoko South-West	akoko-south-west	\N	17	149	2017-11-12 18:50:05.016714	2017-11-12 18:50:05.016735
334	Akoko South-East	akoko-south-east	\N	17	149	2017-11-12 18:50:05.027149	2017-11-12 18:50:05.02717
335	Akure North	akure-north	\N	17	149	2017-11-12 18:50:05.036949	2017-11-12 18:50:05.036967
336	Akure South	akure-south	\N	17	149	2017-11-12 18:50:05.046669	2017-11-12 18:50:05.046678
337	Ese Odo	ese-odo	\N	17	149	2017-11-12 18:50:05.051776	2017-11-12 18:50:05.051783
338	Idanre	idanre	\N	17	149	2017-11-12 18:50:05.058194	2017-11-12 18:50:05.058202
339	Ifedore	ifedore	\N	17	149	2017-11-12 18:50:05.064272	2017-11-12 18:50:05.06428
340	Ilaje	ilaje	\N	17	149	2017-11-12 18:50:05.069192	2017-11-12 18:50:05.069201
341	Ile Oluji/Okeigbo	ile-oluji-okeigbo	\N	17	149	2017-11-12 18:50:05.077271	2017-11-12 18:50:05.077278
342	Irele	irele	\N	17	149	2017-11-12 18:50:05.082161	2017-11-12 18:50:05.082168
343	Odigbo	odigbo	\N	17	149	2017-11-12 18:50:05.088674	2017-11-12 18:50:05.088691
344	Okitipupa	okitipupa	\N	17	149	2017-11-12 18:50:05.099229	2017-11-12 18:50:05.099243
345	Ondo East	ondo-east	\N	17	149	2017-11-12 18:50:05.111815	2017-11-12 18:50:05.111838
346	Ondo West	ondo-west	\N	17	149	2017-11-12 18:50:05.124465	2017-11-12 18:50:05.124482
347	Ose	ose	\N	17	149	2017-11-12 18:50:05.135962	2017-11-12 18:50:05.135976
348	Owo	owo	\N	17	149	2017-11-12 18:50:05.15033	2017-11-12 18:50:05.150343
349	Agaie	agaie	\N	18	149	2017-11-12 18:50:05.160292	2017-11-12 18:50:05.160303
350	Agwara	agwara	\N	18	149	2017-11-12 18:50:05.1721	2017-11-12 18:50:05.172131
351	Bida	bida	\N	18	149	2017-11-12 18:50:05.187651	2017-11-12 18:50:05.18768
352	Borgu	borgu	\N	18	149	2017-11-12 18:50:05.197971	2017-11-12 18:50:05.197981
353	Bosso	bosso	\N	18	149	2017-11-12 18:50:05.209717	2017-11-12 18:50:05.209739
354	Chanchaga	chanchaga	\N	18	149	2017-11-12 18:50:05.221856	2017-11-12 18:50:05.221874
355	Edati	edati	\N	18	149	2017-11-12 18:50:05.234053	2017-11-12 18:50:05.234071
356	Gbako	gbako	\N	18	149	2017-11-12 18:50:05.247421	2017-11-12 18:50:05.247444
357	Gurara	gurara	\N	18	149	2017-11-12 18:50:05.259229	2017-11-12 18:50:05.259241
358	Katcha	katcha	\N	18	149	2017-11-12 18:50:05.270998	2017-11-12 18:50:05.271018
359	Kontagora	kontagora	\N	18	149	2017-11-12 18:50:05.281764	2017-11-12 18:50:05.281777
360	Lapai	lapai	\N	18	149	2017-11-12 18:50:05.294407	2017-11-12 18:50:05.294424
361	Lavun	lavun	\N	18	149	2017-11-12 18:50:05.304335	2017-11-12 18:50:05.304352
362	Magama	magama	\N	18	149	2017-11-12 18:50:05.317722	2017-11-12 18:50:05.317738
363	Mariga	mariga	\N	18	149	2017-11-12 18:50:05.329049	2017-11-12 18:50:05.32906
364	Mashegu	mashegu	\N	18	149	2017-11-12 18:50:05.34031	2017-11-12 18:50:05.340328
365	Mokwa	mokwa	\N	18	149	2017-11-12 18:50:05.353163	2017-11-12 18:50:05.353189
366	Moya	moya	\N	18	149	2017-11-12 18:50:05.363714	2017-11-12 18:50:05.363725
367	Paikoro	paikoro	\N	18	149	2017-11-12 18:50:05.373999	2017-11-12 18:50:05.37401
368	Rafi	rafi	\N	18	149	2017-11-12 18:50:05.384502	2017-11-12 18:50:05.384513
369	Rijau	rijau	\N	18	149	2017-11-12 18:50:05.395528	2017-11-12 18:50:05.395542
370	Shiroro	shiroro	\N	18	149	2017-11-12 18:50:05.405938	2017-11-12 18:50:05.40595
371	Suleja	suleja	\N	18	149	2017-11-12 18:50:05.413185	2017-11-12 18:50:05.413193
372	Tafa	tafa	\N	18	149	2017-11-12 18:50:05.419779	2017-11-12 18:50:05.419789
373	Wushishi	wushishi	\N	18	149	2017-11-12 18:50:05.430649	2017-11-12 18:50:05.43066
374	Abua/Odual	abua-odual	\N	19	149	2017-11-12 18:50:05.441609	2017-11-12 18:50:05.441635
375	Ahoada East	ahoada-east	\N	19	149	2017-11-12 18:50:05.454992	2017-11-12 18:50:05.455013
376	Ahoada West	ahoada-west	\N	19	149	2017-11-12 18:50:05.467036	2017-11-12 18:50:05.467049
377	Akuku-Toru	akuku-toru	\N	19	149	2017-11-12 18:50:05.483112	2017-11-12 18:50:05.483138
378	Andoni	andoni	\N	19	149	2017-11-12 18:50:05.491879	2017-11-12 18:50:05.491892
379	Asari-Toru	asari-toru	\N	19	149	2017-11-12 18:50:05.498849	2017-11-12 18:50:05.498857
380	Bonny	bonny	\N	19	149	2017-11-12 18:50:05.508616	2017-11-12 18:50:05.50863
381	Degema	degema	\N	19	149	2017-11-12 18:50:05.516097	2017-11-12 18:50:05.516104
382	Eleme	eleme	\N	19	149	2017-11-12 18:50:05.524611	2017-11-12 18:50:05.524624
383	Emuoha	emuoha	\N	19	149	2017-11-12 18:50:05.531952	2017-11-12 18:50:05.531959
384	Etche	etche	\N	19	149	2017-11-12 18:50:05.541803	2017-11-12 18:50:05.54182
385	Gokana	gokana	\N	19	149	2017-11-12 18:50:05.549276	2017-11-12 18:50:05.549284
386	Ikwerre	ikwerre	\N	19	149	2017-11-12 18:50:05.557584	2017-11-12 18:50:05.557592
387	Khana	khana	\N	19	149	2017-11-12 18:50:05.564985	2017-11-12 18:50:05.565003
388	Obio/Akpor	obio-akpor	\N	19	149	2017-11-12 18:50:05.578017	2017-11-12 18:50:05.578033
389	Ogba/Egbema/Ndoni	ogba-egbema-ndoni	\N	19	149	2017-11-12 18:50:05.590208	2017-11-12 18:50:05.590224
390	Ogu/Bolo	ogu-bolo	\N	19	149	2017-11-12 18:50:05.602144	2017-11-12 18:50:05.602159
391	Okrika	okrika	\N	19	149	2017-11-12 18:50:05.614059	2017-11-12 18:50:05.614072
392	Omuma	omuma	\N	19	149	2017-11-12 18:50:05.628663	2017-11-12 18:50:05.628694
393	Opobo/Nkoro	opobo-nkoro	\N	19	149	2017-11-12 18:50:05.643728	2017-11-12 18:50:05.643749
394	Oyigbo	oyigbo	\N	19	149	2017-11-12 18:50:05.658083	2017-11-12 18:50:05.658095
395	Port Harcourt	port-harcourt	\N	19	149	2017-11-12 18:50:05.668307	2017-11-12 18:50:05.668321
396	Tai	tai	\N	19	149	2017-11-12 18:50:05.680251	2017-11-12 18:50:05.680266
397	Akko	akko	\N	20	149	2017-11-12 18:50:05.693329	2017-11-12 18:50:05.693346
398	Balanga	balanga	\N	20	149	2017-11-12 18:50:05.706023	2017-11-12 18:50:05.706079
399	Billiri	billiri	\N	20	149	2017-11-12 18:50:05.717083	2017-11-12 18:50:05.717094
400	Dukku	dukku	\N	20	149	2017-11-12 18:50:05.727489	2017-11-12 18:50:05.7275
401	Funakaye	funakaye	\N	20	149	2017-11-12 18:50:05.738107	2017-11-12 18:50:05.738136
402	Gombe	gombe	\N	20	149	2017-11-12 18:50:05.749787	2017-11-12 18:50:05.7498
403	Kaltungo	kaltungo	\N	20	149	2017-11-12 18:50:05.763744	2017-11-12 18:50:05.763766
404	Kwami	kwami	\N	20	149	2017-11-12 18:50:05.773874	2017-11-12 18:50:05.773884
405	Nafada	nafada	\N	20	149	2017-11-12 18:50:05.789607	2017-11-12 18:50:05.78962
406	Shongom	shongom	\N	20	149	2017-11-12 18:50:05.797735	2017-11-12 18:50:05.797743
407	Yamaltu/Deba	yamaltu-deba	\N	20	149	2017-11-12 18:50:05.806329	2017-11-12 18:50:05.80634
408	Auyo	auyo	\N	21	149	2017-11-12 18:50:05.818832	2017-11-12 18:50:05.818854
409	Babura	babura	\N	21	149	2017-11-12 18:50:05.83238	2017-11-12 18:50:05.832394
410	Biriniwa	biriniwa	\N	21	149	2017-11-12 18:50:05.843268	2017-11-12 18:50:05.843289
411	Birnin Kudu	birnin-kudu	\N	21	149	2017-11-12 18:50:05.855466	2017-11-12 18:50:05.855493
412	Buji	buji	\N	21	149	2017-11-12 18:50:05.865085	2017-11-12 18:50:05.865093
413	Dutse	dutse	\N	21	149	2017-11-12 18:50:05.876358	2017-11-12 18:50:05.876379
414	Gagarawa	gagarawa	\N	21	149	2017-11-12 18:50:05.888545	2017-11-12 18:50:05.888561
415	Garki	garki	\N	21	149	2017-11-12 18:50:05.899994	2017-11-12 18:50:05.900009
416	Gumel	gumel	\N	21	149	2017-11-12 18:50:05.911721	2017-11-12 18:50:05.911736
417	Guri	guri	\N	21	149	2017-11-12 18:50:05.922344	2017-11-12 18:50:05.92236
418	Gwaram	gwaram	\N	21	149	2017-11-12 18:50:05.933516	2017-11-12 18:50:05.933531
419	Gwiwa	gwiwa	\N	21	149	2017-11-12 18:50:05.945255	2017-11-12 18:50:05.94527
420	Hadejia	hadejia	\N	21	149	2017-11-12 18:50:05.956575	2017-11-12 18:50:05.95659
421	Jahun	jahun	\N	21	149	2017-11-12 18:50:05.970807	2017-11-12 18:50:05.970834
422	Kafin Hausa	kafin-hausa	\N	21	149	2017-11-12 18:50:05.984617	2017-11-12 18:50:05.984637
423	Kazaure	kazaure	\N	21	149	2017-11-12 18:50:05.997145	2017-11-12 18:50:05.997166
424	Kiri Kasama	kiri-kasama	\N	21	149	2017-11-12 18:50:06.01194	2017-11-12 18:50:06.011961
425	Kiyawa	kiyawa	\N	21	149	2017-11-12 18:50:06.023159	2017-11-12 18:50:06.023177
426	Kaugama	kaugama	\N	21	149	2017-11-12 18:50:06.03619	2017-11-12 18:50:06.036222
427	Maigatari	maigatari	\N	21	149	2017-11-12 18:50:06.048567	2017-11-12 18:50:06.048588
428	Malam Madori	malam-madori	\N	21	149	2017-11-12 18:50:06.062899	2017-11-12 18:50:06.062916
429	Miga	miga	\N	21	149	2017-11-12 18:50:06.077787	2017-11-12 18:50:06.07781
430	Ringim	ringim	\N	21	149	2017-11-12 18:50:06.091005	2017-11-12 18:50:06.091013
431	Roni	roni	\N	21	149	2017-11-12 18:50:06.101297	2017-11-12 18:50:06.101304
432	Sule Tankarkar	sule-tankarkar	\N	21	149	2017-11-12 18:50:06.11692	2017-11-12 18:50:06.116953
433	Taura	taura	\N	21	149	2017-11-12 18:50:06.129686	2017-11-12 18:50:06.1297
434	Yankwashi	yankwashi	\N	21	149	2017-11-12 18:50:06.143229	2017-11-12 18:50:06.143247
435	Aba North	aba-north	\N	22	149	2017-11-12 18:50:06.15653	2017-11-12 18:50:06.156543
436	Aba South	aba-south	\N	22	149	2017-11-12 18:50:06.168792	2017-11-12 18:50:06.168804
437	Arochukwu	arochukwu	\N	22	149	2017-11-12 18:50:06.182678	2017-11-12 18:50:06.18269
438	Bende	bende	\N	22	149	2017-11-12 18:50:06.193875	2017-11-12 18:50:06.193893
439	Ikwuano	ikwuano	\N	22	149	2017-11-12 18:50:06.205274	2017-11-12 18:50:06.205293
440	Isiala Ngwa North	isiala-ngwa-north	\N	22	149	2017-11-12 18:50:06.219422	2017-11-12 18:50:06.219438
441	Isiala Ngwa South	isiala-ngwa-south	\N	22	149	2017-11-12 18:50:06.230651	2017-11-12 18:50:06.230666
442	Isuikwuato	isuikwuato	\N	22	149	2017-11-12 18:50:06.24228	2017-11-12 18:50:06.242291
443	Obi Ngwa	obi-ngwa	\N	22	149	2017-11-12 18:50:06.255026	2017-11-12 18:50:06.255048
444	Ohafia	ohafia	\N	22	149	2017-11-12 18:50:06.2672	2017-11-12 18:50:06.267214
445	Osisioma	osisioma	\N	22	149	2017-11-12 18:50:06.278511	2017-11-12 18:50:06.278524
446	Ugwunagbo	ugwunagbo	\N	22	149	2017-11-12 18:50:06.290316	2017-11-12 18:50:06.290331
447	Ukwa East	ukwa-east	\N	22	149	2017-11-12 18:50:06.301832	2017-11-12 18:50:06.301853
448	Ukwa West	ukwa-west	\N	22	149	2017-11-12 18:50:06.315945	2017-11-12 18:50:06.315967
449	Umuahia North	umuahia-north	\N	22	149	2017-11-12 18:50:06.328558	2017-11-12 18:50:06.32858
450	Umuahia South	umuahia-south	\N	22	149	2017-11-12 18:50:06.3392	2017-11-12 18:50:06.339231
451	Umu Nneochi	umu-nneochi	\N	22	149	2017-11-12 18:50:06.35225	2017-11-12 18:50:06.352266
452	Abaji	abaji	\N	23	149	2017-11-12 18:50:06.364091	2017-11-12 18:50:06.364105
453	Bwari	bwari	\N	23	149	2017-11-12 18:50:06.378068	2017-11-12 18:50:06.37809
454	Gwagwalada	gwagwalada	\N	23	149	2017-11-12 18:50:06.391121	2017-11-12 18:50:06.391135
455	Kuje	kuje	\N	23	149	2017-11-12 18:50:06.402042	2017-11-12 18:50:06.402056
456	Kwali	kwali	\N	23	149	2017-11-12 18:50:06.41456	2017-11-12 18:50:06.414575
457	Municipal Area Council	municipal-area-council	\N	23	149	2017-11-12 18:50:06.427399	2017-11-12 18:50:06.427409
458	Aboh Mbaise	aboh-mbaise	\N	24	149	2017-11-12 18:50:06.436044	2017-11-12 18:50:06.43606
459	Ahiazu Mbaise	ahiazu-mbaise	\N	24	149	2017-11-12 18:50:06.444159	2017-11-12 18:50:06.444167
460	Ehime Mbano	ehime-mbano	\N	24	149	2017-11-12 18:50:06.451496	2017-11-12 18:50:06.451504
461	Ezinihitte	ezinihitte	\N	24	149	2017-11-12 18:50:06.462565	2017-11-12 18:50:06.462579
462	Ideato North	ideato-north	\N	24	149	2017-11-12 18:50:06.476918	2017-11-12 18:50:06.476934
463	Ideato South	ideato-south	\N	24	149	2017-11-12 18:50:06.48736	2017-11-12 18:50:06.487381
464	Ihitte/Uboma	ihitte-uboma	\N	24	149	2017-11-12 18:50:06.494758	2017-11-12 18:50:06.494769
465	Ikeduru	ikeduru	\N	24	149	2017-11-12 18:50:06.50578	2017-11-12 18:50:06.505796
466	Isiala Mbano	isiala-mbano	\N	24	149	2017-11-12 18:50:06.5157	2017-11-12 18:50:06.51571
467	Isu	isu	\N	24	149	2017-11-12 18:50:06.525332	2017-11-12 18:50:06.52534
468	Mbaitoli	mbaitoli	\N	24	149	2017-11-12 18:50:06.534843	2017-11-12 18:50:06.534859
469	Ngor Okpala	ngor-okpala	\N	24	149	2017-11-12 18:50:06.548776	2017-11-12 18:50:06.548789
470	Njaba	njaba	\N	24	149	2017-11-12 18:50:06.560789	2017-11-12 18:50:06.560804
471	Nkwerre	nkwerre	\N	24	149	2017-11-12 18:50:06.572167	2017-11-12 18:50:06.572181
472	Nwangele	nwangele	\N	24	149	2017-11-12 18:50:06.582462	2017-11-12 18:50:06.582475
473	Obowo	obowo	\N	24	149	2017-11-12 18:50:06.592859	2017-11-12 18:50:06.592874
474	Oguta	oguta	\N	24	149	2017-11-12 18:50:06.605103	2017-11-12 18:50:06.605122
475	Ohaji/Egbema	ohaji-egbema	\N	24	149	2017-11-12 18:50:06.616784	2017-11-12 18:50:06.6168
476	Okigwe	okigwe	\N	24	149	2017-11-12 18:50:06.629777	2017-11-12 18:50:06.629784
477	Orlu	orlu	\N	24	149	2017-11-12 18:50:06.637008	2017-11-12 18:50:06.637037
478	Orsu	orsu	\N	24	149	2017-11-12 18:50:06.648095	2017-11-12 18:50:06.64811
479	Oru East	oru-east	\N	24	149	2017-11-12 18:50:06.658845	2017-11-12 18:50:06.658856
480	Oru West	oru-west	\N	24	149	2017-11-12 18:50:06.671217	2017-11-12 18:50:06.671234
481	Owerri Municipal	owerri-municipal	\N	24	149	2017-11-12 18:50:06.684584	2017-11-12 18:50:06.684597
482	Owerri North	owerri-north	\N	24	149	2017-11-12 18:50:06.696652	2017-11-12 18:50:06.696674
483	Owerri West	owerri-west	\N	24	149	2017-11-12 18:50:06.708579	2017-11-12 18:50:06.708596
484	Unuimo	unuimo	\N	24	149	2017-11-12 18:50:06.721027	2017-11-12 18:50:06.721054
485	Aguata	aguata	\N	25	149	2017-11-12 18:50:06.734806	2017-11-12 18:50:06.734819
486	Anambra East	anambra-east	\N	25	149	2017-11-12 18:50:06.746966	2017-11-12 18:50:06.746978
487	Anambra West	anambra-west	\N	25	149	2017-11-12 18:50:06.76002	2017-11-12 18:50:06.760031
488	Anaocha	anaocha	\N	25	149	2017-11-12 18:50:06.77015	2017-11-12 18:50:06.770162
489	Awka North	awka-north	\N	25	149	2017-11-12 18:50:06.78125	2017-11-12 18:50:06.781265
490	Awka South	awka-south	\N	25	149	2017-11-12 18:50:06.796161	2017-11-12 18:50:06.796178
491	Ayamelum	ayamelum	\N	25	149	2017-11-12 18:50:06.806364	2017-11-12 18:50:06.806379
492	Dunukofia	dunukofia	\N	25	149	2017-11-12 18:50:06.815342	2017-11-12 18:50:06.815356
493	Ekwusigo	ekwusigo	\N	25	149	2017-11-12 18:50:06.827554	2017-11-12 18:50:06.827574
494	Idemili North	idemili-north	\N	25	149	2017-11-12 18:50:06.839789	2017-11-12 18:50:06.839807
495	Idemili South	idemili-south	\N	25	149	2017-11-12 18:50:06.849667	2017-11-12 18:50:06.849683
496	Ihiala	ihiala	\N	25	149	2017-11-12 18:50:06.861129	2017-11-12 18:50:06.861143
497	Njikoka	njikoka	\N	25	149	2017-11-12 18:50:06.876632	2017-11-12 18:50:06.876652
498	Nnewi North	nnewi-north	\N	25	149	2017-11-12 18:50:06.892368	2017-11-12 18:50:06.892389
499	Nnewi South	nnewi-south	\N	25	149	2017-11-12 18:50:06.908283	2017-11-12 18:50:06.908304
500	Ogbaru	ogbaru	\N	25	149	2017-11-12 18:50:06.925194	2017-11-12 18:50:06.925217
501	Onitsha North	onitsha-north	\N	25	149	2017-11-12 18:50:06.938429	2017-11-12 18:50:06.938454
502	Onitsha South	onitsha-south	\N	25	149	2017-11-12 18:50:06.948243	2017-11-12 18:50:06.948255
503	Orumba North	orumba-north	\N	25	149	2017-11-12 18:50:06.959367	2017-11-12 18:50:06.959383
504	Orumba South	orumba-south	\N	25	149	2017-11-12 18:50:06.970762	2017-11-12 18:50:06.970778
505	Oyi	oyi	\N	25	149	2017-11-12 18:50:06.984407	2017-11-12 18:50:06.984425
506	Aninri	aninri	\N	26	149	2017-11-12 18:50:06.997039	2017-11-12 18:50:06.997057
507	Awgu	awgu	\N	26	149	2017-11-12 18:50:07.00943	2017-11-12 18:50:07.009442
508	Enugu East	enugu-east	\N	26	149	2017-11-12 18:50:07.020657	2017-11-12 18:50:07.020678
509	Enugu North	enugu-north	\N	26	149	2017-11-12 18:50:07.032222	2017-11-12 18:50:07.032243
510	Enugu South	enugu-south	\N	26	149	2017-11-12 18:50:07.044617	2017-11-12 18:50:07.044633
511	Ezeagu	ezeagu	\N	26	149	2017-11-12 18:50:07.058114	2017-11-12 18:50:07.058148
512	Igbo Etiti	igbo-etiti	\N	26	149	2017-11-12 18:50:07.068396	2017-11-12 18:50:07.068412
513	Igbo Eze North	igbo-eze-north	\N	26	149	2017-11-12 18:50:07.079657	2017-11-12 18:50:07.07968
514	Igbo Eze South	igbo-eze-south	\N	26	149	2017-11-12 18:50:07.091516	2017-11-12 18:50:07.09153
515	Isi Uzo	isi-uzo	\N	26	149	2017-11-12 18:50:07.104363	2017-11-12 18:50:07.104382
516	Nkanu East	nkanu-east	\N	26	149	2017-11-12 18:50:07.119718	2017-11-12 18:50:07.119742
517	Nkanu West	nkanu-west	\N	26	149	2017-11-12 18:50:07.132004	2017-11-12 18:50:07.132015
518	Nsukka	nsukka	\N	26	149	2017-11-12 18:50:07.143087	2017-11-12 18:50:07.143098
519	Oji River	oji-river	\N	26	149	2017-11-12 18:50:07.15283	2017-11-12 18:50:07.152848
520	Udenu	udenu	\N	26	149	2017-11-12 18:50:07.164649	2017-11-12 18:50:07.164667
521	Udi	udi	\N	26	149	2017-11-12 18:50:07.175209	2017-11-12 18:50:07.175221
522	Uzo Uwani	uzo-uwani	\N	26	149	2017-11-12 18:50:07.183424	2017-11-12 18:50:07.183431
523	Demsa	demsa	\N	27	149	2017-11-12 18:50:07.192756	2017-11-12 18:50:07.192767
524	Fufure	fufure	\N	27	149	2017-11-12 18:50:07.202567	2017-11-12 18:50:07.20258
525	Ganye	ganye	\N	27	149	2017-11-12 18:50:07.212658	2017-11-12 18:50:07.212673
526	Gayuk	gayuk	\N	27	149	2017-11-12 18:50:07.225421	2017-11-12 18:50:07.225437
527	Gombi	gombi	\N	27	149	2017-11-12 18:50:07.237989	2017-11-12 18:50:07.238012
528	Grie	grie	\N	27	149	2017-11-12 18:50:07.248329	2017-11-12 18:50:07.248345
529	Hong	hong	\N	27	149	2017-11-12 18:50:07.25983	2017-11-12 18:50:07.259863
530	Jada	jada	\N	27	149	2017-11-12 18:50:07.271597	2017-11-12 18:50:07.271617
531	Larmurde	larmurde	\N	27	149	2017-11-12 18:50:07.282822	2017-11-12 18:50:07.282836
532	Madagali	madagali	\N	27	149	2017-11-12 18:50:07.295814	2017-11-12 18:50:07.295832
533	Maiha	maiha	\N	27	149	2017-11-12 18:50:07.307855	2017-11-12 18:50:07.307875
534	Mayo Belwa	mayo-belwa	\N	27	149	2017-11-12 18:50:07.318113	2017-11-12 18:50:07.318131
535	Michika	michika	\N	27	149	2017-11-12 18:50:07.331556	2017-11-12 18:50:07.331572
536	Mubi North	mubi-north	\N	27	149	2017-11-12 18:50:07.342859	2017-11-12 18:50:07.342877
537	Mubi South	mubi-south	\N	27	149	2017-11-12 18:50:07.353789	2017-11-12 18:50:07.353805
538	Numan	numan	\N	27	149	2017-11-12 18:50:07.368562	2017-11-12 18:50:07.368577
539	Shelleng	shelleng	\N	27	149	2017-11-12 18:50:07.379738	2017-11-12 18:50:07.37975
540	Song	song	\N	27	149	2017-11-12 18:50:07.390097	2017-11-12 18:50:07.390108
541	Toungo	toungo	\N	27	149	2017-11-12 18:50:07.401835	2017-11-12 18:50:07.401849
542	Yola North	yola-north	\N	27	149	2017-11-12 18:50:07.415099	2017-11-12 18:50:07.41511
543	Yola South	yola-south	\N	27	149	2017-11-12 18:50:07.427038	2017-11-12 18:50:07.427063
544	Akoko-Edo	akoko-edo	\N	28	149	2017-11-12 18:50:07.438512	2017-11-12 18:50:07.438525
545	Egor	egor	\N	28	149	2017-11-12 18:50:07.451001	2017-11-12 18:50:07.451019
546	Esan Central	esan-central	\N	28	149	2017-11-12 18:50:07.461421	2017-11-12 18:50:07.461439
547	Esan North-East	esan-north-east	\N	28	149	2017-11-12 18:50:07.474464	2017-11-12 18:50:07.474475
548	Esan South-East	esan-south-east	\N	28	149	2017-11-12 18:50:07.485102	2017-11-12 18:50:07.485121
549	Esan West	esan-west	\N	28	149	2017-11-12 18:50:07.494698	2017-11-12 18:50:07.494708
550	Etsako Central	etsako-central	\N	28	149	2017-11-12 18:50:07.501738	2017-11-12 18:50:07.501747
551	Etsako East	etsako-east	\N	28	149	2017-11-12 18:50:07.510771	2017-11-12 18:50:07.51078
552	Etsako West	etsako-west	\N	28	149	2017-11-12 18:50:07.519103	2017-11-12 18:50:07.519111
553	Igueben	igueben	\N	28	149	2017-11-12 18:50:07.528466	2017-11-12 18:50:07.528474
554	Ikpoba Okha	ikpoba-okha	\N	28	149	2017-11-12 18:50:07.53659	2017-11-12 18:50:07.536599
555	Orhionmwon	orhionmwon	\N	28	149	2017-11-12 18:50:07.546609	2017-11-12 18:50:07.546617
556	Oredo	oredo	\N	28	149	2017-11-12 18:50:07.556056	2017-11-12 18:50:07.55607
557	Ovia North-East	ovia-north-east	\N	28	149	2017-11-12 18:50:07.569171	2017-11-12 18:50:07.569185
558	Ovia South-West	ovia-south-west	\N	28	149	2017-11-12 18:50:07.582976	2017-11-12 18:50:07.582999
559	Owan East	owan-east	\N	28	149	2017-11-12 18:50:07.596929	2017-11-12 18:50:07.596951
560	Owan West	owan-west	\N	28	149	2017-11-12 18:50:07.611015	2017-11-12 18:50:07.611031
561	Uhunmwonde	uhunmwonde	\N	28	149	2017-11-12 18:50:07.625091	2017-11-12 18:50:07.625107
562	Agatu	agatu	\N	29	149	2017-11-12 18:50:07.63593	2017-11-12 18:50:07.635945
563	Apa	apa	\N	29	149	2017-11-12 18:50:07.649264	2017-11-12 18:50:07.649285
564	Ado	ado	\N	29	149	2017-11-12 18:50:07.662296	2017-11-12 18:50:07.662303
565	Buruku	buruku	\N	29	149	2017-11-12 18:50:07.669906	2017-11-12 18:50:07.669914
566	Gboko	gboko	\N	29	149	2017-11-12 18:50:07.681689	2017-11-12 18:50:07.681708
567	Guma	guma	\N	29	149	2017-11-12 18:50:07.69341	2017-11-12 18:50:07.693428
568	Gwer East	gwer-east	\N	29	149	2017-11-12 18:50:07.704347	2017-11-12 18:50:07.704363
569	Gwer West	gwer-west	\N	29	149	2017-11-12 18:50:07.717451	2017-11-12 18:50:07.717469
570	Katsina-Ala	katsina-ala	\N	29	149	2017-11-12 18:50:07.732128	2017-11-12 18:50:07.732142
571	Konshisha	konshisha	\N	29	149	2017-11-12 18:50:07.744518	2017-11-12 18:50:07.74453
572	Kwande	kwande	\N	29	149	2017-11-12 18:50:07.754853	2017-11-12 18:50:07.754871
573	Logo	logo	\N	29	149	2017-11-12 18:50:07.76551	2017-11-12 18:50:07.765525
574	Makurdi	makurdi	\N	29	149	2017-11-12 18:50:07.777201	2017-11-12 18:50:07.777217
575	Obi, Benue State	obi-benue-state	\N	29	149	2017-11-12 18:50:07.791456	2017-11-12 18:50:07.791487
576	Ogbadibo	ogbadibo	\N	29	149	2017-11-12 18:50:07.80419	2017-11-12 18:50:07.804215
577	Ohimini	ohimini	\N	29	149	2017-11-12 18:50:07.815007	2017-11-12 18:50:07.81502
578	Oju	oju	\N	29	149	2017-11-12 18:50:07.825533	2017-11-12 18:50:07.825547
579	Okpokwu	okpokwu	\N	29	149	2017-11-12 18:50:07.836476	2017-11-12 18:50:07.836492
580	Oturkpo	oturkpo	\N	29	149	2017-11-12 18:50:07.847351	2017-11-12 18:50:07.847365
581	Tarka	tarka	\N	29	149	2017-11-12 18:50:07.858823	2017-11-12 18:50:07.858837
582	Ukum	ukum	\N	29	149	2017-11-12 18:50:07.870007	2017-11-12 18:50:07.870021
583	Ushongo	ushongo	\N	29	149	2017-11-12 18:50:07.88293	2017-11-12 18:50:07.882952
584	Vandeikya	vandeikya	\N	29	149	2017-11-12 18:50:07.894598	2017-11-12 18:50:07.894615
585	Bokkos	bokkos	\N	30	149	2017-11-12 18:50:07.905243	2017-11-12 18:50:07.905267
586	Barkin Ladi	barkin-ladi	\N	30	149	2017-11-12 18:50:07.917782	2017-11-12 18:50:07.9178
587	Bassa	bassa	\N	30	149	2017-11-12 18:50:07.930093	2017-11-12 18:50:07.930107
588	Jos East	jos-east	\N	30	149	2017-11-12 18:50:07.94177	2017-11-12 18:50:07.941791
589	Jos North	jos-north	\N	30	149	2017-11-12 18:50:07.952514	2017-11-12 18:50:07.95253
590	Jos South	jos-south	\N	30	149	2017-11-12 18:50:07.966058	2017-11-12 18:50:07.966078
591	Kanam	kanam	\N	30	149	2017-11-12 18:50:07.980009	2017-11-12 18:50:07.980031
592	Kanke	kanke	\N	30	149	2017-11-12 18:50:07.990778	2017-11-12 18:50:07.990797
593	Langtang South	langtang-south	\N	30	149	2017-11-12 18:50:08.002247	2017-11-12 18:50:08.002263
594	Langtang North	langtang-north	\N	30	149	2017-11-12 18:50:08.015802	2017-11-12 18:50:08.015824
595	Mangu	mangu	\N	30	149	2017-11-12 18:50:08.02943	2017-11-12 18:50:08.029443
596	Mikang	mikang	\N	30	149	2017-11-12 18:50:08.040392	2017-11-12 18:50:08.040426
597	Pankshin	pankshin	\N	30	149	2017-11-12 18:50:08.054223	2017-11-12 18:50:08.054244
598	Qua''an Pan	qua-an-pan	\N	30	149	2017-11-12 18:50:08.068169	2017-11-12 18:50:08.068182
599	Riyom	riyom	\N	30	149	2017-11-12 18:50:08.078781	2017-11-12 18:50:08.078794
600	Shendam	shendam	\N	30	149	2017-11-12 18:50:08.090092	2017-11-12 18:50:08.090111
601	Wase	wase	\N	30	149	2017-11-12 18:50:08.100147	2017-11-12 18:50:08.100158
602	Aleiro	aleiro	\N	31	149	2017-11-12 18:50:08.114633	2017-11-12 18:50:08.114654
603	Arewa Dandi	arewa-dandi	\N	31	149	2017-11-12 18:50:08.12854	2017-11-12 18:50:08.128552
604	Argungu	argungu	\N	31	149	2017-11-12 18:50:08.137792	2017-11-12 18:50:08.137805
605	Augie	augie	\N	31	149	2017-11-12 18:50:08.149171	2017-11-12 18:50:08.149187
606	Bagudo	bagudo	\N	31	149	2017-11-12 18:50:08.16279	2017-11-12 18:50:08.162807
607	Birnin Kebbi	birnin-kebbi	\N	31	149	2017-11-12 18:50:08.174988	2017-11-12 18:50:08.17501
608	Bunza	bunza	\N	31	149	2017-11-12 18:50:08.187597	2017-11-12 18:50:08.187612
609	Dandi	dandi	\N	31	149	2017-11-12 18:50:08.199148	2017-11-12 18:50:08.199174
610	Fakai	fakai	\N	31	149	2017-11-12 18:50:08.211625	2017-11-12 18:50:08.211643
611	Gwandu	gwandu	\N	31	149	2017-11-12 18:50:08.220482	2017-11-12 18:50:08.220492
612	Jega	jega	\N	31	149	2017-11-12 18:50:08.232814	2017-11-12 18:50:08.232834
613	Kalgo	kalgo	\N	31	149	2017-11-12 18:50:08.244291	2017-11-12 18:50:08.244303
614	Koko/Besse	koko-besse	\N	31	149	2017-11-12 18:50:08.253722	2017-11-12 18:50:08.253737
615	Maiyama	maiyama	\N	31	149	2017-11-12 18:50:08.264474	2017-11-12 18:50:08.264488
616	Ngaski	ngaski	\N	31	149	2017-11-12 18:50:08.274449	2017-11-12 18:50:08.274465
617	Sakaba	sakaba	\N	31	149	2017-11-12 18:50:08.283409	2017-11-12 18:50:08.283421
618	Shanga	shanga	\N	31	149	2017-11-12 18:50:08.294192	2017-11-12 18:50:08.294203
619	Suru	suru	\N	31	149	2017-11-12 18:50:08.303674	2017-11-12 18:50:08.303686
620	Wasagu/Danko	wasagu-danko	\N	31	149	2017-11-12 18:50:08.313803	2017-11-12 18:50:08.313819
621	Yauri	yauri	\N	31	149	2017-11-12 18:50:08.324355	2017-11-12 18:50:08.324374
622	Zuru	zuru	\N	31	149	2017-11-12 18:50:08.334193	2017-11-12 18:50:08.334207
623	Abakaliki	abakaliki	\N	32	149	2017-11-12 18:50:08.345109	2017-11-12 18:50:08.345124
624	Afikpo North	afikpo-north	\N	32	149	2017-11-12 18:50:08.358378	2017-11-12 18:50:08.358398
625	Afikpo South	afikpo-south	\N	32	149	2017-11-12 18:50:08.369344	2017-11-12 18:50:08.369358
626	Ebonyi	ebonyi	\N	32	149	2017-11-12 18:50:08.381468	2017-11-12 18:50:08.381482
627	Ezza North	ezza-north	\N	32	149	2017-11-12 18:50:08.392944	2017-11-12 18:50:08.392959
628	Ezza South	ezza-south	\N	32	149	2017-11-12 18:50:08.403829	2017-11-12 18:50:08.403905
629	Ikwo	ikwo	\N	32	149	2017-11-12 18:50:08.419199	2017-11-12 18:50:08.419214
630	Ishielu	ishielu	\N	32	149	2017-11-12 18:50:08.430955	2017-11-12 18:50:08.430971
631	Ivo	ivo	\N	32	149	2017-11-12 18:50:08.443678	2017-11-12 18:50:08.443702
632	Izzi	izzi	\N	32	149	2017-11-12 18:50:08.4579	2017-11-12 18:50:08.457927
633	Ohaozara	ohaozara	\N	32	149	2017-11-12 18:50:08.477538	2017-11-12 18:50:08.477552
634	Ohaukwu	ohaukwu	\N	32	149	2017-11-12 18:50:08.486923	2017-11-12 18:50:08.48693
635	Onicha	onicha	\N	32	149	2017-11-12 18:50:08.49574	2017-11-12 18:50:08.495751
636	Alimosho Akowonjo	alimosho-akowonjo	\N	33	149	2017-11-12 18:50:08.505077	2017-11-12 18:50:08.505085
637	Agege Sango	agege-sango	\N	33	149	2017-11-12 18:50:08.512601	2017-11-12 18:50:08.51261
638	Ajeromi Ifelodun Ajegunle	ajeromi-ifelodun-ajegunle	\N	33	149	2017-11-12 18:50:08.524257	2017-11-12 18:50:08.524279
639	Apapa	apapa	\N	33	149	2017-11-12 18:50:08.536834	2017-11-12 18:50:08.536856
640	Amuwo Odofin	amuwo-odofin	\N	33	149	2017-11-12 18:50:08.551676	2017-11-12 18:50:08.551713
641	Badagry	badagry	\N	33	149	2017-11-12 18:50:08.56747	2017-11-12 18:50:08.567484
642	Ita-Maru Epe	ita-maru-epe	\N	33	149	2017-11-12 18:50:08.579429	2017-11-12 18:50:08.579439
643	Igbo-Ifon, Eti Osa West	igbo-ifon-eti-osa-west	\N	33	149	2017-11-12 18:50:08.594568	2017-11-12 18:50:08.594595
644	Ibeju-Lekki	ibeju-lekki	\N	33	149	2017-11-12 18:50:08.60906	2017-11-12 18:50:08.609071
645	Ifako Ijaiye	ifako-ijaiye	\N	33	149	2017-11-12 18:50:08.622884	2017-11-12 18:50:08.622905
646	Ikeja	ikeja	\N	33	149	2017-11-12 18:50:08.634295	2017-11-12 18:50:08.634317
647	Ikorodu	ikorodu	\N	33	149	2017-11-12 18:50:08.6485	2017-11-12 18:50:08.648519
648	Kosofe Ojota	kosofe-ojota	\N	33	149	2017-11-12 18:50:08.661464	2017-11-12 18:50:08.661486
649	Lagos Mainland, Ebute-Metta	lagos-mainland-ebute-metta	\N	33	149	2017-11-12 18:50:08.674346	2017-11-12 18:50:08.674361
650	Lagos Island, Isale-Eko	lagos-island-isale-eko	\N	33	149	2017-11-12 18:50:08.688534	2017-11-12 18:50:08.688548
651	Mushin	mushin	\N	33	149	2017-11-12 18:50:08.703109	2017-11-12 18:50:08.703138
652	Ojo	ojo	\N	33	149	2017-11-12 18:50:08.719749	2017-11-12 18:50:08.719786
653	Oshodi Isolo	oshodi-isolo	\N	33	149	2017-11-12 18:50:08.736053	2017-11-12 18:50:08.736085
654	Shomolu	shomolu	\N	33	149	2017-11-12 18:50:08.749444	2017-11-12 18:50:08.749455
655	Surulere	surulere	\N	33	149	2017-11-12 18:50:08.763206	2017-11-12 18:50:08.76322
656	Abule Egba, Agbado Okeodo	abule-egba-agbado-okeodo	\N	33	149	2017-11-12 18:50:08.778302	2017-11-12 18:50:08.778319
657	Alapere, Ketu	alapere-ketu	\N	33	149	2017-11-12 18:50:08.796109	2017-11-12 18:50:08.796124
658	Badiya, Apapa Iganmu	badiya-apapa-iganmu	\N	33	149	2017-11-12 18:50:08.809511	2017-11-12 18:50:08.809543
659	Ayobo, Ipaja	ayobo-ipaja	\N	33	149	2017-11-12 18:50:08.822751	2017-11-12 18:50:08.822777
660	Badagry West Kankon	badagry-west-kankon	\N	33	149	2017-11-12 18:50:08.833741	2017-11-12 18:50:08.833749
661	Pedro Bariga	pedro-bariga	\N	33	149	2017-11-12 18:50:08.846539	2017-11-12 18:50:08.846562
662	Coker Aguda	coker-aguda	\N	33	149	2017-11-12 18:50:08.858799	2017-11-12 18:50:08.858814
663	Isheri-Olofin, Egbe Idimu	isheri-olofin-egbe-idimu	\N	33	149	2017-11-12 18:50:08.87231	2017-11-12 18:50:08.872322
664	Eredo, Epe/Ijebu	eredo-epe-ijebu	\N	33	149	2017-11-12 18:50:08.885323	2017-11-12 18:50:08.885344
665	Oyonka, Iba	oyonka-iba	\N	33	149	2017-11-12 18:50:08.897721	2017-11-12 18:50:08.897732
666	Eti-Osa East, Ajah	eti-osa-east-ajah	\N	33	149	2017-11-12 18:50:08.910554	2017-11-12 18:50:08.910569
667	Ifelodun, Amukoko	ifelodun-amukoko	\N	33	149	2017-11-12 18:50:08.924139	2017-11-12 18:50:08.924162
668	Igbogbo Baiyeku	igbogbo-baiyeku	\N	33	149	2017-11-12 18:50:08.936889	2017-11-12 18:50:08.936916
669	Ijede, Maidan	ijede-maidan	\N	33	149	2017-11-12 18:50:08.948429	2017-11-12 18:50:08.948442
670	Odogunyan, Ikorodu North	odogunyan-ikorodu-north	\N	33	149	2017-11-12 18:50:08.962	2017-11-12 18:50:08.962018
671	Owutu, Ikorodu West	owutu-ikorodu-west	\N	33	149	2017-11-12 18:50:08.980631	2017-11-12 18:50:08.980645
672	Ikosi Ejinrin, Agbowa	ikosi-ejinrin-agbowa	\N	33	149	2017-11-12 18:50:08.992458	2017-11-12 18:50:08.992483
673	Ikosi Isheri	ikosi-isheri	\N	33	149	2017-11-12 18:50:09.006014	2017-11-12 18:50:09.006031
674	Igando, Ikotun	igando-ikotun	\N	33	149	2017-11-12 18:50:09.02192	2017-11-12 18:50:09.021939
675	Ikoyi Obalende	ikoyi-obalende	\N	33	149	2017-11-12 18:50:09.038331	2017-11-12 18:50:09.038351
676	Imota Ebute-Ajebo	imota-ebute-ajebo	\N	33	149	2017-11-12 18:50:09.049274	2017-11-12 18:50:09.049283
677	Iru-Victoria Island	iru-victoria-island	\N	33	149	2017-11-12 18:50:09.062608	2017-11-12 18:50:09.062623
678	Isolo	isolo	\N	33	149	2017-11-12 18:50:09.076006	2017-11-12 18:50:09.076025
679	Ikate Itire	ikate-itire	\N	33	149	2017-11-12 18:50:09.089001	2017-11-12 18:50:09.089016
680	Kakawa, Lagos Island East	kakawa-lagos-island-east	\N	33	149	2017-11-12 18:50:09.099236	2017-11-12 18:50:09.099246
681	Mosan Okunola	mosan-okunola	\N	33	149	2017-11-12 18:50:09.113424	2017-11-12 18:50:09.113442
682	Odi-Olowo, Ilupeju	odi-olowo-ilupeju	\N	33	149	2017-11-12 18:50:09.128227	2017-11-12 18:50:09.128242
683	Oke-Ira, Ojodu	oke-ira-ojodu	\N	33	149	2017-11-12 18:50:09.142534	2017-11-12 18:50:09.142557
684	Ojokoro, Ijaye	ojokoro-ijaye	\N	33	149	2017-11-12 18:50:09.157141	2017-11-12 18:50:09.157158
685	Olorunda, Iworo	olorunda-iworo	\N	33	149	2017-11-12 18:50:09.167963	2017-11-12 18:50:09.167984
686	Onigbongbo, Opebi	onigbongbo-opebi	\N	33	149	2017-11-12 18:50:09.180272	2017-11-12 18:50:09.180289
687	Oriade Ijegun-Ibasa	oriade-ijegun-ibasa	\N	33	149	2017-11-12 18:50:09.194208	2017-11-12 18:50:09.194229
688	Abekoko, Orile Agege	abekoko-orile-agege	\N	33	149	2017-11-12 18:50:09.208487	2017-11-12 18:50:09.2085
689	Oto Awori, Ijaniki	oto-awori-ijaniki	\N	33	149	2017-11-12 18:50:09.216392	2017-11-12 18:50:09.216403
690	Adekunle, Yaba	adekunle-yaba	\N	33	149	2017-11-12 18:50:09.230024	2017-11-12 18:50:09.230043
691	Ajingi	ajingi	\N	34	149	2017-11-12 18:50:09.24271	2017-11-12 18:50:09.242729
692	Albasu	albasu	\N	34	149	2017-11-12 18:50:09.255374	2017-11-12 18:50:09.255397
693	Bagwai	bagwai	\N	34	149	2017-11-12 18:50:09.266329	2017-11-12 18:50:09.26635
694	Bebeji	bebeji	\N	34	149	2017-11-12 18:50:09.278456	2017-11-12 18:50:09.278479
695	Bichi	bichi	\N	34	149	2017-11-12 18:50:09.290431	2017-11-12 18:50:09.290446
696	Bunkure	bunkure	\N	34	149	2017-11-12 18:50:09.303543	2017-11-12 18:50:09.303566
697	Dala	dala	\N	34	149	2017-11-12 18:50:09.314055	2017-11-12 18:50:09.314069
698	Dambatta	dambatta	\N	34	149	2017-11-12 18:50:09.326759	2017-11-12 18:50:09.326776
699	Dawakin Kudu	dawakin-kudu	\N	34	149	2017-11-12 18:50:09.336354	2017-11-12 18:50:09.336373
700	Dawakin Tofa	dawakin-tofa	\N	34	149	2017-11-12 18:50:09.3473	2017-11-12 18:50:09.347321
701	Doguwa	doguwa	\N	34	149	2017-11-12 18:50:09.358348	2017-11-12 18:50:09.358366
702	Fagge	fagge	\N	34	149	2017-11-12 18:50:09.368773	2017-11-12 18:50:09.368793
703	Gabasawa	gabasawa	\N	34	149	2017-11-12 18:50:09.379132	2017-11-12 18:50:09.379146
704	Garko	garko	\N	34	149	2017-11-12 18:50:09.39048	2017-11-12 18:50:09.390494
705	Garun Mallam	garun-mallam	\N	34	149	2017-11-12 18:50:09.403082	2017-11-12 18:50:09.403109
706	Gaya	gaya	\N	34	149	2017-11-12 18:50:09.414867	2017-11-12 18:50:09.414878
707	Gezawa	gezawa	\N	34	149	2017-11-12 18:50:09.425597	2017-11-12 18:50:09.425608
708	Gwale	gwale	\N	34	149	2017-11-12 18:50:09.433916	2017-11-12 18:50:09.433929
709	Gwarzo	gwarzo	\N	34	149	2017-11-12 18:50:09.446524	2017-11-12 18:50:09.446538
710	Kabo	kabo	\N	34	149	2017-11-12 18:50:09.455852	2017-11-12 18:50:09.455862
711	Kano Municipal	kano-municipal	\N	34	149	2017-11-12 18:50:09.465642	2017-11-12 18:50:09.465661
712	Karaye	karaye	\N	34	149	2017-11-12 18:50:09.478843	2017-11-12 18:50:09.47887
713	Kibiya	kibiya	\N	34	149	2017-11-12 18:50:09.489648	2017-11-12 18:50:09.489665
714	Kiru	kiru	\N	34	149	2017-11-12 18:50:09.496177	2017-11-12 18:50:09.496184
715	Kumbotso	kumbotso	\N	34	149	2017-11-12 18:50:09.501686	2017-11-12 18:50:09.501695
716	Kunchi	kunchi	\N	34	149	2017-11-12 18:50:09.510835	2017-11-12 18:50:09.510844
717	Kura	kura	\N	34	149	2017-11-12 18:50:09.516931	2017-11-12 18:50:09.51694
718	Madobi	madobi	\N	34	149	2017-11-12 18:50:09.525869	2017-11-12 18:50:09.525877
719	Makoda	makoda	\N	34	149	2017-11-12 18:50:09.532074	2017-11-12 18:50:09.532082
720	Minjibir	minjibir	\N	34	149	2017-11-12 18:50:09.538382	2017-11-12 18:50:09.538391
721	Nassarawa, Kano State	nassarawa-kano-state	\N	34	149	2017-11-12 18:50:09.547235	2017-11-12 18:50:09.547244
722	Rano	rano	\N	34	149	2017-11-12 18:50:09.553021	2017-11-12 18:50:09.55303
723	Rimin Gado	rimin-gado	\N	34	149	2017-11-12 18:50:09.559999	2017-11-12 18:50:09.56001
724	Rogo	rogo	\N	34	149	2017-11-12 18:50:09.566802	2017-11-12 18:50:09.566815
725	Shanono	shanono	\N	34	149	2017-11-12 18:50:09.573048	2017-11-12 18:50:09.573056
726	Sumaila	sumaila	\N	34	149	2017-11-12 18:50:09.581784	2017-11-12 18:50:09.581793
727	Takai	takai	\N	34	149	2017-11-12 18:50:09.591905	2017-11-12 18:50:09.591926
728	Tarauni	tarauni	\N	34	149	2017-11-12 18:50:09.601082	2017-11-12 18:50:09.601096
729	Tofa	tofa	\N	34	149	2017-11-12 18:50:09.611707	2017-11-12 18:50:09.611722
730	Tsanyawa	tsanyawa	\N	34	149	2017-11-12 18:50:09.623137	2017-11-12 18:50:09.623154
731	Tudun Wada	tudun-wada	\N	34	149	2017-11-12 18:50:09.633996	2017-11-12 18:50:09.634009
732	Ungogo	ungogo	\N	34	149	2017-11-12 18:50:09.643036	2017-11-12 18:50:09.643048
733	Warawa	warawa	\N	34	149	2017-11-12 18:50:09.6513	2017-11-12 18:50:09.651315
734	Wudil	wudil	\N	34	149	2017-11-12 18:50:09.662472	2017-11-12 18:50:09.662484
735	Abeokuta North	abeokuta-north	\N	35	149	2017-11-12 18:50:09.672954	2017-11-12 18:50:09.672968
736	Abeokuta South	abeokuta-south	\N	35	149	2017-11-12 18:50:09.681546	2017-11-12 18:50:09.681564
737	Ado-Odo/Ota	ado-odo-ota	\N	35	149	2017-11-12 18:50:09.692919	2017-11-12 18:50:09.692938
738	Egbado North	egbado-north	\N	35	149	2017-11-12 18:50:09.702644	2017-11-12 18:50:09.702659
739	Egbado South	egbado-south	\N	35	149	2017-11-12 18:50:09.711952	2017-11-12 18:50:09.711966
740	Ewekoro	ewekoro	\N	35	149	2017-11-12 18:50:09.720825	2017-11-12 18:50:09.720835
741	Ifo	ifo	\N	35	149	2017-11-12 18:50:09.729766	2017-11-12 18:50:09.72978
742	Ijebu East	ijebu-east	\N	35	149	2017-11-12 18:50:09.738872	2017-11-12 18:50:09.738894
743	Ijebu North	ijebu-north	\N	35	149	2017-11-12 18:50:09.749282	2017-11-12 18:50:09.749296
744	Ijebu North East	ijebu-north-east	\N	35	149	2017-11-12 18:50:09.758586	2017-11-12 18:50:09.758597
745	Ijebu Ode	ijebu-ode	\N	35	149	2017-11-12 18:50:09.767258	2017-11-12 18:50:09.767276
746	Ikenne	ikenne	\N	35	149	2017-11-12 18:50:09.777784	2017-11-12 18:50:09.777819
747	Imeko Afon	imeko-afon	\N	35	149	2017-11-12 18:50:09.791699	2017-11-12 18:50:09.791713
748	Ipokia	ipokia	\N	35	149	2017-11-12 18:50:09.804068	2017-11-12 18:50:09.804081
749	Obafemi Owode	obafemi-owode	\N	35	149	2017-11-12 18:50:09.816466	2017-11-12 18:50:09.816485
750	Odeda	odeda	\N	35	149	2017-11-12 18:50:09.829455	2017-11-12 18:50:09.829467
751	Odogbolu	odogbolu	\N	35	149	2017-11-12 18:50:09.843639	2017-11-12 18:50:09.843677
752	Ogun Waterside	ogun-waterside	\N	35	149	2017-11-12 18:50:09.859123	2017-11-12 18:50:09.859143
753	Remo North	remo-north	\N	35	149	2017-11-12 18:50:09.873697	2017-11-12 18:50:09.873706
754	Shagamu	shagamu	\N	35	149	2017-11-12 18:50:09.887159	2017-11-12 18:50:09.887181
755	Bakori	bakori	\N	36	149	2017-11-12 18:50:09.901489	2017-11-12 18:50:09.901509
756	Batagarawa	batagarawa	\N	36	149	2017-11-12 18:50:09.914816	2017-11-12 18:50:09.91484
757	Batsari	batsari	\N	36	149	2017-11-12 18:50:09.927729	2017-11-12 18:50:09.927746
758	Baure	baure	\N	36	149	2017-11-12 18:50:09.941545	2017-11-12 18:50:09.941563
759	Bindawa	bindawa	\N	36	149	2017-11-12 18:50:09.956891	2017-11-12 18:50:09.956913
760	Charanchi	charanchi	\N	36	149	2017-11-12 18:50:09.970684	2017-11-12 18:50:09.970699
761	Dandume	dandume	\N	36	149	2017-11-12 18:50:09.986341	2017-11-12 18:50:09.98637
762	Danja	danja	\N	36	149	2017-11-12 18:50:09.998533	2017-11-12 18:50:09.998544
763	Dan Musa	dan-musa	\N	36	149	2017-11-12 18:50:10.014561	2017-11-12 18:50:10.014574
764	Daura	daura	\N	36	149	2017-11-12 18:50:10.029617	2017-11-12 18:50:10.029635
765	Dutsi	dutsi	\N	36	149	2017-11-12 18:50:10.043891	2017-11-12 18:50:10.043905
766	Dutsin Ma	dutsin-ma	\N	36	149	2017-11-12 18:50:10.059551	2017-11-12 18:50:10.059573
767	Faskari	faskari	\N	36	149	2017-11-12 18:50:10.075421	2017-11-12 18:50:10.075443
768	Funtua	funtua	\N	36	149	2017-11-12 18:50:10.0889	2017-11-12 18:50:10.088922
769	Ingawa	ingawa	\N	36	149	2017-11-12 18:50:10.100153	2017-11-12 18:50:10.100169
770	Jibia	jibia	\N	36	149	2017-11-12 18:50:10.11065	2017-11-12 18:50:10.110658
771	Kafur	kafur	\N	36	149	2017-11-12 18:50:10.121006	2017-11-12 18:50:10.121021
772	Kaita	kaita	\N	36	149	2017-11-12 18:50:10.129519	2017-11-12 18:50:10.129526
773	Kankara	kankara	\N	36	149	2017-11-12 18:50:10.139677	2017-11-12 18:50:10.139684
774	Kankia	kankia	\N	36	149	2017-11-12 18:50:10.150003	2017-11-12 18:50:10.150028
775	Katsina	katsina	\N	36	149	2017-11-12 18:50:10.164413	2017-11-12 18:50:10.164428
776	Kurfi	kurfi	\N	36	149	2017-11-12 18:50:10.177537	2017-11-12 18:50:10.177555
777	Kusada	kusada	\N	36	149	2017-11-12 18:50:10.190988	2017-11-12 18:50:10.191006
778	Mai''Adua	mai-adua	\N	36	149	2017-11-12 18:50:10.203688	2017-11-12 18:50:10.203701
779	Malumfashi	malumfashi	\N	36	149	2017-11-12 18:50:10.214577	2017-11-12 18:50:10.214588
780	Mani	mani	\N	36	149	2017-11-12 18:50:10.227945	2017-11-12 18:50:10.227959
781	Mashi	mashi	\N	36	149	2017-11-12 18:50:10.242127	2017-11-12 18:50:10.24215
782	Matazu	matazu	\N	36	149	2017-11-12 18:50:10.256658	2017-11-12 18:50:10.256673
783	Musawa	musawa	\N	36	149	2017-11-12 18:50:10.270933	2017-11-12 18:50:10.270953
784	Rimi	rimi	\N	36	149	2017-11-12 18:50:10.285569	2017-11-12 18:50:10.285593
785	Sabuwa	sabuwa	\N	36	149	2017-11-12 18:50:10.298774	2017-11-12 18:50:10.298799
786	Safana	safana	\N	36	149	2017-11-12 18:50:10.310542	2017-11-12 18:50:10.310559
787	Sandamu	sandamu	\N	36	149	2017-11-12 18:50:10.323414	2017-11-12 18:50:10.323428
788	Zango	zango	\N	36	149	2017-11-12 18:50:10.337079	2017-11-12 18:50:10.337111
789	Adavi	adavi	\N	37	149	2017-11-12 18:50:10.350467	2017-11-12 18:50:10.350493
790	Ajaokuta	ajaokuta	\N	37	149	2017-11-12 18:50:10.364073	2017-11-12 18:50:10.364088
791	Ankpa	ankpa	\N	37	149	2017-11-12 18:50:10.377316	2017-11-12 18:50:10.377331
792	Bassa, Kogi State	bassa-kogi-state	\N	37	149	2017-11-12 18:50:10.39122	2017-11-12 18:50:10.391233
793	Dekina	dekina	\N	37	149	2017-11-12 18:50:10.40394	2017-11-12 18:50:10.403952
794	Ibaji	ibaji	\N	37	149	2017-11-12 18:50:10.413741	2017-11-12 18:50:10.413752
795	Idah	idah	\N	37	149	2017-11-12 18:50:10.424603	2017-11-12 18:50:10.424624
796	Igalamela Odolu	igalamela-odolu	\N	37	149	2017-11-12 18:50:10.438949	2017-11-12 18:50:10.438973
797	Ijumu	ijumu	\N	37	149	2017-11-12 18:50:10.454055	2017-11-12 18:50:10.454078
798	Kabba/Bunu	kabba-bunu	\N	37	149	2017-11-12 18:50:10.47178	2017-11-12 18:50:10.471796
799	Kogi	kogi	\N	37	149	2017-11-12 18:50:10.483415	2017-11-12 18:50:10.483443
800	Lokoja	lokoja	\N	37	149	2017-11-12 18:50:10.492772	2017-11-12 18:50:10.49278
801	Mopa Muro	mopa-muro	\N	37	149	2017-11-12 18:50:10.502408	2017-11-12 18:50:10.502422
802	Ofu	ofu	\N	37	149	2017-11-12 18:50:10.513764	2017-11-12 18:50:10.51379
803	Ogori/Magongo	ogori-magongo	\N	37	149	2017-11-12 18:50:10.525816	2017-11-12 18:50:10.52583
804	Okehi	okehi	\N	37	149	2017-11-12 18:50:10.540762	2017-11-12 18:50:10.54078
805	Okene	okene	\N	37	149	2017-11-12 18:50:10.553054	2017-11-12 18:50:10.553066
806	Olamaboro	olamaboro	\N	37	149	2017-11-12 18:50:10.559708	2017-11-12 18:50:10.559715
807	Omala	omala	\N	37	149	2017-11-12 18:50:10.566316	2017-11-12 18:50:10.566338
808	Yagba East	yagba-east	\N	37	149	2017-11-12 18:50:10.579134	2017-11-12 18:50:10.579148
809	Yagba West	yagba-west	\N	37	149	2017-11-12 18:50:10.593507	2017-11-12 18:50:10.593529
\.


--
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY client (id, name, address, city_id, state_id, country_id, email, url, sub_domain, last_updated, date_created) FROM stdin;
1	Nikola Tesla	Allen Avenue	646	33	149	demo@atele.org	http://atele.org	demo	2017-11-12 18:50:11.031233	2017-11-12 18:50:11.031258
\.


--
-- Data for Name: country; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY country (id, name, slug, code, enabled, last_updated, date_created) FROM stdin;
1	Bangladesh	bangladesh	BD	f	2017-11-12 18:50:00.628838	2017-11-12 18:50:00.628847
2	Belgium	belgium	BE	f	2017-11-12 18:50:00.632199	2017-11-12 18:50:00.632209
3	Burkina Faso	burkina-faso	BF	f	2017-11-12 18:50:00.633332	2017-11-12 18:50:00.633341
4	Bulgaria	bulgaria	BG	f	2017-11-12 18:50:00.634212	2017-11-12 18:50:00.634218
5	Bosnia and Herzegovina	bosnia-and-herzegovina	BA	f	2017-11-12 18:50:00.635156	2017-11-12 18:50:00.635161
6	Barbados	barbados	BB	f	2017-11-12 18:50:00.636109	2017-11-12 18:50:00.636114
7	Wallis and Futuna	wallis-and-futuna	WF	f	2017-11-12 18:50:00.636991	2017-11-12 18:50:00.636996
8	Johnston Island	johnston-island	JT	f	2017-11-12 18:50:00.637874	2017-11-12 18:50:00.637879
9	Bermuda	bermuda	BM	f	2017-11-12 18:50:00.638849	2017-11-12 18:50:00.638854
10	Brunei	brunei	BN	f	2017-11-12 18:50:00.639679	2017-11-12 18:50:00.639684
11	Bolivia	bolivia	BO	f	2017-11-12 18:50:00.640754	2017-11-12 18:50:00.640769
12	Bahrain	bahrain	BH	f	2017-11-12 18:50:00.642405	2017-11-12 18:50:00.64242
13	Burundi	burundi	BI	f	2017-11-12 18:50:00.643748	2017-11-12 18:50:00.643759
14	Benin	benin	BJ	f	2017-11-12 18:50:00.644916	2017-11-12 18:50:00.644926
15	Bhutan	bhutan	BT	f	2017-11-12 18:50:00.645979	2017-11-12 18:50:00.645992
16	Jamaica	jamaica	JM	f	2017-11-12 18:50:00.646892	2017-11-12 18:50:00.6469
17	Bouvet Island	bouvet-island	BV	f	2017-11-12 18:50:00.647737	2017-11-12 18:50:00.647747
18	Botswana	botswana	BW	f	2017-11-12 18:50:00.649238	2017-11-12 18:50:00.649253
19	Samoa	samoa	WS	f	2017-11-12 18:50:00.650955	2017-11-12 18:50:00.65097
20	British Antarctic Territory	british-antarctic-territory	BQ	f	2017-11-12 18:50:00.652494	2017-11-12 18:50:00.652509
21	Brazil	brazil	BR	f	2017-11-12 18:50:00.653897	2017-11-12 18:50:00.65391
22	Bahamas	bahamas	BS	f	2017-11-12 18:50:00.655618	2017-11-12 18:50:00.655633
23	Jersey	jersey	JE	f	2017-11-12 18:50:00.657074	2017-11-12 18:50:00.657087
24	Wake Island	wake-island	WK	f	2017-11-12 18:50:00.65859	2017-11-12 18:50:00.658608
25	Belarus	belarus	BY	f	2017-11-12 18:50:00.660524	2017-11-12 18:50:00.660545
26	Belize	belize	BZ	f	2017-11-12 18:50:00.661654	2017-11-12 18:50:00.661665
27	Russia	russia	RU	f	2017-11-12 18:50:00.662574	2017-11-12 18:50:00.662586
28	Rwanda	rwanda	RW	f	2017-11-12 18:50:00.663994	2017-11-12 18:50:00.664011
29	Pacific Islands Trust Territory	pacific-islands-trust-territory	PC	f	2017-11-12 18:50:00.665362	2017-11-12 18:50:00.665378
30	Timor-Leste	timor-leste	TL	f	2017-11-12 18:50:00.666883	2017-11-12 18:50:00.666899
31	Réunion	reunion	RE	f	2017-11-12 18:50:00.669092	2017-11-12 18:50:00.669107
32	Turkmenistan	turkmenistan	TM	f	2017-11-12 18:50:00.670394	2017-11-12 18:50:00.670406
33	Tajikistan	tajikistan	TJ	f	2017-11-12 18:50:00.671627	2017-11-12 18:50:00.671643
34	Romania	romania	RO	f	2017-11-12 18:50:00.67355	2017-11-12 18:50:00.673581
35	Tokelau	tokelau	TK	f	2017-11-12 18:50:00.675334	2017-11-12 18:50:00.675353
36	Guinea-Bissau	guinea-bissau	GW	f	2017-11-12 18:50:00.676931	2017-11-12 18:50:00.676946
37	Guam	guam	GU	f	2017-11-12 18:50:00.678586	2017-11-12 18:50:00.678602
38	Guatemala	guatemala	GT	f	2017-11-12 18:50:00.679986	2017-11-12 18:50:00.679998
39	South Georgia and the South Sandwich Islands	south-georgia-and-the-south-sandwich-islands	GS	f	2017-11-12 18:50:00.681465	2017-11-12 18:50:00.681485
40	Greece	greece	GR	f	2017-11-12 18:50:00.683223	2017-11-12 18:50:00.683242
41	Equatorial Guinea	equatorial-guinea	GQ	f	2017-11-12 18:50:00.684921	2017-11-12 18:50:00.684939
42	Guadeloupe	guadeloupe	GP	f	2017-11-12 18:50:00.686327	2017-11-12 18:50:00.686361
43	Japan	japan	JP	f	2017-11-12 18:50:00.687978	2017-11-12 18:50:00.687987
44	Guyana	guyana	GY	f	2017-11-12 18:50:00.689037	2017-11-12 18:50:00.689047
45	Guernsey	guernsey	GG	f	2017-11-12 18:50:00.690249	2017-11-12 18:50:00.690272
46	French Guiana	french-guiana	GF	f	2017-11-12 18:50:00.691694	2017-11-12 18:50:00.691708
47	Georgia	georgia	GE	f	2017-11-12 18:50:00.692942	2017-11-12 18:50:00.692955
48	Grenada	grenada	GD	f	2017-11-12 18:50:00.694245	2017-11-12 18:50:00.694262
49	United Kingdom	united-kingdom	GB	f	2017-11-12 18:50:00.695597	2017-11-12 18:50:00.695614
50	Gabon	gabon	GA	f	2017-11-12 18:50:00.697249	2017-11-12 18:50:00.697269
51	Guinea	guinea	GN	f	2017-11-12 18:50:00.698745	2017-11-12 18:50:00.698762
52	Gambia	gambia	GM	f	2017-11-12 18:50:00.69983	2017-11-12 18:50:00.699881
53	Greenland	greenland	GL	f	2017-11-12 18:50:00.700891	2017-11-12 18:50:00.700904
54	Gibraltar	gibraltar	GI	f	2017-11-12 18:50:00.702151	2017-11-12 18:50:00.702165
55	Ghana	ghana	GH	f	2017-11-12 18:50:00.703772	2017-11-12 18:50:00.703788
56	Oman	oman	OM	f	2017-11-12 18:50:00.705267	2017-11-12 18:50:00.705278
57	Tunisia	tunisia	TN	f	2017-11-12 18:50:00.706304	2017-11-12 18:50:00.706314
58	Jordan	jordan	JO	f	2017-11-12 18:50:00.707317	2017-11-12 18:50:00.707333
59	Croatia	croatia	HR	f	2017-11-12 18:50:00.709017	2017-11-12 18:50:00.709036
60	Haiti	haiti	HT	f	2017-11-12 18:50:00.710467	2017-11-12 18:50:00.710481
61	Hungary	hungary	HU	f	2017-11-12 18:50:00.711961	2017-11-12 18:50:00.711972
62	Hong Kong SAR China	hong-kong-sar-china	HK	f	2017-11-12 18:50:00.71369	2017-11-12 18:50:00.713707
63	Honduras	honduras	HN	f	2017-11-12 18:50:00.715253	2017-11-12 18:50:00.715267
64	Union of Soviet Socialist Republics	union-of-soviet-socialist-republics	SU	f	2017-11-12 18:50:00.716309	2017-11-12 18:50:00.716319
65	Heard Island and McDonald Islands	heard-island-and-mcdonald-islands	HM	f	2017-11-12 18:50:00.717554	2017-11-12 18:50:00.717567
66	United Arab Emirates	united-arab-emirates	AE	f	2017-11-12 18:50:00.718963	2017-11-12 18:50:00.718976
67	Venezuela	venezuela	VE	f	2017-11-12 18:50:00.720376	2017-11-12 18:50:00.720388
68	Puerto Rico	puerto-rico	PR	f	2017-11-12 18:50:00.721799	2017-11-12 18:50:00.721811
69	Palestinian Territories	palestinian-territories	PS	f	2017-11-12 18:50:00.722914	2017-11-12 18:50:00.722923
70	Ukraine	ukraine	UA	f	2017-11-12 18:50:00.724408	2017-11-12 18:50:00.724421
71	Palau	palau	PW	f	2017-11-12 18:50:00.725907	2017-11-12 18:50:00.72592
72	Portugal	portugal	PT	f	2017-11-12 18:50:00.727564	2017-11-12 18:50:00.727577
73	Saint Kitts and Nevis	saint-kitts-and-nevis	KN	f	2017-11-12 18:50:00.729005	2017-11-12 18:50:00.729017
74	Panama Canal Zone	panama-canal-zone	PZ	f	2017-11-12 18:50:00.730292	2017-11-12 18:50:00.730307
75	Afghanistan	afghanistan	AF	f	2017-11-12 18:50:00.731952	2017-11-12 18:50:00.731976
76	Iraq	iraq	IQ	f	2017-11-12 18:50:00.733316	2017-11-12 18:50:00.73333
77	Panama	panama	PA	f	2017-11-12 18:50:00.734453	2017-11-12 18:50:00.73447
78	French Polynesia	french-polynesia	PF	f	2017-11-12 18:50:00.735787	2017-11-12 18:50:00.735804
79	Papua New Guinea	papua-new-guinea	PG	f	2017-11-12 18:50:00.736793	2017-11-12 18:50:00.736802
80	Peru	peru	PE	f	2017-11-12 18:50:00.737618	2017-11-12 18:50:00.737627
81	Pakistan	pakistan	PK	f	2017-11-12 18:50:00.738418	2017-11-12 18:50:00.738426
82	Philippines	philippines	PH	f	2017-11-12 18:50:00.739587	2017-11-12 18:50:00.739599
83	Pitcairn Islands	pitcairn-islands	PN	f	2017-11-12 18:50:00.740858	2017-11-12 18:50:00.740875
84	Poland	poland	PL	f	2017-11-12 18:50:00.743188	2017-11-12 18:50:00.743211
85	Saint Pierre and Miquelon	saint-pierre-and-miquelon	PM	f	2017-11-12 18:50:00.745208	2017-11-12 18:50:00.745228
86	Zambia	zambia	ZM	f	2017-11-12 18:50:00.747146	2017-11-12 18:50:00.747169
87	Western Sahara	western-sahara	EH	f	2017-11-12 18:50:00.748808	2017-11-12 18:50:00.7492
88	Estonia	estonia	EE	f	2017-11-12 18:50:00.751089	2017-11-12 18:50:00.751108
89	Egypt	egypt	EG	f	2017-11-12 18:50:00.752876	2017-11-12 18:50:00.75289
90	South Africa	south-africa	ZA	f	2017-11-12 18:50:00.754509	2017-11-12 18:50:00.75453
91	Ecuador	ecuador	EC	f	2017-11-12 18:50:00.756117	2017-11-12 18:50:00.756132
92	Italy	italy	IT	f	2017-11-12 18:50:00.757212	2017-11-12 18:50:00.757225
93	Vietnam	vietnam	VN	f	2017-11-12 18:50:00.758713	2017-11-12 18:50:00.758738
94	Unknown or Invalid Region	unknown-or-invalid-region	ZZ	f	2017-11-12 18:50:00.760657	2017-11-12 18:50:00.760676
95	Solomon Islands	solomon-islands	SB	f	2017-11-12 18:50:00.762167	2017-11-12 18:50:00.762187
96	Ethiopia	ethiopia	ET	f	2017-11-12 18:50:00.763658	2017-11-12 18:50:00.763682
97	Somalia	somalia	SO	f	2017-11-12 18:50:00.765491	2017-11-12 18:50:00.765522
98	Zimbabwe	zimbabwe	ZW	f	2017-11-12 18:50:00.767174	2017-11-12 18:50:00.767207
99	Saudi Arabia	saudi-arabia	SA	f	2017-11-12 18:50:00.768755	2017-11-12 18:50:00.768766
100	Spain	spain	ES	f	2017-11-12 18:50:00.770311	2017-11-12 18:50:00.770331
101	Eritrea	eritrea	ER	f	2017-11-12 18:50:00.771934	2017-11-12 18:50:00.771944
102	Montenegro	montenegro	ME	f	2017-11-12 18:50:00.773426	2017-11-12 18:50:00.773438
103	Moldova	moldova	MD	f	2017-11-12 18:50:00.775402	2017-11-12 18:50:00.775426
104	Madagascar	madagascar	MG	f	2017-11-12 18:50:00.777807	2017-11-12 18:50:00.777822
105	Saint Martin	saint-martin	MF	f	2017-11-12 18:50:00.780129	2017-11-12 18:50:00.780168
106	Morocco	morocco	MA	f	2017-11-12 18:50:00.782531	2017-11-12 18:50:00.782553
107	Monaco	monaco	MC	f	2017-11-12 18:50:00.784154	2017-11-12 18:50:00.784165
108	Uzbekistan	uzbekistan	UZ	f	2017-11-12 18:50:00.785628	2017-11-12 18:50:00.785643
109	Myanmar [Burma]	myanmar-burma	MM	f	2017-11-12 18:50:00.786918	2017-11-12 18:50:00.786934
110	Mali	mali	ML	f	2017-11-12 18:50:00.788074	2017-11-12 18:50:00.788087
111	Macau SAR China	macau-sar-china	MO	f	2017-11-12 18:50:00.789764	2017-11-12 18:50:00.789782
112	Mongolia	mongolia	MN	f	2017-11-12 18:50:00.79122	2017-11-12 18:50:00.791232
113	Midway Islands	midway-islands	MI	f	2017-11-12 18:50:00.792521	2017-11-12 18:50:00.792532
114	Marshall Islands	marshall-islands	MH	f	2017-11-12 18:50:00.793463	2017-11-12 18:50:00.793473
115	Macedonia	macedonia	MK	f	2017-11-12 18:50:00.794894	2017-11-12 18:50:00.794915
116	Mauritius	mauritius	MU	f	2017-11-12 18:50:00.796294	2017-11-12 18:50:00.796305
117	Malta	malta	MT	f	2017-11-12 18:50:00.79732	2017-11-12 18:50:00.797332
118	Malawi	malawi	MW	f	2017-11-12 18:50:00.79913	2017-11-12 18:50:00.799153
119	Maldives	maldives	MV	f	2017-11-12 18:50:00.801032	2017-11-12 18:50:00.801051
120	Martinique	martinique	MQ	f	2017-11-12 18:50:00.802401	2017-11-12 18:50:00.802418
121	Northern Mariana Islands	northern-mariana-islands	MP	f	2017-11-12 18:50:00.803805	2017-11-12 18:50:00.803822
122	Montserrat	montserrat	MS	f	2017-11-12 18:50:00.805189	2017-11-12 18:50:00.805203
123	Mauritania	mauritania	MR	f	2017-11-12 18:50:00.806135	2017-11-12 18:50:00.806144
124	Isle of Man	isle-of-man	IM	f	2017-11-12 18:50:00.807304	2017-11-12 18:50:00.807322
125	Uganda	uganda	UG	f	2017-11-12 18:50:00.809065	2017-11-12 18:50:00.809083
126	Malaysia	malaysia	MY	f	2017-11-12 18:50:00.810691	2017-11-12 18:50:00.810706
127	Mexico	mexico	MX	f	2017-11-12 18:50:00.812113	2017-11-12 18:50:00.812122
128	Israel	israel	IL	f	2017-11-12 18:50:00.813245	2017-11-12 18:50:00.813255
129	French Southern and Antarctic Territories	french-southern-and-antarctic-territories	FQ	f	2017-11-12 18:50:00.814368	2017-11-12 18:50:00.814378
130	France	france	FR	f	2017-11-12 18:50:00.815644	2017-11-12 18:50:00.815654
131	Aruba	aruba	AW	f	2017-11-12 18:50:00.816882	2017-11-12 18:50:00.816892
132	Metropolitan France	metropolitan-france	FX	f	2017-11-12 18:50:00.817969	2017-11-12 18:50:00.817978
133	Saint Helena	saint-helena	SH	f	2017-11-12 18:50:00.819414	2017-11-12 18:50:00.819437
134	Åland Islands	aland-islands	AX	f	2017-11-12 18:50:00.821111	2017-11-12 18:50:00.821122
135	Svalbard and Jan Mayen	svalbard-and-jan-mayen	SJ	f	2017-11-12 18:50:00.822601	2017-11-12 18:50:00.822612
136	Finland	finland	FI	f	2017-11-12 18:50:00.823891	2017-11-12 18:50:00.823931
137	Fiji	fiji	FJ	f	2017-11-12 18:50:00.826205	2017-11-12 18:50:00.826247
138	Falkland Islands	falkland-islands	FK	f	2017-11-12 18:50:00.828275	2017-11-12 18:50:00.828293
139	Micronesia	micronesia	FM	f	2017-11-12 18:50:00.829874	2017-11-12 18:50:00.829904
140	Faroe Islands	faroe-islands	FO	f	2017-11-12 18:50:00.831558	2017-11-12 18:50:00.831575
141	Nicaragua	nicaragua	NI	f	2017-11-12 18:50:00.833187	2017-11-12 18:50:00.833202
142	Netherlands	netherlands	NL	f	2017-11-12 18:50:00.83464	2017-11-12 18:50:00.83465
143	Norway	norway	NO	f	2017-11-12 18:50:00.83606	2017-11-12 18:50:00.83607
144	Namibia	namibia	NA	f	2017-11-12 18:50:00.837765	2017-11-12 18:50:00.837777
145	Vanuatu	vanuatu	VU	f	2017-11-12 18:50:00.83889	2017-11-12 18:50:00.838901
146	New Caledonia	new-caledonia	NC	f	2017-11-12 18:50:00.840428	2017-11-12 18:50:00.840449
147	Niger	niger	NE	f	2017-11-12 18:50:00.842086	2017-11-12 18:50:00.8421
148	Norfolk Island	norfolk-island	NF	f	2017-11-12 18:50:00.843705	2017-11-12 18:50:00.843731
149	Nigeria	nigeria	NG	t	2017-11-12 18:50:00.845236	2017-11-12 18:50:00.845249
150	New Zealand	new-zealand	NZ	f	2017-11-12 18:50:00.846503	2017-11-12 18:50:00.846514
151	Nepal	nepal	NP	f	2017-11-12 18:50:00.847755	2017-11-12 18:50:00.847764
152	Dronning Maud Land	dronning-maud-land	NQ	f	2017-11-12 18:50:00.849014	2017-11-12 18:50:00.849024
153	Nauru	nauru	NR	f	2017-11-12 18:50:00.850193	2017-11-12 18:50:00.850203
154	Neutral Zone	neutral-zone	NT	f	2017-11-12 18:50:00.851336	2017-11-12 18:50:00.851345
155	Niue	niue	NU	f	2017-11-12 18:50:00.852601	2017-11-12 18:50:00.852613
156	Cook Islands	cook-islands	CK	f	2017-11-12 18:50:00.85378	2017-11-12 18:50:00.853792
157	Côte d’Ivoire	cote-divoire	CI	f	2017-11-12 18:50:00.855664	2017-11-12 18:50:00.855678
158	Switzerland	switzerland	CH	f	2017-11-12 18:50:00.857629	2017-11-12 18:50:00.857645
159	Colombia	colombia	CO	f	2017-11-12 18:50:00.859898	2017-11-12 18:50:00.859917
160	China	china	CN	f	2017-11-12 18:50:00.861841	2017-11-12 18:50:00.861862
161	Cameroon	cameroon	CM	f	2017-11-12 18:50:00.863629	2017-11-12 18:50:00.863643
162	Chile	chile	CL	f	2017-11-12 18:50:00.865353	2017-11-12 18:50:00.865372
163	Cocos [Keeling] Islands	cocos-keeling-islands	CC	f	2017-11-12 18:50:00.867278	2017-11-12 18:50:00.867297
164	Canada	canada	CA	f	2017-11-12 18:50:00.869098	2017-11-12 18:50:00.869117
165	Congo - Brazzaville	congo-brazzaville	CG	f	2017-11-12 18:50:00.870493	2017-11-12 18:50:00.87051
166	Central African Republic	central-african-republic	CF	f	2017-11-12 18:50:00.872005	2017-11-12 18:50:00.872017
167	Congo - Kinshasa	congo-kinshasa	CD	f	2017-11-12 18:50:00.87341	2017-11-12 18:50:00.873425
168	Czech Republic	czech-republic	CZ	f	2017-11-12 18:50:00.874766	2017-11-12 18:50:00.874779
169	Cyprus	cyprus	CY	f	2017-11-12 18:50:00.87625	2017-11-12 18:50:00.87627
170	Christmas Island	christmas-island	CX	f	2017-11-12 18:50:00.877924	2017-11-12 18:50:00.877943
171	Serbia and Montenegro	serbia-and-montenegro	CS	f	2017-11-12 18:50:00.879437	2017-11-12 18:50:00.87945
172	Costa Rica	costa-rica	CR	f	2017-11-12 18:50:00.881088	2017-11-12 18:50:00.881107
173	Paraguay	paraguay	PY	f	2017-11-12 18:50:00.882952	2017-11-12 18:50:00.882966
174	Cape Verde	cape-verde	CV	f	2017-11-12 18:50:00.884442	2017-11-12 18:50:00.884453
175	Cuba	cuba	CU	f	2017-11-12 18:50:00.885713	2017-11-12 18:50:00.885726
176	Canton and Enderbury Islands	canton-and-enderbury-islands	CT	f	2017-11-12 18:50:00.886942	2017-11-12 18:50:00.886954
177	Swaziland	swaziland	SZ	f	2017-11-12 18:50:00.888122	2017-11-12 18:50:00.888133
178	Syria	syria	SY	f	2017-11-12 18:50:00.889302	2017-11-12 18:50:00.889315
179	Kyrgyzstan	kyrgyzstan	KG	f	2017-11-12 18:50:00.891194	2017-11-12 18:50:00.89124
180	Kenya	kenya	KE	f	2017-11-12 18:50:00.893653	2017-11-12 18:50:00.893676
181	Suriname	suriname	SR	f	2017-11-12 18:50:00.89533	2017-11-12 18:50:00.89535
182	Kiribati	kiribati	KI	f	2017-11-12 18:50:00.896832	2017-11-12 18:50:00.896848
183	Cambodia	cambodia	KH	f	2017-11-12 18:50:00.898024	2017-11-12 18:50:00.898036
184	El Salvador	el-salvador	SV	f	2017-11-12 18:50:00.899168	2017-11-12 18:50:00.899179
185	Comoros	comoros	KM	f	2017-11-12 18:50:00.900216	2017-11-12 18:50:00.900228
186	São Tomé and Príncipe	sao-tome-and-principe	ST	f	2017-11-12 18:50:00.90143	2017-11-12 18:50:00.901442
187	Slovakia	slovakia	SK	f	2017-11-12 18:50:00.902465	2017-11-12 18:50:00.902476
188	South Korea	south-korea	KR	f	2017-11-12 18:50:00.903462	2017-11-12 18:50:00.903473
189	Slovenia	slovenia	SI	f	2017-11-12 18:50:00.904437	2017-11-12 18:50:00.904446
190	North Korea	north-korea	KP	f	2017-11-12 18:50:00.905759	2017-11-12 18:50:00.905777
191	Kuwait	kuwait	KW	f	2017-11-12 18:50:00.907384	2017-11-12 18:50:00.907406
192	Senegal	senegal	SN	f	2017-11-12 18:50:00.909262	2017-11-12 18:50:00.909279
193	San Marino	san-marino	SM	f	2017-11-12 18:50:00.91085	2017-11-12 18:50:00.910865
194	Sierra Leone	sierra-leone	SL	f	2017-11-12 18:50:00.912491	2017-11-12 18:50:00.912508
195	Seychelles	seychelles	SC	f	2017-11-12 18:50:00.91401	2017-11-12 18:50:00.914026
196	Kazakhstan	kazakhstan	KZ	f	2017-11-12 18:50:00.915372	2017-11-12 18:50:00.915393
197	Cayman Islands	cayman-islands	KY	f	2017-11-12 18:50:00.917282	2017-11-12 18:50:00.917309
198	Singapore	singapore	SG	f	2017-11-12 18:50:00.918895	2017-11-12 18:50:00.918911
199	Sweden	sweden	SE	f	2017-11-12 18:50:00.920242	2017-11-12 18:50:00.920251
200	Sudan	sudan	SD	f	2017-11-12 18:50:00.921328	2017-11-12 18:50:00.921337
201	Dominican Republic	dominican-republic	DO	f	2017-11-12 18:50:00.92245	2017-11-12 18:50:00.92246
202	Dominica	dominica	DM	f	2017-11-12 18:50:00.923611	2017-11-12 18:50:00.923622
203	Djibouti	djibouti	DJ	f	2017-11-12 18:50:00.924952	2017-11-12 18:50:00.924965
204	Denmark	denmark	DK	f	2017-11-12 18:50:00.9264	2017-11-12 18:50:00.926411
205	East Germany	east-germany	DD	f	2017-11-12 18:50:00.927695	2017-11-12 18:50:00.927705
206	Germany	germany	DE	f	2017-11-12 18:50:00.928999	2017-11-12 18:50:00.929011
207	Yemen	yemen	YE	f	2017-11-12 18:50:00.930206	2017-11-12 18:50:00.930217
208	People's Democratic Republic of Yemen	people-s-democratic-republic-of-yemen	YD	f	2017-11-12 18:50:00.931359	2017-11-12 18:50:00.931369
209	Algeria	algeria	DZ	f	2017-11-12 18:50:00.932607	2017-11-12 18:50:00.932617
210	United States	united-states	US	f	2017-11-12 18:50:00.934049	2017-11-12 18:50:00.934068
211	Uruguay	uruguay	UY	f	2017-11-12 18:50:00.935678	2017-11-12 18:50:00.935698
212	Mayotte	mayotte	YT	f	2017-11-12 18:50:00.937282	2017-11-12 18:50:00.9373
213	U.S. Minor Outlying Islands	u-s-minor-outlying-islands	UM	f	2017-11-12 18:50:00.938905	2017-11-12 18:50:00.938921
214	Lebanon	lebanon	LB	f	2017-11-12 18:50:00.94036	2017-11-12 18:50:00.940372
215	Saint Lucia	saint-lucia	LC	f	2017-11-12 18:50:00.941904	2017-11-12 18:50:00.941916
216	Laos	laos	LA	f	2017-11-12 18:50:00.943735	2017-11-12 18:50:00.943748
217	Tuvalu	tuvalu	TV	f	2017-11-12 18:50:00.945149	2017-11-12 18:50:00.94516
218	Taiwan	taiwan	TW	f	2017-11-12 18:50:00.946773	2017-11-12 18:50:00.946793
219	Trinidad and Tobago	trinidad-and-tobago	TT	f	2017-11-12 18:50:00.948303	2017-11-12 18:50:00.94833
220	Turkey	turkey	TR	f	2017-11-12 18:50:00.950013	2017-11-12 18:50:00.950024
221	Sri Lanka	sri-lanka	LK	f	2017-11-12 18:50:00.951615	2017-11-12 18:50:00.951633
222	Liechtenstein	liechtenstein	LI	f	2017-11-12 18:50:00.95328	2017-11-12 18:50:00.953303
223	Latvia	latvia	LV	f	2017-11-12 18:50:00.955044	2017-11-12 18:50:00.955074
224	Tonga	tonga	TO	f	2017-11-12 18:50:00.956478	2017-11-12 18:50:00.956493
225	Lithuania	lithuania	LT	f	2017-11-12 18:50:00.958101	2017-11-12 18:50:00.958122
226	Luxembourg	luxembourg	LU	f	2017-11-12 18:50:00.959584	2017-11-12 18:50:00.959599
227	Liberia	liberia	LR	f	2017-11-12 18:50:00.960851	2017-11-12 18:50:00.960863
228	Lesotho	lesotho	LS	f	2017-11-12 18:50:00.962053	2017-11-12 18:50:00.962065
229	Thailand	thailand	TH	f	2017-11-12 18:50:00.963361	2017-11-12 18:50:00.963372
230	French Southern Territories	french-southern-territories	TF	f	2017-11-12 18:50:00.964366	2017-11-12 18:50:00.964376
231	Togo	togo	TG	f	2017-11-12 18:50:00.965959	2017-11-12 18:50:00.965975
232	Chad	chad	TD	f	2017-11-12 18:50:00.967496	2017-11-12 18:50:00.967513
233	Turks and Caicos Islands	turks-and-caicos-islands	TC	f	2017-11-12 18:50:00.969147	2017-11-12 18:50:00.969163
234	Libya	libya	LY	f	2017-11-12 18:50:00.970672	2017-11-12 18:50:00.970689
235	Vatican City	vatican-city	VA	f	2017-11-12 18:50:00.972138	2017-11-12 18:50:00.972154
236	Saint Vincent and the Grenadines	saint-vincent-and-the-grenadines	VC	f	2017-11-12 18:50:00.97363	2017-11-12 18:50:00.973643
237	North Vietnam	north-vietnam	VD	f	2017-11-12 18:50:00.974876	2017-11-12 18:50:00.974889
238	Andorra	andorra	AD	f	2017-11-12 18:50:00.976593	2017-11-12 18:50:00.976621
239	Antigua and Barbuda	antigua-and-barbuda	AG	f	2017-11-12 18:50:00.978634	2017-11-12 18:50:00.97866
240	British Virgin Islands	british-virgin-islands	VG	f	2017-11-12 18:50:00.980485	2017-11-12 18:50:00.980522
241	Anguilla	anguilla	AI	f	2017-11-12 18:50:00.982543	2017-11-12 18:50:00.982564
242	U.S. Virgin Islands	u-s-virgin-islands	VI	f	2017-11-12 18:50:00.984548	2017-11-12 18:50:00.984571
243	Iceland	iceland	IS	f	2017-11-12 18:50:00.986427	2017-11-12 18:50:00.986447
244	Iran	iran	IR	f	2017-11-12 18:50:00.988143	2017-11-12 18:50:00.988155
245	Armenia	armenia	AM	f	2017-11-12 18:50:00.989482	2017-11-12 18:50:00.989494
246	Albania	albania	AL	f	2017-11-12 18:50:00.991099	2017-11-12 18:50:00.991113
247	Angola	angola	AO	f	2017-11-12 18:50:00.993224	2017-11-12 18:50:00.993236
248	Netherlands Antilles	netherlands-antilles	AN	f	2017-11-12 18:50:00.994712	2017-11-12 18:50:00.994723
249	Antarctica	antarctica	AQ	f	2017-11-12 18:50:00.996056	2017-11-12 18:50:00.996068
250	American Samoa	american-samoa	AS	f	2017-11-12 18:50:00.997324	2017-11-12 18:50:00.997336
251	Argentina	argentina	AR	f	2017-11-12 18:50:00.998883	2017-11-12 18:50:00.998896
252	Australia	australia	AU	f	2017-11-12 18:50:01.000411	2017-11-12 18:50:01.000426
253	Austria	austria	AT	f	2017-11-12 18:50:01.001569	2017-11-12 18:50:01.001579
254	British Indian Ocean Territory	british-indian-ocean-territory	IO	f	2017-11-12 18:50:01.00296	2017-11-12 18:50:01.002971
255	India	india	IN	f	2017-11-12 18:50:01.004156	2017-11-12 18:50:01.004169
256	Tanzania	tanzania	TZ	f	2017-11-12 18:50:01.005565	2017-11-12 18:50:01.00558
257	Azerbaijan	azerbaijan	AZ	f	2017-11-12 18:50:01.007104	2017-11-12 18:50:01.007119
258	Ireland	ireland	IE	f	2017-11-12 18:50:01.009109	2017-11-12 18:50:01.009148
259	Indonesia	indonesia	ID	f	2017-11-12 18:50:01.011393	2017-11-12 18:50:01.011416
260	Serbia	serbia	RS	f	2017-11-12 18:50:01.013824	2017-11-12 18:50:01.013839
261	U.S. Miscellaneous Pacific Islands	u-s-miscellaneous-pacific-islands	PU	f	2017-11-12 18:50:01.016032	2017-11-12 18:50:01.016053
262	Saint Barthélemy	saint-barthelemy	BL	f	2017-11-12 18:50:01.01774	2017-11-12 18:50:01.017759
263	Qatar	qatar	QA	f	2017-11-12 18:50:01.019448	2017-11-12 18:50:01.019467
264	Mozambique	mozambique	MZ	f	2017-11-12 18:50:01.021069	2017-11-12 18:50:01.021084
\.


--
-- Data for Name: currency; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY currency (id, name, code, enabled, symbol, payment_code, last_updated, date_created) FROM stdin;
1	UAE dirham	AED	f	د.إ;	\N	2017-11-12 18:50:00.369603	2017-11-12 18:50:00.369613
2	Afghan afghani	AFN	f	Afs	\N	2017-11-12 18:50:00.373454	2017-11-12 18:50:00.373466
3	Albanian lek	ALL	f	L	\N	2017-11-12 18:50:00.377123	2017-11-12 18:50:00.377166
4	Armenian dram	AMD	f	AMD	\N	2017-11-12 18:50:00.379237	2017-11-12 18:50:00.379263
5	Netherlands Antillean gulden	ANG	f	NAƒ	\N	2017-11-12 18:50:00.380925	2017-11-12 18:50:00.380949
6	Angolan kwanza	AOA	f	Kz	\N	2017-11-12 18:50:00.382457	2017-11-12 18:50:00.382474
7	Argentine peso	ARS	f	$	\N	2017-11-12 18:50:00.383991	2017-11-12 18:50:00.384034
8	Australian dollar	AUD	f	$	\N	2017-11-12 18:50:00.385098	2017-11-12 18:50:00.385119
9	Aruban florin	AWG	f	ƒ	\N	2017-11-12 18:50:00.386217	2017-11-12 18:50:00.386231
10	Azerbaijani manat	AZN	f	AZN	\N	2017-11-12 18:50:00.387281	2017-11-12 18:50:00.387295
11	Bosnia and Herzegovina konvertibilna marka	BAM	f	KM	\N	2017-11-12 18:50:00.388276	2017-11-12 18:50:00.388287
12	Barbadian dollar	BBD	f	Bds$	\N	2017-11-12 18:50:00.389823	2017-11-12 18:50:00.389857
13	Bangladeshi taka	BDT	f	৳	\N	2017-11-12 18:50:00.391471	2017-11-12 18:50:00.391488
14	Bulgarian lev	BGN	f	BGN	\N	2017-11-12 18:50:00.393559	2017-11-12 18:50:00.393586
15	Bahraini dinar	BHD	f	.د.ب	\N	2017-11-12 18:50:00.395314	2017-11-12 18:50:00.39533
16	Burundi franc	BIF	f	FBu	\N	2017-11-12 18:50:00.396884	2017-11-12 18:50:00.3969
17	Bermudian dollar	BMD	f	BD$	\N	2017-11-12 18:50:00.398299	2017-11-12 18:50:00.398317
18	Brunei dollar	BND	f	B$	\N	2017-11-12 18:50:00.400107	2017-11-12 18:50:00.400125
19	Bolivian boliviano	BOB	f	Bs.	\N	2017-11-12 18:50:00.401474	2017-11-12 18:50:00.401484
20	Brazilian real	BRL	f	R$	\N	2017-11-12 18:50:00.402893	2017-11-12 18:50:00.402904
21	Bahamian dollar	BSD	f	B$	\N	2017-11-12 18:50:00.404208	2017-11-12 18:50:00.404217
22	Bhutanese ngultrum	BTN	f	Nu.	\N	2017-11-12 18:50:00.405106	2017-11-12 18:50:00.405117
23	Botswana pula	BWP	f	P	\N	2017-11-12 18:50:00.406019	2017-11-12 18:50:00.40603
24	Belarusian ruble	BYR	f	Br	\N	2017-11-12 18:50:00.407	2017-11-12 18:50:00.407017
25	Belize dollar	BZD	f	BZ$	\N	2017-11-12 18:50:00.40874	2017-11-12 18:50:00.40876
26	Canadian dollar	CAD	f	$	\N	2017-11-12 18:50:00.410248	2017-11-12 18:50:00.410267
27	Congolese franc	CDF	f	F	\N	2017-11-12 18:50:00.411639	2017-11-12 18:50:00.411662
28	Swiss franc	CHF	f	Fr.	\N	2017-11-12 18:50:00.412711	2017-11-12 18:50:00.412722
29	Chilean peso	CLP	f	$	\N	2017-11-12 18:50:00.413533	2017-11-12 18:50:00.413543
30	Chinese/Yuan renminbi	CNY	f	¥	\N	2017-11-12 18:50:00.414184	2017-11-12 18:50:00.414193
31	Colombian peso	COP	f	Col$	\N	2017-11-12 18:50:00.415271	2017-11-12 18:50:00.41529
32	Costa Rican colon	CRC	f	₡	\N	2017-11-12 18:50:00.416539	2017-11-12 18:50:00.416558
33	Cuban peso	CUC	f	$	\N	2017-11-12 18:50:00.417935	2017-11-12 18:50:00.417954
34	Cape Verdean escudo	CVE	f	Esc	\N	2017-11-12 18:50:00.419253	2017-11-12 18:50:00.419272
35	Czech koruna	CZK	f	Kč	\N	2017-11-12 18:50:00.420571	2017-11-12 18:50:00.420589
36	Djiboutian franc	DJF	f	Fdj	\N	2017-11-12 18:50:00.421832	2017-11-12 18:50:00.421846
37	Danish krone	DKK	f	Kr	\N	2017-11-12 18:50:00.422967	2017-11-12 18:50:00.422977
38	Dominican peso	DOP	f	RD$	\N	2017-11-12 18:50:00.423915	2017-11-12 18:50:00.423929
39	Algerian dinar	DZD	f	د.ج	\N	2017-11-12 18:50:00.425043	2017-11-12 18:50:00.425056
40	Estonian kroon	EEK	f	KR	\N	2017-11-12 18:50:00.42615	2017-11-12 18:50:00.426163
41	Egyptian pound	EGP	f	£	\N	2017-11-12 18:50:00.427199	2017-11-12 18:50:00.427212
42	Eritrean nakfa	ERN	f	Nfa	\N	2017-11-12 18:50:00.428216	2017-11-12 18:50:00.428226
43	Ethiopian birr	ETB	f	Br	\N	2017-11-12 18:50:00.429327	2017-11-12 18:50:00.429343
44	European Euro	EUR	f	€	\N	2017-11-12 18:50:00.430257	2017-11-12 18:50:00.430267
45	Fijian dollar	FJD	f	FJ$	\N	2017-11-12 18:50:00.431307	2017-11-12 18:50:00.431318
46	Falkland Islands pound	FKP	f	£	\N	2017-11-12 18:50:00.432429	2017-11-12 18:50:00.43244
47	British pound	GBP	f	£	\N	2017-11-12 18:50:00.433612	2017-11-12 18:50:00.433625
48	Georgian lari	GEL	f	GEL	\N	2017-11-12 18:50:00.434921	2017-11-12 18:50:00.434935
49	Ghanaian cedi	GHS	f	GH₵	\N	2017-11-12 18:50:00.436231	2017-11-12 18:50:00.436243
50	Gibraltar pound	GIP	f	£	\N	2017-11-12 18:50:00.4374	2017-11-12 18:50:00.437412
51	Gambian dalasi	GMD	f	D	\N	2017-11-12 18:50:00.438253	2017-11-12 18:50:00.438261
52	Guinean franc	GNF	f	FG	\N	2017-11-12 18:50:00.439035	2017-11-12 18:50:00.439044
53	Central African CFA franc	GQE	f	CFA	\N	2017-11-12 18:50:00.439801	2017-11-12 18:50:00.439811
54	Guatemalan quetzal	GTQ	f	Q	\N	2017-11-12 18:50:00.440889	2017-11-12 18:50:00.440904
55	Guyanese dollar	GYD	f	GY$	\N	2017-11-12 18:50:00.442292	2017-11-12 18:50:00.442306
56	Hong Kong dollar	HKD	f	HK$	\N	2017-11-12 18:50:00.443608	2017-11-12 18:50:00.443621
57	Honduran lempira	HNL	f	L	\N	2017-11-12 18:50:00.444957	2017-11-12 18:50:00.444976
58	Croatian kuna	HRK	f	kn	\N	2017-11-12 18:50:00.446614	2017-11-12 18:50:00.446635
59	Haitian gourde	HTG	f	G	\N	2017-11-12 18:50:00.448318	2017-11-12 18:50:00.448342
60	Hungarian forint	HUF	f	Ft	\N	2017-11-12 18:50:00.45012	2017-11-12 18:50:00.450146
61	Indonesian rupiah	IDR	f	Rp	\N	2017-11-12 18:50:00.452	2017-11-12 18:50:00.452023
62	Israeli new sheqel	ILS	f	₪	\N	2017-11-12 18:50:00.453476	2017-11-12 18:50:00.453496
63	Indian rupee	INR	f	₉	\N	2017-11-12 18:50:00.455028	2017-11-12 18:50:00.455046
64	Iraqi dinar	IQD	f	د.ع	\N	2017-11-12 18:50:00.456438	2017-11-12 18:50:00.456472
65	Iranian rial	IRR	f	IRR	\N	2017-11-12 18:50:00.457961	2017-11-12 18:50:00.457982
66	Icelandic króna	ISK	f	kr	\N	2017-11-12 18:50:00.4604	2017-11-12 18:50:00.460415
67	Jamaican dollar	JMD	f	J$	\N	2017-11-12 18:50:00.462149	2017-11-12 18:50:00.462168
68	Jordanian dinar	JOD	f	JOD	\N	2017-11-12 18:50:00.463047	2017-11-12 18:50:00.463059
69	Japanese yen	JPY	f	¥	\N	2017-11-12 18:50:00.464041	2017-11-12 18:50:00.464064
70	Kenyan shilling	KES	f	KSh	\N	2017-11-12 18:50:00.465535	2017-11-12 18:50:00.465557
71	Kyrgyzstani som	KGS	f	сом	\N	2017-11-12 18:50:00.466949	2017-11-12 18:50:00.466971
72	Cambodian riel	KHR	f	៛	\N	2017-11-12 18:50:00.469288	2017-11-12 18:50:00.469305
73	Comorian franc	KMF	f	KMF	\N	2017-11-12 18:50:00.471154	2017-11-12 18:50:00.471176
74	North Korean won	KPW	f	W	\N	2017-11-12 18:50:00.4727	2017-11-12 18:50:00.472722
75	South Korean won	KRW	f	W	\N	2017-11-12 18:50:00.474731	2017-11-12 18:50:00.474761
76	Kuwaiti dinar	KWD	f	KWD	\N	2017-11-12 18:50:00.476811	2017-11-12 18:50:00.47684
77	Cayman Islands dollar	KYD	f	KY$	\N	2017-11-12 18:50:00.479598	2017-11-12 18:50:00.479627
78	Kazakhstani tenge	KZT	f	T	\N	2017-11-12 18:50:00.481406	2017-11-12 18:50:00.481431
79	Lao kip	LAK	f	KN	\N	2017-11-12 18:50:00.483303	2017-11-12 18:50:00.483321
80	Lebanese lira	LBP	f	£	\N	2017-11-12 18:50:00.484461	2017-11-12 18:50:00.484471
81	Sri Lankan rupee	LKR	f	Rs	\N	2017-11-12 18:50:00.485134	2017-11-12 18:50:00.485143
82	Liberian dollar	LRD	f	L$	\N	2017-11-12 18:50:00.48581	2017-11-12 18:50:00.48582
83	Lesotho loti	LSL	f	M	\N	2017-11-12 18:50:00.48643	2017-11-12 18:50:00.486436
84	Lithuanian litas	LTL	f	Lt	\N	2017-11-12 18:50:00.487006	2017-11-12 18:50:00.487013
85	Latvian lats	LVL	f	Ls	\N	2017-11-12 18:50:00.48755	2017-11-12 18:50:00.487555
86	Libyan dinar	LYD	f	LD	\N	2017-11-12 18:50:00.488045	2017-11-12 18:50:00.48805
87	Moroccan dirham	MAD	f	MAD	\N	2017-11-12 18:50:00.488648	2017-11-12 18:50:00.488654
88	Moldovan leu	MDL	f	MDL	\N	2017-11-12 18:50:00.489217	2017-11-12 18:50:00.489223
89	Malagasy ariary	MGA	f	FMG	\N	2017-11-12 18:50:00.489817	2017-11-12 18:50:00.489825
90	Macedonian denar	MKD	f	MKD	\N	2017-11-12 18:50:00.490481	2017-11-12 18:50:00.490488
91	Myanma kyat	MMK	f	K	\N	2017-11-12 18:50:00.491152	2017-11-12 18:50:00.491167
92	Mongolian tugrik	MNT	f	₮	\N	2017-11-12 18:50:00.49252	2017-11-12 18:50:00.492535
93	Macanese pataca	MOP	f	P	\N	2017-11-12 18:50:00.49397	2017-11-12 18:50:00.493987
94	Mauritanian ouguiya	MRO	f	UM	\N	2017-11-12 18:50:00.495135	2017-11-12 18:50:00.495143
95	Mauritian rupee	MUR	f	Rs	\N	2017-11-12 18:50:00.496144	2017-11-12 18:50:00.496154
96	Maldivian rufiyaa	MVR	f	Rf	\N	2017-11-12 18:50:00.496997	2017-11-12 18:50:00.497007
97	Malawian kwacha	MWK	f	MK	\N	2017-11-12 18:50:00.497724	2017-11-12 18:50:00.497735
98	Mexican peso	MXN	f	$	\N	2017-11-12 18:50:00.498357	2017-11-12 18:50:00.498363
99	Malaysian ringgit	MYR	f	RM	\N	2017-11-12 18:50:00.498847	2017-11-12 18:50:00.498853
100	Mozambican metical	MZM	f	MTn	\N	2017-11-12 18:50:00.499336	2017-11-12 18:50:00.499342
101	Namibian dollar	NAD	f	N$	\N	2017-11-12 18:50:00.499825	2017-11-12 18:50:00.499831
102	Nigerian Naira	NGN	t	₦	566	2017-11-12 18:50:00.500391	2017-11-12 18:50:00.500399
103	Nicaraguan córdoba	NIO	f	C$	\N	2017-11-12 18:50:00.500925	2017-11-12 18:50:00.500931
104	Norwegian krone	NOK	f	kr	\N	2017-11-12 18:50:00.501437	2017-11-12 18:50:00.501443
105	Nepalese rupee	NPR	f	NRs	\N	2017-11-12 18:50:00.501932	2017-11-12 18:50:00.501937
106	New Zealand dollar	NZD	f	NZ$	\N	2017-11-12 18:50:00.502399	2017-11-12 18:50:00.502404
107	Omani rial	OMR	f	OMR	\N	2017-11-12 18:50:00.502829	2017-11-12 18:50:00.502834
108	Panamanian balboa	PAB	f	B./	\N	2017-11-12 18:50:00.503258	2017-11-12 18:50:00.503264
109	Peruvian nuevo sol	PEN	f	S/.	\N	2017-11-12 18:50:00.503923	2017-11-12 18:50:00.503929
110	Papua New Guinean kina	PGK	f	K	\N	2017-11-12 18:50:00.504383	2017-11-12 18:50:00.504388
111	Philippine peso	PHP	f	₱	\N	2017-11-12 18:50:00.504854	2017-11-12 18:50:00.504859
112	Pakistani rupee	PKR	f	Rs.	\N	2017-11-12 18:50:00.505319	2017-11-12 18:50:00.505324
113	Polish zloty	PLN	f	zł	\N	2017-11-12 18:50:00.505789	2017-11-12 18:50:00.505794
114	Paraguayan guarani	PYG	f	₲	\N	2017-11-12 18:50:00.50628	2017-11-12 18:50:00.506285
115	Qatari riyal	QAR	f	QR	\N	2017-11-12 18:50:00.507193	2017-11-12 18:50:00.507209
116	Romanian leu	RON	f	L	\N	2017-11-12 18:50:00.508575	2017-11-12 18:50:00.50859
117	Serbian dinar	RSD	f	din.	\N	2017-11-12 18:50:00.509647	2017-11-12 18:50:00.509658
118	Russian ruble	RUB	f	R	\N	2017-11-12 18:50:00.510407	2017-11-12 18:50:00.510413
119	Saudi riyal	SAR	f	SR	\N	2017-11-12 18:50:00.510991	2017-11-12 18:50:00.510998
120	Solomon Islands dollar	SBD	f	SI$	\N	2017-11-12 18:50:00.511533	2017-11-12 18:50:00.511539
121	Seychellois rupee	SCR	f	SR	\N	2017-11-12 18:50:00.512068	2017-11-12 18:50:00.512074
122	Sudanese pound	SDG	f	SDG	\N	2017-11-12 18:50:00.512641	2017-11-12 18:50:00.512649
123	Swedish krona	SEK	f	kr	\N	2017-11-12 18:50:00.51317	2017-11-12 18:50:00.513176
124	Singapore dollar	SGD	f	S$	\N	2017-11-12 18:50:00.513956	2017-11-12 18:50:00.513975
125	Saint Helena pound	SHP	f	£	\N	2017-11-12 18:50:00.515047	2017-11-12 18:50:00.515061
126	Sierra Leonean leone	SLL	f	Le	\N	2017-11-12 18:50:00.515957	2017-11-12 18:50:00.515965
127	Somali shilling	SOS	f	Sh.	\N	2017-11-12 18:50:00.516601	2017-11-12 18:50:00.516608
128	Surinamese dollar	SRD	f	$	\N	2017-11-12 18:50:00.517178	2017-11-12 18:50:00.517185
129	Syrian pound	SYP	f	LS	\N	2017-11-12 18:50:00.517744	2017-11-12 18:50:00.517751
130	Swazi lilangeni	SZL	f	E	\N	2017-11-12 18:50:00.518262	2017-11-12 18:50:00.518269
131	Thai baht	THB	f	฿	\N	2017-11-12 18:50:00.518767	2017-11-12 18:50:00.518773
132	Tajikistani somoni	TJS	f	TJS	\N	2017-11-12 18:50:00.519295	2017-11-12 18:50:00.519303
133	Turkmen manat	TMT	f	m	\N	2017-11-12 18:50:00.519949	2017-11-12 18:50:00.519959
134	Tunisian dinar	TND	f	DT	\N	2017-11-12 18:50:00.520572	2017-11-12 18:50:00.520579
135	Turkish new lira	TRY	f	TRY	\N	2017-11-12 18:50:00.52108	2017-11-12 18:50:00.521085
136	Trinidad and Tobago dollar	TTD	f	TT$	\N	2017-11-12 18:50:00.521586	2017-11-12 18:50:00.521594
137	New Taiwan dollar	TWD	f	NT$	\N	2017-11-12 18:50:00.522131	2017-11-12 18:50:00.522136
138	Tanzanian shilling	TZS	f	TZS	\N	2017-11-12 18:50:00.522645	2017-11-12 18:50:00.522651
139	Ukrainian hryvnia	UAH	f	UAH	\N	2017-11-12 18:50:00.523272	2017-11-12 18:50:00.523285
140	Ugandan shilling	UGX	f	USh	\N	2017-11-12 18:50:00.524689	2017-11-12 18:50:00.524703
141	United States dollar	USD	f	$	844	2017-11-12 18:50:00.525885	2017-11-12 18:50:00.525904
142	Uruguayan peso	UYU	f	$U	\N	2017-11-12 18:50:00.527053	2017-11-12 18:50:00.527061
143	Uzbekistani som	UZS	f	UZS	\N	2017-11-12 18:50:00.528157	2017-11-12 18:50:00.528168
144	Venezuelan bolivar	VEB	f	Bs	\N	2017-11-12 18:50:00.529131	2017-11-12 18:50:00.529141
145	Vietnamese dong	VND	f	₫	\N	2017-11-12 18:50:00.529759	2017-11-12 18:50:00.529768
146	Vanuatu vatu	VUV	f	VT	\N	2017-11-12 18:50:00.530387	2017-11-12 18:50:00.530395
147	Samoan tala	WST	f	WS$	\N	2017-11-12 18:50:00.530957	2017-11-12 18:50:00.530966
148	Central African CFA franc	XAF	f	CFA	\N	2017-11-12 18:50:00.531511	2017-11-12 18:50:00.531519
149	East Caribbean dollar	XCD	f	EC$	\N	2017-11-12 18:50:00.532097	2017-11-12 18:50:00.532104
150	Special Drawing Rights	XDR	f	SDR	\N	2017-11-12 18:50:00.5326	2017-11-12 18:50:00.532607
151	West African CFA franc	XOF	f	CFA	\N	2017-11-12 18:50:00.53312	2017-11-12 18:50:00.533128
152	CFP franc	XPF	f	F	\N	2017-11-12 18:50:00.53361	2017-11-12 18:50:00.533617
153	Yemeni rial	YER	f	YER	\N	2017-11-12 18:50:00.534158	2017-11-12 18:50:00.534166
154	South African rand	ZAR	f	R	\N	2017-11-12 18:50:00.534711	2017-11-12 18:50:00.534719
155	Zambian kwacha	ZMK	f	ZK	\N	2017-11-12 18:50:00.535239	2017-11-12 18:50:00.535247
156	Zimbabwean dollar	ZWR	f	Z$	\N	2017-11-12 18:50:00.535776	2017-11-12 18:50:00.535784
\.


--
-- Data for Name: customer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY customer (id, address_id, client_id, last_updated, date_created, station_id) FROM stdin;
7	2	1	2017-11-21 01:42:34.336979	2017-11-21 01:42:34.336995	1
\.


--
-- Data for Name: customer_billing; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY customer_billing (id, code, start_date, end_date, amount, customer_id, client_id, last_updated, date_created) FROM stdin;
\.


--
-- Data for Name: customer_billing_record; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY customer_billing_record (id, code, customer_billing_id, customer_id, device_id, network_id, client_id, last_updated, date_created) FROM stdin;
\.


--
-- Data for Name: customer_deduction; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY customer_deduction (id, customer_billing_id, customer_billing_record_id, client_id, last_updated, date_created) FROM stdin;
\.


--
-- Data for Name: customer_invoice; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY customer_invoice (id, code, customer_id, customer_billing_id, client_id, last_updated, date_created) FROM stdin;
\.


--
-- Data for Name: customer_receipt; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY customer_receipt (id, code, customer_invoice_id, customer_id, client_id, last_updated, date_created) FROM stdin;
\.


--
-- Data for Name: customer_settings; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY customer_settings (id, debt_threshold_warning, sms_notification, email_notification, client_id, last_updated, date_created, customer_id) FROM stdin;
4	2000	f	t	1	2017-11-21 01:42:34.358118	2017-11-21 01:42:34.358135	7
\.


--
-- Data for Name: deduction; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY deduction (id, billing_id, billing_record_id, client_id, last_updated, date_created) FROM stdin;
\.


--
-- Data for Name: device; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY device (id, code, reference_code, customer_id, point_id, is_coordinator, is_node, is_active, product_id, network_id, station_id, installation_id, client_id, last_updated, date_created) FROM stdin;
1	2TWDQR3NAQ	\N	\N	1	t	t	t	1	1	1	\N	1	2017-11-12 18:54:47.573302	2017-11-12 18:54:47.573311
2	Q2EY0LQQ6X	\N	\N	2	f	t	t	2	1	1	1	1	2017-11-21 08:56:12.762642	2017-11-12 19:21:25.499693
\.


--
-- Data for Name: device_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY device_type (id, name, slug, client_id, last_updated, date_created) FROM stdin;
\.


--
-- Data for Name: geojson_address; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY geojson_address (id, polygon, last_updated, date_created) FROM stdin;
\.


--
-- Data for Name: geojson_point; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY geojson_point (id, point, longitude, latitude, last_updated, date_created) FROM stdin;
1	0101000020E6100000F564FED137590B409E6D7F78A7D41940	3.4185635	6.4576701	2017-11-12 18:54:47.530783	2017-11-12 18:54:47.530811
2	0101000020E6100000F564FED137590B409E6D7F78A7D41940	3.4185635	6.4576701	2017-11-12 19:21:25.48461	2017-11-12 19:21:25.484628
\.


--
-- Data for Name: installation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY installation (id, title, code, model_number, capacity, measurement_index, location_id, station_id, service_id, installation_type_id, client_id, last_updated, date_created) FROM stdin;
1	Gas Generator	SMART-METERING-UWGFMZ	MAX434	10	kWh	1	1	2	5	1	2017-11-12 18:53:27.803251	2017-11-12 18:53:27.803278
\.


--
-- Data for Name: installation_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY installation_type (id, title, code, html, script, last_updated, date_created) FROM stdin;
1	Incubator	incubator	<div><div class='row c_g_r'><div class='col-sm-12'><div class='chart-wrapper'><div class='chart-stage'><div class='row'><div class='col-sm-4'><div class='chart-title' style='text-align: center;'>Temperature</div><div class='chart-stage' id='temperatureChart' style='width: 100%;height:300px;'></div></div><div class='col-sm-4'><div class='chart-title' style='text-align: center;'>Humidity</div><div class='chart-stage' id='humidityChart' style='width: 100%;height:300px;'></div></div><div class='col-sm-4'><div class='chart-title' style='text-align: center;'>CO<sub>2</sub></div><div class='chart-stage' id='coChart' style='width: 100%;height:300px;'></div></div></div></div></div></div></div><div class='row c_g_r'><div class='col-sm-6'><div class='chart-wrapper' style=''><div class='chart-title'>Active Devices</div><div class='chart-stage chart-map-large' style='height: 100%;max-height: 410px;overflow-y: scroll;'><div class='table'><div class='thead tr'><span class='td'>ID</span><span class='td'>code</span><span class='td'>Last Reading</span><span class='td'>Location</span><span class='td center'>Status</span></div><div class='tr'><span class='td'>0895567</span><span class='td'>#eg7850ghb</span><span class='td'>MARCH 22, 2017<span class='caption'>15:18</span></span><span class='td'>jakande, isolo<span class='caption'>oke-afa, ejigbo.</span></span><span class='td center'>Active</span></div></div></div></div></div><div class='col-sm-6 col-md-3'><div class='chart-wrapper'><div class='chart-title'>Available Devices</div><div class='chart-stage chart-map-small'><h1 style='text-align: center;vertical-align: middle;line-height: 165px;font-size: 4rem;color:#46b8da;'>20</h1></div></div></div><div class='col-sm-6 col-md-3'><div class='chart-wrapper'><div class='chart-title'>Connected Devices</div><div class='chart-stage chart-map-small'><h1 style='text-align: center;vertical-align: middle;line-height: 165px;font-size: 4rem;color:#47a447;'>16</h1></div></div></div><div class='col-sm-6 col-md-3'><div class='chart-wrapper'><div class='chart-title'>Deactivated Devices</div><div class='chart-stage chart-map-small'><h1 style='text-align: center;vertical-align: middle;line-height: 165px;font-size: 4rem;color:#d84a38;'>3</h1></div></div></div><div class='col-sm-6 col-md-3'><div class='chart-wrapper'><div class='chart-title'>Irregular Devices</div><div class='chart-stage chart-map-small'><h1 style='text-align: center;vertical-align: middle;line-height: 165px;font-size: 4rem;color:#FF6E01;'>1</h1></div></div></div></div><div class='row c_g_r'><div class='col-sm-12'><div class='chart-wrapper'><div class='chart-stage' id='fullTempChart' style='width: 100%;height:450px;'></div></div></div></div><div class='row c_g_r'><div class='col-sm-12'><div class='chart-wrapper'><div class='chart-stage' id='fullHumidity' style='width: 100%;height:450px;'></div></div></div></div><div class='row c_g_r'><div class='col-sm-12'><div class='chart-wrapper'><div class='chart-stage' id='fullCoChart' style='width: 100%;height:450px;'></div></div></div></div></div>	var humidityChart = echarts.init(document.getElementById('humidityChart'));var humidityChartOption = {tooltip: {formatter: '{a} <br/>{b} : {c}%'},toolbox: {feature: {restore: {},saveAsImage: {}}},series: [{name: 'Humidity',type: 'gauge',detail: {formatter: '{value}%'},data: [{value: 50, name: 'rh'}]}]};setInterval(function () {humidityChartOption.series[0].data[0].value = (Math.random() * 100).toFixed(2) - 0;humidityChart.setOption(humidityChartOption, true);}, 2000);var temperatureChart = echarts.init(document.getElementById('temperatureChart'));var temperatureChartOption = {tooltip: {formatter: '{a} <br/>{b} : {c}%'},toolbox: {feature: {restore: {},saveAsImage: {}}},series: [{name: 'Temp.',type: 'gauge',detail: {formatter: '{value}%'},data: [{value: 50, name: '°C'}]}]};setInterval(function () {temperatureChartOption.series[0].data[0].value = (Math.random() * 100).toFixed(2) - 0;temperatureChart.setOption(temperatureChartOption, true);}, 2000);var coChart = echarts.init(document.getElementById('coChart'));var coChartOption = {tooltip: {formatter: '{a} <br/>{b} : {c}%'},toolbox: {feature: {restore: {},saveAsImage: {}}},series: [{name: 'Temp.',type: 'gauge',detail: {formatter: '{value}%'},data: [{value: 50, name: 'ppm'}]}]};setInterval(function () {coChartOption.series[0].data[0].value = (Math.random() * 3000).toFixed(2) - 0;coChart.setOption(coChartOption, true);}, 2000);var fullTempChart = echarts.init(document.getElementById('fullTempChart'));var fullTempChartOption = {title: {text: 'Temperature Reading',subtext: 'real time temperature measurements from device'},tooltip: {trigger: 'axis'},legend: {data: ['High', 'Low']},toolbox: {show: true,feature: {dataZoom: {yAxisIndex: 'none'},dataView: {readOnly: false},magicType: {type: ['line', 'bar']},restore: {},saveAsImage: {}}},xAxis: {type: 'category',boundaryGap: false,data: ['09:35', '09:50', '10:05', '10:20', '10:35', '10:50', '11:05']},yAxis: {type: 'value',axisLabel: {formatter: '{value} °C'}},series: [{name: 'High',type: 'line',data: [27, 28, 27.5, 27, 27.3, 27.8, 27.0],markPoint: {data: [{type: 'max', name: 'Maximum'},{type: 'min', name: 'Minimum'}]},markLine: {data: [{type: 'average', name: 'Average'}]}},{name: 'Low',type: 'line',data: [1, -2, 2, 5, 3, 2, 0],markPoint: {data: [{name: 'Week Min', value: -2, xAxis: 1, yAxis: -1.5}]},markLine: {data: [{type: 'average', name: 'Average'},[{symbol: 'none',x: '90%',yAxis: 'max'}, {symbol: 'circle',label: {normal: {position: 'start',formatter: 'Max'}},type: 'max',name: 'Highest Point'}]]}}]};fullTempChart.setOption(fullTempChartOption);var fullHumidity = echarts.init(document.getElementById('fullHumidity'));var fullHumidityData = [];var data1 = [];var data2 = [];for (var i = 0; i < 100; i++) {fullHumidityData.push('Period' + i);data1.push((Math.sin(i / 5) * (i / 5 - 10) + i / 6) * 5);data2.push((Math.cos(i / 5) * (i / 5 - 10) + i / 6) * 5);}	2017-11-12 18:50:12.037516	2017-11-12 18:50:12.037534
2	Cargo Trucks	trucks	<div><div class='row c_g_r'><div class='col-sm-12'><div class='chart-wrapper'><div class='chart-stage'><div class='row'><div class='col-sm-4'><div class='chart-title' style='text-align: center;'>Temperature</div><div class='chart-stage' id='temperatureChart' style='width: 100%;height:300px;'></div></div><div class='col-sm-4'><div class='chart-title' style='text-align: center;'>Humidity</div><div class='chart-stage' id='humidityChart' style='width: 100%;height:300px;'></div></div><div class='col-sm-4'><div class='chart-title' style='text-align: center;'>CO<sub>2</sub></div><div class='chart-stage' id='coChart' style='width: 100%;height:300px;'></div></div></div></div></div></div></div><div class='row c_g_r'><div class='col-sm-6'><div class='chart-wrapper' style=''><div class='chart-title'>Active Devices</div><div class='chart-stage chart-map-large' style='height: 100%;max-height: 410px;overflow-y: scroll;'><div class='table'><div class='thead tr'><span class='td'>ID</span><span class='td'>code</span><span class='td'>Last Reading</span><span class='td'>Location</span><span class='td center'>Status</span></div><div class='tr'><span class='td'>0895567</span><span class='td'>#eg7850ghb</span><span class='td'>MARCH 22, 2017<span class='caption'>15:18</span></span><span class='td'>jakande, isolo<span class='caption'>oke-afa, ejigbo.</span></span><span class='td center'>Active</span></div></div></div></div></div><div class='col-sm-6 col-md-3'><div class='chart-wrapper'><div class='chart-title'>Available Devices</div><div class='chart-stage chart-map-small'><h1 style='text-align: center;vertical-align: middle;line-height: 165px;font-size: 4rem;color:#46b8da;'>20</h1></div></div></div><div class='col-sm-6 col-md-3'><div class='chart-wrapper'><div class='chart-title'>Connected Devices</div><div class='chart-stage chart-map-small'><h1 style='text-align: center;vertical-align: middle;line-height: 165px;font-size: 4rem;color:#47a447;'>16</h1></div></div></div><div class='col-sm-6 col-md-3'><div class='chart-wrapper'><div class='chart-title'>Deactivated Devices</div><div class='chart-stage chart-map-small'><h1 style='text-align: center;vertical-align: middle;line-height: 165px;font-size: 4rem;color:#d84a38;'>3</h1></div></div></div><div class='col-sm-6 col-md-3'><div class='chart-wrapper'><div class='chart-title'>Irregular Devices</div><div class='chart-stage chart-map-small'><h1 style='text-align: center;vertical-align: middle;line-height: 165px;font-size: 4rem;color:#FF6E01;'>1</h1></div></div></div></div><div class='row c_g_r'><div class='col-sm-12'><div class='chart-wrapper'><div class='chart-stage' id='fullTempChart' style='width: 100%;height:450px;'></div></div></div></div><div class='row c_g_r'><div class='col-sm-12'><div class='chart-wrapper'><div class='chart-stage' id='fullHumidity' style='width: 100%;height:450px;'></div></div></div></div><div class='row c_g_r'><div class='col-sm-12'><div class='chart-wrapper'><div class='chart-stage' id='fullCoChart' style='width: 100%;height:450px;'></div></div></div></div></div>	var humidityChart = echarts.init(document.getElementById('humidityChart'));var humidityChartOption = {tooltip: {formatter: '{a} <br/>{b} : {c}%'},toolbox: {feature: {restore: {},saveAsImage: {}}},series: [{name: 'Humidity',type: 'gauge',detail: {formatter: '{value}%'},data: [{value: 50, name: 'rh'}]}]};setInterval(function () {humidityChartOption.series[0].data[0].value = (Math.random() * 100).toFixed(2) - 0;humidityChart.setOption(humidityChartOption, true);}, 2000);var temperatureChart = echarts.init(document.getElementById('temperatureChart'));var temperatureChartOption = {tooltip: {formatter: '{a} <br/>{b} : {c}%'},toolbox: {feature: {restore: {},saveAsImage: {}}},series: [{name: 'Temp.',type: 'gauge',detail: {formatter: '{value}%'},data: [{value: 50, name: '°C'}]}]};setInterval(function () {temperatureChartOption.series[0].data[0].value = (Math.random() * 100).toFixed(2) - 0;temperatureChart.setOption(temperatureChartOption, true);}, 2000);var coChart = echarts.init(document.getElementById('coChart'));var coChartOption = {tooltip: {formatter: '{a} <br/>{b} : {c}%'},toolbox: {feature: {restore: {},saveAsImage: {}}},series: [{name: 'Temp.',type: 'gauge',detail: {formatter: '{value}%'},data: [{value: 50, name: 'ppm'}]}]};setInterval(function () {coChartOption.series[0].data[0].value = (Math.random() * 3000).toFixed(2) - 0;coChart.setOption(coChartOption, true);}, 2000);var fullTempChart = echarts.init(document.getElementById('fullTempChart'));var fullTempChartOption = {title: {text: 'Temperature Reading',subtext: 'real time temperature measurements from device'},tooltip: {trigger: 'axis'},legend: {data: ['High', 'Low']},toolbox: {show: true,feature: {dataZoom: {yAxisIndex: 'none'},dataView: {readOnly: false},magicType: {type: ['line', 'bar']},restore: {},saveAsImage: {}}},xAxis: {type: 'category',boundaryGap: false,data: ['09:35', '09:50', '10:05', '10:20', '10:35', '10:50', '11:05']},yAxis: {type: 'value',axisLabel: {formatter: '{value} °C'}},series: [{name: 'High',type: 'line',data: [27, 28, 27.5, 27, 27.3, 27.8, 27.0],markPoint: {data: [{type: 'max', name: 'Maximum'},{type: 'min', name: 'Minimum'}]},markLine: {data: [{type: 'average', name: 'Average'}]}},{name: 'Low',type: 'line',data: [1, -2, 2, 5, 3, 2, 0],markPoint: {data: [{name: 'Week Min', value: -2, xAxis: 1, yAxis: -1.5}]},markLine: {data: [{type: 'average', name: 'Average'},[{symbol: 'none',x: '90%',yAxis: 'max'}, {symbol: 'circle',label: {normal: {position: 'start',formatter: 'Max'}},type: 'max',name: 'Highest Point'}]]}}]};fullTempChart.setOption(fullTempChartOption);var fullHumidity = echarts.init(document.getElementById('fullHumidity'));var fullHumidityData = [];var data1 = [];var data2 = [];for (var i = 0; i < 100; i++) {fullHumidityData.push('Period' + i);data1.push((Math.sin(i / 5) * (i / 5 - 10) + i / 6) * 5);data2.push((Math.cos(i / 5) * (i / 5 - 10) + i / 6) * 5);}	2017-11-12 18:50:12.078222	2017-11-12 18:50:12.07825
3	Shipping Containers	containers	<div><div class='row c_g_r'><div class='col-sm-12'><div class='chart-wrapper'><div class='chart-stage'><div class='row'><div class='col-sm-4'><div class='chart-title' style='text-align: center;'>Temperature</div><div class='chart-stage' id='temperatureChart' style='width: 100%;height:300px;'></div></div><div class='col-sm-4'><div class='chart-title' style='text-align: center;'>Humidity</div><div class='chart-stage' id='humidityChart' style='width: 100%;height:300px;'></div></div><div class='col-sm-4'><div class='chart-title' style='text-align: center;'>CO<sub>2</sub></div><div class='chart-stage' id='coChart' style='width: 100%;height:300px;'></div></div></div></div></div></div></div><div class='row c_g_r'><div class='col-sm-6'><div class='chart-wrapper' style=''><div class='chart-title'>Active Devices</div><div class='chart-stage chart-map-large' style='height: 100%;max-height: 410px;overflow-y: scroll;'><div class='table'><div class='thead tr'><span class='td'>ID</span><span class='td'>code</span><span class='td'>Last Reading</span><span class='td'>Location</span><span class='td center'>Status</span></div><div class='tr'><span class='td'>0895567</span><span class='td'>#eg7850ghb</span><span class='td'>MARCH 22, 2017<span class='caption'>15:18</span></span><span class='td'>jakande, isolo<span class='caption'>oke-afa, ejigbo.</span></span><span class='td center'>Active</span></div></div></div></div></div><div class='col-sm-6 col-md-3'><div class='chart-wrapper'><div class='chart-title'>Available Devices</div><div class='chart-stage chart-map-small'><h1 style='text-align: center;vertical-align: middle;line-height: 165px;font-size: 4rem;color:#46b8da;'>20</h1></div></div></div><div class='col-sm-6 col-md-3'><div class='chart-wrapper'><div class='chart-title'>Connected Devices</div><div class='chart-stage chart-map-small'><h1 style='text-align: center;vertical-align: middle;line-height: 165px;font-size: 4rem;color:#47a447;'>16</h1></div></div></div><div class='col-sm-6 col-md-3'><div class='chart-wrapper'><div class='chart-title'>Deactivated Devices</div><div class='chart-stage chart-map-small'><h1 style='text-align: center;vertical-align: middle;line-height: 165px;font-size: 4rem;color:#d84a38;'>3</h1></div></div></div><div class='col-sm-6 col-md-3'><div class='chart-wrapper'><div class='chart-title'>Irregular Devices</div><div class='chart-stage chart-map-small'><h1 style='text-align: center;vertical-align: middle;line-height: 165px;font-size: 4rem;color:#FF6E01;'>1</h1></div></div></div></div><div class='row c_g_r'><div class='col-sm-12'><div class='chart-wrapper'><div class='chart-stage' id='fullTempChart' style='width: 100%;height:450px;'></div></div></div></div><div class='row c_g_r'><div class='col-sm-12'><div class='chart-wrapper'><div class='chart-stage' id='fullHumidity' style='width: 100%;height:450px;'></div></div></div></div><div class='row c_g_r'><div class='col-sm-12'><div class='chart-wrapper'><div class='chart-stage' id='fullCoChart' style='width: 100%;height:450px;'></div></div></div></div></div>	var humidityChart = echarts.init(document.getElementById('humidityChart'));var humidityChartOption = {tooltip: {formatter: '{a} <br/>{b} : {c}%'},toolbox: {feature: {restore: {},saveAsImage: {}}},series: [{name: 'Humidity',type: 'gauge',detail: {formatter: '{value}%'},data: [{value: 50, name: 'rh'}]}]};setInterval(function () {humidityChartOption.series[0].data[0].value = (Math.random() * 100).toFixed(2) - 0;humidityChart.setOption(humidityChartOption, true);}, 2000);var temperatureChart = echarts.init(document.getElementById('temperatureChart'));var temperatureChartOption = {tooltip: {formatter: '{a} <br/>{b} : {c}%'},toolbox: {feature: {restore: {},saveAsImage: {}}},series: [{name: 'Temp.',type: 'gauge',detail: {formatter: '{value}%'},data: [{value: 50, name: '°C'}]}]};setInterval(function () {temperatureChartOption.series[0].data[0].value = (Math.random() * 100).toFixed(2) - 0;temperatureChart.setOption(temperatureChartOption, true);}, 2000);var coChart = echarts.init(document.getElementById('coChart'));var coChartOption = {tooltip: {formatter: '{a} <br/>{b} : {c}%'},toolbox: {feature: {restore: {},saveAsImage: {}}},series: [{name: 'Temp.',type: 'gauge',detail: {formatter: '{value}%'},data: [{value: 50, name: 'ppm'}]}]};setInterval(function () {coChartOption.series[0].data[0].value = (Math.random() * 3000).toFixed(2) - 0;coChart.setOption(coChartOption, true);}, 2000);var fullTempChart = echarts.init(document.getElementById('fullTempChart'));var fullTempChartOption = {title: {text: 'Temperature Reading',subtext: 'real time temperature measurements from device'},tooltip: {trigger: 'axis'},legend: {data: ['High', 'Low']},toolbox: {show: true,feature: {dataZoom: {yAxisIndex: 'none'},dataView: {readOnly: false},magicType: {type: ['line', 'bar']},restore: {},saveAsImage: {}}},xAxis: {type: 'category',boundaryGap: false,data: ['09:35', '09:50', '10:05', '10:20', '10:35', '10:50', '11:05']},yAxis: {type: 'value',axisLabel: {formatter: '{value} °C'}},series: [{name: 'High',type: 'line',data: [27, 28, 27.5, 27, 27.3, 27.8, 27.0],markPoint: {data: [{type: 'max', name: 'Maximum'},{type: 'min', name: 'Minimum'}]},markLine: {data: [{type: 'average', name: 'Average'}]}},{name: 'Low',type: 'line',data: [1, -2, 2, 5, 3, 2, 0],markPoint: {data: [{name: 'Week Min', value: -2, xAxis: 1, yAxis: -1.5}]},markLine: {data: [{type: 'average', name: 'Average'},[{symbol: 'none',x: '90%',yAxis: 'max'}, {symbol: 'circle',label: {normal: {position: 'start',formatter: 'Max'}},type: 'max',name: 'Highest Point'}]]}}]};fullTempChart.setOption(fullTempChartOption);var fullHumidity = echarts.init(document.getElementById('fullHumidity'));var fullHumidityData = [];var data1 = [];var data2 = [];for (var i = 0; i < 100; i++) {fullHumidityData.push('Period' + i);data1.push((Math.sin(i / 5) * (i / 5 - 10) + i / 6) * 5);data2.push((Math.cos(i / 5) * (i / 5 - 10) + i / 6) * 5);}	2017-11-12 18:50:12.111252	2017-11-12 18:50:12.111268
4	Cars, Bikes, Vans e.t.c	vehicle	<div><div class='row c_g_r'><div class='col-sm-12'><div class='chart-wrapper'><div class='chart-stage'><div class='row'><div class='col-sm-4'><div class='chart-title' style='text-align: center;'>Temperature</div><div class='chart-stage' id='temperatureChart' style='width: 100%;height:300px;'></div></div><div class='col-sm-4'><div class='chart-title' style='text-align: center;'>Humidity</div><div class='chart-stage' id='humidityChart' style='width: 100%;height:300px;'></div></div><div class='col-sm-4'><div class='chart-title' style='text-align: center;'>CO<sub>2</sub></div><div class='chart-stage' id='coChart' style='width: 100%;height:300px;'></div></div></div></div></div></div></div><div class='row c_g_r'><div class='col-sm-6'><div class='chart-wrapper' style=''><div class='chart-title'>Active Devices</div><div class='chart-stage chart-map-large' style='height: 100%;max-height: 410px;overflow-y: scroll;'><div class='table'><div class='thead tr'><span class='td'>ID</span><span class='td'>code</span><span class='td'>Last Reading</span><span class='td'>Location</span><span class='td center'>Status</span></div><div class='tr'><span class='td'>0895567</span><span class='td'>#eg7850ghb</span><span class='td'>MARCH 22, 2017<span class='caption'>15:18</span></span><span class='td'>jakande, isolo<span class='caption'>oke-afa, ejigbo.</span></span><span class='td center'>Active</span></div></div></div></div></div><div class='col-sm-6 col-md-3'><div class='chart-wrapper'><div class='chart-title'>Available Devices</div><div class='chart-stage chart-map-small'><h1 style='text-align: center;vertical-align: middle;line-height: 165px;font-size: 4rem;color:#46b8da;'>20</h1></div></div></div><div class='col-sm-6 col-md-3'><div class='chart-wrapper'><div class='chart-title'>Connected Devices</div><div class='chart-stage chart-map-small'><h1 style='text-align: center;vertical-align: middle;line-height: 165px;font-size: 4rem;color:#47a447;'>16</h1></div></div></div><div class='col-sm-6 col-md-3'><div class='chart-wrapper'><div class='chart-title'>Deactivated Devices</div><div class='chart-stage chart-map-small'><h1 style='text-align: center;vertical-align: middle;line-height: 165px;font-size: 4rem;color:#d84a38;'>3</h1></div></div></div><div class='col-sm-6 col-md-3'><div class='chart-wrapper'><div class='chart-title'>Irregular Devices</div><div class='chart-stage chart-map-small'><h1 style='text-align: center;vertical-align: middle;line-height: 165px;font-size: 4rem;color:#FF6E01;'>1</h1></div></div></div></div><div class='row c_g_r'><div class='col-sm-12'><div class='chart-wrapper'><div class='chart-stage' id='fullTempChart' style='width: 100%;height:450px;'></div></div></div></div><div class='row c_g_r'><div class='col-sm-12'><div class='chart-wrapper'><div class='chart-stage' id='fullHumidity' style='width: 100%;height:450px;'></div></div></div></div><div class='row c_g_r'><div class='col-sm-12'><div class='chart-wrapper'><div class='chart-stage' id='fullCoChart' style='width: 100%;height:450px;'></div></div></div></div></div>	var humidityChart = echarts.init(document.getElementById('humidityChart'));var humidityChartOption = {tooltip: {formatter: '{a} <br/>{b} : {c}%'},toolbox: {feature: {restore: {},saveAsImage: {}}},series: [{name: 'Humidity',type: 'gauge',detail: {formatter: '{value}%'},data: [{value: 50, name: 'rh'}]}]};setInterval(function () {humidityChartOption.series[0].data[0].value = (Math.random() * 100).toFixed(2) - 0;humidityChart.setOption(humidityChartOption, true);}, 2000);var temperatureChart = echarts.init(document.getElementById('temperatureChart'));var temperatureChartOption = {tooltip: {formatter: '{a} <br/>{b} : {c}%'},toolbox: {feature: {restore: {},saveAsImage: {}}},series: [{name: 'Temp.',type: 'gauge',detail: {formatter: '{value}%'},data: [{value: 50, name: '°C'}]}]};setInterval(function () {temperatureChartOption.series[0].data[0].value = (Math.random() * 100).toFixed(2) - 0;temperatureChart.setOption(temperatureChartOption, true);}, 2000);var coChart = echarts.init(document.getElementById('coChart'));var coChartOption = {tooltip: {formatter: '{a} <br/>{b} : {c}%'},toolbox: {feature: {restore: {},saveAsImage: {}}},series: [{name: 'Temp.',type: 'gauge',detail: {formatter: '{value}%'},data: [{value: 50, name: 'ppm'}]}]};setInterval(function () {coChartOption.series[0].data[0].value = (Math.random() * 3000).toFixed(2) - 0;coChart.setOption(coChartOption, true);}, 2000);var fullTempChart = echarts.init(document.getElementById('fullTempChart'));var fullTempChartOption = {title: {text: 'Temperature Reading',subtext: 'real time temperature measurements from device'},tooltip: {trigger: 'axis'},legend: {data: ['High', 'Low']},toolbox: {show: true,feature: {dataZoom: {yAxisIndex: 'none'},dataView: {readOnly: false},magicType: {type: ['line', 'bar']},restore: {},saveAsImage: {}}},xAxis: {type: 'category',boundaryGap: false,data: ['09:35', '09:50', '10:05', '10:20', '10:35', '10:50', '11:05']},yAxis: {type: 'value',axisLabel: {formatter: '{value} °C'}},series: [{name: 'High',type: 'line',data: [27, 28, 27.5, 27, 27.3, 27.8, 27.0],markPoint: {data: [{type: 'max', name: 'Maximum'},{type: 'min', name: 'Minimum'}]},markLine: {data: [{type: 'average', name: 'Average'}]}},{name: 'Low',type: 'line',data: [1, -2, 2, 5, 3, 2, 0],markPoint: {data: [{name: 'Week Min', value: -2, xAxis: 1, yAxis: -1.5}]},markLine: {data: [{type: 'average', name: 'Average'},[{symbol: 'none',x: '90%',yAxis: 'max'}, {symbol: 'circle',label: {normal: {position: 'start',formatter: 'Max'}},type: 'max',name: 'Highest Point'}]]}}]};fullTempChart.setOption(fullTempChartOption);var fullHumidity = echarts.init(document.getElementById('fullHumidity'));var fullHumidityData = [];var data1 = [];var data2 = [];for (var i = 0; i < 100; i++) {fullHumidityData.push('Period' + i);data1.push((Math.sin(i / 5) * (i / 5 - 10) + i / 6) * 5);data2.push((Math.cos(i / 5) * (i / 5 - 10) + i / 6) * 5);}	2017-11-12 18:50:12.137136	2017-11-12 18:50:12.137156
5	Gas Meters, Generators, Others	gas	<div><div class='row c_g_r'><div class='col-sm-12'><div class='chart-wrapper'><div class='chart-stage'><div class='row'><div class='col-sm-4'><div class='chart-title' style='text-align: center;'>Temperature</div><div class='chart-stage' id='temperatureChart' style='width: 100%;height:300px;'></div></div><div class='col-sm-4'><div class='chart-title' style='text-align: center;'>Humidity</div><div class='chart-stage' id='humidityChart' style='width: 100%;height:300px;'></div></div><div class='col-sm-4'><div class='chart-title' style='text-align: center;'>CO<sub>2</sub></div><div class='chart-stage' id='coChart' style='width: 100%;height:300px;'></div></div></div></div></div></div></div><div class='row c_g_r'><div class='col-sm-6'><div class='chart-wrapper' style=''><div class='chart-title'>Active Devices</div><div class='chart-stage chart-map-large' style='height: 100%;max-height: 410px;overflow-y: scroll;'><div class='table'><div class='thead tr'><span class='td'>ID</span><span class='td'>code</span><span class='td'>Last Reading</span><span class='td'>Location</span><span class='td center'>Status</span></div><div class='tr'><span class='td'>0895567</span><span class='td'>#eg7850ghb</span><span class='td'>MARCH 22, 2017<span class='caption'>15:18</span></span><span class='td'>jakande, isolo<span class='caption'>oke-afa, ejigbo.</span></span><span class='td center'>Active</span></div></div></div></div></div><div class='col-sm-6 col-md-3'><div class='chart-wrapper'><div class='chart-title'>Available Devices</div><div class='chart-stage chart-map-small'><h1 style='text-align: center;vertical-align: middle;line-height: 165px;font-size: 4rem;color:#46b8da;'>20</h1></div></div></div><div class='col-sm-6 col-md-3'><div class='chart-wrapper'><div class='chart-title'>Connected Devices</div><div class='chart-stage chart-map-small'><h1 style='text-align: center;vertical-align: middle;line-height: 165px;font-size: 4rem;color:#47a447;'>16</h1></div></div></div><div class='col-sm-6 col-md-3'><div class='chart-wrapper'><div class='chart-title'>Deactivated Devices</div><div class='chart-stage chart-map-small'><h1 style='text-align: center;vertical-align: middle;line-height: 165px;font-size: 4rem;color:#d84a38;'>3</h1></div></div></div><div class='col-sm-6 col-md-3'><div class='chart-wrapper'><div class='chart-title'>Irregular Devices</div><div class='chart-stage chart-map-small'><h1 style='text-align: center;vertical-align: middle;line-height: 165px;font-size: 4rem;color:#FF6E01;'>1</h1></div></div></div></div><div class='row c_g_r'><div class='col-sm-12'><div class='chart-wrapper'><div class='chart-stage' id='fullTempChart' style='width: 100%;height:450px;'></div></div></div></div><div class='row c_g_r'><div class='col-sm-12'><div class='chart-wrapper'><div class='chart-stage' id='fullHumidity' style='width: 100%;height:450px;'></div></div></div></div><div class='row c_g_r'><div class='col-sm-12'><div class='chart-wrapper'><div class='chart-stage' id='fullCoChart' style='width: 100%;height:450px;'></div></div></div></div></div>	var humidityChart = echarts.init(document.getElementById('humidityChart'));var humidityChartOption = {tooltip: {formatter: '{a} <br/>{b} : {c}%'},toolbox: {feature: {restore: {},saveAsImage: {}}},series: [{name: 'Humidity',type: 'gauge',detail: {formatter: '{value}%'},data: [{value: 50, name: 'rh'}]}]};setInterval(function () {humidityChartOption.series[0].data[0].value = (Math.random() * 100).toFixed(2) - 0;humidityChart.setOption(humidityChartOption, true);}, 2000);var temperatureChart = echarts.init(document.getElementById('temperatureChart'));var temperatureChartOption = {tooltip: {formatter: '{a} <br/>{b} : {c}%'},toolbox: {feature: {restore: {},saveAsImage: {}}},series: [{name: 'Temp.',type: 'gauge',detail: {formatter: '{value}%'},data: [{value: 50, name: '°C'}]}]};setInterval(function () {temperatureChartOption.series[0].data[0].value = (Math.random() * 100).toFixed(2) - 0;temperatureChart.setOption(temperatureChartOption, true);}, 2000);var coChart = echarts.init(document.getElementById('coChart'));var coChartOption = {tooltip: {formatter: '{a} <br/>{b} : {c}%'},toolbox: {feature: {restore: {},saveAsImage: {}}},series: [{name: 'Temp.',type: 'gauge',detail: {formatter: '{value}%'},data: [{value: 50, name: 'ppm'}]}]};setInterval(function () {coChartOption.series[0].data[0].value = (Math.random() * 3000).toFixed(2) - 0;coChart.setOption(coChartOption, true);}, 2000);var fullTempChart = echarts.init(document.getElementById('fullTempChart'));var fullTempChartOption = {title: {text: 'Temperature Reading',subtext: 'real time temperature measurements from device'},tooltip: {trigger: 'axis'},legend: {data: ['High', 'Low']},toolbox: {show: true,feature: {dataZoom: {yAxisIndex: 'none'},dataView: {readOnly: false},magicType: {type: ['line', 'bar']},restore: {},saveAsImage: {}}},xAxis: {type: 'category',boundaryGap: false,data: ['09:35', '09:50', '10:05', '10:20', '10:35', '10:50', '11:05']},yAxis: {type: 'value',axisLabel: {formatter: '{value} °C'}},series: [{name: 'High',type: 'line',data: [27, 28, 27.5, 27, 27.3, 27.8, 27.0],markPoint: {data: [{type: 'max', name: 'Maximum'},{type: 'min', name: 'Minimum'}]},markLine: {data: [{type: 'average', name: 'Average'}]}},{name: 'Low',type: 'line',data: [1, -2, 2, 5, 3, 2, 0],markPoint: {data: [{name: 'Week Min', value: -2, xAxis: 1, yAxis: -1.5}]},markLine: {data: [{type: 'average', name: 'Average'},[{symbol: 'none',x: '90%',yAxis: 'max'}, {symbol: 'circle',label: {normal: {position: 'start',formatter: 'Max'}},type: 'max',name: 'Highest Point'}]]}}]};fullTempChart.setOption(fullTempChartOption);var fullHumidity = echarts.init(document.getElementById('fullHumidity'));var fullHumidityData = [];var data1 = [];var data2 = [];for (var i = 0; i < 100; i++) {fullHumidityData.push('Period' + i);data1.push((Math.sin(i / 5) * (i / 5 - 10) + i / 6) * 5);data2.push((Math.cos(i / 5) * (i / 5 - 10) + i / 6) * 5);}	2017-11-12 18:50:12.164738	2017-11-12 18:50:12.164756
6	Electricity Meter	electricity	<div><div class='row c_g_r'><div class='col-sm-12'><div class='chart-wrapper'><div class='chart-stage'><div class='row'><div class='col-sm-4'><div class='chart-title' style='text-align: center;'>Temperature</div><div class='chart-stage' id='temperatureChart' style='width: 100%;height:300px;'></div></div><div class='col-sm-4'><div class='chart-title' style='text-align: center;'>Humidity</div><div class='chart-stage' id='humidityChart' style='width: 100%;height:300px;'></div></div><div class='col-sm-4'><div class='chart-title' style='text-align: center;'>CO<sub>2</sub></div><div class='chart-stage' id='coChart' style='width: 100%;height:300px;'></div></div></div></div></div></div></div><div class='row c_g_r'><div class='col-sm-6'><div class='chart-wrapper' style=''><div class='chart-title'>Active Devices</div><div class='chart-stage chart-map-large' style='height: 100%;max-height: 410px;overflow-y: scroll;'><div class='table'><div class='thead tr'><span class='td'>ID</span><span class='td'>code</span><span class='td'>Last Reading</span><span class='td'>Location</span><span class='td center'>Status</span></div><div class='tr'><span class='td'>0895567</span><span class='td'>#eg7850ghb</span><span class='td'>MARCH 22, 2017<span class='caption'>15:18</span></span><span class='td'>jakande, isolo<span class='caption'>oke-afa, ejigbo.</span></span><span class='td center'>Active</span></div></div></div></div></div><div class='col-sm-6 col-md-3'><div class='chart-wrapper'><div class='chart-title'>Available Devices</div><div class='chart-stage chart-map-small'><h1 style='text-align: center;vertical-align: middle;line-height: 165px;font-size: 4rem;color:#46b8da;'>20</h1></div></div></div><div class='col-sm-6 col-md-3'><div class='chart-wrapper'><div class='chart-title'>Connected Devices</div><div class='chart-stage chart-map-small'><h1 style='text-align: center;vertical-align: middle;line-height: 165px;font-size: 4rem;color:#47a447;'>16</h1></div></div></div><div class='col-sm-6 col-md-3'><div class='chart-wrapper'><div class='chart-title'>Deactivated Devices</div><div class='chart-stage chart-map-small'><h1 style='text-align: center;vertical-align: middle;line-height: 165px;font-size: 4rem;color:#d84a38;'>3</h1></div></div></div><div class='col-sm-6 col-md-3'><div class='chart-wrapper'><div class='chart-title'>Irregular Devices</div><div class='chart-stage chart-map-small'><h1 style='text-align: center;vertical-align: middle;line-height: 165px;font-size: 4rem;color:#FF6E01;'>1</h1></div></div></div></div><div class='row c_g_r'><div class='col-sm-12'><div class='chart-wrapper'><div class='chart-stage' id='fullTempChart' style='width: 100%;height:450px;'></div></div></div></div><div class='row c_g_r'><div class='col-sm-12'><div class='chart-wrapper'><div class='chart-stage' id='fullHumidity' style='width: 100%;height:450px;'></div></div></div></div><div class='row c_g_r'><div class='col-sm-12'><div class='chart-wrapper'><div class='chart-stage' id='fullCoChart' style='width: 100%;height:450px;'></div></div></div></div></div>	var humidityChart = echarts.init(document.getElementById('humidityChart'));var humidityChartOption = {tooltip: {formatter: '{a} <br/>{b} : {c}%'},toolbox: {feature: {restore: {},saveAsImage: {}}},series: [{name: 'Humidity',type: 'gauge',detail: {formatter: '{value}%'},data: [{value: 50, name: 'rh'}]}]};setInterval(function () {humidityChartOption.series[0].data[0].value = (Math.random() * 100).toFixed(2) - 0;humidityChart.setOption(humidityChartOption, true);}, 2000);var temperatureChart = echarts.init(document.getElementById('temperatureChart'));var temperatureChartOption = {tooltip: {formatter: '{a} <br/>{b} : {c}%'},toolbox: {feature: {restore: {},saveAsImage: {}}},series: [{name: 'Temp.',type: 'gauge',detail: {formatter: '{value}%'},data: [{value: 50, name: '°C'}]}]};setInterval(function () {temperatureChartOption.series[0].data[0].value = (Math.random() * 100).toFixed(2) - 0;temperatureChart.setOption(temperatureChartOption, true);}, 2000);var coChart = echarts.init(document.getElementById('coChart'));var coChartOption = {tooltip: {formatter: '{a} <br/>{b} : {c}%'},toolbox: {feature: {restore: {},saveAsImage: {}}},series: [{name: 'Temp.',type: 'gauge',detail: {formatter: '{value}%'},data: [{value: 50, name: 'ppm'}]}]};setInterval(function () {coChartOption.series[0].data[0].value = (Math.random() * 3000).toFixed(2) - 0;coChart.setOption(coChartOption, true);}, 2000);var fullTempChart = echarts.init(document.getElementById('fullTempChart'));var fullTempChartOption = {title: {text: 'Temperature Reading',subtext: 'real time temperature measurements from device'},tooltip: {trigger: 'axis'},legend: {data: ['High', 'Low']},toolbox: {show: true,feature: {dataZoom: {yAxisIndex: 'none'},dataView: {readOnly: false},magicType: {type: ['line', 'bar']},restore: {},saveAsImage: {}}},xAxis: {type: 'category',boundaryGap: false,data: ['09:35', '09:50', '10:05', '10:20', '10:35', '10:50', '11:05']},yAxis: {type: 'value',axisLabel: {formatter: '{value} °C'}},series: [{name: 'High',type: 'line',data: [27, 28, 27.5, 27, 27.3, 27.8, 27.0],markPoint: {data: [{type: 'max', name: 'Maximum'},{type: 'min', name: 'Minimum'}]},markLine: {data: [{type: 'average', name: 'Average'}]}},{name: 'Low',type: 'line',data: [1, -2, 2, 5, 3, 2, 0],markPoint: {data: [{name: 'Week Min', value: -2, xAxis: 1, yAxis: -1.5}]},markLine: {data: [{type: 'average', name: 'Average'},[{symbol: 'none',x: '90%',yAxis: 'max'}, {symbol: 'circle',label: {normal: {position: 'start',formatter: 'Max'}},type: 'max',name: 'Highest Point'}]]}}]};fullTempChart.setOption(fullTempChartOption);var fullHumidity = echarts.init(document.getElementById('fullHumidity'));var fullHumidityData = [];var data1 = [];var data2 = [];for (var i = 0; i < 100; i++) {fullHumidityData.push('Period' + i);data1.push((Math.sin(i / 5) * (i / 5 - 10) + i / 6) * 5);data2.push((Math.cos(i / 5) * (i / 5 - 10) + i / 6) * 5);}	2017-11-12 18:50:12.197313	2017-11-12 18:50:12.197337
\.


--
-- Data for Name: invoice; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY invoice (id, code, month, year, is_paid, service_charge, billing_amount, total_charge, client_id, last_updated, date_created) FROM stdin;
\.


--
-- Data for Name: location; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY location (id, line1, line2, city_id, state_id, country_id, longitude, latitude, last_updated, date_created) FROM stdin;
1	228a Eti-osa way, Dophin Estate		675	33	149	3.4185635	6.4576701	2017-11-12 18:51:46.80733	2017-11-12 18:51:45.489012
\.


--
-- Data for Name: metric; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY metric (id, name, slug, dimension, last_updated, date_created) FROM stdin;
1	Voltage	voltage	V	2017-11-12 18:50:12.234724	2017-11-12 18:50:12.234744
2	Current	current	I	2017-11-12 18:50:12.244064	2017-11-12 18:50:12.244091
3	Power	power	kW	2017-11-12 18:50:12.251435	2017-11-12 18:50:12.251456
4	Energy	energy	kWh	2017-11-12 18:50:12.258966	2017-11-12 18:50:12.258996
5	Pressure	pressure	bar	2017-11-12 18:50:12.267652	2017-11-12 18:50:12.267673
6	Power Factor	power-factor	pF	2017-11-12 18:50:12.275138	2017-11-12 18:50:12.275157
7	Temperature	temperature	Celsius	2017-11-12 18:50:12.284715	2017-11-12 18:50:12.28474
8	Humidity	humidity	RH	2017-11-12 18:50:12.294452	2017-11-12 18:50:12.294477
\.


--
-- Data for Name: navigation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY navigation (id, name, code, url, icon, is_img, last_updated, date_created) FROM stdin;
1	Overview	home	/	fa fa-home	f	2017-11-12 18:50:10.672361	2017-11-12 18:50:10.67238
2	Network	networks	networks	/static/images/icons/mesh.png	t	2017-11-12 18:50:10.682462	2017-11-12 18:50:10.68248
3	Installations	installations	installations	/static/images/icons/devices.png	t	2017-11-12 18:50:10.724121	2017-11-12 18:50:10.724147
4	Customers	customers	customers	fa-users	f	2017-11-12 18:50:10.770519	2017-11-12 18:50:10.770544
5	Reports	client_report_dashboard	/reports	fa-bar-chart-o	f	2017-11-12 18:50:10.807449	2017-11-12 18:50:10.807464
\.


--
-- Data for Name: network; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY network (id, code, geojson_address_id, station_id, client_id, last_updated, date_created) FROM stdin;
1	1:1510512706.88:U4OUA	\N	1	1	2017-11-12 18:51:46.877382	2017-11-12 18:51:46.877406
\.


--
-- Data for Name: network_services; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY network_services (network_id, service_id) FROM stdin;
\.


--
-- Data for Name: product; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product (id, name, description, code, on_default, is_network, is_alert, last_updated, date_created) FROM stdin;
1	Mesh Gateway	Co-ordinator device for relaying information between server and measuring devices	CRDT-c05	t	t	f	2017-11-12 18:50:12.308912	2017-11-12 18:50:12.308931
2	Gas Digital Interfacer Device	measurement device connected to analog installation or metering device	GDI-d04	t	f	f	2017-11-12 18:50:12.488489	2017-11-12 18:50:12.488509
3	Electricity Digital Interfacer Device	measurement device connected to analog installation or metering device	EDI-d02	t	f	f	2017-11-12 18:50:12.588784	2017-11-12 18:50:12.588796
4	Alert Device	Alert device that's excited when an alert threshold has been reached	ALT-x01	f	f	t	2017-11-12 18:50:12.710795	2017-11-12 18:50:12.71081
\.


--
-- Data for Name: product_image; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product_image (id, product_id, image_url, last_updated, date_created) FROM stdin;
\.


--
-- Data for Name: product_metrics; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product_metrics (metric_id, product_id) FROM stdin;
5	2
7	2
8	2
1	3
2	3
6	3
\.


--
-- Data for Name: product_services; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product_services (product_id, service_id) FROM stdin;
1	4
1	1
1	2
1	6
1	5
1	3
1	7
2	2
2	6
2	7
3	2
3	6
3	7
4	4
4	1
4	2
4	6
4	5
4	3
4	7
\.


--
-- Data for Name: receipt; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY receipt (id, code, invoice_id, client_id, last_updated, date_created) FROM stdin;
\.


--
-- Data for Name: recurrent_card; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY recurrent_card (id, token, mask, brand, exp_month, exp_year, client_id, last_updated, date_created) FROM stdin;
\.


--
-- Data for Name: report; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY report (id, handle, name, description, start, "end", client_id, file_path, last_updated, date_created) FROM stdin;
\.


--
-- Data for Name: restricted_domain; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY restricted_domain (id, domain, last_updated, date_created) FROM stdin;
\.


--
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY role (id, name, slug, description, client_id, is_admin, assignable, last_updated, date_created) FROM stdin;
1	super admin	super-admin	Super Admin Rights	\N	t	f	2017-11-12 18:50:00.257762	2017-11-12 18:50:00.257784
2	admin reports permission	admin-reports-permission	View & Generate Reports	\N	t	t	2017-11-12 18:50:00.267689	2017-11-12 18:50:00.267702
3	admin clients permission	admin-clients-permission	Manage Clients Relations	\N	t	t	2017-11-12 18:50:00.268796	2017-11-12 18:50:00.268807
4	admin client activation permission	admin-client-activation-permission	Activate/Deactivate Clients	\N	t	t	2017-11-12 18:50:00.269614	2017-11-12 18:50:00.269619
5	admin messages permission	admin-messages-permission	View & Respond to Messages	\N	t	t	2017-11-12 18:50:00.270301	2017-11-12 18:50:00.270307
6	admin complaints permission	admin-complaints-permission	View & Respond to Complaints	\N	t	t	2017-11-12 18:50:00.27087	2017-11-12 18:50:00.270875
7	admin finance permission	admin-finance-permission	Manage Finance & Remittance	\N	t	t	2017-11-12 18:50:00.271393	2017-11-12 18:50:00.271398
8	admin assets permission	admin-assets-permission	Manage Platform Assets	\N	t	t	2017-11-12 18:50:00.27201	2017-11-12 18:50:00.272016
9	admin staff permission	admin-staff-permission	Manage Staff Members	\N	t	t	2017-11-12 18:50:00.272623	2017-11-12 18:50:00.272628
10	admin transactions permission	admin-transactions-permission	View & Manage Transactions	\N	t	t	2017-11-12 18:50:00.273175	2017-11-12 18:50:00.273181
11	client admin	client-admin	Client Admin Rights	\N	f	t	2017-11-12 18:50:00.274033	2017-11-12 18:50:00.274039
12	client reports permission	client-reports-permission	View & Generate Reports	\N	f	t	2017-11-12 18:50:00.274628	2017-11-12 18:50:00.274634
13	client customers permission	client-customers-permission	Manage Customers Relations	\N	f	t	2017-11-12 18:50:00.275247	2017-11-12 18:50:00.275252
14	client messages permission	client-messages-permission	View & Respond to Messages	\N	f	t	2017-11-12 18:50:00.276151	2017-11-12 18:50:00.276159
15	client complaints permission	client-complaints-permission	View & Respond to Complaints	\N	f	t	2017-11-12 18:50:00.277068	2017-11-12 18:50:00.277076
16	client finance permission	client-finance-permission	Manage Finance & Remittance	\N	f	t	2017-11-12 18:50:00.277995	2017-11-12 18:50:00.278001
17	client assets permission	client-assets-permission	Manage Assets	\N	f	t	2017-11-12 18:50:00.278652	2017-11-12 18:50:00.278659
18	client staff permission	client-staff-permission	Manage Staff Members	\N	f	t	2017-11-12 18:50:00.279281	2017-11-12 18:50:00.279286
19	client transactions permission	client-transactions-permission	View & Manage Transactions	\N	f	t	2017-11-12 18:50:00.279948	2017-11-12 18:50:00.279954
\.


--
-- Data for Name: role_access_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY role_access_groups (role_id, access_group_id) FROM stdin;
\.


--
-- Data for Name: service; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY service (id, name, code, icon_class, is_active, parent_id, description, service_charge, last_updated, date_created) FROM stdin;
1	Grid Monitoring Service	grid-monitoring	fa fa-exchange	t	\N	Monitor the complex electrical grids in real time from the point of generation to consumption	10000	2017-11-12 18:50:10.604081	2017-11-12 18:50:10.604098
2	Smart Metering Service	smart-metering	fa fa-clock-o	t	\N	Monitor in real time utility installations	10000	2017-11-12 18:50:10.618164	2017-11-12 18:50:10.618203
3	Energy Management Service	energy-management	fa fa-lightbulb-o	t	\N	Manage energy consumption billing and metering of a group of consumers	10000	2017-11-12 18:50:10.628692	2017-11-12 18:50:10.62871
4	Mobile Assets Tracking Service	mobile-assets	fa fa-truck	f	\N	Monitor the health of mobile assets in real time. View critical asset parameters on customizable dashboards	10000	2017-11-12 18:50:10.636778	2017-11-12 18:50:10.636808
5	Remote Video Monitoring Service	remote-monitoring	fa fa-video-camera	f	\N	Remotely monitor mission-critical plants and equipment, assets securely.	10000	2017-11-12 18:50:10.644757	2017-11-12 18:50:10.644781
6	Atmospheric Monitoring	atmospheric-monitoring	fa fa-genderless	t	\N	Atmospheric monitor mission-critical plants and equipment, assets securely.	10000	2017-11-12 18:50:10.653825	2017-11-12 18:50:10.653851
7	Process Monitoring	process-monitoring	fa fa-circle-thin	t	\N	monitor remotely and in real-time	10000	2017-11-12 18:50:10.661383	2017-11-12 18:50:10.6614
\.


--
-- Data for Name: settings; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY settings (id, allow_debt_warning, debt_threshold_warning, billing_period, is_postpaid, logo, alerts_dep_resolved, stations_dep_resolved, accounts_dep_resolved, bank_account_dep_resolved, billing_dep_resolved, client_id, last_updated, date_created) FROM stdin;
1	f	2000	1	t	\N	f	t	t	f	t	1	2017-11-12 18:51:46.853486	2017-11-12 18:50:11.06085
\.


--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) FROM stdin;
\.


--
-- Data for Name: state; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY state (id, name, slug, code, country_id, last_updated, date_created) FROM stdin;
1	Bauchi	bauchi	BCH	149	2017-11-12 18:50:01.065742	2017-11-12 18:50:01.065752
2	Ekiti	ekiti	EKI	149	2017-11-12 18:50:01.069845	2017-11-12 18:50:01.069854
3	Zamfara	zamfara	ZFR	149	2017-11-12 18:50:01.071776	2017-11-12 18:50:01.071786
4	Borno	borno	BOR	149	2017-11-12 18:50:01.073349	2017-11-12 18:50:01.073361
5	Bayelsa	bayelsa	BAY	149	2017-11-12 18:50:01.075401	2017-11-12 18:50:01.075425
6	Oyo	oyo	OYO	149	2017-11-12 18:50:01.077381	2017-11-12 18:50:01.077402
7	Nassarawa	nassarawa	NAS	149	2017-11-12 18:50:01.078812	2017-11-12 18:50:01.078821
8	Akwa Ibom	akwa-ibom	AKI	149	2017-11-12 18:50:01.080019	2017-11-12 18:50:01.080029
9	Delta	delta	DEL	149	2017-11-12 18:50:01.081452	2017-11-12 18:50:01.081463
10	Cross River	cross-river	CRI	149	2017-11-12 18:50:01.082712	2017-11-12 18:50:01.082723
11	Yobe	yobe	YBE	149	2017-11-12 18:50:01.083823	2017-11-12 18:50:01.083834
12	Kwara	kwara	KWA	149	2017-11-12 18:50:01.085284	2017-11-12 18:50:01.085297
13	Taraba	taraba	TAR	149	2017-11-12 18:50:01.086616	2017-11-12 18:50:01.086629
14	Sokoto	sokoto	SOK	149	2017-11-12 18:50:01.088052	2017-11-12 18:50:01.088066
15	Osun	osun	OSU	149	2017-11-12 18:50:01.089506	2017-11-12 18:50:01.089523
16	Kaduna	kaduna	KDA	149	2017-11-12 18:50:01.091723	2017-11-12 18:50:01.091747
17	Ondo	ondo	OND	149	2017-11-12 18:50:01.09379	2017-11-12 18:50:01.093809
18	Niger	niger	NIG	149	2017-11-12 18:50:01.095381	2017-11-12 18:50:01.095395
19	Rivers	rivers	RVS	149	2017-11-12 18:50:01.097004	2017-11-12 18:50:01.097018
20	Gombe	gombe	GMB	149	2017-11-12 18:50:01.09863	2017-11-12 18:50:01.098657
21	Jigawa	jigawa	JIG	149	2017-11-12 18:50:01.10056	2017-11-12 18:50:01.10058
22	Abia	abia	ABI	149	2017-11-12 18:50:01.102109	2017-11-12 18:50:01.102122
23	Abuja	abuja	ABJ	149	2017-11-12 18:50:01.103454	2017-11-12 18:50:01.103466
24	Imo	imo	IMO	149	2017-11-12 18:50:01.104939	2017-11-12 18:50:01.104959
25	Anambra	anambra	ABR	149	2017-11-12 18:50:01.106588	2017-11-12 18:50:01.106603
26	Enugu	enugu	ENU	149	2017-11-12 18:50:01.108522	2017-11-12 18:50:01.108538
27	Adamawa	adamawa	ADA	149	2017-11-12 18:50:01.111078	2017-11-12 18:50:01.111112
28	Edo	edo	EDO	149	2017-11-12 18:50:01.113335	2017-11-12 18:50:01.113363
29	Benue	benue	BNE	149	2017-11-12 18:50:01.115393	2017-11-12 18:50:01.115412
30	Plateau	plateau	PLT	149	2017-11-12 18:50:01.117278	2017-11-12 18:50:01.117297
31	Kebbi	kebbi	KEB	149	2017-11-12 18:50:01.119091	2017-11-12 18:50:01.11911
32	Ebonyi	ebonyi	EBN	149	2017-11-12 18:50:01.120833	2017-11-12 18:50:01.120849
33	Lagos	lagos	LAG	149	2017-11-12 18:50:01.122543	2017-11-12 18:50:01.122562
34	Kano	kano	KAN	149	2017-11-12 18:50:01.124458	2017-11-12 18:50:01.124471
35	Ogun	ogun	OGN	149	2017-11-12 18:50:01.126068	2017-11-12 18:50:01.12608
36	Katsina	katsina	KAT	149	2017-11-12 18:50:01.127565	2017-11-12 18:50:01.127577
37	Kogi	kogi	KGI	149	2017-11-12 18:50:01.129713	2017-11-12 18:50:01.129726
\.


--
-- Data for Name: station; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY station (id, name, is_active, location_id, manager_id, client_id, last_updated, date_created) FROM stdin;
1	Kuje Waste 2 Watt Power Plant	t	1	1	1	2017-11-12 18:51:46.826821	2017-11-12 18:51:46.826872
\.


--
-- Data for Name: station_assignees; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY station_assignees (user_id, station_id) FROM stdin;
\.


--
-- Data for Name: station_services; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY station_services (service_id, station_id) FROM stdin;
2	1
\.


--
-- Data for Name: sub_navigation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY sub_navigation (id, navigation_id, name, code, url, is_img, icon, last_updated, date_created) FROM stdin;
1	2	Add Network	networks_new	#!/networks/new	t	static/images/icons/mesh.png	2017-11-12 18:50:10.696663	2017-11-12 18:50:10.696677
2	2	Networks	networks_list	#!/networks/list	t	static/images/icons/mesh.png	2017-11-12 18:50:10.708311	2017-11-12 18:50:10.708327
3	2	Dashboard	networks	#!/networks	f	fa-group	2017-11-12 18:50:10.718042	2017-11-12 18:50:10.718067
4	3	Add Installation	installations	#!/installations/new	f	fa-book	2017-11-12 18:50:10.73908	2017-11-12 18:50:10.739108
5	3	Installations	installation_list	#!/installations/list	f	fa-male	2017-11-12 18:50:10.751192	2017-11-12 18:50:10.751223
6	3	Dashboard	installations	#!/installations	f	fa-group	2017-11-12 18:50:10.76276	2017-11-12 18:50:10.762773
7	4	Add Customer	customers	#!/customers/new	f	fa-book	2017-11-12 18:50:10.781674	2017-11-12 18:50:10.781686
8	4	Customers List	customers	#!/customers/list	f	fa-book	2017-11-12 18:50:10.792757	2017-11-12 18:50:10.792782
9	4	Dashboard	customers	#!/customers	f	fa-group	2017-11-12 18:50:10.801412	2017-11-12 18:50:10.80144
10	5	Devices	client_device_report	/reports	f	fa-group	2017-11-12 18:50:10.814737	2017-11-12 18:50:10.814748
11	5	Payments	client_payment_report	/reports	f	fa-group	2017-11-12 18:50:10.824944	2017-11-12 18:50:10.82497
12	5	Customers	client_customer_report	/reports	f	fa-group	2017-11-12 18:50:10.834895	2017-11-12 18:50:10.834917
13	5	Dashboard	client_report_dashboard	/reports	f	fa-group	2017-11-12 18:50:10.844062	2017-11-12 18:50:10.844079
\.


--
-- Data for Name: subscription; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY subscription (id, service_id, subscription_fee, is_active, devices_dep_resolved, alerts_dep_resolved, inst_dep_resolved, client_id, last_updated, date_created) FROM stdin;
2	6	10000	t	f	f	f	1	2017-11-12 18:50:11.599654	2017-11-12 18:50:11.599668
1	2	10000	t	f	t	t	1	2017-11-20 17:15:13.397698	2017-11-12 18:50:11.569628
\.


--
-- Data for Name: table_desc; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY table_desc (id, cls, description, last_updated, date_created) FROM stdin;
\.


--
-- Data for Name: timezone; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY timezone (id, name, code, "offset", last_updated, date_created) FROM stdin;
1	(GMT+0100) Africa/Lagos	Africa/Lagos	+0100	2017-11-12 18:50:00.336534	2017-11-12 18:50:00.336555
\.


--
-- Data for Name: us_gaz; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY us_gaz (id, seq, word, stdword, token, is_custom) FROM stdin;
\.


--
-- Data for Name: us_lex; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY us_lex (id, seq, word, stdword, token, is_custom) FROM stdin;
\.


--
-- Data for Name: us_rules; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY us_rules (id, rule, is_custom) FROM stdin;
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "user" (id, address_id, email, password, is_active, is_verified, is_demo_account, is_blocked, client_id, login_count, last_login_at, current_login_at, last_login_ip, current_login_ip, last_updated, date_created) FROM stdin;
1	1	demo@atele.org	$2b$12$xSKpTRcjRZq/6sxf3.YgJ.HKWlO0V95dG34coHzIWUfPAmAOJ48xK	t	t	f	f	1	4	2017-11-16 17:56:58.48956	2017-11-16 19:38:16.209599	192.168.1.25	192.168.1.25	2017-11-16 18:38:16.247767	2017-11-12 18:50:11.487016
\.


--
-- Data for Name: user_access_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY user_access_groups (user_id, access_group_id) FROM stdin;
1	5
\.


--
-- Data for Name: user_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY user_roles (user_id, role_id) FROM stdin;
\.


--
-- Data for Name: wallet; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY wallet (id, debt_balance, credit_balance, client_id, last_updated, date_created) FROM stdin;
1	0	0	1	2017-11-12 18:50:11.07888	2017-11-12 18:50:11.078909
\.


SET search_path = tiger, pg_catalog;

--
-- Data for Name: geocode_settings; Type: TABLE DATA; Schema: tiger; Owner: postgres
--

COPY geocode_settings (name, setting, unit, category, short_desc) FROM stdin;
\.


--
-- Data for Name: pagc_gaz; Type: TABLE DATA; Schema: tiger; Owner: postgres
--

COPY pagc_gaz (id, seq, word, stdword, token, is_custom) FROM stdin;
\.


--
-- Data for Name: pagc_lex; Type: TABLE DATA; Schema: tiger; Owner: postgres
--

COPY pagc_lex (id, seq, word, stdword, token, is_custom) FROM stdin;
\.


--
-- Data for Name: pagc_rules; Type: TABLE DATA; Schema: tiger; Owner: postgres
--

COPY pagc_rules (id, rule, is_custom) FROM stdin;
\.


SET search_path = topology, pg_catalog;

--
-- Data for Name: topology; Type: TABLE DATA; Schema: topology; Owner: postgres
--

COPY topology (id, name, srid, "precision", hasz) FROM stdin;
\.


--
-- Data for Name: layer; Type: TABLE DATA; Schema: topology; Owner: postgres
--

COPY layer (topology_id, layer_id, schema_name, table_name, feature_column, feature_type, level, child_id) FROM stdin;
\.


SET search_path = public, pg_catalog;

--
-- Name: access_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('access_group_id_seq', 8, true);


--
-- Name: address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('address_id_seq', 2, true);


--
-- Name: admin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('admin_id_seq', 1, true);


--
-- Name: alert_channel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('alert_channel_id_seq', 1, false);


--
-- Name: alert_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('alert_id_seq', 5, true);


--
-- Name: alert_recipient_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('alert_recipient_id_seq', 5, true);


--
-- Name: alert_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('alert_type_id_seq', 2, true);


--
-- Name: bank_account_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('bank_account_id_seq', 1, false);


--
-- Name: bank_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('bank_id_seq', 20, true);


--
-- Name: billing_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('billing_id_seq', 1, false);


--
-- Name: billing_record_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('billing_record_id_seq', 1, false);


--
-- Name: card_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('card_id_seq', 1, false);


--
-- Name: city_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('city_id_seq', 809, true);


--
-- Name: client_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('client_id_seq', 1, true);


--
-- Name: country_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('country_id_seq', 264, true);


--
-- Name: currency_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('currency_id_seq', 156, true);


--
-- Name: customer_billing_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('customer_billing_id_seq', 1, false);


--
-- Name: customer_billing_record_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('customer_billing_record_id_seq', 1, false);


--
-- Name: customer_deduction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('customer_deduction_id_seq', 1, false);


--
-- Name: customer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('customer_id_seq', 7, true);


--
-- Name: customer_invoice_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('customer_invoice_id_seq', 1, false);


--
-- Name: customer_receipt_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('customer_receipt_id_seq', 1, false);


--
-- Name: customer_settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('customer_settings_id_seq', 4, true);


--
-- Name: deduction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('deduction_id_seq', 1, false);


--
-- Name: device_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('device_id_seq', 2, true);


--
-- Name: device_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('device_type_id_seq', 1, false);


--
-- Name: geojson_address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('geojson_address_id_seq', 1, false);


--
-- Name: geojson_point_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('geojson_point_id_seq', 2, true);


--
-- Name: installation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('installation_id_seq', 1, true);


--
-- Name: installation_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('installation_type_id_seq', 6, true);


--
-- Name: invoice_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('invoice_id_seq', 1, false);


--
-- Name: location_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('location_id_seq', 1, true);


--
-- Name: metric_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('metric_id_seq', 8, true);


--
-- Name: navigation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('navigation_id_seq', 5, true);


--
-- Name: network_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('network_id_seq', 1, true);


--
-- Name: product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('product_id_seq', 4, true);


--
-- Name: product_image_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('product_image_id_seq', 1, false);


--
-- Name: receipt_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('receipt_id_seq', 1, false);


--
-- Name: recurrent_card_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('recurrent_card_id_seq', 1, false);


--
-- Name: report_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('report_id_seq', 1, false);


--
-- Name: restricted_domain_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('restricted_domain_id_seq', 1, false);


--
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('role_id_seq', 19, true);


--
-- Name: service_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('service_id_seq', 7, true);


--
-- Name: settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('settings_id_seq', 1, true);


--
-- Name: state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('state_id_seq', 37, true);


--
-- Name: station_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('station_id_seq', 1, true);


--
-- Name: sub_navigation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sub_navigation_id_seq', 13, true);


--
-- Name: subscription_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('subscription_id_seq', 2, true);


--
-- Name: table_desc_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('table_desc_id_seq', 1, false);


--
-- Name: timezone_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('timezone_id_seq', 1, true);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('user_id_seq', 1, true);


--
-- Name: wallet_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('wallet_id_seq', 1, true);


--
-- Name: access_group _access_group_name; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY access_group
    ADD CONSTRAINT _access_group_name UNIQUE (name, client_id);


--
-- Name: installation _installation_model_number; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY installation
    ADD CONSTRAINT _installation_model_number UNIQUE (model_number, client_id);


--
-- Name: access_group access_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY access_group
    ADD CONSTRAINT access_group_pkey PRIMARY KEY (id);


--
-- Name: address address_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY address
    ADD CONSTRAINT address_pkey PRIMARY KEY (id);


--
-- Name: admin admin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin
    ADD CONSTRAINT admin_pkey PRIMARY KEY (id);


--
-- Name: admin admin_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin
    ADD CONSTRAINT admin_username_key UNIQUE (username);


--
-- Name: alembic_version alembic_version_pkc; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alembic_version
    ADD CONSTRAINT alembic_version_pkc PRIMARY KEY (version_num);


--
-- Name: alert_channel alert_channel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alert_channel
    ADD CONSTRAINT alert_channel_pkey PRIMARY KEY (id);


--
-- Name: alert alert_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alert
    ADD CONSTRAINT alert_pkey PRIMARY KEY (id);


--
-- Name: alert_recipient alert_recipient_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alert_recipient
    ADD CONSTRAINT alert_recipient_pkey PRIMARY KEY (id);


--
-- Name: alert_type alert_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alert_type
    ADD CONSTRAINT alert_type_pkey PRIMARY KEY (id);


--
-- Name: bank_account bank_account_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bank_account
    ADD CONSTRAINT bank_account_pkey PRIMARY KEY (id);


--
-- Name: bank bank_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bank
    ADD CONSTRAINT bank_pkey PRIMARY KEY (id);


--
-- Name: billing billing_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY billing
    ADD CONSTRAINT billing_pkey PRIMARY KEY (id);


--
-- Name: billing_record billing_record_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY billing_record
    ADD CONSTRAINT billing_record_pkey PRIMARY KEY (id);


--
-- Name: card card_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY card
    ADD CONSTRAINT card_pkey PRIMARY KEY (id);


--
-- Name: city city_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY city
    ADD CONSTRAINT city_pkey PRIMARY KEY (id);


--
-- Name: client client_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY client
    ADD CONSTRAINT client_pkey PRIMARY KEY (id);


--
-- Name: country country_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY country
    ADD CONSTRAINT country_pkey PRIMARY KEY (id);


--
-- Name: currency currency_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY currency
    ADD CONSTRAINT currency_pkey PRIMARY KEY (id);


--
-- Name: customer_billing customer_billing_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_billing
    ADD CONSTRAINT customer_billing_pkey PRIMARY KEY (id);


--
-- Name: customer_billing_record customer_billing_record_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_billing_record
    ADD CONSTRAINT customer_billing_record_pkey PRIMARY KEY (id);


--
-- Name: customer_deduction customer_deduction_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_deduction
    ADD CONSTRAINT customer_deduction_pkey PRIMARY KEY (id);


--
-- Name: customer_invoice customer_invoice_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_invoice
    ADD CONSTRAINT customer_invoice_pkey PRIMARY KEY (id);


--
-- Name: customer customer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (id);


--
-- Name: customer_receipt customer_receipt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_receipt
    ADD CONSTRAINT customer_receipt_pkey PRIMARY KEY (id);


--
-- Name: customer_settings customer_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_settings
    ADD CONSTRAINT customer_settings_pkey PRIMARY KEY (id);


--
-- Name: deduction deduction_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY deduction
    ADD CONSTRAINT deduction_pkey PRIMARY KEY (id);


--
-- Name: device device_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY device
    ADD CONSTRAINT device_pkey PRIMARY KEY (id);


--
-- Name: device_type device_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY device_type
    ADD CONSTRAINT device_type_pkey PRIMARY KEY (id);


--
-- Name: geojson_address geojson_address_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY geojson_address
    ADD CONSTRAINT geojson_address_pkey PRIMARY KEY (id);


--
-- Name: geojson_point geojson_point_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY geojson_point
    ADD CONSTRAINT geojson_point_pkey PRIMARY KEY (id);


--
-- Name: installation installation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY installation
    ADD CONSTRAINT installation_pkey PRIMARY KEY (id);


--
-- Name: installation_type installation_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY installation_type
    ADD CONSTRAINT installation_type_pkey PRIMARY KEY (id);


--
-- Name: invoice invoice_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY invoice
    ADD CONSTRAINT invoice_pkey PRIMARY KEY (id);


--
-- Name: location location_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY location
    ADD CONSTRAINT location_pkey PRIMARY KEY (id);


--
-- Name: metric metric_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY metric
    ADD CONSTRAINT metric_pkey PRIMARY KEY (id);


--
-- Name: navigation navigation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY navigation
    ADD CONSTRAINT navigation_pkey PRIMARY KEY (id);


--
-- Name: network network_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY network
    ADD CONSTRAINT network_pkey PRIMARY KEY (id);


--
-- Name: product_image product_image_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_image
    ADD CONSTRAINT product_image_pkey PRIMARY KEY (id);


--
-- Name: product product_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);


--
-- Name: receipt receipt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY receipt
    ADD CONSTRAINT receipt_pkey PRIMARY KEY (id);


--
-- Name: recurrent_card recurrent_card_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY recurrent_card
    ADD CONSTRAINT recurrent_card_pkey PRIMARY KEY (id);


--
-- Name: report report_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY report
    ADD CONSTRAINT report_pkey PRIMARY KEY (id);


--
-- Name: restricted_domain restricted_domain_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY restricted_domain
    ADD CONSTRAINT restricted_domain_pkey PRIMARY KEY (id);


--
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- Name: service service_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY service
    ADD CONSTRAINT service_pkey PRIMARY KEY (id);


--
-- Name: settings settings_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (id);


--
-- Name: state state_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY state
    ADD CONSTRAINT state_pkey PRIMARY KEY (id);


--
-- Name: station station_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY station
    ADD CONSTRAINT station_pkey PRIMARY KEY (id);


--
-- Name: sub_navigation sub_navigation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sub_navigation
    ADD CONSTRAINT sub_navigation_pkey PRIMARY KEY (id);


--
-- Name: subscription subscription_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY subscription
    ADD CONSTRAINT subscription_pkey PRIMARY KEY (id);


--
-- Name: table_desc table_desc_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY table_desc
    ADD CONSTRAINT table_desc_pkey PRIMARY KEY (id);


--
-- Name: timezone timezone_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY timezone
    ADD CONSTRAINT timezone_pkey PRIMARY KEY (id);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: wallet wallet_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY wallet
    ADD CONSTRAINT wallet_pkey PRIMARY KEY (id);


--
-- Name: ix_access_group_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_access_group_client_id ON access_group USING btree (client_id);


--
-- Name: ix_access_group_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_access_group_date_created ON access_group USING btree (date_created);


--
-- Name: ix_access_group_is_client; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_access_group_is_client ON access_group USING btree (is_client);


--
-- Name: ix_access_group_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_access_group_last_updated ON access_group USING btree (last_updated);


--
-- Name: ix_access_group_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_access_group_name ON access_group USING btree (name);


--
-- Name: ix_access_group_permanent; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_access_group_permanent ON access_group USING btree (permanent);


--
-- Name: ix_access_group_slug; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_access_group_slug ON access_group USING btree (slug);


--
-- Name: ix_address_city_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_address_city_id ON address USING btree (city_id);


--
-- Name: ix_address_country_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_address_country_id ON address USING btree (country_id);


--
-- Name: ix_address_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_address_date_created ON address USING btree (date_created);


--
-- Name: ix_address_email; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_address_email ON address USING btree (email);


--
-- Name: ix_address_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_address_last_updated ON address USING btree (last_updated);


--
-- Name: ix_address_phone; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_address_phone ON address USING btree (phone);


--
-- Name: ix_address_state_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_address_state_id ON address USING btree (state_id);


--
-- Name: ix_admin_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_admin_date_created ON admin USING btree (date_created);


--
-- Name: ix_admin_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_admin_last_updated ON admin USING btree (last_updated);


--
-- Name: ix_alert_channel_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_alert_channel_date_created ON alert_channel USING btree (date_created);


--
-- Name: ix_alert_channel_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_alert_channel_last_updated ON alert_channel USING btree (last_updated);


--
-- Name: ix_alert_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_alert_client_id ON alert USING btree (client_id);


--
-- Name: ix_alert_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_alert_date_created ON alert USING btree (date_created);


--
-- Name: ix_alert_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_alert_last_updated ON alert USING btree (last_updated);


--
-- Name: ix_alert_recipient_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_alert_recipient_client_id ON alert_recipient USING btree (client_id);


--
-- Name: ix_alert_recipient_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_alert_recipient_date_created ON alert_recipient USING btree (date_created);


--
-- Name: ix_alert_recipient_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_alert_recipient_last_updated ON alert_recipient USING btree (last_updated);


--
-- Name: ix_alert_type_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_alert_type_date_created ON alert_type USING btree (date_created);


--
-- Name: ix_alert_type_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_alert_type_last_updated ON alert_type USING btree (last_updated);


--
-- Name: ix_bank_account_account_number; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_bank_account_account_number ON bank_account USING btree (account_number);


--
-- Name: ix_bank_account_bank_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_bank_account_bank_id ON bank_account USING btree (bank_id);


--
-- Name: ix_bank_account_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_bank_account_client_id ON bank_account USING btree (client_id);


--
-- Name: ix_bank_account_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_bank_account_date_created ON bank_account USING btree (date_created);


--
-- Name: ix_bank_account_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_bank_account_last_updated ON bank_account USING btree (last_updated);


--
-- Name: ix_bank_country_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_bank_country_id ON bank USING btree (country_id);


--
-- Name: ix_bank_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_bank_date_created ON bank USING btree (date_created);


--
-- Name: ix_bank_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_bank_last_updated ON bank USING btree (last_updated);


--
-- Name: ix_billing_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_billing_client_id ON billing USING btree (client_id);


--
-- Name: ix_billing_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_billing_date_created ON billing USING btree (date_created);


--
-- Name: ix_billing_invoice_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_billing_invoice_id ON billing USING btree (invoice_id);


--
-- Name: ix_billing_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_billing_last_updated ON billing USING btree (last_updated);


--
-- Name: ix_billing_record_billing_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_billing_record_billing_id ON billing_record USING btree (billing_id);


--
-- Name: ix_billing_record_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_billing_record_client_id ON billing_record USING btree (client_id);


--
-- Name: ix_billing_record_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_billing_record_date_created ON billing_record USING btree (date_created);


--
-- Name: ix_billing_record_device_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_billing_record_device_id ON billing_record USING btree (device_id);


--
-- Name: ix_billing_record_installation_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_billing_record_installation_id ON billing_record USING btree (installation_id);


--
-- Name: ix_billing_record_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_billing_record_last_updated ON billing_record USING btree (last_updated);


--
-- Name: ix_billing_record_network_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_billing_record_network_id ON billing_record USING btree (network_id);


--
-- Name: ix_billing_record_subscription_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_billing_record_subscription_id ON billing_record USING btree (subscription_id);


--
-- Name: ix_card_address_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_card_address_id ON card USING btree (address_id);


--
-- Name: ix_card_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_card_client_id ON card USING btree (client_id);


--
-- Name: ix_card_customer_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_card_customer_id ON card USING btree (customer_id);


--
-- Name: ix_card_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_card_date_created ON card USING btree (date_created);


--
-- Name: ix_card_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_card_last_updated ON card USING btree (last_updated);


--
-- Name: ix_card_wallet_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_card_wallet_id ON card USING btree (wallet_id);


--
-- Name: ix_city_code; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_city_code ON city USING btree (code);


--
-- Name: ix_city_country_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_city_country_id ON city USING btree (country_id);


--
-- Name: ix_city_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_city_date_created ON city USING btree (date_created);


--
-- Name: ix_city_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_city_last_updated ON city USING btree (last_updated);


--
-- Name: ix_city_slug; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_city_slug ON city USING btree (slug);


--
-- Name: ix_city_state_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_city_state_id ON city USING btree (state_id);


--
-- Name: ix_client_city_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_client_city_id ON client USING btree (city_id);


--
-- Name: ix_client_country_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_client_country_id ON client USING btree (country_id);


--
-- Name: ix_client_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_client_date_created ON client USING btree (date_created);


--
-- Name: ix_client_email; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_client_email ON client USING btree (email);


--
-- Name: ix_client_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_client_last_updated ON client USING btree (last_updated);


--
-- Name: ix_client_state_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_client_state_id ON client USING btree (state_id);


--
-- Name: ix_client_url; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_client_url ON client USING btree (url);


--
-- Name: ix_country_code; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_country_code ON country USING btree (code);


--
-- Name: ix_country_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_country_date_created ON country USING btree (date_created);


--
-- Name: ix_country_enabled; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_country_enabled ON country USING btree (enabled);


--
-- Name: ix_country_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_country_last_updated ON country USING btree (last_updated);


--
-- Name: ix_country_slug; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_country_slug ON country USING btree (slug);


--
-- Name: ix_currency_code; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_currency_code ON currency USING btree (code);


--
-- Name: ix_currency_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_currency_date_created ON currency USING btree (date_created);


--
-- Name: ix_currency_enabled; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_currency_enabled ON currency USING btree (enabled);


--
-- Name: ix_currency_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_currency_last_updated ON currency USING btree (last_updated);


--
-- Name: ix_currency_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_currency_name ON currency USING btree (name);


--
-- Name: ix_currency_payment_code; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_currency_payment_code ON currency USING btree (payment_code);


--
-- Name: ix_currency_symbol; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_currency_symbol ON currency USING btree (symbol);


--
-- Name: ix_customer_address_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_address_id ON customer USING btree (address_id);


--
-- Name: ix_customer_billing_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_billing_client_id ON customer_billing USING btree (client_id);


--
-- Name: ix_customer_billing_customer_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_billing_customer_id ON customer_billing USING btree (customer_id);


--
-- Name: ix_customer_billing_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_billing_date_created ON customer_billing USING btree (date_created);


--
-- Name: ix_customer_billing_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_billing_last_updated ON customer_billing USING btree (last_updated);


--
-- Name: ix_customer_billing_record_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_billing_record_client_id ON customer_billing_record USING btree (client_id);


--
-- Name: ix_customer_billing_record_customer_billing_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_billing_record_customer_billing_id ON customer_billing_record USING btree (customer_billing_id);


--
-- Name: ix_customer_billing_record_customer_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_billing_record_customer_id ON customer_billing_record USING btree (customer_id);


--
-- Name: ix_customer_billing_record_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_billing_record_date_created ON customer_billing_record USING btree (date_created);


--
-- Name: ix_customer_billing_record_device_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_billing_record_device_id ON customer_billing_record USING btree (device_id);


--
-- Name: ix_customer_billing_record_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_billing_record_last_updated ON customer_billing_record USING btree (last_updated);


--
-- Name: ix_customer_billing_record_network_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_billing_record_network_id ON customer_billing_record USING btree (network_id);


--
-- Name: ix_customer_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_client_id ON customer USING btree (client_id);


--
-- Name: ix_customer_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_date_created ON customer USING btree (date_created);


--
-- Name: ix_customer_deduction_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_deduction_client_id ON customer_deduction USING btree (client_id);


--
-- Name: ix_customer_deduction_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_deduction_date_created ON customer_deduction USING btree (date_created);


--
-- Name: ix_customer_deduction_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_deduction_last_updated ON customer_deduction USING btree (last_updated);


--
-- Name: ix_customer_invoice_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_invoice_client_id ON customer_invoice USING btree (client_id);


--
-- Name: ix_customer_invoice_customer_billing_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_invoice_customer_billing_id ON customer_invoice USING btree (customer_billing_id);


--
-- Name: ix_customer_invoice_customer_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_invoice_customer_id ON customer_invoice USING btree (customer_id);


--
-- Name: ix_customer_invoice_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_invoice_date_created ON customer_invoice USING btree (date_created);


--
-- Name: ix_customer_invoice_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_invoice_last_updated ON customer_invoice USING btree (last_updated);


--
-- Name: ix_customer_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_last_updated ON customer USING btree (last_updated);


--
-- Name: ix_customer_receipt_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_receipt_client_id ON customer_receipt USING btree (client_id);


--
-- Name: ix_customer_receipt_customer_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_receipt_customer_id ON customer_receipt USING btree (customer_id);


--
-- Name: ix_customer_receipt_customer_invoice_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_receipt_customer_invoice_id ON customer_receipt USING btree (customer_invoice_id);


--
-- Name: ix_customer_receipt_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_receipt_date_created ON customer_receipt USING btree (date_created);


--
-- Name: ix_customer_receipt_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_receipt_last_updated ON customer_receipt USING btree (last_updated);


--
-- Name: ix_customer_settings_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_settings_client_id ON customer_settings USING btree (client_id);


--
-- Name: ix_customer_settings_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_settings_date_created ON customer_settings USING btree (date_created);


--
-- Name: ix_customer_settings_email_notification; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_settings_email_notification ON customer_settings USING btree (email_notification);


--
-- Name: ix_customer_settings_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_settings_last_updated ON customer_settings USING btree (last_updated);


--
-- Name: ix_customer_settings_sms_notification; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_settings_sms_notification ON customer_settings USING btree (sms_notification);


--
-- Name: ix_deduction_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_deduction_client_id ON deduction USING btree (client_id);


--
-- Name: ix_deduction_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_deduction_date_created ON deduction USING btree (date_created);


--
-- Name: ix_deduction_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_deduction_last_updated ON deduction USING btree (last_updated);


--
-- Name: ix_device_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_device_client_id ON device USING btree (client_id);


--
-- Name: ix_device_code; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_device_code ON device USING btree (code);


--
-- Name: ix_device_customer_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_device_customer_id ON device USING btree (customer_id);


--
-- Name: ix_device_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_device_date_created ON device USING btree (date_created);


--
-- Name: ix_device_installation_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_device_installation_id ON device USING btree (installation_id);


--
-- Name: ix_device_is_active; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_device_is_active ON device USING btree (is_active);


--
-- Name: ix_device_is_coordinator; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_device_is_coordinator ON device USING btree (is_coordinator);


--
-- Name: ix_device_is_node; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_device_is_node ON device USING btree (is_node);


--
-- Name: ix_device_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_device_last_updated ON device USING btree (last_updated);


--
-- Name: ix_device_network_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_device_network_id ON device USING btree (network_id);


--
-- Name: ix_device_point_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_device_point_id ON device USING btree (point_id);


--
-- Name: ix_device_product_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_device_product_id ON device USING btree (product_id);


--
-- Name: ix_device_reference_code; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_device_reference_code ON device USING btree (reference_code);


--
-- Name: ix_device_station_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_device_station_id ON device USING btree (station_id);


--
-- Name: ix_device_type_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_device_type_client_id ON device_type USING btree (client_id);


--
-- Name: ix_device_type_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_device_type_date_created ON device_type USING btree (date_created);


--
-- Name: ix_device_type_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_device_type_last_updated ON device_type USING btree (last_updated);


--
-- Name: ix_device_type_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_device_type_name ON device_type USING btree (name);


--
-- Name: ix_device_type_slug; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_device_type_slug ON device_type USING btree (slug);


--
-- Name: ix_geojson_address_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_geojson_address_date_created ON geojson_address USING btree (date_created);


--
-- Name: ix_geojson_address_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_geojson_address_last_updated ON geojson_address USING btree (last_updated);


--
-- Name: ix_geojson_point_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_geojson_point_date_created ON geojson_point USING btree (date_created);


--
-- Name: ix_geojson_point_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_geojson_point_last_updated ON geojson_point USING btree (last_updated);


--
-- Name: ix_geojson_point_latitude; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_geojson_point_latitude ON geojson_point USING btree (latitude);


--
-- Name: ix_geojson_point_longitude; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_geojson_point_longitude ON geojson_point USING btree (longitude);


--
-- Name: ix_installation_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_installation_client_id ON installation USING btree (client_id);


--
-- Name: ix_installation_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_installation_date_created ON installation USING btree (date_created);


--
-- Name: ix_installation_installation_type_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_installation_installation_type_id ON installation USING btree (installation_type_id);


--
-- Name: ix_installation_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_installation_last_updated ON installation USING btree (last_updated);


--
-- Name: ix_installation_location_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_installation_location_id ON installation USING btree (location_id);


--
-- Name: ix_installation_model_number; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_installation_model_number ON installation USING btree (model_number);


--
-- Name: ix_installation_service_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_installation_service_id ON installation USING btree (service_id);


--
-- Name: ix_installation_station_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_installation_station_id ON installation USING btree (station_id);


--
-- Name: ix_installation_type_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_installation_type_date_created ON installation_type USING btree (date_created);


--
-- Name: ix_installation_type_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_installation_type_last_updated ON installation_type USING btree (last_updated);


--
-- Name: ix_invoice_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_invoice_client_id ON invoice USING btree (client_id);


--
-- Name: ix_invoice_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_invoice_date_created ON invoice USING btree (date_created);


--
-- Name: ix_invoice_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_invoice_last_updated ON invoice USING btree (last_updated);


--
-- Name: ix_location_city_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_location_city_id ON location USING btree (city_id);


--
-- Name: ix_location_country_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_location_country_id ON location USING btree (country_id);


--
-- Name: ix_location_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_location_date_created ON location USING btree (date_created);


--
-- Name: ix_location_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_location_last_updated ON location USING btree (last_updated);


--
-- Name: ix_location_latitude; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_location_latitude ON location USING btree (latitude);


--
-- Name: ix_location_longitude; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_location_longitude ON location USING btree (longitude);


--
-- Name: ix_location_state_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_location_state_id ON location USING btree (state_id);


--
-- Name: ix_metric_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_metric_date_created ON metric USING btree (date_created);


--
-- Name: ix_metric_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_metric_last_updated ON metric USING btree (last_updated);


--
-- Name: ix_metric_slug; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_metric_slug ON metric USING btree (slug);


--
-- Name: ix_navigation_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_navigation_date_created ON navigation USING btree (date_created);


--
-- Name: ix_navigation_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_navigation_last_updated ON navigation USING btree (last_updated);


--
-- Name: ix_network_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_network_client_id ON network USING btree (client_id);


--
-- Name: ix_network_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_network_date_created ON network USING btree (date_created);


--
-- Name: ix_network_geojson_address_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_network_geojson_address_id ON network USING btree (geojson_address_id);


--
-- Name: ix_network_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_network_last_updated ON network USING btree (last_updated);


--
-- Name: ix_network_station_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_network_station_id ON network USING btree (station_id);


--
-- Name: ix_product_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_product_date_created ON product USING btree (date_created);


--
-- Name: ix_product_image_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_product_image_date_created ON product_image USING btree (date_created);


--
-- Name: ix_product_image_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_product_image_last_updated ON product_image USING btree (last_updated);


--
-- Name: ix_product_is_alert; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_product_is_alert ON product USING btree (is_alert);


--
-- Name: ix_product_is_network; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_product_is_network ON product USING btree (is_network);


--
-- Name: ix_product_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_product_last_updated ON product USING btree (last_updated);


--
-- Name: ix_receipt_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_receipt_client_id ON receipt USING btree (client_id);


--
-- Name: ix_receipt_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_receipt_date_created ON receipt USING btree (date_created);


--
-- Name: ix_receipt_invoice_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_receipt_invoice_id ON receipt USING btree (invoice_id);


--
-- Name: ix_receipt_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_receipt_last_updated ON receipt USING btree (last_updated);


--
-- Name: ix_recurrent_card_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_recurrent_card_client_id ON recurrent_card USING btree (client_id);


--
-- Name: ix_recurrent_card_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_recurrent_card_date_created ON recurrent_card USING btree (date_created);


--
-- Name: ix_recurrent_card_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_recurrent_card_last_updated ON recurrent_card USING btree (last_updated);


--
-- Name: ix_report_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_report_date_created ON report USING btree (date_created);


--
-- Name: ix_report_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_report_last_updated ON report USING btree (last_updated);


--
-- Name: ix_restricted_domain_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_restricted_domain_date_created ON restricted_domain USING btree (date_created);


--
-- Name: ix_restricted_domain_domain; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_restricted_domain_domain ON restricted_domain USING btree (domain);


--
-- Name: ix_restricted_domain_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_restricted_domain_last_updated ON restricted_domain USING btree (last_updated);


--
-- Name: ix_role_assignable; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_role_assignable ON role USING btree (assignable);


--
-- Name: ix_role_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_role_client_id ON role USING btree (client_id);


--
-- Name: ix_role_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_role_date_created ON role USING btree (date_created);


--
-- Name: ix_role_is_admin; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_role_is_admin ON role USING btree (is_admin);


--
-- Name: ix_role_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_role_last_updated ON role USING btree (last_updated);


--
-- Name: ix_role_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_role_name ON role USING btree (name);


--
-- Name: ix_role_slug; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_role_slug ON role USING btree (slug);


--
-- Name: ix_service_code; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_service_code ON service USING btree (code);


--
-- Name: ix_service_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_service_date_created ON service USING btree (date_created);


--
-- Name: ix_service_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_service_last_updated ON service USING btree (last_updated);


--
-- Name: ix_settings_allow_debt_warning; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_settings_allow_debt_warning ON settings USING btree (allow_debt_warning);


--
-- Name: ix_settings_billing_period; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_settings_billing_period ON settings USING btree (billing_period);


--
-- Name: ix_settings_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_settings_client_id ON settings USING btree (client_id);


--
-- Name: ix_settings_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_settings_date_created ON settings USING btree (date_created);


--
-- Name: ix_settings_is_postpaid; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_settings_is_postpaid ON settings USING btree (is_postpaid);


--
-- Name: ix_settings_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_settings_last_updated ON settings USING btree (last_updated);


--
-- Name: ix_state_code; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_state_code ON state USING btree (code);


--
-- Name: ix_state_country_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_state_country_id ON state USING btree (country_id);


--
-- Name: ix_state_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_state_date_created ON state USING btree (date_created);


--
-- Name: ix_state_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_state_last_updated ON state USING btree (last_updated);


--
-- Name: ix_state_slug; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_state_slug ON state USING btree (slug);


--
-- Name: ix_station_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_station_client_id ON station USING btree (client_id);


--
-- Name: ix_station_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_station_date_created ON station USING btree (date_created);


--
-- Name: ix_station_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_station_last_updated ON station USING btree (last_updated);


--
-- Name: ix_station_location_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_station_location_id ON station USING btree (location_id);


--
-- Name: ix_station_manager_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_station_manager_id ON station USING btree (manager_id);


--
-- Name: ix_sub_navigation_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_sub_navigation_date_created ON sub_navigation USING btree (date_created);


--
-- Name: ix_sub_navigation_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_sub_navigation_last_updated ON sub_navigation USING btree (last_updated);


--
-- Name: ix_sub_navigation_navigation_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_sub_navigation_navigation_id ON sub_navigation USING btree (navigation_id);


--
-- Name: ix_subscription_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_subscription_client_id ON subscription USING btree (client_id);


--
-- Name: ix_subscription_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_subscription_date_created ON subscription USING btree (date_created);


--
-- Name: ix_subscription_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_subscription_last_updated ON subscription USING btree (last_updated);


--
-- Name: ix_subscription_service_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_subscription_service_id ON subscription USING btree (service_id);


--
-- Name: ix_table_desc_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_table_desc_date_created ON table_desc USING btree (date_created);


--
-- Name: ix_table_desc_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_table_desc_last_updated ON table_desc USING btree (last_updated);


--
-- Name: ix_timezone_code; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_timezone_code ON timezone USING btree (code);


--
-- Name: ix_timezone_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_timezone_date_created ON timezone USING btree (date_created);


--
-- Name: ix_timezone_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_timezone_last_updated ON timezone USING btree (last_updated);


--
-- Name: ix_timezone_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_timezone_name ON timezone USING btree (name);


--
-- Name: ix_user_address_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_user_address_id ON "user" USING btree (address_id);


--
-- Name: ix_user_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_user_client_id ON "user" USING btree (client_id);


--
-- Name: ix_user_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_user_date_created ON "user" USING btree (date_created);


--
-- Name: ix_user_email; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_user_email ON "user" USING btree (email);


--
-- Name: ix_user_is_active; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_user_is_active ON "user" USING btree (is_active);


--
-- Name: ix_user_is_blocked; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_user_is_blocked ON "user" USING btree (is_blocked);


--
-- Name: ix_user_is_demo_account; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_user_is_demo_account ON "user" USING btree (is_demo_account);


--
-- Name: ix_user_is_verified; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_user_is_verified ON "user" USING btree (is_verified);


--
-- Name: ix_user_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_user_last_updated ON "user" USING btree (last_updated);


--
-- Name: ix_wallet_client_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_wallet_client_id ON wallet USING btree (client_id);


--
-- Name: ix_wallet_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_wallet_date_created ON wallet USING btree (date_created);


--
-- Name: ix_wallet_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_wallet_last_updated ON wallet USING btree (last_updated);


--
-- Name: access_group access_group_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY access_group
    ADD CONSTRAINT access_group_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: address address_city_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY address
    ADD CONSTRAINT address_city_id_fkey FOREIGN KEY (city_id) REFERENCES city(id);


--
-- Name: address address_country_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY address
    ADD CONSTRAINT address_country_id_fkey FOREIGN KEY (country_id) REFERENCES country(id);


--
-- Name: address address_state_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY address
    ADD CONSTRAINT address_state_id_fkey FOREIGN KEY (state_id) REFERENCES state(id);


--
-- Name: alert alert_alert_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alert
    ADD CONSTRAINT alert_alert_type_id_fkey FOREIGN KEY (alert_type_id) REFERENCES alert_type(id);


--
-- Name: alert alert_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alert
    ADD CONSTRAINT alert_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: alert alert_device_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alert
    ADD CONSTRAINT alert_device_id_fkey FOREIGN KEY (device_id) REFERENCES device(id);


--
-- Name: alert alert_installation_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alert
    ADD CONSTRAINT alert_installation_id_fkey FOREIGN KEY (installation_id) REFERENCES installation(id);


--
-- Name: alert_recipient alert_recipient_alert_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alert_recipient
    ADD CONSTRAINT alert_recipient_alert_id_fkey FOREIGN KEY (alert_id) REFERENCES alert(id);


--
-- Name: alert_recipient alert_recipient_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alert_recipient
    ADD CONSTRAINT alert_recipient_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: alert alert_service_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alert
    ADD CONSTRAINT alert_service_id_fkey FOREIGN KEY (service_id) REFERENCES service(id);


--
-- Name: alert alert_station_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alert
    ADD CONSTRAINT alert_station_id_fkey FOREIGN KEY (station_id) REFERENCES station(id);


--
-- Name: associated_channels associated_channels_alert_channel_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY associated_channels
    ADD CONSTRAINT associated_channels_alert_channel_id_fkey FOREIGN KEY (alert_channel_id) REFERENCES alert_channel(id);


--
-- Name: associated_channels associated_channels_alert_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY associated_channels
    ADD CONSTRAINT associated_channels_alert_id_fkey FOREIGN KEY (alert_id) REFERENCES alert(id);


--
-- Name: associated_services associated_services_installation_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY associated_services
    ADD CONSTRAINT associated_services_installation_type_id_fkey FOREIGN KEY (installation_type_id) REFERENCES installation_type(id);


--
-- Name: associated_services associated_services_service_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY associated_services
    ADD CONSTRAINT associated_services_service_id_fkey FOREIGN KEY (service_id) REFERENCES service(id);


--
-- Name: bank_account bank_account_bank_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bank_account
    ADD CONSTRAINT bank_account_bank_id_fkey FOREIGN KEY (bank_id) REFERENCES bank(id);


--
-- Name: bank_account bank_account_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bank_account
    ADD CONSTRAINT bank_account_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: bank bank_country_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bank
    ADD CONSTRAINT bank_country_id_fkey FOREIGN KEY (country_id) REFERENCES country(id);


--
-- Name: billing billing_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY billing
    ADD CONSTRAINT billing_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: billing billing_invoice_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY billing
    ADD CONSTRAINT billing_invoice_id_fkey FOREIGN KEY (invoice_id) REFERENCES invoice(id);


--
-- Name: billing_record billing_record_billing_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY billing_record
    ADD CONSTRAINT billing_record_billing_id_fkey FOREIGN KEY (billing_id) REFERENCES billing(id);


--
-- Name: billing_record billing_record_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY billing_record
    ADD CONSTRAINT billing_record_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: billing_record billing_record_device_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY billing_record
    ADD CONSTRAINT billing_record_device_id_fkey FOREIGN KEY (device_id) REFERENCES device(id);


--
-- Name: billing_record billing_record_installation_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY billing_record
    ADD CONSTRAINT billing_record_installation_id_fkey FOREIGN KEY (installation_id) REFERENCES installation(id);


--
-- Name: billing_record billing_record_network_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY billing_record
    ADD CONSTRAINT billing_record_network_id_fkey FOREIGN KEY (network_id) REFERENCES network(id);


--
-- Name: billing_record billing_record_subscription_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY billing_record
    ADD CONSTRAINT billing_record_subscription_id_fkey FOREIGN KEY (subscription_id) REFERENCES subscription(id);


--
-- Name: card card_address_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY card
    ADD CONSTRAINT card_address_id_fkey FOREIGN KEY (address_id) REFERENCES address(id);


--
-- Name: card card_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY card
    ADD CONSTRAINT card_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: card card_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY card
    ADD CONSTRAINT card_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(id);


--
-- Name: card card_wallet_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY card
    ADD CONSTRAINT card_wallet_id_fkey FOREIGN KEY (wallet_id) REFERENCES wallet(id);


--
-- Name: city city_country_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY city
    ADD CONSTRAINT city_country_id_fkey FOREIGN KEY (country_id) REFERENCES country(id);


--
-- Name: city city_state_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY city
    ADD CONSTRAINT city_state_id_fkey FOREIGN KEY (state_id) REFERENCES state(id);


--
-- Name: client client_city_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY client
    ADD CONSTRAINT client_city_id_fkey FOREIGN KEY (city_id) REFERENCES city(id);


--
-- Name: client client_country_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY client
    ADD CONSTRAINT client_country_id_fkey FOREIGN KEY (country_id) REFERENCES country(id);


--
-- Name: client client_state_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY client
    ADD CONSTRAINT client_state_id_fkey FOREIGN KEY (state_id) REFERENCES state(id);


--
-- Name: customer customer_address_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_address_id_fkey FOREIGN KEY (address_id) REFERENCES address(id);


--
-- Name: customer_billing customer_billing_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_billing
    ADD CONSTRAINT customer_billing_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: customer_billing customer_billing_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_billing
    ADD CONSTRAINT customer_billing_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(id);


--
-- Name: customer_billing_record customer_billing_record_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_billing_record
    ADD CONSTRAINT customer_billing_record_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: customer_billing_record customer_billing_record_customer_billing_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_billing_record
    ADD CONSTRAINT customer_billing_record_customer_billing_id_fkey FOREIGN KEY (customer_billing_id) REFERENCES customer_billing(id);


--
-- Name: customer_billing_record customer_billing_record_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_billing_record
    ADD CONSTRAINT customer_billing_record_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(id);


--
-- Name: customer_billing_record customer_billing_record_device_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_billing_record
    ADD CONSTRAINT customer_billing_record_device_id_fkey FOREIGN KEY (device_id) REFERENCES device(id);


--
-- Name: customer_billing_record customer_billing_record_network_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_billing_record
    ADD CONSTRAINT customer_billing_record_network_id_fkey FOREIGN KEY (network_id) REFERENCES network(id);


--
-- Name: customer customer_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: customer_deduction customer_deduction_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_deduction
    ADD CONSTRAINT customer_deduction_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: customer_deduction customer_deduction_customer_billing_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_deduction
    ADD CONSTRAINT customer_deduction_customer_billing_id_fkey FOREIGN KEY (customer_billing_id) REFERENCES customer_billing(id);


--
-- Name: customer_deduction customer_deduction_customer_billing_record_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_deduction
    ADD CONSTRAINT customer_deduction_customer_billing_record_id_fkey FOREIGN KEY (customer_billing_record_id) REFERENCES customer_billing_record(id);


--
-- Name: customer_invoice customer_invoice_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_invoice
    ADD CONSTRAINT customer_invoice_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: customer_invoice customer_invoice_customer_billing_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_invoice
    ADD CONSTRAINT customer_invoice_customer_billing_id_fkey FOREIGN KEY (customer_billing_id) REFERENCES customer_billing(id);


--
-- Name: customer_invoice customer_invoice_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_invoice
    ADD CONSTRAINT customer_invoice_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(id);


--
-- Name: customer_receipt customer_receipt_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_receipt
    ADD CONSTRAINT customer_receipt_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: customer_receipt customer_receipt_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_receipt
    ADD CONSTRAINT customer_receipt_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(id);


--
-- Name: customer_receipt customer_receipt_customer_invoice_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_receipt
    ADD CONSTRAINT customer_receipt_customer_invoice_id_fkey FOREIGN KEY (customer_invoice_id) REFERENCES customer_invoice(id);


--
-- Name: customer_settings customer_settings_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_settings
    ADD CONSTRAINT customer_settings_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: customer_settings customer_settings_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_settings
    ADD CONSTRAINT customer_settings_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(id);


--
-- Name: customer customer_station_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_station_id_fkey FOREIGN KEY (station_id) REFERENCES station(id);


--
-- Name: deduction deduction_billing_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY deduction
    ADD CONSTRAINT deduction_billing_id_fkey FOREIGN KEY (billing_id) REFERENCES billing(id);


--
-- Name: deduction deduction_billing_record_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY deduction
    ADD CONSTRAINT deduction_billing_record_id_fkey FOREIGN KEY (billing_record_id) REFERENCES billing_record(id);


--
-- Name: deduction deduction_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY deduction
    ADD CONSTRAINT deduction_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: device device_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY device
    ADD CONSTRAINT device_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: device device_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY device
    ADD CONSTRAINT device_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(id);


--
-- Name: device device_installation_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY device
    ADD CONSTRAINT device_installation_id_fkey FOREIGN KEY (installation_id) REFERENCES installation(id);


--
-- Name: device device_network_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY device
    ADD CONSTRAINT device_network_id_fkey FOREIGN KEY (network_id) REFERENCES network(id);


--
-- Name: device device_point_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY device
    ADD CONSTRAINT device_point_id_fkey FOREIGN KEY (point_id) REFERENCES geojson_point(id);


--
-- Name: device device_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY device
    ADD CONSTRAINT device_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: device device_station_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY device
    ADD CONSTRAINT device_station_id_fkey FOREIGN KEY (station_id) REFERENCES station(id);


--
-- Name: device_type device_type_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY device_type
    ADD CONSTRAINT device_type_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: installation installation_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY installation
    ADD CONSTRAINT installation_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: installation installation_installation_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY installation
    ADD CONSTRAINT installation_installation_type_id_fkey FOREIGN KEY (installation_type_id) REFERENCES installation_type(id);


--
-- Name: installation installation_location_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY installation
    ADD CONSTRAINT installation_location_id_fkey FOREIGN KEY (location_id) REFERENCES location(id);


--
-- Name: installation installation_service_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY installation
    ADD CONSTRAINT installation_service_id_fkey FOREIGN KEY (service_id) REFERENCES service(id);


--
-- Name: installation installation_station_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY installation
    ADD CONSTRAINT installation_station_id_fkey FOREIGN KEY (station_id) REFERENCES station(id);


--
-- Name: invoice invoice_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY invoice
    ADD CONSTRAINT invoice_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: location location_city_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY location
    ADD CONSTRAINT location_city_id_fkey FOREIGN KEY (city_id) REFERENCES city(id);


--
-- Name: location location_country_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY location
    ADD CONSTRAINT location_country_id_fkey FOREIGN KEY (country_id) REFERENCES country(id);


--
-- Name: location location_state_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY location
    ADD CONSTRAINT location_state_id_fkey FOREIGN KEY (state_id) REFERENCES state(id);


--
-- Name: network network_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY network
    ADD CONSTRAINT network_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: network network_geojson_address_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY network
    ADD CONSTRAINT network_geojson_address_id_fkey FOREIGN KEY (geojson_address_id) REFERENCES geojson_address(id);


--
-- Name: network_services network_services_network_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY network_services
    ADD CONSTRAINT network_services_network_id_fkey FOREIGN KEY (network_id) REFERENCES network(id);


--
-- Name: network_services network_services_service_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY network_services
    ADD CONSTRAINT network_services_service_id_fkey FOREIGN KEY (service_id) REFERENCES service(id);


--
-- Name: network network_station_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY network
    ADD CONSTRAINT network_station_id_fkey FOREIGN KEY (station_id) REFERENCES station(id);


--
-- Name: product_image product_image_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_image
    ADD CONSTRAINT product_image_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: product_metrics product_metrics_metric_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_metrics
    ADD CONSTRAINT product_metrics_metric_id_fkey FOREIGN KEY (metric_id) REFERENCES metric(id);


--
-- Name: product_metrics product_metrics_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_metrics
    ADD CONSTRAINT product_metrics_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: product_services product_services_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_services
    ADD CONSTRAINT product_services_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: product_services product_services_service_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_services
    ADD CONSTRAINT product_services_service_id_fkey FOREIGN KEY (service_id) REFERENCES service(id);


--
-- Name: receipt receipt_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY receipt
    ADD CONSTRAINT receipt_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: receipt receipt_invoice_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY receipt
    ADD CONSTRAINT receipt_invoice_id_fkey FOREIGN KEY (invoice_id) REFERENCES invoice(id);


--
-- Name: recurrent_card recurrent_card_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY recurrent_card
    ADD CONSTRAINT recurrent_card_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: report report_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY report
    ADD CONSTRAINT report_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: role_access_groups role_access_groups_access_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY role_access_groups
    ADD CONSTRAINT role_access_groups_access_group_id_fkey FOREIGN KEY (access_group_id) REFERENCES access_group(id);


--
-- Name: role_access_groups role_access_groups_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY role_access_groups
    ADD CONSTRAINT role_access_groups_role_id_fkey FOREIGN KEY (role_id) REFERENCES role(id);


--
-- Name: role role_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: settings settings_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY settings
    ADD CONSTRAINT settings_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: state state_country_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY state
    ADD CONSTRAINT state_country_id_fkey FOREIGN KEY (country_id) REFERENCES country(id);


--
-- Name: station_assignees station_assignees_station_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY station_assignees
    ADD CONSTRAINT station_assignees_station_id_fkey FOREIGN KEY (station_id) REFERENCES station(id);


--
-- Name: station_assignees station_assignees_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY station_assignees
    ADD CONSTRAINT station_assignees_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: station station_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY station
    ADD CONSTRAINT station_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: station station_location_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY station
    ADD CONSTRAINT station_location_id_fkey FOREIGN KEY (location_id) REFERENCES location(id);


--
-- Name: station station_manager_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY station
    ADD CONSTRAINT station_manager_id_fkey FOREIGN KEY (manager_id) REFERENCES "user"(id);


--
-- Name: station_services station_services_service_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY station_services
    ADD CONSTRAINT station_services_service_id_fkey FOREIGN KEY (service_id) REFERENCES service(id);


--
-- Name: station_services station_services_station_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY station_services
    ADD CONSTRAINT station_services_station_id_fkey FOREIGN KEY (station_id) REFERENCES station(id);


--
-- Name: sub_navigation sub_navigation_navigation_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sub_navigation
    ADD CONSTRAINT sub_navigation_navigation_id_fkey FOREIGN KEY (navigation_id) REFERENCES navigation(id);


--
-- Name: subscription subscription_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY subscription
    ADD CONSTRAINT subscription_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: subscription subscription_service_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY subscription
    ADD CONSTRAINT subscription_service_id_fkey FOREIGN KEY (service_id) REFERENCES service(id);


--
-- Name: user_access_groups user_access_groups_access_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_access_groups
    ADD CONSTRAINT user_access_groups_access_group_id_fkey FOREIGN KEY (access_group_id) REFERENCES access_group(id);


--
-- Name: user_access_groups user_access_groups_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_access_groups
    ADD CONSTRAINT user_access_groups_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: user user_address_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_address_id_fkey FOREIGN KEY (address_id) REFERENCES address(id);


--
-- Name: user user_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: user_roles user_roles_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_roles
    ADD CONSTRAINT user_roles_role_id_fkey FOREIGN KEY (role_id) REFERENCES role(id);


--
-- Name: user_roles user_roles_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_roles
    ADD CONSTRAINT user_roles_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: wallet wallet_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY wallet
    ADD CONSTRAINT wallet_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(id);


--
-- PostgreSQL database dump complete
--

