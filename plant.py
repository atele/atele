from logging import Formatter, ERROR, handlers

from flask import Flask
from flask_restful import Api
from flask_migrate import Migrate
from flask_script import Manager
from flask_bcrypt import Bcrypt
from flask_assets import Environment
from flask_cache import Cache
from flask_login import LoginManager
from flask_marshmallow import Marshmallow
from flask_sitemap import Sitemap
from flask_principal import Principal
from celery import Celery
from raven.contrib.flask import Sentry
from model_package.models import db
from messagerie import AWSEntity

import settings

from integrations_package.flutterwave import Flutterwave
from integrations_package.reporting import Reporting


def make_celery(app):
    """ Enables celery to run within the flask application context """

    celery = Celery(app.import_name, backend=app.config.get("CELERY_RESULT_BACKEND"),
                    broker=app.config.get("CELERY_BROKER_URL"))
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return celery.Task.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery


def setup_app(name, config_class):
    module = getattr(settings, config_class)
    app = Flask(name, template_folder='templates', static_folder='static')
    app.config.from_object(module)

    # api resources
    app.api = Api(app, prefix='/v1')

    # database migration
    db.init_app(app)
    app.db = db
    app.migrate = Migrate(app, app.db)
    # app.db = SQLAlchemy(app)

    # flask script manager
    app.manager = Manager(app)

    # inject celery into the app
    app.celery = make_celery(app)

    # password encryption
    app.bcrypt = Bcrypt(app)

    # initializing assets - js/css/sass
    app.assets = Environment(app)
    app.assets.from_yaml('asset.yml')

    # Initialize Flutterwave
    app.flutterwave = Flutterwave(app)

    # templates caching
    app.cache = Cache(app, config={'CACHE_TYPE': 'simple'})

    # sitemap
    app.ext = Sitemap(app)

    # principal identity management
    app.principal = Principal(app)

    app.reporting = Reporting(app)

    # Initializing login manager
    login_manager = LoginManager()
    login_manager.blueprint_login_views = {'client_blueprint': '/login', 'admin_blueprint': '/admin/login'}
    login_manager.session_protection = 'strong'
    login_manager.init_app(app)
    app.login_manager = login_manager

    # flask marshmallow api implementation of database models
    app.marshmallow = Marshmallow(app)

    file_handler = handlers.RotatingFileHandler("/var/log/atele/%s.log" % app.config.get("LOGFILE_NAME", name),
                                                maxBytes=500 * 1024)
    file_handler.setLevel(ERROR)
    file_handler.setFormatter(Formatter(
        '%(asctime)s %(levelname)s: %(message)s '
        '[in %(pathname)s:%(lineno)d]'
    ))
    app.logger.addHandler(file_handler)

    # configure sentry
    if not app.config.get("DEBUG", True):
        sentry = Sentry(app)
        app.sentry = sentry

    # custom aws wrapper
    app.aws_wrapper = AWSEntity(access_id=app.config['AWS_CONFIG']['ACCESS_KEY_ID'],
                                access_key=app.config['AWS_CONFIG']['SECRET_ACCESS_KEY'], s3_bucket='atele',
                                region='us-west-1')

    return app


def initialize_views(app, *views):
    """
    Registers a set of blueprints to an application
    """
    for view in views:
        app.register_blueprint(view)


def initialize_api(app, api):
    """ Register all resources for the API """
    api.init_app(app=app)  # Initialize api first
    _resources = getattr(app, "api_registry", None)
    print(_resources)
    if _resources and isinstance(_resources, (list, tuple,)):
        for cls, args, kwargs in _resources:
            api.add_resource(cls, *args, **kwargs)
