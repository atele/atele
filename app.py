from plant import setup_app, initialize_views

app = setup_app('atele', 'ProductionConfig')

with app.app_context():
    from application.views import main
    initialize_views(app, main.landing)

if __name__ == '__main__':
    app.run(use_reloader=False)
