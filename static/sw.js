'use strict';

importScripts('/static/cache-polyfill.js');

var CACHE_NAME = 'atele-cache';
var urlsToCache = ['/'];
self.addEventListener('install', function (event) {
    event.waitUntil(
        caches.open(CACHE_NAME).then(function (cache) {
            return cache.addAll(urlsToCache);
        })
    )
});

self.addEventListener('fetch', function (event) {
    //event.respondWith(
    //    caches.match(event.request)
    //        .then(function (response) {
    //            // Cache hit - return response
    //            if (response) {
    //                return response;
    //            }
    //            var fetchRequest = event.request.clone();
    //            return fetch(fetchRequest).then(function(response){
    //                // Check if we received a valid response
    //                if(!response || response.status !== 200 || response.type !== 'basic') {
    //                  return response;
    //                }
    //                var responseToCache = response.clone();
    //
    //                caches.open(CACHE_NAME)
    //                  .then(function(cache) {
    //                    cache.put(event.request, responseToCache);
    //                  });
    //
    //                return response;
    //            });
    //        }
    //    )
    //);
});


self.addEventListener('push', function (event) {
    if (event.data) {
        var data = event.data.json();

        self.clients.matchAll().then(function (clients) {
            clients.forEach(function (client) {
                client.postMessage(data);
            })
        });
    } else {
        console.log('This push event has no data.');
    }
});

self.addEventListener('notificationclick', function (event) {
    event.notification.close();
    //event.waitUntil(
    //    clients.openWindow('https://developers.google.com/web/')
    //);
});