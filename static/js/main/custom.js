/* Theme Name: The Project - Responsive Website Template
 * Author:HtmlCoder
 * Author URI:http://www.htmlcoder.me
 * Author e-mail:htmlcoder.me@gmail.com
 * Version:1.2.0
 * Created:March 2015
 * License URI:http://support.wrapbootstrap.com/
 * File Description: Place here your custom scripts
 */


 var SplashScreen = function () {

     return {
         //main function to initiate the module

         initSplashScreen: function() {
         	$("#splash-screen, #splash-screen-dialog").removeClass('visible');

         	$("#accept-button").click(function(e){
         		SplashScreen.hideSplashScreen();
         	});
         },

         showSplashScreen: function() {
         	$("#splash-screen, #splash-screen-dialog").addClass('visible');
         },

         hideSplashScreen: function() {
         	$("#splash-screen, #splash-screen-dialog").removeClass('visible');
         },

         init: function () {
         	// first initialize the splash screen
         	SplashScreen.initSplashScreen();

         	var show_splash = $.cookie('show_splash');
         	if(!show_splash) {
         		// show the spashscreen here then wait 2 hours and show it again
         		var date = new Date();
         		var minutes = 1;
         		date.setTime(date.getTime()) + (minutes * 60 * 1000);
         		$.cookie("show_splash", "true", {expires: 1, path: '/'});

         		SplashScreen.showSplashScreen();
         	}
         }
     };

 }();
