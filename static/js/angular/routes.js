/**
 * Created by stikks-workstation on 2/25/17.
 */
'use strict';
var app = angular.module('atele.routes', ['ui.router']);

app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: 'partials/client/overview',
            controller: 'DashboardController',
            resolve: {
                devices: function ($q, $timeout, $rootScope, Device) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Device.query({
                            service_id: $rootScope.service.id,
                            client_id: $rootScope.client.id
                        });
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                },
                stations: function ($q, $timeout, $rootScope, Station) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Station.query({
                            service_id: $rootScope.service.id,
                            client_id: $rootScope.client.id
                        });
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                },
                installations: function ($q, $timeout, $rootScope, Installation) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Installation.query({
                            service_id: $rootScope.service.id,
                            client_id: $rootScope.client.id
                        });
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                },
                networks: function ($q, $timeout, $rootScope, Network) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Network.query({
                            service_id: $rootScope.service.id,
                            client_id: $rootScope.client.id
                        });
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                }
            }
        })
        // DEVICES routes
        // =================================
        .state('devices', {
            url: '/devices',
            templateUrl: 'partials/client/dashboard/device',
            controller: 'DeviceController',
            resolve: {
                data: [
                    'Device', function (Device) {
                        var devData = Device.query();
                        return devData.$promise;
                    }
                ]
            }
        })
        .state('devices_list', {
            url: '/devices/list',
            templateUrl: 'partials/client/list/devices',
            controller: 'DeviceController',
            resolve: {
                data: [
                    'Device', function (Device) {
                        var devData = Device.query();
                        return devData.$promise;
                    }
                ]
            }
        })
        .state('device_details', {
            url: '/devices/:id',
            templateUrl: 'partials/client/details/device',
            controller: 'DeviceDetailController',
            resolve: {
                installation: [
                    'Device', '$stateParams', function (Device, $stateParams) {
                        var instData = Device.get({id: $stateParams.id});
                        return instData.$promise;
                    }
                ]
            }
        })
        // Network network routes
        // =================================
        .state('networks', {
            url: '/networks',
            templateUrl: 'partials/client/dashboard/network',
            controller: 'NetworkDashboardController',
            resolve: {
                data: function ($q, $timeout, Network, $rootScope) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Network.query({service_id: $rootScope.service.id, client_id: $rootScope.client.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                }
            }
        })
        .state('networks_list', {
            url: '/networks/list',
            templateUrl: 'partials/client/list/network',
            controller: 'NetworkController',
            resolve: {
                data: function ($q, $timeout, Network, $rootScope) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Network.query({service_id: $rootScope.service.id, client_id: $rootScope.client.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                }
            }
        })
        .state('network_requests', {
            url: '/networks/:id/requests',
            templateUrl: 'partials/client/list/network_requests',
            controller: 'NetworkRequestsController',
            resolve: {
                objects: function ($q, $timeout, NetworkRequest, $stateParams) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = NetworkRequest.query({network_id: $stateParams.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                }
            }
        })
        .state('networks_new', {
            url: '/networks/new',
            templateUrl: 'partials/client/forms/network',
            controller: 'AddNetworkController',
            resolve: {
                prevState: [
                    '$state', function ($state) {
                        return {
                            Name: $state.current.name,
                            Params: $state.params,
                            URL: $state.href($state.current.name, $state.params)
                        };
                    }
                ],
                 stations: function ($q, $timeout, Station, $rootScope) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Station.query({'client_id': $rootScope.client.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                },
                products: function ($q, $timeout, Product, $rootScope) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Product.query({'is_network': true});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                }
            }
        })
        .state('network_details', {
            url: '/networks/:id',
            templateUrl: 'partials/client/details/network',
            controller: 'NetworkDetailController',
            resolve: {
                network: [
                    'Network', '$stateParams', function (Network, $stateParams) {
                        var instData = Network.get({id: $stateParams.id});
                        return instData.$promise;
                    }
                ],
                requests: [
                    'NetworkRequest', '$stateParams', function (NetworkRequest, $stateParams) {
                        var devData = NetworkRequest.query({network_id: $stateParams.id});
                        return devData.$promise;
                    }
                ]
            }
        })
        .state('network_devices', {
            url: '/networks/:id/devices',
            templateUrl: 'partials/client/list/devices',
            controller: 'DeviceController',
            resolve: {
                currentNav: [
                    '$rootScope', function ($rootScope) {
                        return $rootScope.current_nav;
                    }
                ],
                data: function ($q, $timeout, Device, $stateParams) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Device.query({network_id: $stateParams.id});
                        deferred.resolve(instData.$promise);
                    }, 2000);
                    return deferred.promise;
                }
            }
        })
        .state('network_dev_new', {
            url: '/networks/:id/device',
            templateUrl: 'partials/client/forms/network_dev',
            controller: 'NetworkDeviceController',
            resolve: {
                prevState: [
                    '$state', function ($state) {
                        return {
                            Name: $state.current.name,
                            Params: $state.params,
                            URL: $state.href($state.current.name, $state.params)
                        };
                    }
                ],
                network: [
                    'Network', '$stateParams', function (Network, $stateParams) {
                        var _data = Network.get({id: $stateParams.id});
                        return _data.$promise;
                    }
                ],
                devices: function ($q, $timeout, Device, $rootScope) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Device.query({'client_id': $rootScope.client.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                }
            }
        })
        .state('network_update', {
            url: '/networks/:id/update',
            templateUrl: 'partials/client/forms/network',
            controller: 'UpdateNetworkController',
            resolve: {
                network: [
                    'Network', '$stateParams', function (Network, $stateParams) {
                        var _data = Network.get({id: $stateParams.id});
                        return _data.$promise;
                    }
                ],
                stations: function ($q, $timeout, Station, $rootScope) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Station.query({'client_id': $rootScope.client.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                }
            }
        })

        // installations routes
        // =================================
        .state('installations', {
            url: '/installations',
            templateUrl: 'partials/client/dashboard/installation',
            controller: 'InstallationsController',
            resolve: {
                data: function ($q, $timeout, Installation, $rootScope) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Installation.query({'client_id': $rootScope.client.id,
                            service_id: $rootScope.service.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                }
            }
        })
        .state('installation_list', {
            url: '/installations/list',
            templateUrl: 'partials/client/list/single_installation',
            controller: 'InstallationController',
            resolve: {
                data: function ($q, $timeout, Installation, $rootScope) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Installation.query({'client_id': $rootScope.client.id,
                            service_id: $rootScope.service.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                }
            }
        })
        .state('installations_new', {
            url: '/installations/new',
            templateUrl: 'partials/client/forms/installation',
            controller: 'AddInstallationController',
            resolve: {
                stations: function ($q, $timeout, Station, $rootScope) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Station.query({'client_id': $rootScope.client.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                },
                installation_types: function ($q, $timeout, InstallationType, $rootScope) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = InstallationType.query({'service_id': $rootScope.service.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                }
            }
        })
        .state('installation_details', {
            url: '/installations/:id',
            templateUrl: 'partials/client/details/installation',
            controller: 'InstallationDetailController',
            resolve: {
                installation: [
                    'Installation', '$stateParams', function (Installation, $stateParams) {
                        var instData = Installation.get({id: $stateParams.id});
                        return instData.$promise;
                    }
                ],
                requests: function ($q, $timeout, DeviceRequest, $stateParams) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = DeviceRequest.query({installation_id: $stateParams.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                },
                alerts: function ($q, $timeout, Alert, $stateParams) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Alert.query({installation_id: $stateParams.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                }
            }
        })
        .state('installation_requests', {
            url: '/installations/:id/requests',
            templateUrl: 'partials/client/list/device_requests',
            controller: 'InstallationRequestsController',
            resolve: {
                objects: function ($q, $timeout, DeviceRequest, $stateParams) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = DeviceRequest.query({installation_id: $stateParams.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                }
            }
        })
        .state('installation_devices', {
            url: '/installations/:id/devices',
            templateUrl: 'partials/client/list/devices',
            controller: 'DeviceController',
            resolve: {
                currentNav: [
                    '$rootScope', function ($rootScope) {
                        return $rootScope.current_nav;
                    }
                ],
                installation: [
                    'Installation', '$stateParams', function (Installation, $stateParams) {
                        var _data = Installation.get({id: $stateParams.id});
                        return _data.$promise;
                    }
                ],
                data: function ($q, $timeout, Device, $stateParams) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Device.query({installation_id: $stateParams.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                }
            }
        })
        .state('devices_new', {
            url: '/installations/:id/devices/new',
            templateUrl: 'partials/client/forms/device',
            controller: 'AddDeviceController',
            resolve: {
                prevState: [
                    '$state', function ($state) {
                        return {
                            Name: $state.current.name,
                            Params: $state.params,
                            URL: $state.href($state.current.name, $state.params)
                        };
                    }
                ],
                installation: [
                    'Installation', '$stateParams', function (Installation, $stateParams) {
                        var _data = Installation.get({id: $stateParams.id});
                        return _data.$promise;
                    }
                ]
            }
        })
        .state('installation_update', {
            url: '/installations/:id/update',
            templateUrl: 'partials/client/forms/installation',
            controller: 'UpdateInstallationController',
            resolve: {
                installation: [
                    'Installation', '$stateParams', function (Installation, $stateParams) {
                        var _data = Installation.get({id: $stateParams.id});
                        return _data.$promise;
                    }
                ],
                stations: function ($q, $timeout, Station, $rootScope) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Station.query({'client_id': $rootScope.client.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                },
                installation_types: function ($q, $timeout, InstallationType, $rootScope) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = InstallationType.query({'client_id': $rootScope.client.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                }
            }
        })
        // =================================
        // alert routes
        // =================================
        .state('installation_alerts', {
            url: '/installations/:id/alerts',
            templateUrl: 'partials/client/list/alerts',
            controller: 'AlertsController',
            resolve: {
                data: function ($q, $timeout, Alert, $stateParams) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Alert.query({installation_id: $stateParams.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                },
                installation: [
                    'Installation', '$stateParams', function (Installation, $stateParams) {
                        var instData = Installation.get({id: $stateParams.id});
                        return instData.$promise;
                    }
                ]
            }
        })
        .state('alerts_new', {
            url: '/installations/:id/alerts/new',
            templateUrl: 'partials/client/forms/alert',
            controller: 'AddAlertController',
            resolve: {
                alert_types: [
                    'AlertType', function (AlertType) {
                        var typeData = AlertType.query();
                        return typeData.$promise;
                    }
                ],
                installation: [
                    'Installation', '$stateParams', function (Installation, $stateParams) {
                        var instData = Installation.get({id: $stateParams.id});
                        return instData.$promise;
                    }
                ]
            }
        })
        .state('alert', {
            url: '/alerts/:id',
            templateUrl: 'partials/client/details/alert',
            controller: 'AlertController',
            resolve: {
                alert: [
                    'Alert', '$stateParams', function (Alert, $stateParams) {
                        var _data = Alert.get({id: $stateParams.id});
                        return _data.$promise;
                    }
                ]
            }
        })
        .state('alert_update', {
            url: '/alerts/:id/update',
            templateUrl: 'partials/client/forms/alert',
            controller: 'UpdateAlertController',
            resolve: {
                alert: [
                    'Alert', '$stateParams', function (Alert, $stateParams) {
                        var _data = Alert.get({id: $stateParams.id});
                        return _data.$promise;
                    }
                ]
            }
        })
        // =================================
        // end alert routes
        // =================================

        // =================================
        // customer routes
        // =================================
        .state('customers', {
            url: '/customers',
            templateUrl: 'partials/client/dashboard/customer',
            controller: 'CustomerDashboardController',
            resolve: {
                data: function ($q, $timeout, Client, $rootScope) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        // var instData = Client.query({client_id: $rootScope.client.id});
                        var instData = Client.get({id: $rootScope.client.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                }
            }
        })
        .state('customer_list', {
            url: '/customers/list',
            templateUrl: 'partials/client/list/customers',
            controller: 'CustomerController',
            resolve: {
                data: function ($q, $timeout, Customer, $rootScope) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Customer.query({client_id: $rootScope.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                }
            }
        })
        .state('customer_devices', {
            url: '/customers/:id/devices',
            templateUrl: 'partials/client/list/customer_devices',
            controller: 'CustomerDeviceController',
            resolve: {
                customer: [
                    'Customer', '$stateParams', function (Customer, $stateParams) {
                        var _data = Customer.get({id: $stateParams.id});
                        return _data.$promise;
                    }
                ],
                data: function ($q, $timeout, Device, $stateParams) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Device.query({customer_id: $stateParams.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                }
            }
        })
        .state('customer_devices_new', {
            url: '/customers/:id/devices/new',
            templateUrl: 'partials/client/forms/customer_device',
            controller: 'AddCustomerDeviceController',
            resolve: {
                prevState: [
                    '$state', function ($state) {
                        return {
                            Name: $state.current.name,
                            Params: $state.params,
                            URL: $state.href($state.current.name, $state.params)
                        };
                    }
                ],
                customer: [
                    'Customer', '$stateParams', function (Customer, $stateParams) {
                        var _data = Customer.get({id: $stateParams.id});
                        return _data.$promise;
                    }
                ],
                networks: function ($q, $timeout, Network, $rootScope) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Network.query({client_id: $rootScope.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                }
            }
        })
        .state('customers_new', {
            url: '/customers/new',
            templateUrl: 'partials/client/forms/customer',
            controller: 'AddCustomerController',
            resolve: {
                products: function ($q, $timeout, Product) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Product.query({'is_network': false});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                },
                stations: function ($q, $timeout, Station, $rootScope) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Station.query({'client_id': $rootScope.client.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                },
                networks: function ($q, $timeout, $rootScope, Network) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Network.query({
                            client_id: $rootScope.client.id
                        });
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                },
                cities: function ($q, $timeout, $rootScope, City) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = City.query();
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                },
                states: function ($q, $timeout, $rootScope, State) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = State.query({country_id: 149, per_page:50, order_by:'name', asc_desc: 'asc'});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                },
                countries: function ($q, $timeout, $rootScope, Country) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Country.query({slug: 'nigeria'});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                }
            }
        })
        .state('customer_details', {
            url: '/customers/:id',
            templateUrl: 'partials/client/details/customer',
            controller: 'CustomerDetailController',
            resolve: {
                customer: [
                    'Customer', '$stateParams', function (Customer, $stateParams) {
                        var instData = Customer.get({id: $stateParams.id});
                        return instData.$promise;
                    }
                ],
                requests: function ($q, $timeout, CustomerRequest, $stateParams) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = CustomerRequest.query({customer_id: $stateParams.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                },
                devices: function ($q, $timeout, Device, $stateParams) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = Device.query({customer_id: $stateParams.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                }
            }
        })
        .state('customer_requests', {
            url: '/customers/:id/requests',
            templateUrl: 'partials/client/list/device_requests',
            controller: 'CustomerRequestsController',
            resolve: {
                objects: function ($q, $timeout, CustomerRequest, $stateParams) {
                    var deferred = $q.defer();
                    $timeout(function() {
                        var instData = CustomerRequest.query({installation_id: $stateParams.id});
                        deferred.resolve(instData.$promise);
                    }, 500);
                    return deferred.promise;
                }
            }
        });
        // =================================
        // end customer routes
        // =================================
    $urlRouterProvider.otherwise('/');
});
