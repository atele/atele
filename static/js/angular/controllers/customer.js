/**
 * Created by stikks-workstation on 2/25/17.
 */
var app = angular.module('atele.controllers.customer', ["pubnub.angular.service", 'atele.services']);

app.controller('CustomerDashboardController', function ($scope, FactotumService, data) {
    $scope.data = FactotumService.buildPage(data);
});

app.controller('CustomerController', function ($scope, Customer, data, FactotumService) {
    $scope.data = FactotumService.buildPage(data);
});

app.controller('CustomerDeviceController', function ($scope, Customer, data, customer, FactotumService) {
    $scope.data = FactotumService.buildPage(data);
    $scope.customer = new Customer(customer);
});


app.controller('AddCustomerDeviceController', ['$scope', '$rootScope', '$state', 'prevState', 'customer', 'PopupService', 'CustomerRequest', 'networks',
    function ($scope, $rootScope, $state, prevState, customer, PopupService, CustomerRequest, networks) {

        $scope.products = $rootScope.service.products.filter(function (elem) {
            if (elem.is_network == false && elem.is_alert == false) {
                return elem
            }
        });

        $scope.customer = customer;
        $scope.deviceForm = {customer_id: $scope.customer.id};
        $scope.networks = networks.results;

        $scope.send = function () {
            var req = new CustomerRequest($scope.deviceForm);
            req.$save(function (data, status) {
                $state.go('customer_details', {id: $scope.customer.id})
            }, function (err) {
                $rootScope.data.error = err.data
            });
        }
    }]);

app.controller('AddCustomerController', ['$scope', '$rootScope', '$state', 'products', 'networks', 'PopupService', 'Customer', 'Network', 'cities', 'states', 'countries', 'City', 'State', 'stations',
    function ($scope, $rootScope, $state, products, networks, PopupService, Customer, Network, cities, states, countries, City, State, stations) {

        $scope.products = products.results.filter(function (elem) {
            if (elem.is_network == false && elem.is_alert == false) {
                return elem
            }
        });

        $scope.networks = networks.results.filter(function (res) {
            return new Network(res);
        });

        $scope.cities = cities.results;
        $scope.states = states.results;
        $scope.countries = countries.results;
        $scope.stations = stations.results;

        $scope.deviceForm = {client_id: $rootScope.client.id};

        $scope.changeStates = function () {
            State.query({country_id: $scope.deviceForm.country_id, per_page:50, order_by:'name', asc_desc: 'asc'}).$promise.then(function (elem) {
               $scope.states = elem.results
            });
        };

        $scope.changeCities = function () {
             City.query({state_id: $scope.deviceForm.state_id, per_page:150, order_by:'name', asc_desc: 'asc'}).$promise.then(function (elem) {
               $scope.cities = elem.results
            });
        };

        $scope.send = function () {
            var req = new Customer($scope.deviceForm);
            req.$save(function (data, status) {
                $state.go('customer_list')
            }, function (err) {
                console.log(err);
                $rootScope.data.error = err.data
            });
        }
    }]);


app.controller('CustomerDetailController', function ($scope, Customer, customer, devices, requests) {
    $scope.customer = new Customer(customer);
    $scope.device_requests = requests.data;
    $scope.devices = devices.results;
});


app.controller('CustomerRequestsController', ['FactotumService', '$scope', 'objects', 'Product', function (FactotumService, $scope, objects, Product) {
    $scope.data = {objects: objects.data};
    $scope.data.objects = objects.data.map(function (elem) {
        var product = Product.get({id: elem.product_id});
        product.$promise.then(function (resp) {
            elem.product = resp
        });
        return elem;
    });
}]);

