/**
 * Created by stikks-workstation on 5/9/17.
 */


var app = angular.module('atele.services', ['ngCookies', 'ngMaterial']);

app.service('TokenService', function ($cookies) {
    this.retrieve = function () {
        return $cookies.get('__cred__')
    }
});

app.service('MethodService', function ($mdDialog, PopupService) {
    this.delete = function (obj) {
        obj.$delete({id: obj.id}, function (resp) {
            $mdDialog.hide();
            PopupService.showAlert('Success', 'Object has been successfully deleted');
            location.reload();
        }, function (err) {
            $mdDialog.hide();
            PopupService.showAlert(err)
        });
    }
});

app.service('PopupService', function ($mdDialog) {
    this.showAlert = function (text, subtitle) {
        $mdDialog.show(
            $mdDialog.alert()
                .clickOutsideToClose(true)
                .title(text)
                .textContent(subtitle)
                .ok('ok')
        );
    };

    this.showConfirm = function (text, subtitle, yesResponse, noResponse, yesAction, variable) {
        variable = variable || null;
        var confirm = $mdDialog.confirm()
            .title(text)
            .textContent(subtitle)
            .targetEvent()
            .ok(yesResponse)
            .cancel(noResponse);

        $mdDialog.show(confirm).then(function () {
            if (variable) {
                yesAction(variable);
            } else {
                yesAction();
            }
        }, function () {
            console.warn('Action cancelled');
        });
    };

    this.showDialog = function (title, body, templateUrl) {
        var ctrlName = 'DialogController';
        var template = templateUrl || "dialogModal";
        $mdDialog.show({
            controller: ctrlName,
            templateUrl: template,
            clickOutsideToClose: true,
            resolve: {
                title: function () {
                    return title;
                },
                body: function () {
                    return body;
                }
            }
        }).then(function (resp) {
            console.log(resp);
        }, function (err) {
            console.log(err);
        });
    };

    this.close = function () {
        $mdDialog.hide();
    }

});

app.factory('FactotumService', function (PopupService, MethodService) {
    return {
        buildPage: function (param) {
            var data = {};
            data.pages = param.pages;
            data.per_page = param.per_page;
            data.asc_desc = param.asc_desc;
            data.order_by = param.order_by;
            data.total = param.total;
            data.page = param.page;
            data.objects = param.results;

            if (param.prev_page) {
                data.prevPage = param.prev_page;
            }
            else {
                data.prevPage = null
            }

            if (param.next_page) {
                data.nextPage = param.next_page;
            }
            else {
                data.nextPage = null
            }
            return data;
        },

        deleteObject: function (obj) {
            return PopupService.showConfirm('Are you sure you want to delete this object',
                'If you confirm, the object will be deleted. This process is irreversible', 'Confirm', 'Cancel', MethodService.delete, obj)
        },

        formatDate: function () {
            var end_date = new Date(),
                start_date = new Date(end_date - 15 * 60000),
                end_month = '' + (end_date.getMonth() + 1),
                end_day = '' + end_date.getDate(),
                start_month = '' + (start_date.getMonth() + 1),
                start_day = '' + start_date.getDate();

            if (end_month.length < 2) end_month = '0' + end_month;
            if (end_day.length < 2) end_day = '0' + end_day;

            if (start_month.length < 2) start_month = '0' + start_month;
            if (start_day.length < 2) start_day = '0' + start_day;

            var start_minute = '' + start_date.getMinutes(),
                end_minute = '' + end_date.getMinutes();

            if (start_minute.length < 2) start_minute = '0' + start_minute;
            if (end_minute.length < 2) end_minute = String('0' + end_minute);

            var start = String(start_date.getFullYear() + '-' + start_month + '-' + start_day + 'T' + start_date.getHours() + ':' + start_minute + ':00Z'),
                end = String(end_date.getFullYear() + '-' + end_month + '-' + end_day + 'T' + end_date.getHours() + ':' + end_minute + ':00Z');

            return {start: start, end: end};
        },

        formatTimestamp: function () {
            var end_date = new Date(),
                start_date = new Date(end_date - 15 * 60000);
            start_date.setSeconds(0);
            end_date.setSeconds(59);

            return {start: Math.round(start_date / 1000), end: Math.round(end_date / 1000)}
        },

        computeRange: function () {
            var endTime = new Date();
            var timeRange = [String(endTime.getHours() + ':' + endTime.getMinutes())];
            for (var i=1; i < 7; ++i) {
                var x = new Date(endTime - i * 60000);
                timeRange.unshift(String(x.getHours() + ':' + x.getMinutes()))
            }
            return timeRange
        }
    }
});


app.service('DeviceService', function (PopupService, TokenService, $mdDialog, $timeout, Mqtt, FactotumService) {
    this.ping = function (code) {
        PopupService.showAlert('PING', 'Pinging ' + code + '...');
        $.ajax({
            url: '/v1/mqtt/ping/' + code,
            data: {},
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + TokenService.retrieve());
            },
            type: 'POST',
            success: function (resp, status, xhr) {
                if (resp.response == false) {
                    $mdDialog.hide();
                    PopupService.showAlert('ERROR', 'Device (' + code + ') inactive ');
                    $timeout(function () {
                        $mdDialog.hide()
                    }, 2000)
                }
            },
            error: function (xhr, status, err) {
                console.log(err);
            }
        });
    };
    this.query = function (code) {
        var range = FactotumService.formatTimestamp();
        $.ajax({
            url: '/v1/tsdb/' + code,
            data: {start_date: range.start, end_date: range.end},
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + TokenService.retrieve());
            },
            type: 'GET',
            success: function (resp, status, xhr) {
                console.log(resp);
            },
            error: function (xhr, status, err) {
                console.log(err);
            }
        });
    }
});

app.factory('ChartService', function (FactotumService) {
    return {
        ChartService: function () {
            return this;
        },
        buildGauge: function (code, name, symbol) {
            var chartCode = '#' + code + '-' + name + 'Gauge';
            var gaugeChart = echarts.init(document.getElementById(chartCode));
            var gaugeOption = {
                tooltip: {
                    formatter: "{a} <br/>{b} : {c}%"
                },
                toolbox: {
                    show: false,
                    feature: {
                        mark: {show: true},
                        restore: {show: true},
                        saveAsImage: {show: true}
                    }
                },
                series: [
                    {
                        pointer: {show: true},
                        axisTick: {show: true},
                        splitLine: {show: false},
                        name: name,
                        type: 'gauge',
                        detail: {formatter: '{value}'},
                        data: [{value: 0, name: ''}]
                    }
                ]
            };

            gaugeOption.series[0].data[0].value = 0;
            gaugeChart.setOption(gaugeOption, true);
            return {'gaugeChart': gaugeChart, 'gaugeOption': gaugeOption};
        },
        updateGauge: function (gaugeChart, gaugeOption, value) {
            try {
                gaugeOption.series[0].data[0].value = value;
                gaugeChart.setOption(gaugeOption, true);
                return true;
            } catch (err) {
                console.warn(err);
                return false;
            }

        },
        buildChart: function (code, name, symbol) {
            var chartCode = '#' + code + '-' + name + 'Chart';
            var chartMap = echarts.init(document.getElementById(chartCode));
            var chartMapOption = {
                title: {
                    text: name + ' Events in ' + symbol,
                    subtext: ''
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: [name]
                },
                toolbox: {
                    show: true,
                    feature: {
                        dataZoom: {
                            yAxisIndex: 'none'
                        },
                        dataView: {readOnly: false},
                        magicType: {type: ['line', 'bar']},
                        restore: {},
                        saveAsImage: {}
                    }
                },
                xAxis: {
                    type: 'category',
                    boundaryGap: false,
                    data: FactotumService.computeRange()
                },
                yAxis: {
                    type: 'value',
                    axisLabel: {
                        formatter: '{value} '
                    }
                },
                series: [
                    {
                        name: name,
                        type: 'line',
                        data: [0, 0, 0, 0, 0, 0, 0],
                        markPoint: {
                            data: [
                                {type: 'max', name: 'Maximum'},
                                {type: 'min', name: 'Minimum'}
                            ]
                        },
                        markLine: {
                            data: [
                                {type: 'average', name: 'Average'}
                            ]
                        }
                    }
                ]
            };
            chartMap.setOption(chartMapOption);
            return {'chartMap': chartMap, 'chartMapOption': chartMapOption};
        },
        updateChart: function (chartMap, chartOption, value, timestamp) {
            try {
                chartOption.series[0].data.push(value);
                chartOption.xAxis.data.push(moment(timestamp * 1000).format("HH:mm:ss"));
                chartMap.setOption(chartOption, true);
                return true;
            } catch (err) {
                console.warn(err);
                return false;
            }

        },
        mqttListener: function (ChartService, deviceID, metric, cInfo, gInfo) {
            var client = new Paho.MQTT.Client("95.85.18.169", Number(9883), "", "myclientid_" + parseInt(Math.random() * 100, 10));

            // set callback handlers
            client.onConnectionLost = onConnectionLost;
            client.onMessageArrived = onMessageArrived;

            // connect the client
            client.connect({onSuccess: onConnect});

            var updateChart = this.$parent.updateChart;
            var updateGauge = this.$parent.updateGuage;

            // called when the client connects
            function onConnect() {
                // Once a connection has been made, make a subscription and send a message.
                console.log("Connected to broker");
                client.subscribe(deviceID);
                // message = new Paho.MQTT.Message("Hello");
                // message.destinationName = "World";
                // client.send(message);
            }

            // called when the client loses its connection
            function onConnectionLost(responseObject) {
                if (responseObject.errorCode !== 0) {
                    console.log("onConnectionLost:" + responseObject.errorMessage);
                }
            }

            // called when a message arrives
            function onMessageArrived(message) {
                var data = JSON.parse(message.payloadString);
                if (message.destinationName ===  deviceID && data.metric === metric) {
                    ChartService.updateChart(cInfo.chartMap, cInfo.chartMapOption, data.value, data.timestamp);
                    ChartService.updateGauge(gInfo.gaugeChart, gInfo.gaugeOption, data.value, data.timestamp);
                }
            }
        }
    }
});
