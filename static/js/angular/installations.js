'use strict';

var scope = angular.element($('#installationsPage')).scope();

if (scope.devices != undefined) {
    var map = L.map('meshMap', {
        center: [9.0820, 8.6753],
        minZoom: 2,
        zoom: 6
    });
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        subdomains: ['a', 'b', 'c']
    }).addTo(map);

    var myIcon = L.icon({
        iconUrl: '/static/images/blue-pin.png',
        iconRetinaUrl: '/static/images/blue-pin-md.png',
        iconSize: [29, 24],
        iconAnchor: [9, 21],
        popupAnchor: [0, -14]
    });

    for (var i = 0; i < scope.devices.length; ++i) {
        var device = scope.devices[i];
        var dev_link = "#!/installations/" + device.installation_id;
             L.marker([device.point.latitude, device.point.longitude], {icon: myIcon})
                .bindPopup('<a href="' + dev_link + '" target="_self">#' + device.code + '</a>')
                .addTo(map);
    }

    setTimeout(function () {
        $.each(scope.metrics, function (pos, metric) {
            buildMultipleChart(metric.name, metric.dimension, metric.devices);
        });
    }, 1000);
}
