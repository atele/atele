var app = angular.module('atele.components', []);

app.component('overview',  {
    templateUrl: 'partials/client/overview',
    controller: 'DashboardController'
});