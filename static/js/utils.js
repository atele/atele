'use strict';
var computeRange = function () {
    var endTime = new Date();
    var timeRange = [String(endTime.getHours() + ':' + endTime.getMinutes())];
    for (var i = 1; i < 7; ++i) {
        var x = new Date(endTime - i * 60000);
        timeRange.unshift(String(x.getHours() + ':' + x.getMinutes()))
    }
    return timeRange
};

var buildMultipleChart = function (name, symbol, devices) {
    var chartCode = '#' + name + 'Chart';
    var element = document.getElementById(chartCode);
    var chartMap = echarts.init(element);
    var mapSeries = [];
    var legend = devices.map(function (leg) {
        return leg.code
    });

    for (var num = 0; num < devices.length; ++num) {
        var dev = devices[num];
        var seriesData = {
            name: dev.code,
            type: 'line',
            data: [0, 0, 0, 0, 0, 0, 0],
            markPoint: {
                data: [
                    {type: 'max', name: 'Maximum'},
                    {type: 'min', name: 'Minimum'}
                ]
            },
            markLine: {
                data: [
                    {type: 'average', name: 'Average'}
                ]
            }
        };
        mapSeries.push(seriesData);
    }
    var chartMapOption = {
        title: {
            text: name + ' Events in ' + symbol,
            subtext: ''
        },
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: legend
        },
        toolbox: {
            show: true,
            feature: {
                dataZoom: {
                    yAxisIndex: 'none'
                },
                dataView: {readOnly: false},
                magicType: {type: ['line', 'bar']},
                restore: {},
                saveAsImage: {}
            }
        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: computeRange()
        },
        yAxis: {
            type: 'value',
            axisLabel: {
                formatter: '{value} '
            }
        },
        series: mapSeries
    };
    chartMap.setOption(chartMapOption);
    return {'chartMap': chartMap, 'chartMapOption': chartMapOption};
};

var buildChart = function (code, name, symbol) {
    var chartCode = '#' + code + '-' + name + 'Chart';
    var chartMap = echarts.init(document.getElementById(chartCode));
    var chartMapOption = {
        title: {
            text: name + ' Events in ' + symbol,
            subtext: ''
        },
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: [name]
        },
        toolbox: {
            show: true,
            feature: {
                dataZoom: {
                    yAxisIndex: 'none'
                },
                dataView: {readOnly: false},
                magicType: {type: ['line', 'bar']},
                restore: {},
                saveAsImage: {}
            }
        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: computeRange()
        },
        yAxis: {
            type: 'value',
            axisLabel: {
                formatter: '{value} '
            }
        },
        series: [
            {
                name: name,
                type: 'line',
                data: [0, 0, 0, 0, 0, 0, 0],
                markPoint: {
                    data: [
                        {type: 'max', name: 'Maximum'},
                        {type: 'min', name: 'Minimum'}
                    ]
                },
                markLine: {
                    data: [
                        {type: 'average', name: 'Average'}
                    ]
                }
            }
        ]
    };
    chartMap.setOption(chartMapOption);
    return {'chartMap': chartMap, 'chartMapOption': chartMapOption};
};

var updateChart = function (chartMap, chartOption, value, timestamp) {
    try {
        chartOption.series[0].data[0].push(value);
        chartOption.xAxis.data.push(timestamp);
        chartMap.setOption(chartOption, true);
        return true;
    } catch (err) {
        console.warn(err);
        return false;
    }
};

var updateGauge = function (gaugeChart, gaugeOption, value) {
    try {
        gaugeOption.series[0].data[0].value = value;
        gaugeChart.setOption(gaugeOption, true);
        return true;
    } catch (err) {
        console.warn(err);
        return false;
    }

};

function mqttChartListener(deviceID, cMap, cOption, metric) {
    var client = new Paho.MQTT.Client("95.85.18.169", Number(9883), "", "myclientid_" + parseInt(Math.random() * 100, 10));

    // set callback handlers
    client.onConnectionLost = onConnectionLost;
    client.onMessageArrived = onMessageArrived;

    // connect the client
    client.connect({onSuccess: onConnect});


    // called when the client connects
    function onConnect() {
        // Once a connection has been made, make a subscription and send a message.
        console.log("Connected to broker");
        client.subscribe(deviceID);
        // message = new Paho.MQTT.Message("Hello");
        // message.destinationName = "World";
        // client.send(message);
    }

    // called when the client loses its connection
    function onConnectionLost(responseObject) {
        if (responseObject.errorCode !== 0) {
            console.log("onConnectionLost:" + responseObject.errorMessage);
        }
    }

    // called when a message arrives
    function onMessageArrived(message) {
        var data = JSON.parse(message.payloadString);
        if (message.destinationName ===  deviceID && data.metric === metric) {
            updateChart(cMap, cOption, data.value, data.timestamp);
        }

    }
}

function mqttListener(deviceID, cMap, cOption, gChart, gOption) {
    var client = new Paho.MQTT.Client("95.85.18.169", Number(9883), "", "myclientid_" + parseInt(Math.random() * 100, 10));

    // set callback handlers
    client.onConnectionLost = onConnectionLost;
    client.onMessageArrived = onMessageArrived;

    // connect the client
    client.connect({onSuccess: onConnect});


    // called when the client connects
    function onConnect() {
        // Once a connection has been made, make a subscription and send a message.
        console.log("Connected to broker");
        client.subscribe(deviceID);
        // message = new Paho.MQTT.Message("Hello");
        // message.destinationName = "World";
        // client.send(message);
    }

    // called when the client loses its connection
    function onConnectionLost(responseObject) {
        if (responseObject.errorCode !== 0) {
            console.log("onConnectionLost:" + responseObject.errorMessage);
        }
    }

    // called when a message arrives
    function onMessageArrived(message) {
        var data = JSON.parse(message.payloadString);
        updateChart(cMap, cOption, data.value, data.timestamp);
        updateGauge(gChart, gOption, data.value, data.timestamp);
    }
}