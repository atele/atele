from plant import setup_app, initialize_views, initialize_api

app = setup_app('atele', 'ProductionConfig')

with app.app_context():
    from application.views import client
    initialize_views(app, client.client_blueprint)

    from application.resources import account, base, api, authentication, equipment, payment
    initialize_api(app, app.api)

if __name__ == '__main__':
    app.run(use_reloader=False)